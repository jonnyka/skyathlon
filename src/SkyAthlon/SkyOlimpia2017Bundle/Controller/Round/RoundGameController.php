<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Controller\Round;

use SkyAthlon\SkyOlimpia2017Bundle\Entity\Game;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameResult;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Player;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\PlayerResult;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Round;
use SkyAthlon\SkyOlimpia2017Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/skyolimpia-2017/versenynap")
 */
class RoundGameController extends Controller
{
    /**
     * Show current round games.
     *
     * @Route("/{round}/eremtablazat/", name="skyolimpia2017_round_medals", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            $r = $repo->findBy(array('number' => $round));
            $gameService = $this->get('skyolimpia2017_game');
            $m = $gameService->getMedalsForGames($r[0]);
            $medals = $m['medals'];
            $playerResults = $m['players'];

            $players = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Player')->getPlayers();

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            return array(
                'rounds'        => $pagination,
                'medals'        => $medals,
                'playerResults' => $playerResults,
                'players'       => $players,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{roundid}/merkozes/{game}/szerkesztes/", name="skyolimpia2017_game_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editGameAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $editForm = $this->createForm('SkyAthlon\SkyOlimpia2017Bundle\Form\GameType', $game);
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $players = $ranks = $sranks = array();

                    if ($request->getMethod() === 'POST') {
                        $values = $request->request->all();

                        foreach ($values['game'] as $key => $value) {
                            if (strpos($key, 'player-') !== false) {
                                if ($editForm->get($key)->getData()) {
                                    $players[$key] = $editForm->get($key)->getData();
                                }
                            }
                            if (strpos($key, 'rank-') !== false) {
                                if ($editForm->get($key)->getData()) {
                                    $ranks[$key] = $editForm->get($key)->getData();
                                }
                            }
                            if (strpos($key, 'sr-') !== false) {
                                if ($editForm->get($key)->getData()) {
                                    $sranks[$key] = $editForm->get($key)->getData();
                                }
                            }
                        }
                    }

                    $gameService = $this->get('skyolimpia2017_game');
                    $gameService->createGameResultsForGame($game, $players, $ranks, $sranks);

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyolimpia2017.game.edited');

                    $anchor = '#verseny-' . $game->getId();
                    return $this->redirect($this->generateUrl('skyolimpia2017_round', array('round' => $roundid)) . $anchor);
                }

                return array(
                    'game' => $game,
                    'roundid' => $roundid,
                    'edit_form' => $editForm->createView(),
                );
            }
            else {
                return $this->redirectToRoute('skyolimpia2017_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
