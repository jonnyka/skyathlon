<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Controller\Round;

use SkyAthlon\SkyOlimpia2017Bundle\Entity\Round;
use SkyAthlon\SkyOlimpia2017Bundle\Repository\RoundRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round team controller.
 *
 * @Route("/skyolimpia-2017/versenynap")
 */
class RoundTeamController extends Controller
{
    /**
     * Show current round player results.
     *
     * @Route("/{round}/nemzetek-eredmenyei/", name="skyolimpia2017_round_teams", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexTeamsAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            /** @var Round $r */
            $r = $repo->findOneBy(array('number' => $round));

            $teamService = $this->get('skyolimpia2017_team');
            $teams = $teamService->getMedalsAndPoints($r);
            $teamMovements = $teamService->getMedalsAndPointsMovements($r);

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            return array(
                'rounds'        => $pagination,
                'teams'         => $teams,
                'teamMovements' => $teamMovements,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
