<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Controller;

use AppBundle\Entity\Settings;
use Doctrine\ORM\Query;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Game;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameType;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Round;
use SkyAthlon\SkyOlimpia2017Bundle\Repository\GameRepository;
use SkyAthlon\SkyOlimpia2017Bundle\Repository\GameTypeRepository;
use SkyAthlon\SkyOlimpia2017Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/skyolimpia-2017/versenynap")
 */
class RoundController extends Controller
{
    /**
     * Redirect to the current round page.
     *
     * @Route("/", name="skyolimpia2017_round_redirect", options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToIndexAction() {
        if ($this->getUser()) {
            $round = 1;
            /** @var Settings $currentRoundObject */
            $currentRoundObject = $this->getDoctrine()->getManager()->getRepository('AppBundle:Settings')->findOneBy(array(
                'name' => 'skyolimpia2017_currentRound',
            ));
            if ($currentRoundObject) {
                $round = (int)$currentRoundObject->getValue();
            }

            return $this->redirectToRoute('skyolimpia2017_round', array('round' => $round));
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Show current round matches.
     *
     * @Route("/{round}/", name="skyolimpia2017_round", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            //$this->get('skyolimpia2017_game')->createCache();

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            return array(
                'rounds' => $pagination,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing Round entity.
     *
     * @Route("/{id}/szerkesztes/", name="skyolimpia2017_round_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Round $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editAction(Request $request, Round $round)
    {
        if ($this->getUser()) {
            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $roundNum = null;
                /** @var Settings $currentRoundObject */
                $currentRoundObject = $this->getDoctrine()->getManager()->getRepository('AppBundle:Settings')->findOneBy(array(
                    'name' => 'skyolimpia2017_currentRound',
                ));
                if ($currentRoundObject) {
                    $roundNum = (int)$currentRoundObject->getValue();
                }

                $deleteForm = $this->createDeleteForm($round);
                $editForm = $this->createForm('SkyAthlon\SkyOlimpia2017Bundle\Form\RoundType', $round, array('roundNum' => $roundNum));
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($round);

                    $isCurrent = $editForm->get('current')->getData();
                    if ($isCurrent) {
                        if (!$currentRoundObject) {
                            $currentRoundObject = new Settings();
                            $currentRoundObject->setName('skyolimpia2017_currentRound');
                        }

                        $currentRoundObject->setValue($round->getNumber());
                        $em->persist($currentRoundObject);
                    }

                    $gameTypes = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameType')->findAll();
                    /** @var GameType $gameType */
                    foreach ($gameTypes as $gameType) {
                        $game = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game')->findOneBy(array(
                            'round' => $round,
                            'gameType' => $gameType,
                        ));
                        /** @var Game $game */
                        if ($game) {
                            $weight = $round->getReversed() ? $gameType->getReversedWeight() : $gameType->getWeight();
                            $game->setWeight($weight);

                            $em->persist($game);
                        }
                    }

                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyolimpia2017.round.edited');

                    return $this->redirectToRoute('skyolimpia2017_round', array('round' => $round->getNumber()));
                }

                return array(
                    'round' => $round,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                );
            }
            else {
                return $this->redirectToRoute('skyolimpia2017_round', array('round' => $round->getNumber()));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Deletes a Round entity.
     *
     * @Route("/{id}/", name="skyolimpia2017_round_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Round $round
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Round $round)
    {
        if ($this->getUser()) {
            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $form = $this->createDeleteForm($round);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();

                    $games = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game')->findBy(array(
                        'round' => $round,
                    ));
                    if ($games) {
                        foreach ($games as $game) {
                            $em->remove($game);
                        }
                    }

                    $em->remove($round);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyolimpia2017.round.deleted', 'warning');
                }

                return $this->redirectToRoute('skyolimpia2017_round_redirect');
            }
            else {
                return $this->redirectToRoute('skyolimpia2017_round_redirect');
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a form to delete a Round entity.
     *
     * @param Round $round The Round entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Round $round)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('skyolimpia2017_round_delete', array('id' => $round->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    /**
     * Reorder playoff games.
     *
     * @Route("/selejtezo-sorrend/", name="skyolimpia2017_reorder_playoff")
     * @Template()
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function reorderPlayoffAction()
    {
        if ($this->getUser()) {
            if ($this->getUser()->isAdmin()) {
                $em = $this->getDoctrine()->getManager();
                $gameTypes = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameType')->getPlayoffs();

                return array(
                    'gameTypes' => $gameTypes,
                );
            }
            else {
                return $this->redirectToRoute('skyolimpia2017_round_redirect');
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Save playoff order.
     *
     * @Route("/selejtezo-sorrend-mentes/", name="skyolimpia2017_reorder_playoff_save", options={"expose" = true})
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function reorderPlayoffSave(Request $request) {
        $data = $request->request->get('data');
        $em = $this->getDoctrine()->getManager();
        /** @var GameTypeRepository $gtRepo */
        $gtRepo = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameType');
        /** @var GameRepository $gRepo */
        $gRepo = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game');
        $startWeight = 20;

        foreach ($data as $gtId) {
            $gameTypes = $gtRepo->getSubTypes($gtId);
            $currentWeight = $startWeight + 1;

            /** @var GameType $gameType */
            foreach ($gameTypes as $gameType) {
                $gameType->setWeight($currentWeight);
                $em->persist($gameType);

                $games = $gRepo->findBy(array('gameType' => $gameType));
                if ($games) {
                    /** @var Game $game */
                    foreach ($games as $game) {
                        $game->setWeight($currentWeight);
                        $em->persist($game);
                    }
                }

                $currentWeight++;
            }

            $startWeight += 10;
        }

        $em->flush();

        return new JsonResponse('ok');
    }
}
