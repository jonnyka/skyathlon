<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Controller;

use SkyAthlon\SkyOlimpia2017Bundle\Entity\Game;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameResult;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Player;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\PlayerResult;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Round;
use SkyAthlon\SkyOlimpia2017Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Game controller.
 *
 * @Route("/skyolimpia-2017/versenyszamok")
 */
class GameController extends Controller
{
    /**
     * Show all games.
     *
     * @Route("/", name="skyolimpia2017_medals")
     * @Method("GET")
     * @Template()
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            $gameService = $this->get('skyolimpia2017_game');
            $medals = $gameService->getAllMedals();

            return array(
                'medals' => $medals,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
