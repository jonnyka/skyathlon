<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Controller;

use SkyAthlon\SkyOlimpia2017Bundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Graph controller.
 *
 * @Route("/skyolimpia-2017/grafikonok")
 */
class GraphController extends Controller
{
    /**
     * Lists all Player entities.
     *
     * @Route("/", name="skyolimpia2017_graph")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            return array();
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Lists all Team entities.
     *
     * @Route("/nemzetek/", name="skyolimpia2017_graph_team")
     * @Method("GET")
     * @Template()
     */
    public function indexTeamAction()
    {
        if ($this->getUser()) {
            return array();
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Lists all ds.
     *
     * @Route("/be-nem-fejezett/", name="skyolimpia2017_graph_ds")
     * @Method("GET")
     * @Template()
     */
    public function indexDsAction()
    {
        if ($this->getUser()) {
            return array();
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Gets all player medals.
     *
     * @Route("api/ermek/", name="skyolimpia2017_api_medals", options={"expose" = true})
     * @Method("GET")
     */
    public function apiMedalsAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $players = $teamService->getMedalsAndPointsForPlayers();

            uasort($players, function($a, $b) {
                $aWin = 1;

                if ($b['gold']['point'] < $a['gold']['point']) {
                    $aWin = 0;
                }
                elseif ($b['gold']['point'] == $a['gold']['point']) {
                    if ($b['silver']['point'] < $a['silver']['point']) {
                        $aWin = 0;
                    }
                    elseif ($b['silver']['point'] == $a['silver']['point']) {
                        if ($b['bronze']['point'] < $a['bronze']['point']) {
                            $aWin = 0;
                        }
                    }
                }

                return $aWin;
            });

            $playersArr = array();

            foreach ($players as $player) {
                $playersArr[] = array(
                    'number' => '' . $player['number'],
                    'gold'   => $player['gold']['point'],
                    'silver' => $player['silver']['point'],
                    'bronze' => $player['bronze']['point'],
                    'team'   => $player['team'],
                    'sex'    => $player['sex'],
                );
            }

            return new JsonResponse($playersArr);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Gets all player points.
     *
     * @Route("api/pontszamok/", name="skyolimpia2017_api_points", options={"expose" = true})
     * @Method("GET")
     */
    public function apiPointsAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $players = $teamService->getMedalsAndPointsForPlayers();

            uasort($players, function($a, $b) {
                return $b['point'] - $a['point'];
            });

            $playersArr = array();

            foreach ($players as $player) {
                $playersArr[] = array(
                    'number' => '' . $player['number'],
                    'point'  => $player['point'],
                    'team'   => $player['team'],
                    'sex'    => $player['sex'],
                );
            }

            return new JsonResponse($playersArr);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Gets all player ds.
     *
     * @Route("api/ds/", name="skyolimpia2017_api_ds", options={"expose" = true})
     * @Method("GET")
     */
    public function apiDsAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $players = $teamService->getMedalsAndPointsForPlayers();

            usort($players, function($a, $b) {
                $aWin = 1;
                $asum = $a['dq'] + $a['dns'] + $a['dnf'];
                $bsum = $b['dq'] + $b['dns'] + $b['dnf'];

                if ($bsum < $asum) {
                    $aWin = 0;
                }
                elseif ($bsum == $asum) {
                    if ($b['dq'] < $a['dq']) {
                        $aWin = 0;
                    }
                    elseif ($b['dq'] == $a['dq']) {
                        if ($b['dns'] < $a['dns']) {
                            $aWin = 0;
                        }
                        elseif ($b['dns'] == $a['dns']) {
                            if ($b['dnf'] < $a['dnf']) {
                                $aWin = 0;
                            }
                        }
                    }
                }

                return $aWin;
            });

            foreach ($players as &$player) {
                $player['number'] = '' . $player['number'];
            }

            return new JsonResponse($players);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Gets all team medals.
     *
     * @Route("api/ermek/nemzet/", name="skyolimpia2017_api_medals_team", options={"expose" = true})
     * @Method("GET")
     */
    public function apiMedalsTeamAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $teams = $teamService->getMedalsAndPoints();

            usort($teams, function($a, $b) {
                $aWin = 1;

                if ($b['gold'] < $a['gold']) {
                    $aWin = 0;
                }
                elseif ($b['gold'] == $a['gold']) {
                    if ($b['silver'] < $a['silver']) {
                        $aWin = 0;
                    }
                    elseif ($b['silver'] == $a['silver']) {
                        if ($b['bronze'] < $a['bronze']) {
                            $aWin = 0;
                        }
                    }
                }

                return $aWin;
            });

            return new JsonResponse($teams);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Gets all team points.
     *
     * @Route("api/pontszamok/nemzet/", name="skyolimpia2017_api_points_team", options={"expose" = true})
     * @Method("GET")
     */
    public function apiPointsTeamAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $teams = $teamService->getMedalsAndPoints();

            usort($teams, function($a, $b) {
                return $b['point'] - $a['point'];
            });

            return new JsonResponse($teams);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Gets all team ds.
     *
     * @Route("api/ds/nemzet/", name="skyolimpia2017_api_ds_team", options={"expose" = true})
     * @Method("GET")
     */
    public function apiDsTeamAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $teams = $teamService->getMedalsAndPoints();

            usort($teams, function($a, $b) {
                $aWin = 1;
                $asum = $a['dq'] + $a['dns'] + $a['dnf'];
                $bsum = $b['dq'] + $b['dns'] + $b['dnf'];

                if ($bsum < $asum) {
                    $aWin = 0;
                }
                elseif ($bsum == $asum) {
                    if ($b['dq'] < $a['dq']) {
                        $aWin = 0;
                    }
                    elseif ($b['dq'] == $a['dq']) {
                        if ($b['dns'] < $a['dns']) {
                            $aWin = 0;
                        }
                        elseif ($b['dns'] == $a['dns']) {
                            if ($b['dnf'] < $a['dnf']) {
                                $aWin = 0;
                            }
                        }
                    }
                }

                return $aWin;
            });

            return new JsonResponse($teams);
        }
        else {
            return new JsonResponse('nope');
        }
    }
}
