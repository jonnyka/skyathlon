<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Controller;

use SkyAthlon\SkyOlimpia2017Bundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Player controller.
 *
 * @Route("/skyolimpia-2017/")
 */
class PlayerController extends Controller
{
    /**
     * Lists all Player entities.
     *
     * @Route("versenyzok/", name="skyolimpia2017_player")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $players = $teamService->getMedalsAndPointsForPlayers();

            return array(
                'players' => $players,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Lists all Player entities (men).
     *
     * @Route("versenyzok/ferfiak/", name="skyolimpia2017_player_men")
     * @Method("GET")
     * @Template()
     */
    public function indexMenAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $players = $teamService->getMedalsAndPointsForPlayers();

            return array(
                'players' => $players,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
    /**
     * Lists all Player entities (women).
     *
     * @Route("versenyzok/nok/", name="skyolimpia2017_player_women")
     * @Method("GET")
     * @Template()
     */
    public function indexWomenAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $players = $teamService->getMedalsAndPointsForPlayers();

            return array(
                'players' => $players,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
