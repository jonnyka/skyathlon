<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Controller;

use SkyAthlon\SkyOlimpia2017Bundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Team controller.
 *
 * @Route("/skyolimpia-2017/")
 */
class TeamController extends Controller
{
    /**
     * Lists all Team entities.
     *
     * @Route("nemzetek/", name="skyolimpia2017_team")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $teams = $teamService->getMedalsAndPoints();

            return array(
                'teams' => $teams,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Finds and displays a Team entity.
     *
     * @Route("nemzet/{name}/", name="skyolimpia2017_team_show")
     * @Method("GET")
     * @Template
     * @param $name
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showAction($name)
    {
        if ($this->getUser()) {
            $teamService = $this->get('skyolimpia2017_team');
            $data = $teamService->getMedalsAndPointsForTeam($name);

            return array(
                'team'        => $data['team'],
                'results'     => $data['results'],
                'teamResults' => $data['teamResults'],
                'pstring'     => $data['pstring'],
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
