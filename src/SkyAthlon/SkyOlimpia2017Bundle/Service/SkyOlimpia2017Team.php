<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Service;

use SkyAthlon\SkyOlimpia2017Bundle\Entity\Game;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Player;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Round;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameResult;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use SkyAthlon\SkyOlimpia2017Bundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SkyOlimpia2017Team extends Controller
{
    protected $em;
    protected $gtype;
    protected $gstype;
    protected $longName;
    protected $gold;
    protected $silver;
    protected $bronze;
    protected $rank;
    protected $srank;
    protected $point;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function isGame($game) {
        return strpos($this->gtype, $game) !== false;
    }

    protected function isA() {
        return $this->gstype === 'adonto';
    }
    protected function isB() {
        return $this->gstype === 'bdonto';
    }
    protected function isC() {
        return $this->gstype === 'cdonto';
    }
    protected function isD() {
        return $this->gstype === 'elodonto';
    }
    protected function iswsD() {
        return $this->gstype === 'wselodonto';
    }
    protected function isQualified() {
        return strpos($this->rank, 'D') === false;
    }

    protected function getPointsAndMedals() {
        if ($this->isGame('skypong') || $this->isGame('handfall')) {
            if ($this->isA() || $this->isB() || $this->isC()) {
                if ($this->srank == 1) {
                }
            }

            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }

                $this->srank = $this->rank;
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 3;
                }

                $this->srank = $this->rank + 2;
            }
            elseif ($this->isC()) {
                if ($this->rank == 1) {
                    $this->point = 2;
                }
                else if ($this->rank == 2) {
                    $this->point = 1;
                }

                $this->srank = $this->rank + 4;
            }
        }
        elseif ($this->isGame('skyball')) {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }

                $this->srank = $this->rank;
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 3;
                }

                $this->srank = $this->rank + 2;
            }
            elseif ($this->iswsD()) {
                if ($this->rank == 2) {
                    $this->point = 2;
                    $this->srank = $this->rank + 3;
                }
            }
        }
        elseif ($this->isGame('skyfoci')) {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 4) {
                    $this->point = 3;
                }
                else if ($this->rank == 5) {
                    $this->point = 2;
                }
                else if ($this->rank == 6) {
                    $this->point = 1;
                }

                $this->srank = $this->rank;
            }
        }
        elseif ($this->isGame('x')) {
            if ($this->isD()) {
                if ($this->rank == 1) {
                    $this->srank = $this->isQualified() ? 4 : null;
                    $this->point = 3;
                }
                if ($this->rank == 2) {
                    $this->srank = $this->isQualified() ? 5 : null;
                    $this->point = 2;
                }
                if ($this->rank == 3) {
                    $this->srank = $this->isQualified() ? 6 : null;
                    $this->point = 1;
                }
            }
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }

                $this->srank = $this->isQualified() ? $this->rank : null;
            }
        }
        elseif ($this->isGame('_ero')) {
            if (in_array($this->rank, array(1, 2, 3, 4, 5, 6))) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 4) {
                    $this->point = 3;
                }
                else if ($this->rank == 5) {
                    $this->point = 2;
                }
                else if ($this->rank == 6) {
                    $this->point = 1;
                }

                $this->srank = $this->isQualified() ? $this->rank : null;
            }
        }
        elseif ($this->isGame('800')) {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 4) {
                    $this->point = 3;
                }

                $this->srank = $this->isQualified() ? $this->rank : null;
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 2;
                    $this->srank = $this->isQualified() ? ($this->rank + 4) : null;
                }
                else if ($this->rank == 2) {
                    $this->point = 1;
                    $this->srank = $this->isQualified() ? ($this->rank + 4) : null;
                }
            }
        }
        else {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }

                $this->srank = $this->rank;
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 3;
                }
                else if ($this->rank == 2) {
                    $this->point = 2;
                }
                else if ($this->rank == 3) {
                    $this->point = 1;
                }

                $this->srank = $this->rank + 3;
            }
        }
    }

    protected function clear() {
        $this->gold = $this->silver = $this->bronze = $this->point = 0;
        $this->srank = null;
    }

    public function getMedalsAndPoints(Round $round = null)
    {
        $em = $this->em;

        if ($round) {
            $teams = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Team')->getTeamsForRound($round->getId());
        }
        else {
            $teams = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Team')->getTeams();
        }
        $teamsArr = array();

        /** @var Team $team */
        foreach ($teams as $team) {
            $teamsArr[$team['id']] = array(
                'team'   => $team,
                'gold'   => 0,
                'silver' => 0,
                'bronze' => 0,
                'point'  => 0,
                'dq'     => 0,
                'dns'    => 0,
                'dnf'    => 0,
                'match'  => 0,
            );
            $ta = &$teamsArr[$team['id']];

            foreach ($team['gameResults'] as $gameResult) {
                /** @var Game $game */
                $game = $gameResult['game'];
                $this->gtype = $game['gameType']['computerName'];
                $this->gstype =  $game['gameSubType']['computerName'];
                $this->rank = $gameResult['rank'];
                $this->clear();
                $this->srank = $gameResult['sumRank'];

                $go = (($game['numberOfTeamPlayers'] && $game['numberOfTeamPlayers'] > 1 && $gameResult['medal']) || (!$game['numberOfTeamPlayers'] || $game['numberOfTeamPlayers'] === 1)) && $game['closed'] && $this->isQualified();

                if ($go) {
                    //   $ta['point'] += $gameResult['point'];
                    //$this->getPointsAndMedals();
                    if ($this->srank == 1) {
                        $ta['gold'] += 1;
                        $ta['point'] += 10;
                    }
                    else if ($this->srank == 2) {
                        $ta['silver'] += 1;
                        $ta['point'] += 7;
                    }
                    else if ($this->srank == 3) {
                        $ta['bronze'] += 1;
                        $ta['point'] += 5;
                    }
                    else if ($this->srank == 4) {
                        $ta['point'] += 3;
                    }
                    else if ($this->srank == 5) {
                        $ta['point'] += 2;
                    }
                    else if ($this->srank == 6) {
                        $ta['point'] += 1;
                    }
                }

                if ($this->rank === 'DQ') {
                    $ta['dq'] += 1;
                }
                if ($this->rank === 'DNS') {
                    $ta['dns'] += 1;
                }
                if ($this->rank === 'DNF') {
                    $ta['dnf'] += 1;
                }

                $ta['match'] += 1;
            }
        }

        return $teamsArr;
    }

    public function getMedalsAndPointsMovements(Round $round)
    {
        $em = $this->em;
        $teamsArr = $teamsPrevArr = $ret = array();

        if ($round->getNumber() && $round->getNumber() > 1) {
            $teams = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Team')->getTeamsUntilRound($round->getNumber());

            /** @var Team $team */
            foreach ($teams as $team) {
                $ret[$team['id']] = array(
                    'prevMedalRank'    => 0,
                    'currentMedalRank' => 0,
                    'medalMovement'    => 0,
                    'prevPointRank'    => 0,
                    'currentPointRank' => 0,
                    'pointMovement'    => 0,
                    'computerName'     => $team['computerName'],
                    'name'             => $team['name'],
                );
                $teamsArr[$team['id']] = $teamsPrevArr[$team['id']] = array(
                    'team' => $team,
                    'gold' => 0,
                    'silver' => 0,
                    'bronze' => 0,
                    'point' => 0,
                    'pointRank' => 0,
                    'medalRank' => 0,
                );
                $ta = &$teamsArr[$team['id']];
                $tpa = &$teamsPrevArr[$team['id']];

                foreach ($team['gameResults'] as $gameResult) {
                    /** @var Game $game */
                    $game = $gameResult['game'];
                    $this->gtype = $game['gameType']['computerName'];
                    $rnum = $game['gameType']['round']['number'];
                    $this->gstype = $game['gameSubType']['computerName'];
                    $this->rank = $gameResult['rank'];
                    $this->clear();

                    $go = (($game['numberOfTeamPlayers'] && $game['numberOfTeamPlayers'] > 1 && $gameResult['medal']) || (!$game['numberOfTeamPlayers'] || $game['numberOfTeamPlayers'] === 1)) && $game['closed'] && $this->isQualified();

                    if ($go) {
                        //   $ta['point'] += $gameResult['point'];
                        $this->getPointsAndMedals();
                        $ta['gold'] += $this->gold;
                        $ta['silver'] += $this->silver;
                        $ta['bronze'] += $this->bronze;
                        $ta['point'] += $this->point;

                        if ($rnum != $round->getNumber()) {
                            $tpa['gold'] += $this->gold;
                            $tpa['silver'] += $this->silver;
                            $tpa['bronze'] += $this->bronze;
                            $tpa['point'] += $this->point;
                        }
                    }
                }
            }
        }

        $sort = array();
        foreach($teamsArr as $k => $v) {
            $sort['gold'][$k] = $v['gold'];
            $sort['silver'][$k] = $v['silver'];
            $sort['bronze'][$k] = $v['bronze'];
        }
        $keys = array_keys($teamsArr);

        array_multisort($sort['gold'], SORT_DESC, $sort['silver'], SORT_DESC, $sort['bronze'], SORT_DESC, $teamsArr, $keys);
        $teamsArr = array_combine($keys, $teamsArr);

        $sort = array();
        foreach($teamsPrevArr as $k => $v) {
            $sort['gold'][$k] = $v['gold'];
            $sort['silver'][$k] = $v['silver'];
            $sort['bronze'][$k] = $v['bronze'];
        }
        $keys = array_keys($teamsPrevArr);

        array_multisort($sort['gold'], SORT_DESC, $sort['silver'], SORT_DESC, $sort['bronze'], SORT_DESC, $teamsPrevArr, $keys);
        $teamsPrevArr = array_combine($keys, $teamsPrevArr);

        $rank = 0;
        $gold = $silver = $bronze = -1;
        foreach ($teamsArr as $tid => &$team) {
            if ($team['gold'] != $gold || $team['silver'] != $silver || $team['bronze'] != $bronze) {
                $rank++;
            }

            $team['medalRank'] = $rank;

            $gold = $team['gold'];
            $silver = $team['silver'];
            $bronze = $team['bronze'];
        }

        $rank = 0;
        $gold = $silver = $bronze = -1;
        foreach ($teamsPrevArr as $tid => &$team) {
            if ($team['gold'] != $gold || $team['silver'] != $silver || $team['bronze'] != $bronze) {
                $rank++;
            }

            $team['medalRank'] = $rank;

            $gold = $team['gold'];
            $silver = $team['silver'];
            $bronze = $team['bronze'];
        }

        foreach ($teamsArr as $tid => $team) {
            $t = &$ret[$tid];

            $t['prevMedalRank']    = $teamsPrevArr[$tid]['medalRank'];
            $t['currentMedalRank'] = $team['medalRank'];
            $t['medalMovement']    = $t['prevMedalRank'] - $t['currentMedalRank'];
        }

        uasort($teamsArr, function($a, $b) {
            return $b['point'] - $a['point'];
        });
        uasort($teamsPrevArr, function($a, $b) {
            return $b['point'] - $a['point'];
        });

        $rank = 0;
        $point = -1;
        foreach ($teamsArr as $tid => &$team) {
            if ($team['point'] != $point) {
                $rank++;
            }

            $team['pointRank'] = $rank;

            $point = $team['point'];
        }

        $rank = 0;
        $point = -1;
        foreach ($teamsPrevArr as $tid => &$team) {
            if ($team['point'] != $point) {
                $rank++;
            }

            $team['pointRank'] = $rank;

            $point = $team['point'];
        }

        foreach ($teamsArr as $tid => $team) {
            $t = &$ret[$tid];

            $t['prevPointRank']    = $teamsPrevArr[$tid]['pointRank'];
            $t['currentPointRank'] = $team['pointRank'];
            $t['pointMovement']    = $t['prevPointRank'] - $t['currentPointRank'];
        }

        return $ret;
    }

    public function getMedalsAndPointsForPlayers()
    {
        $em = $this->em;

        $players = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Player')->getAllPlayers();
        $playersArr = array();

        /** @var Player $player */
        foreach ($players as $player) {
            $playersArr[$player['number']] = array(
                'sex'    => $player['sex'],
                'number' => $player['number'],
                'team'   => $player['team']['computerName'],
                'gold'   => array('gameTypes' => array(), 'point' => 0),
                'silver' => array('gameTypes' => array(), 'point' => 0),
                'bronze' => array('gameTypes' => array(), 'point' => 0),
                'four'   => array('gameTypes' => array(), 'point' => 0),
                'five'   => array('gameTypes' => array(), 'point' => 0),
                'six'    => array('gameTypes' => array(), 'point' => 0),
                'dq'     => 0,
                'dns'    => 0,
                'dnf'    => 0,
                'point'  => 0,
                'match'  => 0,
            );
            $r = &$playersArr[$player['number']];

            foreach ($player['gameResults'] as $gameResult) {
                /** @var Game $game */
                $game = $gameResult['game'];
                $this->gtype = $game['gameType']['computerName'];
                $this->gstype = $game['gameSubType']['computerName'];
                $this->longName = $gameResult['game']['gameType']['name'];
                $this->rank = $gameResult['rank'];
                $this->clear();
                $this->srank = $gameResult['sumRank'];

                $go = $game['closed'] && $this->isQualified();

                if ($go) {
                    if ($this->srank == 1) {
                        $r['gold']['point'] += 1;
                        $r['gold']['gameTypes'][] = $this->longName;
                        $r['point'] += 10;
                    }
                    elseif ($this->srank == 2) {
                        $r['silver']['point'] += 1;
                        $r['silver']['gameTypes'][] = $this->longName;
                        $r['point'] += 7;
                    }
                    elseif ($this->srank == 3) {
                        $r['bronze']['point'] += 1;
                        $r['bronze']['gameTypes'][] = $this->longName;
                        $r['point'] += 5;
                    }
                    elseif ($this->srank == 4) {
                        $r['four']['point'] += 1;
                        $r['four']['gameTypes'][] = $this->longName;
                        $r['point'] += 3;
                    }
                    elseif ($this->srank == 5) {
                        $r['five']['point'] += 1;
                        $r['five']['gameTypes'][] = $this->longName;
                        $r['point'] += 2;
                    }
                    elseif ($this->srank == 6) {
                        $r['six']['point'] += 1;
                        $r['six']['gameTypes'][] = $this->longName;
                        $r['point'] += 1;
                    }
                }

                if ($this->rank === 'DQ') {
                    $r['dq'] += 1;
                }
                if ($this->rank === 'DNS') {
                    $r['dns'] += 1;
                }
                if ($this->rank === 'DNF') {
                    $r['dnf'] += 1;
                }

                $r['match'] += 1;
            }
        }

        return $playersArr;
    }

    public function getMedalsAndPointsForTeam($name) {
        /** @var TeamRepository $repo */
        $repo = $this->em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Team');
        $team = $repo->getTeam($name);
        $pArr = array();
        $results = array();
        $teamResults = array(
            'gold'   => array('gameTypes' => array(), 'point' => 0),
            'silver' => array('gameTypes' => array(), 'point' => 0),
            'bronze' => array('gameTypes' => array(), 'point' => 0),
            'four' => array('gameTypes' => array(), 'point' => 0),
            'five' => array('gameTypes' => array(), 'point' => 0),
            'six' => array('gameTypes' => array(), 'point' => 0),
            'point' => 0
        );
        foreach ($team['players'] as $player) {
            $pArr[] = $player['leader'] ? ('<strong>' . $player['number'] . '</strong>') : $player['number'];
            $results[$player['id']] = array(
                'sex'    => $player['sex'],
                'number' => $player['number'],
                'gold'   => array('gameTypes' => array(), 'point' => 0),
                'silver' => array('gameTypes' => array(), 'point' => 0),
                'bronze' => array('gameTypes' => array(), 'point' => 0),
                'four' => array('gameTypes' => array(), 'point' => 0),
                'five' => array('gameTypes' => array(), 'point' => 0),
                'six' => array('gameTypes' => array(), 'point' => 0),
                'point'  => 0,
            );
        }

        if (array_key_exists('gameResults', $team)) {
            foreach ($team['gameResults'] as $gr) {
                $player = $gr['player'];
                $r = &$results[$player['id']];
                $game = $gr['game'];
                $this->gtype = $game['gameType']['computerName'];
                $this->gstype = $game['gameSubType']['computerName'];
                $this->longName = $gr['game']['gameType']['name'];
                $this->rank = $gr['rank'];
                $this->clear();
                $this->srank = $gr['sumRank'];

                $go = $game['closed'] && $this->isQualified();

                if ($go) {
                    //$this->getPointsAndMedals();

                    if ($this->srank == 1) {
                        $r['gold']['point'] += 1;
                        $r['gold']['gameTypes'][] = $this->longName;
                        $r['point'] += 10;

                        if ($gr['medal']) {
                            $teamResults['gold']['point'] += 1;
                            $teamResults['gold']['gameTypes'][] = $this->longName;
                            $teamResults['point'] += 10;
                        }
                    }
                    elseif ($this->srank == 2) {
                        $r['silver']['point'] += 1;
                        $r['silver']['gameTypes'][] = $this->longName;
                        $r['point'] += 7;

                        if ($gr['medal']) {
                            $teamResults['silver']['point'] += 1;
                            $teamResults['silver']['gameTypes'][] = $this->longName;
                            $teamResults['point'] += 7;
                        }
                    }
                    elseif ($this->srank == 3) {
                        $r['bronze']['point'] += 1;
                        $r['bronze']['gameTypes'][] = $this->longName;
                        $r['point'] += 5;

                        if ($gr['medal']) {
                            $teamResults['bronze']['point'] += 1;
                            $teamResults['bronze']['gameTypes'][] = $this->longName;
                            $teamResults['point'] += 5;
                        }
                    }
                    elseif ($this->srank == 4) {
                        $r['four']['point'] += 1;
                        $r['four']['gameTypes'][] = $this->longName;
                        $r['point'] += 3;

                        if ($gr['medal']) {
                            $teamResults['four']['point'] += 1;
                            $teamResults['four']['gameTypes'][] = $this->longName;
                            $teamResults['point'] += 3;
                        }
                    }
                    elseif ($this->srank == 5) {
                        $r['five']['point'] += 1;
                        $r['five']['gameTypes'][] = $this->longName;
                        $r['point'] += 2;

                        if ($gr['medal']) {
                            $teamResults['five']['point'] += 1;
                            $teamResults['five']['gameTypes'][] = $this->longName;
                            $teamResults['point'] += 2;
                        }
                    }
                    elseif ($this->srank == 6) {
                        $r['six']['point'] += 1;
                        $r['six']['gameTypes'][] = $this->longName;
                        $r['point'] += 1;

                        if ($gr['medal']) {
                            $teamResults['six']['point'] += 1;
                            $teamResults['six']['gameTypes'][] = $this->longName;
                            $teamResults['point'] += 1;
                        }
                    }
                }
            }
        }

        return array(
            'team'        => $team,
            'results'     => $results,
            'teamResults' => $teamResults,
            'pstring'     => implode(", ", $pArr),
        );
    }
}
