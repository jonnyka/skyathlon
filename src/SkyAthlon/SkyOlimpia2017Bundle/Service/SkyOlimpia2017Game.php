<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Service;

use SkyAthlon\SkyOlimpia2017Bundle\Entity\Game;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameResult;
use Doctrine\ORM\EntityManager;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Player;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Round;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\DateTime;

class SkyOlimpia2017Game extends Controller
{
    protected $em;
    protected $gtype;
    protected $gstype;
    protected $rank;
    protected $srank;
    protected $gold;
    protected $silver;
    protected $bronze;
    protected $point;
    protected $medal;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->medal = true;
    }

    protected function clear() {
        $this->gold = $this->silver = $this->bronze = $this->point = 0;
    }

    protected function isGame($game) {
        return strpos($this->gtype, $game) !== false;
    }

    protected function isA() {
        return $this->gstype === 'adonto';
    }
    protected function isB() {
        return $this->gstype === 'bdonto';
    }
    protected function isC() {
        return $this->gstype === 'cdonto';
    }
    protected function isD() {
        return $this->gstype === 'elodonto';
    }
    protected function iswsD() {
        return $this->gstype === 'wselodonto';
    }
    protected function isQualified() {
        return strpos($this->rank, 'D') === false;
    }

    protected function setPoint($reset = true) {
        if ($reset) {
            $this->srank = null;
        }
        $this->point = 0;

        if ($this->isGame('skypong') || $this->isGame('handfall')) {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? $this->rank : null;
                }
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 5;
                }
                else if ($this->rank == 2) {
                    $this->point = 3;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? ($this->rank + 2) : null;
                }
            }
            elseif ($this->isC()) {
                if ($this->rank == 1) {
                    $this->point = 2;
                }
                else if ($this->rank == 2) {
                    $this->point = 1;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? ($this->rank + 4) : null;
                }
            }
        }
        elseif ($this->isGame('skyball')) {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }

                if ($reset) {
                    $this->srank = $this->rank;
                }
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 3;
                }

                if ($reset) {
                    $this->srank = $this->rank + 2;
                }
            }
            elseif ($this->iswsD()) {
                if ($this->rank == 2) {
                    $this->point = 2;
                    if ($reset) {
                        $this->srank = $this->rank + 3;
                    }
                }
            }
        }
        elseif ($this->isGame('skyfoci')) {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                }
                else if ($this->rank == 4) {
                    $this->point = 3;
                }
                else if ($this->rank == 5) {
                    $this->point = 2;
                }
                else if ($this->rank == 6) {
                    $this->point = 1;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? $this->rank : null;
                }
            }
        }
        elseif ($this->isGame('x')) {
            if ($this->isD()) {
                if ($this->rank == 1) {
                    if ($reset) {
                        $this->srank = $this->isQualified() ? 4 : null;
                    }
                    $this->point = 3;
                }
                if ($this->rank == 2) {
                    if ($reset) {
                        $this->srank = $this->isQualified() ? 5 : null;
                    }
                    $this->point = 2;
                }
                if ($this->rank == 3) {
                    if ($reset) {
                        $this->srank = $this->isQualified() ? 6 : null;
                    }
                    $this->point = 1;
                }
            }
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? $this->rank : null;
                }
            }
        }
        elseif ($this->isGame('_ero')) {
            if (in_array($this->rank, array(1, 2, 3, 4, 5, 6))) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 4) {
                    $this->point = 3;
                }
                else if ($this->rank == 5) {
                    $this->point = 2;
                }
                else if ($this->rank == 6) {
                    $this->point = 1;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? $this->rank : null;
                }
            }
        }
        elseif ($this->isGame('800')) {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                    $this->gold = 1;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                    $this->silver = 1;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                    $this->bronze = 1;
                }
                else if ($this->rank == 4) {
                    $this->point = 3;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? $this->rank : null;
                }
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 2;
                    if ($reset) {
                        $this->srank = $this->isQualified() ? ($this->rank + 4) : null;
                    }
                }
                else if ($this->rank == 2) {
                    $this->point = 1;
                    if ($reset) {
                        $this->srank = $this->isQualified() ? ($this->rank + 4) : null;
                    }
                }
            }
        }
        else {
            if ($this->isA()) {
                if ($this->rank == 1) {
                    $this->point = 10;
                }
                else if ($this->rank == 2) {
                    $this->point = 7;
                }
                else if ($this->rank == 3) {
                    $this->point = 5;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? $this->rank : null;
                }
            }
            elseif ($this->isB()) {
                if ($this->rank == 1) {
                    $this->point = 3;
                }
                else if ($this->rank == 2) {
                    $this->point = 2;
                }
                else if ($this->rank == 3) {
                    $this->point = 1;
                }

                if ($reset) {
                    $this->srank = $this->isQualified() ? $this->rank + 3 : null;
                }
            }
        }
    }

    public function createGameResultsForGame(Game $game, $players, $ranks, $sranks)
    {
        $em = $this->em;

        $grRepo = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameResult');
        $gameResults = $grRepo->findBy(array(
            'game' => $game,
        ));
        foreach ($gameResults as $gameResult) {
            $em->remove($gameResult);
        }

        $this->gtype = $game->getGameType()->getComputerName();
        $this->gstype = $game->getGameSubType()->getComputerName();

        $i = $this->point = 1;
        /** @var Player $player */
        foreach ($players as $key => $player) {
            $gr = new GameResult();
            $gr->setGame($game);
            $gr->setPlayer($player);
            $gr->setTeam($player->getTeam());

            $rkey = str_replace('player-', 'rank-', $key);
            $skey = str_replace('player-', 'sr-', $key);
            $this->rank = (array_key_exists($rkey, $ranks)) ? $ranks[$rkey] : null;
            $this->srank = (array_key_exists($skey, $sranks)) ? $sranks[$skey] : null;
            $weight = (int)str_replace('player-', '', $key);
            $gr->setWeight($weight);

            $this->setPoint(false);

            if (!$this->isQualified()) {
                $this->srank = null;
            }
            $gr->setRank($this->rank);
            $gr->setSumRank($this->srank);

            $this->medal = true;

            if ($game->getNumberOfTeamPlayers() && $game->getNumberOfTeamPlayers() > 1) {
                $teamHasRes = $grRepo->findOneBy(array(
                    'game'  => $game,
                    'team'  => $gr->getTeam(),
                    'rank'  => $this->rank,
                    'medal' => true,
                ));

                if ($teamHasRes && $teamHasRes->getId() !== $gr->getId()) {
                    $this->medal = false;
                }
            }

            $gr->setMedal($this->medal);
            $gr->setPoint($this->point);
            $em->persist($gr);
            $em->flush();

            $i++;
        }

        /*
        if (count($players) !== $game->getNumberOfPlayers() && $game->getClosed()) {
            $game->setClosed(false);
        }
        */

        $em->persist($game);

        $em->flush();
    }

    public function createCache() {
        $em = $this->em;
        $grRepo = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameResult');
        $gameResults = $grRepo->getResults();

        /** @var $gameResult GameResult */
        foreach ($gameResults as $gameResult) {
            $game = $gameResult->getGame();

            $this->gtype = $game->getGameType()->getComputerName();
            $this->gstype = $game->getGameSubType()->getComputerName();
            $this->rank = $gameResult->getRank();
            $this->medal = true;

            $this->setPoint(false);

            if (!$this->isQualified()) {
                $this->srank = null;
            }
            $gameResult->setSumRank($this->srank);
            $gameResult->setPoint($this->point);

            if ($game->getNumberOfTeamPlayers() && $game->getNumberOfTeamPlayers() > 1) {
                $teamHasRes = $grRepo->findOneBy(array(
                    'game'  => $game,
                    'team'  => $gameResult->getTeam(),
                    'rank'  => $this->rank,
                    'medal' => true,
                ));

                if ($teamHasRes && $teamHasRes->getId() !== $gameResult->getId()) {
                    $this->medal = false;
                }
            }

            $gameResult->setMedal($this->medal);

            $em->persist($gameResult);
            if ($game->getNumberOfTeamPlayers() && $game->getNumberOfTeamPlayers() > 1) {
                $em->flush();
            }
        }

        $em->flush();
    }

    protected function getRoundName($round) {
        /** @var \DateTime $date */
        $date = $round['date'];
        $dayOfWeek = idate('w', $date->getTimestamp());

        $days = array(
            1 => 'hétfő',
            2 => 'kedd',
            3 => 'szerda',
            4 => 'csütörtök',
            5 => 'péntek',
            6 => 'szombat',
            0 => 'vasárnap',
        );

        return $round['number'] . '. versenynap (' . $date->format('M.d') . ' - ' . $days[$dayOfWeek] . ')';
    }

    protected function getPastResultsForPlayer($rnum, $pid) {
        $em = $this->em;
        $roundRep = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round')->getAllPreviousRounds($rnum, $pid);
        $playersArr = array();

        if ($roundRep) {
            foreach ($roundRep as $round) {
                foreach ($round['gameTypes'] as $gameType) {
                    foreach ($gameType['games'] as $game) {
                        foreach ($game['gameResults'] as $gameResult) {
                            $this->gtype = $gameType['computerName'];
                            $this->gstype = $game['gameSubType']['computerName'];
                            $this->rank = $gameResult['rank'];
                            $this->clear();

                            if ($game['closed'] && $this->isQualified()) {
                                $this->setPoint();

                                if (!array_key_exists('gold', $playersArr)) {
                                    $playersArr = array(
                                        'gold'   => array(),
                                        'silver' => array(),
                                        'bronze' => array(),
                                        'four'   => array(),
                                        'five'   => array(),
                                        'six'    => array(),
                                        'point'  => 0,
                                    );
                                }

                                if ($this->srank == 1) {
                                    $playersArr['gold'][] = $gameType['name'];
                                    $playersArr['point'] += 10;
                                }
                                elseif ($this->srank == 2) {
                                    $playersArr['silver'][] = $gameType['name'];
                                    $playersArr['point'] += 7;
                                }
                                elseif ($this->srank == 3) {
                                    $playersArr['bronze'][] = $gameType['name'];
                                    $playersArr['point'] += 5;
                                }
                                elseif ($this->srank == 4) {
                                    $playersArr['four'][] = $gameType['name'];
                                    $playersArr['point'] += 3;
                                }
                                elseif ($this->srank == 5) {
                                    $playersArr['five'][] = $gameType['name'];
                                    $playersArr['point'] += 2;
                                }
                                elseif ($this->srank == 6) {
                                    $playersArr['six'][] = $gameType['name'];
                                    $playersArr['point'] += 1;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $playersArr;
    }

    public function getMedalsForGames(Round $round)
    {
        $em = $this->em;

        $roundRep = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round')->getRound($round->getId());
        $gameTypesArr = $playersArr = array();
        $ranks = array(1, 2, 3, 4, 5, 6);

        if ($roundRep) {
            foreach ($roundRep['gameTypes'] as $gameType) {
                $gameTypesArr[$gameType['computerName']] = array(
                    'gameType' => $gameType,
                    'gold' => array('teams' => array(), 'number' => 0),
                    'silver' => array('teams' => array(), 'number' => 0),
                    'bronze' => array('teams' => array(), 'number' => 0),
                    'four' => array('teams' => array(), 'number' => 0),
                    'five' => array('teams' => array(), 'number' => 0),
                    'six' => array('teams' => array(), 'number' => 0),
                );
                $gta = &$gameTypesArr[$gameType['computerName']];

                foreach ($gameType['games'] as $game) {
                    foreach ($game['gameResults'] as $gameResult) {
                        $this->gtype = $gameType['computerName'];
                        $this->gstype =  $game['gameSubType']['computerName'];
                        $this->rank = $gameResult['rank'];
                        $this->clear();

                        if ($game['closed'] && $this->isQualified()) {
                            $this->setPoint();
                            $this->srank = $gameResult['sumRank'];

                            if (in_array($this->srank, $ranks)) {
                                $team = $gameResult['team']['computerName'];
                                $player = $gameResult['player'];
                                $playerNum = $player['number'];
                                if (!array_key_exists($playerNum, $playersArr)) {
                                    $playersArr[$playerNum] = $player;
                                }

                                if ($this->srank == 1) {
                                    if (!array_key_exists($team, $gta['gold']['teams'])) {
                                        $gta['gold']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['gold']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['gold']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 2) {
                                    if (!array_key_exists($team, $gta['silver']['teams'])) {
                                        $gta['silver']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['silver']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['silver']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 3) {
                                    if (!array_key_exists($team, $gta['bronze']['teams'])) {
                                        $gta['bronze']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['bronze']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['bronze']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 4) {
                                    if (!array_key_exists($team, $gta['four']['teams'])) {
                                        $gta['four']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['four']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    $gta['four']['number'] += 1;
                                }
                                elseif ($this->srank == 5) {
                                    if (!array_key_exists($team, $gta['five']['teams'])) {
                                        $gta['five']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['five']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    $gta['five']['number'] += 1;
                                }
                                elseif ($this->srank == 6) {
                                    if (!array_key_exists($team, $gta['six']['teams'])) {
                                        $gta['six']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['six']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    $gta['six']['number'] += 1;
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($gameTypesArr as $gameTypeName => &$gameType) {
            foreach ($gameType['gold']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['silver']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['bronze']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['four']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['five']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['six']['teams'] as &$team) {
                sort($team['players']);
            }
        }

        $parr = array();
        /*
        foreach ($playersArr as $pnum => $player) {
            $parr[$pnum] = $this->getPastResultsForPlayer($round->getNumber(), $player['id']);
        }
        ksort($parr);
        */

        return array('medals' => $gameTypesArr, 'players' => $parr);
    }

    public function getAllMedals() {
        $em = $this->em;

        $rounds = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round')->getAllRounds();
        $gameTypesArr = array();

        if ($rounds) {
            foreach ($rounds as $round) {
                foreach ($round['gameTypes'] as $gameType) {
                    if (!array_key_exists($gameType['computerName'], $gameTypesArr)) {
                        $gameTypesArr[$gameType['computerName']] = array(
                            'gameType' => $gameType,
                            'gold' => array('teams' => array(), 'number' => 0),
                            'silver' => array('teams' => array(), 'number' => 0),
                            'bronze' => array('teams' => array(), 'number' => 0),
                            'four' => array('teams' => array(), 'number' => 0),
                            'five' => array('teams' => array(), 'number' => 0),
                            'six' => array('teams' => array(), 'number' => 0),
                        );
                    }
                    $gta = &$gameTypesArr[$gameType['computerName']];

                    foreach ($gameType['games'] as $game) {
                        foreach ($game['gameResults'] as $gameResult) {
                            $this->gtype = $gameType['computerName'];
                            $this->gstype = $game['gameSubType']['computerName'];
                            $this->rank = $gameResult['rank'];

                            if ($game['closed']) {
                                $this->setPoint();
                                $this->srank = $gameResult['sumRank'];
                                $team = $gameResult['team']['computerName'];
                                $player = $gameResult['player'];
                                $playerNum = $player['number'];

                                if ($this->srank == 1) {
                                    if (!array_key_exists($team, $gta['gold']['teams'])) {
                                        $gta['gold']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['gold']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['gold']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 2) {
                                    if (!array_key_exists($team, $gta['silver']['teams'])) {
                                        $gta['silver']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['silver']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['silver']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 3) {
                                    if (!array_key_exists($team, $gta['bronze']['teams'])) {
                                        $gta['bronze']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['bronze']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['bronze']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 4) {
                                    if (!array_key_exists($team, $gta['four']['teams'])) {
                                        $gta['four']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['four']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['four']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 5) {
                                    if (!array_key_exists($team, $gta['five']['teams'])) {
                                        $gta['five']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['five']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['five']['number'] += 1;
                                    }
                                }
                                elseif ($this->srank == 6) {
                                    if (!array_key_exists($team, $gta['six']['teams'])) {
                                        $gta['six']['teams'][$team] = array(
                                            'team' => $gameResult['team'],
                                            'players' => array(),
                                        );
                                    }
                                    $gta['six']['teams'][$team]['players'][$playerNum] = $playerNum;

                                    if ($gameResult['medal']) {
                                        $gta['six']['number'] += 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($gameTypesArr as $gameTypeName => &$gameType) {
            foreach ($gameType['gold']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['silver']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['bronze']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['four']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['five']['teams'] as &$team) {
                sort($team['players']);
            }
            foreach ($gameType['six']['teams'] as &$team) {
                sort($team['players']);
            }
        }

        return $gameTypesArr;
    }
}
