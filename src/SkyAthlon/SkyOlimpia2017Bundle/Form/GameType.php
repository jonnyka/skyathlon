<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Form;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Game;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameResult;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Player;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    protected $em;
    protected $sexType;

    public function __construct(EntityManager $em) {
        $this->em = $em;
        $this->sexType = 3;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Game $game */
        $game = $options['data'];

        $this->sexType = ($game->getGameType() && $game->getGameType()->getSexType()) ? $game->getGameType()->getSexType() : 3;

        if ($game->getNumberOfPlayers()) {
            for ($i = 1; $i <= $game->getNumberOfPlayers(); $i++) {
                $p = $r = $s = null;
                if ($game->getGameResults()) {
                    /** @var GameResult $gameResult */
                    foreach ($game->getGameResults() as $gameResult) {
                        if ($gameResult->getWeight() === $i) {
                            /** @var Player $p */
                            $p = $gameResult->getPlayer();
                            $r = $gameResult->getRank();
                            $s = $gameResult->getSumRank();
                        }
                    }
                }

                $tp = $game->getNumberOfTeamPlayers() ? $game->getNumberOfTeamPlayers() : 1;
                if ($tp === 6) {
                    $tp = 3;
                }
                $clearfix = ($tp === 1) ? 0 : 1;
                $col = 12 / $tp;
                $pcol = ceil($col / 3);
                $rcol = ceil(($col - $pcol) / 2);
                $scol = $col - $pcol - $rcol;
                $clearFix = ($i % $tp === $clearfix) ? ' clearfix' : '';

                $builder->add('player-' . ($i), EntityType::class, array(
                    'class'         => 'SkyAthlonSkyOlimpia2017Bundle:Player',
                    'query_builder' => function(EntityRepository $er) {
                        $qb = $er->createQueryBuilder('p');
                        if ($this->sexType === 1) {
                            $qb->where('p.sex = 1');
                        }
                        elseif ($this->sexType === 2) {
                            $qb->where('p.sex = 0');
                        }

                        return $qb;
                    },
                    'mapped'        => false,
                    'required'      => false,
                    'label'         => ($i == 1) ? 'skyolimpia2017.game.players' : false,
                    'empty_data'    => null,
                    'data'          => $p,
                    'attr'          => array(
                        'class' => 'noformcontrol round-player-select col-sm-' . $pcol . $clearFix,
                    ),
                    'label_attr'    => array(
                        'class' => 'block',
                    ),
                    'choice_attr'   => function($val, $key, $index) {
                        /** @var Player $val */
                        return array(
                            'data-data' => '{"team": "' . ($val ? $val->getTeam()->getComputerName() . '-selectize' : '') . ' team-selectize"}',
                        );
                    },
                ));
                $builder->add('rank-' . ($i), ChoiceType::class, array(
                    'mapped'      => false,
                    'required'    => false,
                    'label'       => false,
                    'empty_data'  => null,
                    'data'        => $r,
                    'attr'        => array(
                        'class' => 'noformcontrol player-rank col-sm-' . $rcol,
                    ),
                    'label_attr'  => array(
                        'class' => 'block',
                    ),
                    'choices' => array(
                        1     => 1,
                        2     => 2,
                        3     => 3,
                        4     => 4,
                        5     => 5,
                        6     => 6,
                        'DQ'  => 'DQ',
                        'DNS' => 'DNS',
                        'DNF' => 'DNF',
                    ),
                ));
                $builder->add('sr-' . ($i), ChoiceType::class, array(
                    'mapped'      => false,
                    'required'    => false,
                    'label'       => false,
                    'empty_data'  => null,
                    'data'        => $s,
                    'attr'        => array(
                        'class' => 'noformcontrol player-rank col-sm-' . $scol,
                    ),
                    'label_attr'  => array(
                        'class' => 'block',
                    ),
                    'choices' => array(
                        1     => 1,
                        2     => 2,
                        3     => 3,
                        4     => 4,
                        5     => 5,
                        6     => 6,
                    ),
                ));
            }
        }

        $builder
            ->add('closed', null, array(
                'label' => 'skyolimpia2017.game.closed',
                'attr'  => array(
                    'class' => 'block',
                ),
                'label_attr' => array(
                    'class' => 'block',
                ),
            ))
            ->add('description', null, array(
                'label' => 'skyolimpia2017.game.description',
                'attr'  => array(
                    'class' => 'block',
                ),
                'label_attr' => array(
                    'class' => 'block',
                ),
            ));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SkyAthlon\SkyOlimpia2017Bundle\Entity\Game'
        ));
    }
}
