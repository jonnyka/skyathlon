<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameResult
 *
 * @ORM\Table(name="skyolimpia2017_game_result")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyOlimpia2017Bundle\Repository\GameResultRepository")
 */
class GameResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="gameResults")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gameResults")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="gameResults")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;

    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="string", nullable=true)
     */
    protected $rank;

    /**
     * @var int
     *
     * @ORM\Column(name="sumrank", type="smallint", nullable=true)
     */
    protected $sumRank;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint", nullable=true)
     */
    protected $weight;

    /**
     * @var int
     *
     * @ORM\Column(name="point", type="smallint", nullable=true)
     */
    protected $point;

    /**
     * @var boolean
     *
     * @ORM\Column(name="medal", type="boolean", options={"default" = false})
     */
    protected $medal;

    public function __toString() {
        return (string)$this->rank;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set team
     *
     * @param Team $team
     *
     * @return GameResult
     */
    public function setTeam(Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set rank
     *
     * @param string $rank
     *
     * @return GameResult
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return string
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set point
     *
     * @param integer $point
     *
     * @return GameResult
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set medal
     *
     * @param boolean $medal
     *
     * @return GameResult
     */
    public function setMedal($medal)
    {
        $this->medal = $medal;

        return $this;
    }

    /**
     * Get medal
     *
     * @return boolean
     */
    public function getMedal()
    {
        return $this->medal;
    }

    /**
     * Set player
     *
     * @param Player $player
     *
     * @return GameResult
     */
    public function setPlayer(Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set game
     *
     * @param Game $game
     *
     * @return GameResult
     */
    public function setGame(Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return GameResult
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set sumRank
     *
     * @param integer $sumRank
     *
     * @return GameResult
     */
    public function setSumRank($sumRank)
    {
        $this->sumRank = $sumRank;

        return $this;
    }

    /**
     * Get sumRank
     *
     * @return integer
     */
    public function getSumRank()
    {
        return $this->sumRank;
    }
}
