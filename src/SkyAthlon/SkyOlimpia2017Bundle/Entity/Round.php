<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Round
 *
 * @ORM\Table(name="skyolimpia2017_round")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyOlimpia2017Bundle\Repository\RoundRepository")
 */
class Round
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint", unique=true)
     */
    protected $number;

    /**
     * @ORM\OneToMany(targetEntity="GameType", mappedBy="round")
     */
    protected $gameTypes;

    /**
     * @ORM\Column(name="closed", type="boolean", nullable=true)
     */
    protected $closed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    protected $date;

    public function __toString() {
        $dayOfWeek = idate('w', $this->date->getTimestamp());

        $days = array(
            1 => 'hétfő',
            2 => 'kedd',
            3 => 'szerda',
            4 => 'csütörtök',
            5 => 'péntek',
            6 => 'szombat',
            0 => 'vasárnap',
        );

        return $this->number . '. versenynap (' . $this->date->format('M.d') . ' - ' . $days[$dayOfWeek] . ')';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gameTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Round
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Add gametype
     *
     * @param GameType $gameType
     *
     * @return Round
     */
    public function addGameType(GameType $gameType)
    {
        $this->gameTypes[] = $gameType;

        return $this;
    }

    /**
     * Remove gametype
     *
     * @param GameType $gameType
     */
    public function removeGameType(GameType $gameType)
    {
        $this->gameTypes->removeElement($gameType);
    }

    /**
     * Get gametypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameTypes()
    {
        return $this->gameTypes;
    }

    /**
     * Set closed
     *
     * @param boolean $closed
     *
     * @return Round
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return boolean
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Round
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
