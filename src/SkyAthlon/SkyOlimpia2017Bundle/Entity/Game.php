<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="skyolimpia2017_game")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyOlimpia2017Bundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="GameType", inversedBy="games")
     * @ORM\JoinColumn(name="gametype_id", referencedColumnName="id")
     */
    protected $gameType;

    /**
     * @ORM\ManyToOne(targetEntity="GameSubType", inversedBy="games")
     * @ORM\JoinColumn(name="gamesubtype_id", referencedColumnName="id")
     */
    protected $gameSubType;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="integer")
     */
    protected $weight;

    /**
     * @ORM\ManyToMany(targetEntity="Player", mappedBy="games")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    protected $players;

    /**
     * @ORM\OneToMany(targetEntity="GameResult", mappedBy="game")
     * @ORM\OrderBy({"rank" = "ASC"})
     */
    protected $gameResults;

    /**
     * @var int
     *
     * @ORM\Column(name="numberofplayers", type="smallint")
     */
    protected $numberOfPlayers;

    /**
     * @var int
     *
     * @ORM\Column(name="numberofteamplayers", type="smallint")
     */
    protected $numberOfTeamPlayers;

    /**
     * @var int
     *
     * @ORM\Column(name="numberofwinners", type="smallint")
     */
    protected $numberOfWinners;

    /**
     * @var string
     *
     * @ORM\Column(name="groupTitle", type="string", length=255, nullable=true)
     */
    protected $groupTitle;

    /**
     * @ORM\Column(name="closed", type="boolean", nullable=true)
     */
    protected $closed;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    public function __toString() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->gameResults = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set gameType
     *
     * @param GameType $gameType
     *
     * @return Game
     */
    public function setGameType(GameType $gameType = null)
    {
        $this->gameType = $gameType;

        return $this;
    }

    /**
     * Get gameType
     *
     * @return GameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Game
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Set number of players
     *
     * @param integer $numberOfPlayers
     *
     * @return Game
     */
    public function setNumberOfPlayers($numberOfPlayers)
    {
        $this->numberOfPlayers = $numberOfPlayers;

        return $this;
    }

    /**
     * Get numberOfPlayers
     *
     * @return int
     */
    public function getNumberOfPlayers()
    {
        return $this->numberOfPlayers;
    }

    /**
     * Set number of team players
     *
     * @param integer $numberOfTeamPlayers
     *
     * @return Game
     */
    public function setNumberOfTeamPlayers($numberOfTeamPlayers)
    {
        $this->numberOfTeamPlayers = $numberOfTeamPlayers;

        return $this;
    }

    /**
     * Get numberOfTeamPlayers
     *
     * @return int
     */
    public function getNumberOfTeamPlayers()
    {
        return $this->numberOfTeamPlayers;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set numberOfWinners
     *
     * @param integer $numberOfWinners
     *
     * @return Game
     */
    public function setNumberOfWinners($numberOfWinners)
    {
        $this->numberOfWinners = $numberOfWinners;

        return $this;
    }

    /**
     * Get numberOfWinners
     *
     * @return integer
     */
    public function getNumberOfWinners()
    {
        return $this->numberOfWinners;
    }

    /**
     * Add player
     *
     * @param Player $player
     *
     * @return Game
     */
    public function addPlayer(Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param Player $player
     */
    public function removePlayer(Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Remove players
     */
    public function removePlayers()
    {
        $this->players->clear();
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add gameResult
     *
     * @param GameResult $gameResult
     *
     * @return Game
     */
    public function addGameResult(GameResult $gameResult)
    {
        $this->gameResults[] = $gameResult;

        return $this;
    }

    /**
     * Remove gameResult
     *
     * @param GameResult $gameResult
     */
    public function removeGameResult(GameResult $gameResult)
    {
        $this->gameResults->removeElement($gameResult);
    }

    /**
     * Get gameResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameResults()
    {
        return $this->gameResults;
    }

    /**
     * Set gameSubType
     *
     * @param GameSubType $gameSubType
     *
     * @return Game
     */
    public function setGameSubType(GameSubType $gameSubType = null)
    {
        $this->gameSubType = $gameSubType;

        return $this;
    }

    /**
     * Get gameSubType
     *
     * @return GameSubType
     */
    public function getGameSubType()
    {
        return $this->gameSubType;
    }

    /**
     * Set groupTitle
     *
     * @param string $groupTitle
     *
     * @return Game
     */
    public function setGroupTitle($groupTitle)
    {
        $this->groupTitle = $groupTitle;

        return $this;
    }

    /**
     * Get groupTitle
     *
     * @return string
     */
    public function getGroupTitle()
    {
        return $this->groupTitle;
    }

    /**
     * Set closed
     *
     * @param boolean $closed
     *
     * @return Game
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return boolean
     */
    public function getClosed()
    {
        return $this->closed;
    }
}
