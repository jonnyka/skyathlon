<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="skyolimpia2017_team")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyOlimpia2017Bundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=255)
     */
    protected $computerName;

    /**
     * @var string
     *
     * @ORM\Column(name="shortName", type="string", length=255)
     */
    protected $shortName;

    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="team")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    protected $players;

    /**
     * @var int
     *
     * @ORM\Column(name="point", type="integer", nullable=true)
     */
    protected $point;

    /**
     * @ORM\OneToMany(targetEntity="GameResult", mappedBy="team")
     */
    protected $gameResults;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->gameResults = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add player
     *
     * @param Player $player
     *
     * @return Team
     */
    public function addPlayer(Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param Player $player
     */
    public function removePlayer(Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Set point
     *
     * @param integer $point
     *
     * @return Team
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return Team
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }

    /**
     * Add gameResult
     *
     * @param GameResult $gameResult
     *
     * @return Team
     */
    public function addGameResult(GameResult $gameResult)
    {
        $this->gameResults[] = $gameResult;

        return $this;
    }

    /**
     * Remove gameResult
     *
     * @param GameResult $gameResult
     */
    public function removeGameResult(GameResult $gameResult)
    {
        $this->gameResults->removeElement($gameResult);
    }

    /**
     * Get gameResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameResults()
    {
        return $this->gameResults;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return Team
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }
}
