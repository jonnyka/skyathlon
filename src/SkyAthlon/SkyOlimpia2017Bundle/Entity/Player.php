<?php

namespace SkyAthlon\SkyOlimpia2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table(name="skyolimpia2017_player")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyOlimpia2017Bundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint", unique=true)
     */
    protected $number;
    
    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="players")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * @var boolean
     *
     * @ORM\Column(name="leader", type="boolean", options={"default" = false})
     */
    protected $leader;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sex", type="boolean", options={"default" = false})
     */
    protected $sex;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint", options={"default" = 1})
     */
    protected $weight;

    /**
     * @ORM\OneToMany(targetEntity="GameResult", mappedBy="player")
     */
    protected $gameResults;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="ref")
     */
    protected $refs;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="sref")
     */
    protected $srefs;

    /**
     * @ORM\ManyToMany(targetEntity="Game", inversedBy="players")
     * @ORM\JoinTable(name="skyolimpia2017_players_games")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    protected $games;

    public function __toString() {
        return (string)$this->number/* . ' - ' . $this->name*/;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gameResults = new ArrayCollection();
        $this->refs = new ArrayCollection();
        $this->srefs = new ArrayCollection();
        $this->games = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Player
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set team
     *
     * @param Team $team
     *
     * @return Player
     */
    public function setTeam(Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set leader
     *
     * @param boolean $leader
     *
     * @return Player
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Get leader
     *
     * @return boolean
     */
    public function getLeader()
    {
        return $this->leader;
    }

    /**
     * Set sex
     *
     * @param boolean $sex
     *
     * @return Player
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return boolean
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Player
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add ref
     *
     * @param Game $ref
     *
     * @return Player
     */
    public function addRef(Game $ref)
    {
        $this->refs[] = $ref;

        return $this;
    }

    /**
     * Remove ref
     *
     * @param Game $ref
     */
    public function removeRef(Game $ref)
    {
        $this->refs->removeElement($ref);
    }

    /**
     * Get refs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRefs()
    {
        return $this->refs;
    }

    /**
     * Add sref
     *
     * @param Game $sref
     *
     * @return Player
     */
    public function addSref(Game $sref)
    {
        $this->srefs[] = $sref;

        return $this;
    }

    /**
     * Remove sref
     *
     * @param Game $sref
     */
    public function removeSref(Game $sref)
    {
        $this->srefs->removeElement($sref);
    }

    /**
     * Get srefs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSrefs()
    {
        return $this->srefs;
    }

    /**
     * Add gameResult
     *
     * @param GameResult $gameResult
     *
     * @return Player
     */
    public function addGameResult(GameResult $gameResult)
    {
        $this->gameResults[] = $gameResult;

        return $this;
    }

    /**
     * Remove gameResult
     *
     * @param GameResult $gameResult
     */
    public function removeGameResult(GameResult $gameResult)
    {
        $this->gameResults->removeElement($gameResult);
    }

    /**
     * Get gameResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameResults()
    {
        return $this->gameResults;
    }

    /**
     * Add game
     *
     * @param Game $game
     *
     * @return Player
     */
    public function addGame(Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param Game $game
     */
    public function removeGame(Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }
}
