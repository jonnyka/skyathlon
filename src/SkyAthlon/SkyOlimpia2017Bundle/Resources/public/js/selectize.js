var roundPlayerSelect = function(data) {
    return '<span class="' + data.team + '">' + data.text + '</span>';
};

$('.round-player-select').selectize({
    openOnFocus: false,
    closeAfterSelect: true,
    loadThrottle: 0,
    render: {
        option: function(data, escape) {
            return roundPlayerSelect(data);
        },
        item: function(data, escape) {
            return roundPlayerSelect(data);
        }
    }
});

$('.player-rank').selectize({
    openOnFocus: false,
    closeAfterSelect: true,
    loadThrottle: 0
});
