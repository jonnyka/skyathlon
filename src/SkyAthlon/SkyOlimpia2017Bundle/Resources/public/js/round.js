// Load player stats via ajax
$('.round-game-showhide2').click(function() {
    var gid = $(this).data('target'),
        otherDiv = $('#round-game-body-' + gid),
        thisDiv = $('#round-game-body2-' + gid),
        i,
        k,
        player,
        kerchief,
        value,
        computerName,
        maxValue,
        tr,
        text,
        table = $('#round-game-table-' + gid),
        spinner = $('#spinner-' + gid);

    otherDiv.hide();

    if (thisDiv.is(':visible')) {
        thisDiv.hide();
    }
    else if (table.hasClass('content-loaded')) {
        spinner.hide();
        table.show();
        thisDiv.show();
    }
    else {
        spinner.show();
        thisDiv.show();
        table.hide();

        $.ajax({
            type: "GET",
            url: Routing.generate('skyolimpia2017_game_kerchiefs', { game: gid}),
            success: function (data) {
                spinner.hide();
                table.show().addClass('content-loaded');

                var div = $('#round-game-tbody-' + gid);

                for (i in data['players']) {
                    player = data['players'][i];

                    tr = div.find('#round-game-player-' + gid + '-' + i);
                    text = '<span class="sortby">' + i + '</span><span class="player-number">' + player[0]['player']['number'] + '</span> <b>' + player[0].player.name + '</b>';
                    tr.find('#round-game-player-td-' + gid + '-p').html(text).addClass(player[0]['player']['team'] + '-text ' + player[0]['player']['team'] + '-flag-left');

                    for (k in player) {
                        kerchief = player[k];
                        computerName = kerchief['kerchiefType']['computerName'];
                        maxValue = data['maxValues'][computerName];
                        value = kerchief.value;
                        if (value === maxValue && value !== 0) {
                            value = '<b>' + value + '</b>';
                        }

                        tr.find('#round-game-player-td-' + gid + '-' + k).html(value);
                    }
                }

                table.dataTable({
                    'paging':   false,
                    'info':     false,
                    'filter':   false,
                    'language': {
                        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
                    },
                    'columnDefs': [
                        {
                            'targets': 0,
                            'orderDataType': 'sortby'
                        },
                        {
                            'targets': [1,2,3,4,5,6,7,8,9,10],
                            'orderSequence': ["desc", "asc"]
                        }
                    ]
                });
            }
        });
    }
});

// Filter teams on round view
$('#filter-team').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $('.round-game').show().filter(function() {
        var text = $(this).find('.round-game-head').find('.round-game-team').text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

// Focus first number on jegyzokony form
function focusFirstNumber() {
    $(':input[type="number"]').first().focus().select();
}

if (window.location.href.indexOf('jatekos-statisztika/szerkesztes') > 1) {
    focusFirstNumber();
    var timeOut = setTimeout(focusFirstNumber, 1000);
}

// Hide stuff if it's a playoff round
function playOffs() {
    if ($('#isplayoff').length) {
        $('#orderPlayoffButton').css("visibility", "visible");
    }

    if (typeof isPlayoffs !== 'undefined' && isPlayoffs) {
        $('#content').addClass('rajatszas');
        $('#filter-teams').hide();
    }
}

playOffs();

function stickyHeader() {
    var stickyHeader = $('.sticky-header');

    if (stickyHeader.length) {
        stickyHeader.floatThead({top: 75});
    }
}

stickyHeader();