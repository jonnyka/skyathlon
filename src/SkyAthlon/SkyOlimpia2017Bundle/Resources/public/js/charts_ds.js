google.charts.load('current', {'packages': ['corechart']});
if (document.getElementById('chart_ds_sum')) {
    google.charts.setOnLoadCallback(drawDsSum);
}
if (document.getElementById('chart_ds_male')) {
    google.charts.setOnLoadCallback(drawDsMale);
}
if (document.getElementById('chart_ds_female')) {
    google.charts.setOnLoadCallback(drawDsFemale);
}
if (document.getElementById('chart_ds_team')) {
    google.charts.setOnLoadCallback(drawDsTeam);
}

var ds = {
    sum: {
        title: 'Versenyzők be nem fejezett versenyei',
        sex: 2,
        div: 'chart_ds_sum',
        width: '80%',
        font: 7
    },
    male: {
        title: 'Férfiak be nem fejezett versenyei',
        sex: 1,
        div: 'chart_ds_male',
        width: '90%',
        font: 9
    },
    female: {
        title: 'Nők be nem fejezett versenyei',
        sex: 0,
        div: 'chart_ds_female',
        width: '90%',
        font: 8
    }
};

function drawDsSum() {
    drawDs(ds, 'sum');
}

function drawDsMale() {
    drawDs(ds, 'male');
}

function drawDsFemale() {
    drawDs(ds, 'female');
}

function drawDs(ds, sex) {
    $.ajax({
        type: "GET",
        url: Routing.generate('skyolimpia2017_api_ds'),
        success: function (d) {
            var redrawDs = function() {
                var medal = ds[sex],
                    data,
                    i,
                    f = 0,
                    chart,
                    options,
                    columns = [],
                    dataArray = [],
                    dataTable = new google.visualization.DataTable({
                        cols: [
                            {id: 'player', label: 'Versenyző', type: 'string'},
                            {id: 'tooltip', type: 'string', role: 'tooltip', p: {'html': true}},
                            {id: 'dq', label: 'DQ', type: 'number', color: '#d9534f'},
                            {id: 'dns', label: 'DNS', type: 'number', color: '#4FD968'},
                            {id: 'dnf', label: 'DNF', type: 'number', color: '#4F8BD9'},
                            {id: 'team', type: 'string', role: 'annotationText'},
                            {id: 'sum', type: 'number', role: 'annotationText'},
                            {id: 'match', type: 'number', role: 'annotationText'}
                        ]
                    });

                for (i in d) {
                    data = d[i];
                    if (data.sex == medal.sex || medal.sex == 2) {
                        dataArray.push([
                            data.number,
                            dsTooltip(data.team, data.number, data.dq, data.dns, data.dnf, data.match, f, true, true, true),
                            data.dq,
                            data.dns,
                            data.dnf,
                            data.team,
                            +data.dq + +data.dns + +data.dnf,
                            data.match
                        ]);

                        f++;
                    }
                }

                dataTable.addRows(dataArray);

                options = {
                    title: medal.title,
                    vAxis: {
                        title: 'Be nem fejezett versenyek száma',
                        gridlines: {
                            count: 5
                        },
                        viewWindow: {
                            max: 20,
                            min: 0
                        }
                    },
                    hAxis: {
                        title: 'Versenyzők',
                        textStyle: {
                            fontSize: medal.font
                        }
                    },
                    animation: {
                        startup: true,
                        duration: 1000,
                        easing: 'out'
                    },
                    colors: ['#d9534f', '#4FD968', '#4F8BD9'],
                    bar: {groupWidth: medal.width},
                    focusTarget: 'category',
                    isStacked: true,
                    tooltip: {isHtml: true}
                };

                chart = new google.visualization.ColumnChart(document.getElementById(medal.div));
                chart.draw(dataTable, options);

                for (var j = 0; j < dataTable.getNumberOfColumns(); j++) {
                    columns.push(j);
                }

                google.visualization.events.addListener(chart, 'select', function () {
                    var sel = chart.getSelection(),
                        view,
                        column,
                        c,
                        col,
                        row,
                        j,
                        goldColor = '#d9534f',
                        silverColor = '#4FD968',
                        bronzeColor = '#4F8BD9',
                        naturalColor = '#fff',
                        sum = 0,
                        toSort = [6],
                        s,
                        sorting = [],
                        gold = true,
                        silver = true,
                        bronze = true;

                    if (sel.length > 0) {
                        col = sel[0].column;
                        row = sel[0].row;

                        if (row == null) {
                            if (columns[col] == col) {
                                columns[col] = {
                                    label: dataTable.getColumnLabel(col),
                                    type: dataTable.getColumnType(col),
                                    calc: function () {
                                        return null;
                                    }
                                };
                            }
                            else {
                                columns[col] = col;
                            }

                            // col var - arany: 2, ezust: 3, bronz: 4
                            for (c in columns) {
                                column = columns[c];

                                if (typeof column === 'object') {
                                    if (c == 2) {
                                        gold = false;
                                        goldColor = naturalColor;
                                    }
                                    else if (c == 3) {
                                        silver = false;
                                        silverColor = naturalColor;
                                    }
                                    else if (c == 4) {
                                        bronze = false;
                                        bronzeColor = naturalColor;
                                    }
                                }
                            }

                            options.colors = [goldColor, silverColor, bronzeColor];

                            for (j = 0; j < dataTable.getNumberOfRows(); j++) {
                                sum = 0;
                                toSort = [6];

                                if (gold === true) {
                                    sum += dataTable.getValue(j, 2);
                                    toSort.push(2);
                                }

                                if (silver === true) {
                                    sum += dataTable.getValue(j, 3);
                                    toSort.push(3);
                                }

                                if (bronze === true) {
                                    sum += dataTable.getValue(j, 4);
                                    toSort.push(4)
                                }

                                dataTable.setValue(j, 6, sum);
                            }

                            for (s in toSort) {
                                sorting.push({column: toSort[s], desc: true});
                            }

                            dataTable.sort(sorting);

                            for (j = 0; j < dataTable.getNumberOfRows(); j++) {
                                dataTable.setValue(j, 1, dsTooltip(dataTable.getValue(j, 5), dataTable.getValue(j, 0), dataTable.getValue(j, 2), dataTable.getValue(j, 3), dataTable.getValue(j, 4), dataTable.getValue(j, 7), j, gold, silver, bronze));
                            }

                            view = new google.visualization.DataView(dataTable);
                            view.setColumns(columns);

                            chart.draw(view, options);
                        }
                    }
                });
            };

            redrawDs();

            $(window).resize(redrawDs);
        }
    });
}

function dsTooltip(team, number, totalGold, totalSilver, totalBronze, match, i, gold, silver, bronze) {
    var sum = 0,
        ret,
        goldStyle = '',
        silverStyle = '',
        bronzeStyle = '',
        sumStyle = '',
        dPercentage;

    i++;

    if (gold) {
        sum += totalGold;
    }
    else {
        goldStyle = ' hidden';
    }

    if (silver) {
        sum += totalSilver;
    }
    else {
        silverStyle = ' hidden';
    }

    if (bronze) {
        sum += totalBronze;
    }
    else {
        bronzeStyle = ' hidden';
    }
    if (goldStyle && silverStyle && bronzeStyle) {
        sumStyle = ' hidden';
    }

    dPercentage = Math.round((+sum / +match) * 100) + '%';

    ret = '<div style="width: 175px; padding: 5px;">' +
        '<span class="block team-flag-left ' + team + '-flag-left" style="text-align: center; margin: 0 auto; font-size: 20px;">' + number + '</span><hr style="margin: 5px" />' +
        '<span class="medal medal-sum"><b>Helyezés:</b> ' + i + '</span><hr style="margin: 5px" />' +
        '<span class="block' + goldStyle + '">DQ: ' + totalGold + '</span>' +
        '<span class="block' + silverStyle + '">DNS: ' + totalSilver + '</span>' +
        '<span class="block' + bronzeStyle + '">DNF: ' + totalBronze + '</span><hr class="' + sumStyle + '" style="margin: 5px" />' +
        '<span class="block medal medal-sum' + sumStyle + '"><b>Összesen:</b> ' + sum + '</span>' +
        '<span class="block medal medal-sum' + sumStyle + '"><b>Összes versenyszám:</b> ' + match + '</span>' +
        '<span class="block medal medal-sum' + sumStyle + '"><b>Ezek aránya:</b> ' + dPercentage + '</span>' +
        '</div>';

    return ret;
}

function drawDsTeam() {
    $.ajax({
        type: "GET",
        url: Routing.generate('skyolimpia2017_api_ds_team'),
        success: function (d) {
            var redrawDts = function() {
                var data,
                    i,
                    f = 0,
                    chart,
                    options,
                    columns = [],
                    dataArray = [],
                    dataTable = new google.visualization.DataTable({
                        cols: [
                            {id: 'team', label: 'Nemzet', type: 'string'},
                            {id: 'tooltip', type: 'string', role: 'tooltip', p: {'html': true}},
                            {id: 'dq', label: 'DQ', type: 'number', color: '#d9534f'},
                            {id: 'dns', label: 'DNS', type: 'number', color: '#4FD968'},
                            {id: 'dnf', label: 'DNF', type: 'number', color: '#4F8BD9'},
                            {id: 'teamstr', type: 'string', role: 'annotationText'},
                            {id: 'teamstrr', type: 'string', role: 'annotationText'},
                            {id: 'sum', type: 'number', role: 'annotationText'},
                            {id: 'match', type: 'number', role: 'annotationText'}
                        ]
                    });

                for (i in d) {
                    data = d[i];
                    dataArray.push([
                        data.team.shortName,
                        dstTooltip(data.team.computerName, data.team.name, data.dq, data.dns, data.dnf, data.match, f, true, true, true),
                        data.dq,
                        data.dns,
                        data.dnf,
                        data.team.computerName,
                        data.team.name,
                        +data.dq + +data.dns + +data.dnf,
                        data.match
                    ]);

                    f++;
                }

                dataTable.addRows(dataArray);

                options = {
                    title : 'Nemzetek be nem fejezett versenyei',
                    vAxis: {
                        title: 'Be nem fejezett versenyek száma',
                        gridlines: {
                            count: 5
                        },
                        viewWindow:{
                            max: 80,
                            min: 0
                        }
                    },
                    hAxis: {
                        title: 'Nemzetek',
                        textStyle: {
                            fontSize: 10
                        }
                    },
                    animation: {
                        startup: true,
                        duration: 1000,
                        easing: 'out'
                    },
                    colors: ['#d9534f', '#4FD968', '#4F8BD9'],
                    bar: { groupWidth: '90%' },
                    focusTarget: 'category',
                    isStacked: true,
                    tooltip: {isHtml: true}
                };

                chart = new google.visualization.ColumnChart(document.getElementById('chart_ds_team'));
                chart.draw(dataTable, options);

                columns = [];
                for (var j = 0; j < dataTable.getNumberOfColumns(); j++) {
                    columns.push(j);
                }

                google.visualization.events.addListener(chart, 'select', function () {
                    var sel = chart.getSelection(),
                        view,
                        column,
                        c,
                        col,
                        row,
                        j,
                        sum = 0,
                        s,
                        toSort = [7],
                        sorting = [],
                        goldColor = '#d9534f',
                        silverColor = '#4FD968',
                        bronzeColor = '#4F8BD9',
                        naturalColor = '#fff',
                        gold = true,
                        silver = true,
                        bronze = true;

                    if (sel.length > 0) {
                        col = sel[0].column;
                        row = sel[0].row;

                        if (row == null) {
                            if (columns[col] == col) {
                                columns[col] = {
                                    label: dataTable.getColumnLabel(col),
                                    type: dataTable.getColumnType(col),
                                    calc: function () {
                                        return null;
                                    }
                                };
                            }
                            else {
                                columns[col] = col;
                            }

                            // col var - arany: 2, ezust: 3, bronz: 4
                            for (c in columns) {
                                column = columns[c];

                                if (typeof column === 'object') {
                                    if (c == 2) {
                                        gold = false;
                                        goldColor = naturalColor;
                                    }
                                    else if (c == 3) {
                                        silver = false;
                                        silverColor = naturalColor;
                                    }
                                    else if (c == 4) {
                                        bronze = false;
                                        bronzeColor = naturalColor;
                                    }
                                }
                            }

                            options.colors = [goldColor, silverColor, bronzeColor];

                            for (j = 0; j < dataTable.getNumberOfRows(); j++) {
                                sum = 0;
                                toSort = [7];

                                if (gold === true) {
                                    sum += dataTable.getValue(j, 2);
                                    toSort.push(2);
                                }

                                if (silver === true) {
                                    sum += dataTable.getValue(j, 3);
                                    toSort.push(3);
                                }

                                if (bronze === true) {
                                    sum += dataTable.getValue(j, 4);
                                    toSort.push(4)
                                }

                                dataTable.setValue(j, 7, sum);
                            }

                            for (s in toSort) {
                                sorting.push({column: toSort[s], desc: true});
                            }

                            dataTable.sort(sorting);

                            for (j = 0; j < dataTable.getNumberOfRows(); j++) {
                                dataTable.setValue(j, 1, dstTooltip(dataTable.getValue(j, 5), dataTable.getValue(j, 6), dataTable.getValue(j, 2), dataTable.getValue(j, 3), dataTable.getValue(j, 4), dataTable.getValue(j, 8), j, gold, silver, bronze));
                            }

                            view = new google.visualization.DataView(dataTable);
                            view.setColumns(columns);

                            chart.draw(view, options);
                        }
                    }
                });
            };

            redrawDts();

            $(window).resize(redrawDts);
        }
    });
}

function dstTooltip(computerName, name, totalGold, totalSilver, totalBronze, match, i, gold, silver, bronze) {
    var sum = 0,
        ret,
        goldStyle = '',
        silverStyle = '',
        bronzeStyle = '',
        sumStyle = '',
        dPercentage;

    i++;

    if (gold) {
        sum += totalGold;
    }
    else {
        goldStyle = ' hidden';
    }

    if (silver) {
        sum += totalSilver;
    }
    else {
        silverStyle = ' hidden';
    }

    if (bronze) {
        sum += totalBronze;
    }
    else {
        bronzeStyle = ' hidden';
    }

    if (goldStyle && silverStyle && bronzeStyle) {
        sumStyle = ' hidden';
    }

    dPercentage = Math.round((+sum / +match) * 100) + '%';

    ret = '<div style="width: 225px; padding: 5px;">' +
        '<span class="block team-flag-left ' + computerName + '-flag-left" style="text-align: center; margin: 0 auto; font-size: 20px;">' + name + '</span><hr style="margin: 5px" />' +
        '<span class="medal medal-sum"><b>Helyezés:</b> ' + i + '</span><hr style="margin: 5px" />' +
        '<span class="block' + goldStyle + '">DQ: ' + totalGold + '</span>' +
        '<span class="block' + silverStyle + '">DNS: ' + totalSilver + '</span>' +
        '<span class="block' + bronzeStyle + '">DNF: ' + totalBronze + '</span><hr class="' + sumStyle + '" style="margin: 5px" />' +
        '<span class="block medal medal-sum' + sumStyle + '"><b>Összesen:</b> ' + sum + '</span>' +
        '<span class="block medal medal-sum' + sumStyle + '"><b>Összes versenyszám:</b> ' + match + '</span>' +
        '<span class="block medal medal-sum' + sumStyle + '"><b>Ezek aránya:</b> ' + dPercentage + '</span>' +
        '</div>';

    return ret;
}
