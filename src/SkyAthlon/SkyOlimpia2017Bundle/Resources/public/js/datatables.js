$('#team-medals-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortbytext',
            'sType': 'string',
            'orderable': false
        },
        {
            'targets': [1,2,3],
            'orderSequence': ["desc", "asc"],
            'orderable': false
        }
    ],
    'order': [
        [1, 'desc'],
        [2, 'desc'],
        [3, 'desc'],
        [0, 'asc']
    ]
});

$('#team-movements-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortbytext',
            'sType': 'string',
            'orderable': false
        },
        {
            'targets': [1,3],
            'orderSequence': ["asc", "desc"]
        },
        {
            'targets': [2,4],
            'orderable': false
        }
    ],
    'order': [
        [1, 'asc'],
        [3, 'asc']
    ]
});

$('#team-points-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortbytext',
            'sType': 'string',
            'orderable': false
        },
        {
            'targets': [1],
            'orderSequence': ["desc", "asc"],
            'orderable': false
        }
    ],
    'order': [
        [1, 'desc'],
        [0, 'asc']
    ]
});

var t = $('#players-table').DataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'searchable': false,
            'orderable': false,
            'targets': 0
        },
        {
            'targets': 1,
            'orderDataType': 'numeric',
            'sType': 'numeric'
        },
        {
            'targets': [2, 3, 4, 5, 6, 7, 8],
            'orderDataType': 'sortbyresult',
            'orderSequence': ["desc", "asc"],
            'sType': 'numeric'
        }
    ],
    'order': [
        [8, 'desc'],
        [1, 'asc']
    ]
});

t.on('order.dt search.dt', function () {
    t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
    });
}).draw();

var tt = $('#players-table2').DataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'searchable': false,
            'orderable': false,
            'targets': 0
        },
        {
            'targets': 1,
            'orderDataType': 'numeric',
            'sType': 'numeric'
        },
        {
            'targets': [2, 3, 4],
            'orderDataType': 'sortbyresult',
            'orderSequence': ["desc", "asc"],
            'sType': 'numeric'
        }
    ],
    'order': [
        [2, 'desc'],
        [3, 'desc'],
        [4, 'desc'],
        [1, 'asc']
    ]
});

tt.on('order.dt search.dt', function () {
    tt.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i + 1;
    });
}).draw();
