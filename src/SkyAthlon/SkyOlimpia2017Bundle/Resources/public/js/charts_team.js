google.charts.load('current', {'packages': ['corechart']});
if (document.getElementById('chart_medal_team')) {
    google.charts.setOnLoadCallback(drawMedalTeam);
}
if (document.getElementById('chart_point_team')) {
    google.charts.setOnLoadCallback(drawPointTeam);
}

function drawMedalTeam() {
    $.ajax({
        type: "GET",
        url: Routing.generate('skyolimpia2017_api_medals_team'),
        success: function (d) {
            var redrawMedals = function() {
                var data,
                    i,
                    f = 0,
                    chart,
                    options,
                    columns = [],
                    dataArray = [],
                    dataTable = new google.visualization.DataTable({
                        cols: [
                            {id: 'team', label: 'Nemzet', type: 'string'},
                            {id: 'tooltip', type: 'string', role: 'tooltip', p: {'html': true}},
                            {id: 'gold', label: 'Arany', type: 'number', color: '#FFD700'},
                            {id: 'silver', label: 'Ezüst', type: 'number', color: '#C0C0C0'},
                            {id: 'bronze', label: 'Bronz', type: 'number', color: '#8C7853'},
                            {id: 'teamstr', type: 'string', role: 'annotationText'},
                            {id: 'teamstrr', type: 'string', role: 'annotationText'}
                        ]
                    });

                for (i in d) {
                    data = d[i];
                    dataArray.push([
                        data.team.shortName,
                        medalTooltip(data.team.computerName, data.team.name, data.gold, data.silver, data.bronze, f, true, true, true),
                        data.gold,
                        data.silver,
                        data.bronze,
                        data.team.computerName,
                        data.team.name
                    ]);

                    f++;
                }

                dataTable.addRows(dataArray);

                options = {
                    title : 'Nemzetek érmei',
                    vAxis: {
                        title: 'Érmek száma',
                        gridlines: {
                            count: 5
                        },
                        viewWindow:{
                            max: 80,
                            min: 0
                        }
                    },
                    hAxis: {
                        title: 'Nemzetek',
                        textStyle: {
                            fontSize: 10
                        }
                    },
                    animation: {
                        startup: true,
                        duration: 1000,
                        easing: 'out'
                    },
                    colors: ['#FFD700', '#C0C0C0', '#8C7853'],
                    bar: { groupWidth: '90%' },
                    focusTarget: 'category',
                    isStacked: true,
                    tooltip: {isHtml: true}
                };

                chart = new google.visualization.ColumnChart(document.getElementById('chart_medal_team'));
                chart.draw(dataTable, options);

                columns = [];
                for (var j = 0; j < dataTable.getNumberOfColumns(); j++) {
                    columns.push(j);
                }

                google.visualization.events.addListener(chart, 'select', function () {
                    var sel = chart.getSelection(),
                        view,
                        column,
                        c,
                        col,
                        row,
                        sorting = [],
                        goldColor = '#FFD700',
                        silverColor = '#C0C0C0',
                        bronzeColor = '#8C7853',
                        naturalColor = '#fff',
                        gold = true,
                        silver = true,
                        bronze = true;

                    if (sel.length > 0) {
                        col = sel[0].column;
                        row = sel[0].row;

                        if (row == null) {
                            if (columns[col] == col) {
                                columns[col] = {
                                    label: dataTable.getColumnLabel(col),
                                    type: dataTable.getColumnType(col),
                                    calc: function () {
                                        return null;
                                    }
                                };
                            }
                            else {
                                columns[col] = col;
                            }

                            // col var - arany: 2, ezust: 3, bronz: 4
                            for (c in columns) {
                                column = columns[c];

                                if (typeof column === 'object') {
                                    if (c == 2) {
                                        gold = false;
                                        goldColor = naturalColor;
                                    }
                                    else if (c == 3) {
                                        silver = false;
                                        silverColor = naturalColor;
                                    }
                                    else if (c == 4) {
                                        bronze = false;
                                        bronzeColor = naturalColor;
                                    }
                                }
                            }

                            if (gold === true && silver === true && bronze === true ||
                                gold === true && silver === true && bronze === false ||
                                gold === true && silver === false && bronze === false ||
                                gold === false && silver === false && bronze === false) {
                                sorting = [{column: 2, desc: true}, {column: 3, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === true && silver === false && bronze === true) {
                                sorting = [{column: 2, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === false && silver === true && bronze === true ||
                                gold === false && silver === true && bronze === false) {
                                sorting = [{column: 3, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === false && silver === false && bronze === true) {
                                sorting = [{column: 4, desc: true}];
                            }

                            options.colors = [goldColor, silverColor, bronzeColor];
                            dataTable.sort(sorting);

                            for (var j = 0; j < dataTable.getNumberOfRows(); j++) {
                                dataTable.setValue(j, 1, medalTooltip(dataTable.getValue(j, 5), dataTable.getValue(j, 6), dataTable.getValue(j, 2), dataTable.getValue(j, 3), dataTable.getValue(j, 4), j, gold, silver, bronze));
                            }

                            view = new google.visualization.DataView(dataTable);
                            view.setColumns(columns);

                            chart.draw(view, options);
                        }
                    }
                });
            };

            redrawMedals();

            $(window).resize(redrawMedals);
        }
    });
}

function medalTooltip(computerName, name, totalGold, totalSilver, totalBronze, i, gold, silver, bronze) {
    var sum = 0,
        ret,
        goldStyle = '',
        silverStyle = '',
        bronzeStyle = '',
        sumStyle = '';

    i++;

    if (gold) {
        sum += totalGold;
    }
    else {
        goldStyle = ' hidden';
    }

    if (silver) {
        sum += totalSilver;
    }
    else {
        silverStyle = ' hidden';
    }

    if (bronze) {
        sum += totalBronze;
    }
    else {
        bronzeStyle = ' hidden';
    }

    if (goldStyle && silverStyle && bronzeStyle) {
        sumStyle = ' hidden';
    }

    ret = '<div style="width: 225px; padding: 5px;">' +
        '<span class="block team-flag-left ' + computerName + '-flag-left" style="text-align: center; margin: 0 auto; font-size: 20px;">' + name + '</span><hr style="margin: 5px" />' +
        '<span class="medal medal-sum"><b>Helyezés:</b> ' + i + '</span><hr style="margin: 5px" />' +
        '<span class="block medal medal-gold' + goldStyle + '">' + totalGold + '</span>' +
        '<span class="block medal medal-silver' + silverStyle + '">' + totalSilver + '</span>' +
        '<span class="block medal medal-bronze' + bronzeStyle + '">' + totalBronze + '</span><hr class="' + sumStyle + '" style="margin: 5px" />' +
        '<span class="medal medal-sum' + sumStyle + '"><b>Összesen:</b> ' + sum + '</span>' +
        '</div>';

    return ret;
}

function drawPointTeam() {
    $.ajax({
        type: "GET",
        url: Routing.generate('skyolimpia2017_api_points_team'),
        success: function (d) {
            var data,
                i,
                f = 0,
                chart,
                options,
                dataArray = [],
                dataTable = new google.visualization.DataTable({
                    cols: [
                        {id: 'team', label: 'Nemzet', type: 'string'},
                        {id: 'tooltip', type: 'string', role: 'tooltip', p: {'html': true}},
                        {id: 'point', label: 'Pontszám', type: 'number', color: '#5bbdff'},
                        {id: 'teamstr', type: 'string', role: 'annotationText'},
                        {id: 'teamstrr', type: 'string', role: 'annotationText'}
                    ]
                });

            for (i in d) {
                data = d[i];
                dataArray.push([
                    data.team.shortName,
                    pointTooltip(data.team.computerName, data.team.name, data.point, f),
                    data.point,
                    data.team.computerName,
                    data.team.name
                ]);

                f++;
            }

            dataTable.addRows(dataArray);

            options = {
                title : 'Nemzetek pontszámai',
                vAxis: {
                    title: 'Pontszámok',
                    gridlines: {
                        count: 5
                    },
                    viewWindow:{
                        max: 700,
                        min: 0
                    }
                },
                hAxis: {
                    title: 'Nemzetek',
                    textStyle: {
                        fontSize: 10
                    }
                },
                animation: {
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                colors: ['#5bbdff'],
                bar: { groupWidth: '90%' },
                focusTarget: 'category',
                isStacked: true,
                tooltip: {isHtml: true}
            };

            var redrawPoints = function() {
                chart = new google.visualization.ColumnChart(document.getElementById('chart_point_team'));
                chart.draw(dataTable, options);
            };

            redrawPoints();
            $(window).resize(function() {
                redrawPoints();
            });
        }
    });
}

function pointTooltip(computerName, name, point, i) {
    i++;

    return '<div style="width: 225px; padding: 5px;">' +
        '<span class="block team-flag-left ' + computerName + '-flag-left" style="text-align: center; margin: 0 auto; font-size: 20px;">' + name + '</span><hr style="margin: 5px" />'  +
        '<span class="medal medal-sum"><b>Helyezés:</b> ' + i + '</span><hr style="margin: 5px" />' +
        '<span class="medal medal-sum"><b>Pontszám:</b> ' + point + '</span>' +
        '</div>';
}
