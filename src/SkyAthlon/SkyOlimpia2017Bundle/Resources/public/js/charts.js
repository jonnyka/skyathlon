google.charts.load('current', {'packages': ['corechart']});
if (document.getElementById('chart_medal_sum')) {
    google.charts.setOnLoadCallback(drawMedalSum);
}
if (document.getElementById('chart_medal_male')) {
    google.charts.setOnLoadCallback(drawMedalMale);
}
if (document.getElementById('chart_medal_female')) {
    google.charts.setOnLoadCallback(drawMedalFemale);
}

if (document.getElementById('chart_point_sum')) {
    google.charts.setOnLoadCallback(drawPointSum);
}
if (document.getElementById('chart_point_male')) {
    google.charts.setOnLoadCallback(drawPointMale);
}
if (document.getElementById('chart_point_female')) {
    google.charts.setOnLoadCallback(drawPointFemale);
}

var medals = {
    sum: {
        title: 'Összesített érmek',
        sex: 2,
        div: 'chart_medal_sum',
        width: '80%',
        font: 7
    },
    male: {
        title: 'Férfiak érmei',
        sex: 1,
        div: 'chart_medal_male',
        width: '90%',
        font: 9
    },
    female: {
        title: 'Nők érmei',
        sex: 0,
        div: 'chart_medal_female',
        width: '90%',
        font: 8
    }
};

function drawMedalSum() {
    drawMedals(medals, 'sum');
}

function drawMedalMale() {
    drawMedals(medals, 'male');
}

function drawMedalFemale() {
    drawMedals(medals, 'female');
}

function drawMedals(medals, sex) {
    $.ajax({
        type: "GET",
        url: Routing.generate('skyolimpia2017_api_medals'),
        success: function (d) {
            var redrawMedals = function() {
                var medal = medals[sex],
                    data,
                    i,
                    f = 0,
                    chart,
                    options,
                    columns = [],
                    dataArray = [],
                    dataTable = new google.visualization.DataTable({
                        cols: [
                            {id: 'player', label: 'Versenyző', type: 'string'},
                            {id: 'tooltip', type: 'string', role: 'tooltip', p: {'html': true}},
                            {id: 'gold', label: 'Arany', type: 'number', color: '#FFD700'},
                            {id: 'silver', label: 'Ezüst', type: 'number', color: '#C0C0C0'},
                            {id: 'bronze', label: 'Bronz', type: 'number', color: '#8C7853'},
                            {id: 'team', type: 'string', role: 'annotationText'}
                        ]
                    });

                for (i in d) {
                    data = d[i];
                    if (data.sex == medal.sex || medal.sex == 2) {
                        dataArray.push([
                            data.number,
                            medalTooltip(data.team, data.number, data.gold, data.silver, data.bronze, f, true, true, true),
                            data.gold,
                            data.silver,
                            data.bronze,
                            data.team
                        ]);

                        f++;
                    }
                }

                dataTable.addRows(dataArray);

                options = {
                    title: medal.title,
                    vAxis: {
                        title: 'Érmek száma',
                        gridlines: {
                            count: 5
                        },
                        viewWindow: {
                            max: 32,
                            min: 0
                        }
                    },
                    hAxis: {
                        title: 'Versenyzők',
                        textStyle: {
                            fontSize: medal.font
                        }
                    },
                    animation: {
                        startup: true,
                        duration: 1000,
                        easing: 'out'
                    },
                    colors: ['#FFD700', '#C0C0C0', '#8C7853'],
                    bar: {groupWidth: medal.width},
                    focusTarget: 'category',
                    isStacked: true,
                    tooltip: {isHtml: true}
                };

                chart = new google.visualization.ColumnChart(document.getElementById(medal.div));
                chart.draw(dataTable, options);

                for (var j = 0; j < dataTable.getNumberOfColumns(); j++) {
                    columns.push(j);
                }

                google.visualization.events.addListener(chart, 'select', function () {
                    var sel = chart.getSelection(),
                        view,
                        column,
                        c,
                        col,
                        row,
                        sorting = [],
                        goldColor = '#FFD700',
                        silverColor = '#C0C0C0',
                        bronzeColor = '#8C7853',
                        naturalColor = '#fff',
                        gold = true,
                        silver = true,
                        bronze = true;

                    if (sel.length > 0) {
                        col = sel[0].column;
                        row = sel[0].row;

                        if (row == null) {
                            if (columns[col] == col) {
                                columns[col] = {
                                    label: dataTable.getColumnLabel(col),
                                    type: dataTable.getColumnType(col),
                                    calc: function () {
                                        return null;
                                    }
                                };
                            }
                            else {
                                columns[col] = col;
                            }

                            // col var - arany: 2, ezust: 3, bronz: 4
                            for (c in columns) {
                                column = columns[c];

                                if (typeof column === 'object') {
                                    if (c == 2) {
                                        gold = false;
                                        goldColor = naturalColor;
                                    }
                                    else if (c == 3) {
                                        silver = false;
                                        silverColor = naturalColor;
                                    }
                                    else if (c == 4) {
                                        bronze = false;
                                        bronzeColor = naturalColor;
                                    }
                                }
                            }

                            if (gold === true && silver === true && bronze === true ||
                                gold === true && silver === true && bronze === false ||
                                gold === true && silver === false && bronze === false ||
                                gold === false && silver === false && bronze === false) {
                                sorting = [{column: 2, desc: true}, {column: 3, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === true && silver === false && bronze === true) {
                                sorting = [{column: 2, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === false && silver === true && bronze === true ||
                                gold === false && silver === true && bronze === false) {
                                sorting = [{column: 3, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === false && silver === false && bronze === true) {
                                sorting = [{column: 4, desc: true}];
                            }

                            options.colors = [goldColor, silverColor, bronzeColor];
                            dataTable.sort(sorting);

                            for (var j = 0; j < dataTable.getNumberOfRows(); j++) {
                                dataTable.setValue(j, 1, medalTooltip(dataTable.getValue(j, 5), dataTable.getValue(j, 0), dataTable.getValue(j, 2), dataTable.getValue(j, 3), dataTable.getValue(j, 4), j, gold, silver, bronze));
                            }

                            view = new google.visualization.DataView(dataTable);
                            view.setColumns(columns);

                            chart.draw(view, options);
                        }
                    }
                });
            };

            redrawMedals();

            $(window).resize(redrawMedals);
        }
    });
}

function medalTooltip(team, number, totalGold, totalSilver, totalBronze, i, gold, silver, bronze) {
    var sum = 0,
        ret,
        goldStyle = '',
        silverStyle = '',
        bronzeStyle = '',
        sumStyle = '';

    i++;

    if (gold) {
        sum += totalGold;
    }
    else {
        goldStyle = ' hidden';
    }

    if (silver) {
        sum += totalSilver;
    }
    else {
        silverStyle = ' hidden';
    }

    if (bronze) {
        sum += totalBronze;
    }
    else {
        bronzeStyle = ' hidden';
    }
    if (goldStyle && silverStyle && bronzeStyle) {
        sumStyle = ' hidden';
    }

    ret = '<div style="width: 105px; padding: 5px;">' +
        '<span class="block team-flag-left ' + team + '-flag-left" style="text-align: center; margin: 0 auto; font-size: 20px;">' + number + '</span><hr style="margin: 5px" />' +
        '<span class="medal medal-sum"><b>Helyezés:</b> ' + i + '</span><hr style="margin: 5px" />' +
        '<span class="block medal medal-gold' + goldStyle + '">' + totalGold + '</span>' +
        '<span class="block medal medal-silver' + silverStyle + '">' + totalSilver + '</span>' +
        '<span class="block medal medal-bronze' + bronzeStyle + '">' + totalBronze + '</span><hr class="' + sumStyle + '" style="margin: 5px" />' +
        '<span class="medal medal-sum' + sumStyle + '"><b>Összesen:</b> ' + sum + '</span>' +
        '</div>';

    return ret;
}

var points = {
    sum: {
        title: 'Összesített pontszám',
        sex: 2,
        div: 'chart_point_sum',
        width: '80%',
        font: 7
    },
    male: {
        title: 'Férfiak pontszámai',
        sex: 1,
        div: 'chart_point_male',
        width: '90%',
        font: 9
    },
    female: {
        title: 'Nők pontszámai',
        sex: 0,
        div: 'chart_point_female',
        width: '90%',
        font: 8
    }
};

function drawPointSum() {
    drawPoints(points, 'sum');
}

function drawPointMale() {
    drawPoints(points, 'male');
}

function drawPointFemale() {
    drawPoints(points, 'female');
}

function drawPoints(points, sex) {
    $.ajax({
        type: "GET",
        url: Routing.generate('skyolimpia2017_api_points'),
        success: function (d) {
            var point = points[sex],
                data,
                i,
                f = 0,
                chart,
                options,
                dataArray = [],
                dataTable = new google.visualization.DataTable({
                    cols: [
                        {id: 'player', label: 'Versenyző', type: 'string'},
                        {id: 'tooltip', type: 'string', role: 'tooltip', p: {'html': true}},
                        {id: 'point', label: 'Pontszám', type: 'number', color: '#5bbdff'},
                        {id: 'team', type: 'string', role: 'annotationText'}
                    ]
                });

            for (i in d) {
                data = d[i];
                if (data.sex == point.sex || point.sex == 2) {
                    dataArray.push([
                        data.number,
                        pointTooltip(data.team, data.number, data.point, f),
                        data.point,
                        data.team
                    ]);

                    f++;
                }
            }

            dataTable.addRows(dataArray);

            options = {
                title : point.title,
                vAxis: {
                    title: 'Pontszámok',
                    gridlines: {
                        count: 4
                    },
                    viewWindow:{
                        max: 300,
                        min: 0
                    }
                },
                hAxis: {
                    title: 'Versenyzők',
                    textStyle: {
                        fontSize: point.font
                    }
                },
                animation: {
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                colors: ['#5bbdff'],
                bar: { groupWidth: point.width },
                focusTarget: 'category',
                isStacked: true,
                tooltip: {isHtml: true}
            };

            var redrawPoints = function() {
                chart = new google.visualization.ColumnChart(document.getElementById(point.div));
                chart.draw(dataTable, options);
            };

            redrawPoints();
            $(window).resize(function() {
                redrawPoints();
            });
        }
    });
}

function pointTooltip(team, number, point, i) {
    i++;

    return '<div style="width: 120px; padding: 5px;">' +
        '<span class="block team-flag-left ' + team + '-flag-left" style="text-align: center; margin: 0 auto; font-size: 20px;">' + number + '</span><hr style="margin: 5px" />'  +
        '<span class="medal medal-sum"><b>Helyezés:</b> ' + i + '</span><hr style="margin: 5px" />' +
        '<span class="medal medal-sum"><b>Pontszám:</b> ' + point + '</span>' +
        '</div>';
}

var ds = {
    sum: {
        title: 'Versenyzők be nem fejezett versenyei',
        sex: 2,
        div: 'chart_ds_sum',
        width: '80%',
        font: 7
    },
    male: {
        title: 'Férfiak be nem fejezett versenyei',
        sex: 1,
        div: 'chart_ds_male',
        width: '90%',
        font: 9
    },
    female: {
        title: 'Nők be nem fejezett versenyei',
        sex: 0,
        div: 'chart_ds_female',
        width: '90%',
        font: 8
    }
};

function drawDsSum() {
    drawDs(ds, 'sum');
}

function drawDsMale() {
    drawDs(ds, 'male');
}

function drawDsFemale() {
    drawDs(medals, 'female');
}

function drawDs(ds, sex) {
    $.ajax({
        type: "GET",
        url: Routing.generate('skyolimpia2017_api_medals'),
        success: function (d) {
            var redrawDs = function() {
                var medal = ds[sex],
                    data,
                    i,
                    f = 0,
                    chart,
                    options,
                    columns = [],
                    dataArray = [],
                    dataTable = new google.visualization.DataTable({
                        cols: [
                            {id: 'player', label: 'Versenyző', type: 'string'},
                            {id: 'tooltip', type: 'string', role: 'tooltip', p: {'html': true}},
                            {id: 'dq', label: 'DQ', type: 'number', color: '#d9534f'},
                            {id: 'dns', label: 'DNS', type: 'number', color: '#4FD968'},
                            {id: 'dnf', label: 'DNF', type: 'number', color: '#4F8BD9'},
                            {id: 'team', type: 'string', role: 'annotationText'}
                        ]
                    });

                for (i in d) {
                    data = d[i];
                    if (data.sex == medal.sex || medal.sex == 2) {
                        dataArray.push([
                            data.number,
                            dsTooltip(data.team, data.number, data.dq, data.dns, data.dnf, f, true, true, true),
                            data.dq,
                            data.dns,
                            data.dnf,
                            data.team
                        ]);

                        f++;
                    }
                }

                dataTable.addRows(dataArray);

                options = {
                    title: medal.title,
                    vAxis: {
                        title: 'Be nem fejezett versenyek száma',
                        gridlines: {
                            count: 5
                        },
                        viewWindow: {
                            max: 32,
                            min: 0
                        }
                    },
                    hAxis: {
                        title: 'Versenyzők',
                        textStyle: {
                            fontSize: medal.font
                        }
                    },
                    animation: {
                        startup: true,
                        duration: 1000,
                        easing: 'out'
                    },
                    colors: ['#d9534f', '#4FD968', '#4F8BD9'],
                    bar: {groupWidth: medal.width},
                    focusTarget: 'category',
                    isStacked: true,
                    tooltip: {isHtml: true}
                };

                chart = new google.visualization.ColumnChart(document.getElementById(medal.div));
                chart.draw(dataTable, options);

                for (var j = 0; j < dataTable.getNumberOfColumns(); j++) {
                    columns.push(j);
                }

                google.visualization.events.addListener(chart, 'select', function () {
                    var sel = chart.getSelection(),
                        view,
                        column,
                        c,
                        col,
                        row,
                        sorting = [],
                        goldColor = '#d9534f',
                        silverColor = '#4FD968',
                        bronzeColor = '#4F8BD9',
                        naturalColor = '#fff',
                        gold = true,
                        silver = true,
                        bronze = true;

                    if (sel.length > 0) {
                        col = sel[0].column;
                        row = sel[0].row;

                        if (row == null) {
                            if (columns[col] == col) {
                                columns[col] = {
                                    label: dataTable.getColumnLabel(col),
                                    type: dataTable.getColumnType(col),
                                    calc: function () {
                                        return null;
                                    }
                                };
                            }
                            else {
                                columns[col] = col;
                            }

                            // col var - arany: 2, ezust: 3, bronz: 4
                            for (c in columns) {
                                column = columns[c];

                                if (typeof column === 'object') {
                                    if (c == 2) {
                                        gold = false;
                                        goldColor = naturalColor;
                                    }
                                    else if (c == 3) {
                                        silver = false;
                                        silverColor = naturalColor;
                                    }
                                    else if (c == 4) {
                                        bronze = false;
                                        bronzeColor = naturalColor;
                                    }
                                }
                            }

                            if (gold === true && silver === true && bronze === true ||
                                gold === true && silver === true && bronze === false ||
                                gold === true && silver === false && bronze === false ||
                                gold === false && silver === false && bronze === false) {
                                sorting = [{column: 2, desc: true}, {column: 3, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === true && silver === false && bronze === true) {
                                sorting = [{column: 2, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === false && silver === true && bronze === true ||
                                gold === false && silver === true && bronze === false) {
                                sorting = [{column: 3, desc: true}, {column: 4, desc: true}];
                            }
                            else if (gold === false && silver === false && bronze === true) {
                                sorting = [{column: 4, desc: true}];
                            }

                            options.colors = [goldColor, silverColor, bronzeColor];
                            dataTable.sort(sorting);

                            for (var j = 0; j < dataTable.getNumberOfRows(); j++) {
                                dataTable.setValue(j, 1, dsTooltip(dataTable.getValue(j, 5), dataTable.getValue(j, 0), dataTable.getValue(j, 2), dataTable.getValue(j, 3), dataTable.getValue(j, 4), j, gold, silver, bronze));
                            }

                            view = new google.visualization.DataView(dataTable);
                            view.setColumns(columns);

                            chart.draw(view, options);
                        }
                    }
                });
            };

            redrawDs();

            $(window).resize(redrawDs);
        }
    });
}

function dsTooltip(team, number, totalGold, totalSilver, totalBronze, i, gold, silver, bronze) {
    var sum = 0,
        ret,
        goldStyle = '',
        silverStyle = '',
        bronzeStyle = '',
        sumStyle = '';

    i++;

    if (gold) {
        sum += totalGold;
    }
    else {
        goldStyle = ' hidden';
    }

    if (silver) {
        sum += totalSilver;
    }
    else {
        silverStyle = ' hidden';
    }

    if (bronze) {
        sum += totalBronze;
    }
    else {
        bronzeStyle = ' hidden';
    }
    if (goldStyle && silverStyle && bronzeStyle) {
        sumStyle = ' hidden';
    }

    ret = '<div style="width: 105px; padding: 5px;">' +
        '<span class="block team-flag-left ' + team + '-flag-left" style="text-align: center; margin: 0 auto; font-size: 20px;">' + number + '</span><hr style="margin: 5px" />' +
        '<span class="medal medal-sum"><b>Helyezés:</b> ' + i + '</span><hr style="margin: 5px" />' +
        '<span class="block' + goldStyle + '">DQ: ' + totalGold + '</span>' +
        '<span class="block' + silverStyle + '">DNS: ' + totalSilver + '</span>' +
        '<span class="block' + bronzeStyle + '">DNF: ' + totalBronze + '</span><hr class="' + sumStyle + '" style="margin: 5px" />' +
        '<span class="medal medal-sum' + sumStyle + '"><b>Összesen:</b> ' + sum + '</span>' +
        '</div>';

    return ret;
}
