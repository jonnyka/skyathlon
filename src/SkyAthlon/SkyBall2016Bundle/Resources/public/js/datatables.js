$('#edit-player-results').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10],
            'orderSequence': ["desc", "asc"],
            'orderDataType': 'sortbyInput',
            'sType': 'numeric'
        }
    ]
}).find('input').bind('keyup mouseup', function() {
    var player = $(this).data('player'),
        sniper = $('*[data-player="' + player + '"][data-type="S"]'),
        sniperVal = sniper.val() || 0,
        action = $('*[data-player="' + player + '"][data-type="A"]'),
        actionVal = action.val() || 0,
        goal = $('*[data-player="' + player + '"][data-type="G"]'),
        goalSum = ((+sniperVal * 2) + +actionVal),
        assist = $('*[data-player="' + player + '"][data-type="GA"]'),
        assistVal = assist.val() || 0,
        canadian = $('*[data-player="' + player + '"][data-type="P"]'),
        canadianSum = (+goalSum + +assistVal)
        ;

    goal.val(goalSum);
    canadian.val(canadianSum);
});

$('#by-players-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10,11],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [11, 'desc']
    ]
});

$('.round-round-table, .by-players-table:not("#by-players-table")').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [1, 'desc']
    ]
});

$('.refs-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': [0],
            'orderable': false
        },

        {
            'targets': [1,2,3],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [1, 'desc']
    ]
});

$('#by-players-ranks-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10],
            'orderSequence': ["asc", "desc"]
        },
        {
            'targets': 11,
            'orderDataType': 'sortbyshow',
            'sType': 'numeric'
        }
    ],
    'order': [
        [11, 'asc']
    ]
});

$('#team-table').dataTable({
    'paging':   false,
    'info':     false,
    'bFilter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columns': [
        {'name': 'first', 'orderable': true},
        {'name': 'flag', 'orderable': false},
        {'name': 'player1', 'orderable': false},
        {'name': 'player2', 'orderable': false},
        {'name': 'player3', 'orderable': false},
        {'name': 'point', 'orderable': true, 'orderDataType': 'point', type: 'numeric'}
    ],
    'order': [
        [5, 'asc'],
        [0, 'asc']
    ]
});