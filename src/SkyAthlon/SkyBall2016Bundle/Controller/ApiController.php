<?php

namespace SkyAthlon\SkyBall2016Bundle\Controller;

use AppBundle\Repository\SettingsRepository;
use Doctrine\ORM\EntityManager;
use SkyAthlon\SkyBall2016Bundle\Entity\Team;
use SkyAthlon\SkyBall2016Bundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

/**
 * Team controller.
 *
 * @Route("/skyball-2016/api/")
 */
class ApiController extends Controller
{
    /**
     * Gets all Teams.
     *
     * @Route("teams", name="skyball2016_api_teams")
     * @Method("GET")
     */
    public function teamsAction()
    {
        /** @var TeamRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('SkyAthlonSkyBall2016Bundle:Team');
        $teams = $repo->getTeams();

        usort($teams, function($a, $b) {
            return $b['point'] - $a['point'];
        });

        return new JsonResponse($teams);
    }

    /**
     * Gets all Kerchief results.
     *
     * @Route("kerchiefs", name="skyball2016_api_kerchiefs")
     * @Method("GET")
     */
    public function kerchiefsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2016Bundle:KerchiefType')->getKerchiefTypes();

        $playerService = $this->get('skyball2016_player');
        $kerchiefs = $playerService->getAllGlobalKerchiefsJson();

        $players = $em->getRepository('SkyAthlonSkyBall2016Bundle:Player')->getPlayers();

        $ret = array(
            'kerchiefs' => $kerchiefs,
            'kcTypes'   => $kcTypes,
            'players'   => $players,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets all Kerchief results.
     *
     * @Route("kerchief-types", name="skyball2016_api_kerchief_types")
     * @Method("GET")
     */
    public function kerchiefTypesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2016Bundle:KerchiefType')->getKerchiefTypes();

        return new JsonResponse($kcTypes);
    }

    /**
     * Gets all current data.
     *
     * @Route("current-data", name="skyball2016_api_current_data")
     * @Method("GET")
     */
    public function currentDataAction()
    {
        $em = $this->getDoctrine()->getManager();
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2016Bundle:KerchiefType')->getKerchiefTypes();

        $playerService = $this->get('skyball2016_player');
        $kerchiefs = $playerService->getAllGlobalKerchiefsJson();

        $teams = $em->getRepository('SkyAthlonSkyBall2016Bundle:Team')->getTeams();

        usort($teams, function($a, $b) {
            return $b['point'] - $a['point'];
        });

        foreach ($teams as &$team) {
            $team['logo'] = 'http://' . $this->getParameter('skyathlon_url') . '/bundles/skyathlonskyball2016/images/logos/' . $team['computerName'] . '.png';
        }

        $ret = array(
            'kerchiefs' => $kerchiefs,
            'kcTypes'   => $kcTypes,
            'teams'     => $teams,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets current round data.
     *
     * @Route("current-round", name="skyball2016_api_current_round")
     * @Method("GET")
     */
    public function currentRoundAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');
        $currentGameId = 0;
        $currentGameObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2016_currentGame',
        ));
        if ($currentGameObject) {
            $currentGameId = $currentGameObject->getValue();
        }

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2016_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }

        $rounds = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->getRound($currentRoundNumber);
        $round = [];
        $groupTitles = $games = 0;

        if ($rounds) {
            $translator = $this->get('translator');
            $round = $rounds[0];

            foreach ($round['games'] as &$game) {
                $games++;
                $gt = &$game['gameType'];
                if ($gt['groupTitle']) {
                    $groupTitles++;
                    $gt['groupTitleTrans'] = $translator->trans($gt['groupTitle']);
                }
            }
        }

        $ret = array(
            'round'       => $round,
            'currentGame' => $currentGameId,
            'groupTitles' => $groupTitles,
            'games'       => $games,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets round Teams.
     *
     * @Route("round-teams", name="skyball2016_api_round_teams")
     * @Method("GET")
     */
    public function roundTeamsAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2016_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }
        $currentRound = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array(
            'number' => $currentRoundNumber,
        ));

        $teams = array();
        if ($currentRound) {
            $teamService = $this->get('skyball2016_team');
            $teamObjects = $teamService->getRealRoundResults($currentRound);
            $i = 0;
            $points = array(
                array(
                    'point' => '12',
                    'rank'  => '1',
                ),
                array(
                    'point' => '9',
                    'rank'  => '2',
                ),
                array(
                    'point' => '7',
                    'rank'  => '3',
                ),
                array(
                    'point' => '5',
                    'rank'  => '4',
                ),
                array(
                    'point' => '4',
                    'rank'  => '5',
                ),
                array(
                    'point' => '3',
                    'rank'  => '6',
                ),
                array(
                    'point' => '2',
                    'rank'  => '7',
                ),
                array(
                    'point' => '1',
                    'rank'  => '8',
                ),
                array(
                    'point' => '0',
                    'rank'  => '9',
                ),
                array(
                    'point' => '0',
                    'rank'  => '10',
                ),
            );

            /** @var Team $teamObject */
            foreach ($teamObjects as $teamObject) {
                $teams[$i] = array(
                    'computerName' => $teamObject ? $teamObject->getComputerName() : 'noteam',
                    'name'         => $teamObject ? $teamObject->getName() : '?',
                    'rank'         => $points[$i]['rank'],
                    'point'        => $points[$i]['point'],
                );

                $i++;
            }
        }

        return new JsonResponse($teams);
    }

    /**
     * Gets round Kerchief results.
     *
     * @Route("round-kerchiefs", name="skyball2016_api_round_kerchiefs")
     * @Method("GET")
     */
    public function roundKerchiefsAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2016_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }
        $currentRound = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array(
            'number' => $currentRoundNumber,
        ));
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2016Bundle:KerchiefType')->getKerchiefTypes();

        $playerService = $this->get('skyball2016_player');
        $kerchiefs = $playerService->getRoundKerchiefsJson($currentRound);

        $players = $em->getRepository('SkyAthlonSkyBall2016Bundle:Player')->getPlayers();

        $ret = array(
            'kerchiefs' => $kerchiefs,
            'kcTypes'   => $kcTypes,
            'players'   => $players,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets current round.
     *
     * @Route("round", name="skyball2016_api_round")
     * @Method("GET")
     */
    public function roundAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2016_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }
        $currentRound = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->getPureRound($currentRoundNumber);

        if ($currentRound) {
            $currentRound = $currentRound[0];
        }

        $ret = array(
            'round' => $currentRound,
        );

        return new JsonResponse($ret);
    }
}
