<?php

namespace SkyAthlon\SkyBall2016Bundle\Controller;

use SkyAthlon\SkyBall2016Bundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Team controller.
 *
 * @Route("/skyball-2016/")
 */
class TeamController extends Controller
{
    /**
     * Lists all Team entities.
     *
     * @Route("csapatok/", name="skyball2016_team")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            /** @var TeamRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('SkyAthlonSkyBall2016Bundle:Team');
            $teams = $repo->getTeams();

            return array(
                'teams' => $teams,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Lists all Team entities in json.
     *
     * @Route("api/teams/", name="skyball2016_team_api_all", options={"expose" = true})
     * @Method("GET")
     */
    public function getAllAction()
    {
        if ($this->getUser()) {
            /** @var TeamRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('SkyAthlonSkyBall2016Bundle:Team');
            $teams = $repo->getTeams();

            return new JsonResponse($teams);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Finds and displays a Team entity.
     *
     * @Route("csapat/{id}/", name="skyball2016_team_show")
     * @Method("GET")
     * @Template
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        if ($this->getUser()) {
            /** @var TeamRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('SkyAthlonSkyBall2016Bundle:Team');
            $team = $repo->getTeamByName($id);
            $team['games'] = array_merge($team['gamesA'], $team['gamesB']);

            uasort($team['games'], function ($i, $j) {
                $a = $i['round']['number'];
                $b = $j['round']['number'];
                $c = $i['gameType']['weight'];
                $d = $j['gameType']['weight'];

                if ($a === $b) {
                    if ($c === $d) {
                        return 0;
                    }
                    elseif ($c > $d) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                }
                elseif ($a > $b) {
                    return 1;
                }
                else {
                    return -1;
                }
            });

            return array(
                'team' => $team,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Finds and displays a Team entity.
     *
     * @Route("api/teams/{id}/", name="skyball2016_team_api_one")
     * @Method("GET")
     * @Template
     * @param $id
     * @return array|JsonResponse
     */
    public function getOneAction($id)
    {
        if ($this->getUser()) {
        /** @var TeamRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('SkyAthlonSkyBall2016Bundle:Team');
        $team = $repo->getTeam($id);

        return new JsonResponse($team);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Displays a form to edit the team stats.
     *
     * @Route("tabella-szerkesztes/", name="skyball2016_team_editranks")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editRanksAction(Request $request)
    {
        if ($this->getUser()) {
            if ($this->getUser()->isSu()) {
                $em = $this->getDoctrine()->getManager();
                $teams = $em->getRepository('SkyAthlonSkyBall2016Bundle:Team')->findBy(array(), array('rank' => 'ASC'));

                if ($request->getMethod() === 'POST') {
                    $values = $request->request->all();


                    foreach ($values as $rankString => $tid) {
                        $rank = str_replace('round-team-', '', $rankString);
                        $team = $em->getRepository('SkyAthlonSkyBall2016Bundle:Team')->find($tid);
                        $team->setRank($rank);
                        $em->persist($team);
                    }

                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2016.teamstats.edited');

                    return $this->redirectToRoute('skyball2016_team');
                }

                return array(
                    'teams' => $teams,
                );
            }
            else {
                return $this->redirectToRoute('skyball2016_team');
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
