<?php

namespace SkyAthlon\SkyBall2016Bundle\Controller\Round;

use AppBundle\Entity\Settings;
use SkyAthlon\SkyBall2016Bundle\Entity\Game;
use SkyAthlon\SkyBall2016Bundle\Entity\PlayerResult;
use SkyAthlon\SkyBall2016Bundle\Entity\Round;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/skyball-2016/fordulo")
 */
class RoundGameController extends Controller
{

    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{roundid}/merkozes/uj/", name="skyball2016_game_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function newGameAction(Request $request, $roundid)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed() && $round->getPlayoffs()) || $this->getUser()->isSu()) {
                $game = new Game();

                $games = $em->getRepository('SkyAthlonSkyBall2016Bundle:Game')->findBy(array(
                    'round' => $round,
                ));
                $arr = array(
                    'attr' => $games,
                );

                $form = $this->createForm('SkyAthlon\SkyBall2016Bundle\Form\GameType', $game, $arr);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $gameType = $form['gameType']->getData();

                    $game->setWeight($gameType->getWeight());
                    $game->setRound($round);

                    $em->persist($game);

                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2016.game.created');

                    if ($game->getTeamA() && $game->getTeamB()) {
                        $playerService = $this->get('skyball2016_player');
                        $playerService->getKerchiefs($game);
                    }

                    if (!$game->getPointA() && !$game->getPointB() && !$game->isPreGame() && $game->getTeamA() && $game->getTeamB()) {
                        return $this->redirectToRoute('skyball2016_game_edit_player_stats', array('roundid' => $roundid, 'game' => $game->getId()));
                    }

                    $anchor = '#' . $game->getGameType()->getComputerName();
                    return $this->redirect($this->generateUrl('skyball2016_round', array('round' => $roundid)) . $anchor);
                }

                return array(
                    'game' => $game,
                    'roundid' => $roundid,
                    'form' => $form->createView(),
                );
            }
            else {
                return $this->redirectToRoute('skyball2016_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{roundid}/merkozes/{game}/szerkesztes/", name="skyball2016_game_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editGameAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $oldTeamA = $game->getTeamA();
                $oldTeamB = $game->getTeamB();

                $games = $em->getRepository('SkyAthlonSkyBall2016Bundle:Game')->findBy(array(
                    'round' => $round,
                ));
                $arr = array(
                    'attr' => $games,
                );

                $editForm = $this->createForm('SkyAthlon\SkyBall2016Bundle\Form\GameType', $game, $arr);
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $weight = $round->getReversed() ? $game->getGameType()->getReversedWeight() : $game->getGameType()->getWeight();
                    $game->setWeight($weight);
                    $em->persist($game);

                    if (!($game->getTeamA() === $oldTeamA && $game->getTeamB() === $oldTeamB)) {
                        $playerResults = $em->getRepository('SkyAthlonSkyBall2016Bundle:PlayerResult')->findBy(array(
                            'game' => $game,
                        ));

                        foreach ($playerResults as $playerResult) {
                            $em->remove($playerResult);
                        }
                    }

                    /** @var Settings $currentGameObject */
                    $currentGameObject = $this->getDoctrine()->getManager()->getRepository('AppBundle:Settings')->findOneBy(array(
                        'name' => 'skyball2016_currentGame',
                    ));
                    $isCurrent = $editForm->get('current')->getData();
                    if ($isCurrent) {
                        if (!$currentGameObject) {
                            $currentGameObject = new Settings();
                            $currentGameObject->setName('skyball2016_currentGame');
                        }

                        $currentGameObject->setValue($game->getId());
                        $em->persist($currentGameObject);
                    }
                    elseif ($currentGameObject && $currentGameObject->getValue() == $game->getId()) {
                        $currentGameObject->setValue(0);
                        $em->persist($currentGameObject);
                    }

                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2016.game.edited');

                    if ($game->getTeamA() && $game->getTeamB()) {
                        $playerService = $this->get('skyball2016_player');
                        $playerService->getKerchiefs($game);
                    }

                    if (!$game->getPointA() && !$game->getPointB() && !$game->isPreGame() && $game->getTeamA() && $game->getTeamB()) {
                        return $this->redirectToRoute('skyball2016_game_edit_player_stats', array('roundid' => $roundid, 'game' => $game->getId()));
                    }

                    $anchor = '#' . $game->getGameType()->getComputerName();
                    return $this->redirect($this->generateUrl('skyball2016_round', array('round' => $roundid)) . $anchor);
                }

                $form = $this->createDeleteGameForm($roundid, $game);

                return array(
                    'game' => $game,
                    'roundid' => $roundid,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $form->createView(),
                );
            }
            else {
                return $this->redirectToRoute('skyball2016_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit the players stats for a game.
     *
     * @Route("/{roundid}/merkozes/{game}/jatekos-statisztika/szerkesztes/", name="skyball2016_game_edit_player_stats")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editPlayerStatsAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if ((($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) && !$game->isPreGame() && $game->getTeamA() && $game->getTeamB()) {
                $values = array();

                $current = false;
                $currentGameObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                    'name' => 'skyball2016_currentGame'
                ));

                if ($currentGameObject && $currentGameObject->getValue() == $game->getId()) {
                    $current = true;
                }

                if ($request->getMethod() === 'POST') {
                    $values = $request->request->all();
                    $recalc = false;
                    $hasCurrent = false;

                    foreach ($values as $id => $value) {
                        if ($id === 'recalc') {
                            $recalc = true;
                        }
                        else if ($id === 'description') {
                            if (!$value) {
                                $value = '';
                            }
                            $game->setDescription($value);
                            $em->persist($game);
                        }
                        else if ($id === 'current') {
                            $hasCurrent = true;
                            $isCurrent = $value;
                            if ($isCurrent) {
                                if (!$currentGameObject) {
                                    $currentGameObject = new Settings();
                                    $currentGameObject->setName('skyball2016_currentGame');
                                }

                                $currentGameObject->setValue($game->getId());
                                $em->persist($currentGameObject);
                            }
                        }
                        else {
                            /** @var PlayerResult $playerResult */
                            $playerResult = $em->getRepository('SkyAthlonSkyBall2016Bundle:PlayerResult')->find($id);
                            $playerResult->setValue($value);
                            $playerResult->setRound($round);
                            $em->persist($playerResult);
                        }
                    }

                    if (!$hasCurrent) {
                        if ($currentGameObject && $currentGameObject->getValue() == $game->getId()) {
                            $currentGameObject->setValue(0);
                            $em->persist($currentGameObject);
                        }
                    }

                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2016.playerstats.edited');

                    if ($recalc) {
                        $cacheService = $this->get('skyball2016_cache');
                        $cacheService->setGamePointsFromPlayerResults($game);
                    }

                    $anchor = '#' . $game->getGameType()->getComputerName();
                    return $this->redirect($this->generateUrl('skyball2016_round', array('round' => $roundid)) . $anchor);
                }

                $playerService = $this->get('skyball2016_player');
                $playerResults = $playerService->getKerchiefs($game);
                $kerchiefTypes = $em->getRepository('SkyAthlonSkyBall2016Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

                return array(
                    'game'    => $game,
                    'results' => $playerResults,
                    'roundid' => $roundid,
                    'kcTypes' => $kerchiefTypes,
                    'values'  => $values,
                    'current' => $current,
                );
            }
            else {
                return $this->redirectToRoute('skyball2016_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Deletes a Game entity.
     *
     * @Route("/{roundid}/merkozes/{id}/", name="skyball2016_game_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteGameAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array('number' => $roundid));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $form = $this->createDeleteGameForm($roundid, $game);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                    $playerResults = $em->getRepository('SkyAthlonSkyBall2016Bundle:PlayerResult')->findBy(array(
                        'game' => $game,
                    ));
                    if ($playerResults) {
                        foreach ($playerResults as $playerResult) {
                            $em->remove($playerResult);
                        }
                    }

                    $em->remove($game);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2016.game.deleted', 'warning');
                }

                return $this->redirectToRoute('skyball2016_round', array('round' => $roundid));
            }
            else {
                return $this->redirectToRoute('skyball2016_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a form to delete a Game entity.
     *
     * @param $roundid
     * @param Game $game The Game entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteGameForm($roundid, Game $game)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('skyball2016_game_delete', array('id' => $game->getId(), 'roundid' => $roundid)))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    /**
     * Get kerchiefs for a game.
     *
     * @Route("/merkozes/{game}/kendo/", name="skyball2016_game_kerchiefs", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function getKerchiefs(Game $game) {
        $playerService = $this->get('skyball2016_player');
        $ret = $playerService->getKerchiefs($game);

        return new JsonResponse($ret);
    }
}
