<?php

namespace SkyAthlon\SkyBall2016Bundle\Controller\Round;

use SkyAthlon\SkyBall2016Bundle\Entity\Round;
use SkyAthlon\SkyBall2016Bundle\Entity\RoundResultType;
use SkyAthlon\SkyBall2016Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/skyball-2016/fordulo")
 */
class RoundTeamController extends Controller
{
    /**
     * Show current round player results.
     *
     * @Route("/{round}/csapat-statisztika/", name="skyball2016_round_teams", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexTeamsAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            /** @var Round $r */
            $r = $repo->findOneBy(array('number' => $round));

            if (!$r->getPlayoffs()) {
                $teamService = $this->get('skyball2016_team');
                $teams = $teamService->getRealRoundResults($r);

                $paginator = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $rounds,
                    $round,
                    1,
                    array(
                        'pageParameterName' => 'round',
                        'wrap-queries' => true,
                    )
                );

                $rrTypes = $em->getRepository('SkyAthlonSkyBall2016Bundle:RoundResultType')->findAll();
                $rrTypesArr = [];
                /** @var RoundResultType $rrType */
                foreach ($rrTypes as $rrType) {
                    $id = str_replace('hely_', '', $rrType->getComputerName());
                    $rrTypesArr[$id] = $rrType->getPoint();
                }

                return array(
                    'rounds' => $pagination,
                    'teams' => $teams,
                    'rrTypes' => $rrTypesArr,
                );
            }
            else {
                return $this->redirectToRoute('skyball2016_round', array('round' => $round));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit the team stats for a round.
     *
     * @Route("/{roundid}/csapat-statisztika/szerkesztes/", name="skyball2016_round_edit_team_stats")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editTeamStatsAction(Request $request, $roundid)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if ((($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) && !$round->getPlayoffs()) {
                $teamService = $this->get('skyball2016_team');

                if ($request->getMethod() === 'POST') {
                    $values = $request->request->all();
                    $teamService->createRoundResults($round, $values);
                    $cacheService = $this->get('skyball2016_cache');
                    $cacheService->calculateTeamPoints();

                    $round->setClosed(true);
                    $em->persist($round);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2016.teamstats.edited');

                    return $this->redirectToRoute('skyball2016_round_teams', array('round' => $roundid));
                }

                $teams = $em->getRepository('SkyAthlonSkyBall2016Bundle:Team')->findBy(array(), array(
                    'rank' => 'ASC'
                ));
                if ($round->getClosed()) {
                    $rankings = $teamService->getRealRoundResults($round, true);
                }
                else {
                    $rankings = $teamService->getRoundResults($round, true);
                }

                return array(
                    'roundid'  => $roundid,
                    'teams'    => $teams,
                    'rankings' => $rankings,
                );
            }
            else {
                return $this->redirectToRoute('skyball2016_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
