<?php

namespace SkyAthlon\SkyBall2016Bundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    protected $em;

    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $em */
        $em = $this->em;
        $current = false;

        if (!$options['data']->getId() || ($options['data']->getGameType() && $options['data']->getGameType()->getPlayoffs())) {
            $gameTypes = $em->getRepository('SkyAthlonSkyBall2016Bundle:GameType')->getPlayoffGameTypes($options['attr'], $options['data']->getId());

            $builder
                ->add('gameType', ChoiceType::class, array(
                    'choices' => $gameTypes,
                    'label' => 'skyball2016.game.type',
                ));
        }

        if ($options['data']->getId()) {
            $currentGameId = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                'name' => 'skyball2016_currentGame'
            ));

            if ($currentGameId && $currentGameId->getValue() == $options['data']->getId()) {
                $current = true;
            }
        }

        $builder
            ->add('teamA', null, array(
                'label' => 'skyball2016.game.teamA',
                'attr' => array(
                    'class' => 'teamA'
                )
            ))
            ->add('teamB', null, array(
                'label' => 'skyball2016.game.teamB'
            ))
            ->add('pointA', null, array(
                'label' => 'skyball2016.game.pointA'
            ))
            ->add('pointB', null, array(
                'label' => 'skyball2016.game.pointB'
            ))
            ->add('ref', null, array(
                'label' => 'skyball2016.game.ref'
            ))
            ->add('sref', null, array(
                'label' => 'skyball2016.game.sref'
            ))
            ->add('srefb', null, array(
                'label' => 'skyball2016.game.srefb'
            ))
            ->add('jref', null, array(
                'label' => 'skyball2016.game.jref'
            ))
            ->add('current', CheckboxType::class, array(
                'mapped' => false,
                'label' => 'skyball2016.game.current',
                'data' => $current,
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SkyAthlon\SkyBall2016Bundle\Entity\Game'
        ));
    }
}
