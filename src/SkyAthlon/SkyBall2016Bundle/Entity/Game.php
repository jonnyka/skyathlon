<?php

namespace SkyAthlon\SkyBall2016Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="skyball2016_game")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2016Bundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesA")
     * @ORM\JoinColumn(name="teama_id", referencedColumnName="id")
     */
    protected $teamA;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesB")
     * @ORM\JoinColumn(name="teamb_id", referencedColumnName="id")
     */
    protected $teamB;

    /**
     * @var int
     *
     * @ORM\Column(name="pointa", type="smallint", nullable=true)
     */
    protected $pointA;

    /**
     * @var int
     *
     * @ORM\Column(name="pointb", type="smallint", nullable=true)
     */
    protected $pointB;

    /**
     * @ORM\OneToMany(targetEntity="PlayerResult", mappedBy="game")
     */
    protected $playerResults;

    /**
     * @ORM\ManyToOne(targetEntity="GameType", inversedBy="games")
     * @ORM\JoinColumn(name="gametype_id", referencedColumnName="id")
     */
    protected $gameType;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint")
     */
    protected $weight;

    /**
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="games")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    protected $round;

    protected $kerch;
    
    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="refs")
     * @ORM\JoinColumn(name="ref_id", referencedColumnName="id")
     */
    protected $ref;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="srefs")
     * @ORM\JoinColumn(name="sref_id", referencedColumnName="id")
     */
    protected $sref;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="srefbs")
     * @ORM\JoinColumn(name="srefb_id", referencedColumnName="id")
     */
    protected $srefb;
    
    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="jrefs")
     * @ORM\JoinColumn(name="jref_id", referencedColumnName="id")
     */
    protected $jref;

    public function __toString() {
        $teamA = $this->teamA ? $this->teamA : '?';
        $teamB = $this->teamB ? $this->teamB : '?';
        return $teamA . ' vs ' . $teamB;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playerResults = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set kerch
     *
     * @param array $kerch
     *
     * @return Game
     */
    public function setKerch($kerch)
    {
        $this->kerch = $kerch;

        return $this;
    }

    /**
     * Get kerch
     *
     * @return array
     */
    public function getKerch()
    {
        return $this->kerch;
    }

    /**
     * Set pointA
     *
     * @param integer $pointA
     *
     * @return Game
     */
    public function setPointA($pointA)
    {
        $this->pointA = $pointA;

        return $this;
    }

    /**
     * Get pointA
     *
     * @return integer
     */
    public function getPointA()
    {
        return $this->pointA;
    }

    /**
     * Set pointB
     *
     * @param integer $pointB
     *
     * @return Game
     */
    public function setPointB($pointB)
    {
        $this->pointB = $pointB;

        return $this;
    }

    /**
     * Get pointB
     *
     * @return integer
     */
    public function getPointB()
    {
        return $this->pointB;
    }

    /**
     * Set teamA
     *
     * @param Team $teamA
     *
     * @return Game
     */
    public function setTeamA(Team $teamA = null)
    {
        $this->teamA = $teamA;

        return $this;
    }

    /**
     * Get teamA
     *
     * @return Team
     */
    public function getTeamA()
    {
        return $this->teamA;
    }

    /**
     * Set teamB
     *
     * @param Team $teamB
     *
     * @return Game
     */
    public function setTeamB(Team $teamB = null)
    {
        $this->teamB = $teamB;

        return $this;
    }

    /**
     * Get teamB
     *
     * @return Team
     */
    public function getTeamB()
    {
        return $this->teamB;
    }

    /**
     * Add playerResult
     *
     * @param PlayerResult $playerResult
     *
     * @return Game
     */
    public function addPlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults[] = $playerResult;

        return $this;
    }

    /**
     * Remove playerResult
     *
     * @param PlayerResult $playerResult
     */
    public function removePlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults->removeElement($playerResult);
    }

    /**
     * Get playerResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerResults()
    {
        return $this->playerResults;
    }

    /**
     * Set gameType
     *
     * @param GameType $gameType
     *
     * @return Game
     */
    public function setGameType(GameType $gameType = null)
    {
        $this->gameType = $gameType;

        return $this;
    }

    /**
     * Get gameType
     *
     * @return GameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * Get gameType
     *
     * @return string
     */
    public function getRealGameType()
    {
        $gameType = $this->getGameType()->getName();

        /** @var Round $round */
        $round = $this->getRound();
        if ($round->getReversed()) {
            if (strpos($gameType, ' csoport körmérkőzés') !== false) {
                $gameType = strtr($gameType, '"A""B"', '"B""A"');
            }
        }

        return $gameType;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return Game
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set ref
     *
     * @param Player $ref
     *
     * @return Game
     */
    public function setRef(Player $ref = null)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return Player
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set sref
     *
     * @param Player $sref
     *
     * @return Game
     */
    public function setSref(Player $sref = null)
    {
        $this->sref = $sref;

        return $this;
    }

    /**
     * Get sref
     *
     * @return Player
     */
    public function getSref()
    {
        return $this->sref;
    }

    /**
     * Set jref
     *
     * @param Player $jref
     *
     * @return Game
     */
    public function setJref(Player $jref = null)
    {
        $this->jref = $jref;

        return $this;
    }

    /**
     * Get jref
     *
     * @return Player
     */
    public function getJref()
    {
        return $this->jref;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight() {
        return $this->getRound()->getReversed() ? $this->getGameType()->getReversedWeight() : $this->getGameType()->getWeight();
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Game
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Set srefb
     *
     * @param Player $srefb
     *
     * @return Game
     */
    public function setSrefb(Player $srefb = null)
    {
        $this->srefb = $srefb;

        return $this;
    }

    /**
     * Get srefb
     *
     * @return Player
     */
    public function getSrefb()
    {
        return $this->srefb;
    }

    public function isPreGame() {
        return $this->getGameType()->getComputerName() === 'eloselejtezo_1' || $this->getGameType()->getComputerName() === 'eloselejtezo_2';
    }
}
