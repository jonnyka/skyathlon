<?php

namespace SkyAthlon\SkyBall2016Bundle\Service;

use SkyAthlon\SkyBall2016Bundle\Entity\Game;
use SkyAthlon\SkyBall2016Bundle\Entity\Round;
use SkyAthlon\SkyBall2016Bundle\Entity\RoundResult;
use SkyAthlon\SkyBall2016Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SkyBall2016Team extends Controller
{
    protected $em;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRoundResults(Round $round, $onlyIds = false) {
        $em = $this->em;

        $games = $em->getRepository('SkyAthlonSkyBall2016Bundle:Game')->findBy(array(
            'round' => $round,
        ));

        $team10 = $team9 = $team8 = $team7 = $team6 = $team5 = $team4 = $team3 = $team2 = $team1 = new Team();

        foreach ($games as $game) {
            switch ($game->getGameType()->getComputerName()) {
                case 'eloselejtezo_1':
                    $team10 = $game->getTeamA();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team10 = $game->getTeamB();
                    }

                    break;

                case 'eloselejtezo_2':
                    $team9 = $game->getTeamA();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team9 = $game->getTeamB();
                    }

                    break;

                case 'helyoszto_7_8':
                    $team8 = $game->getTeamA();
                    $team7 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team7 = $game->getTeamA();
                        $team8 = $game->getTeamB();
                    }

                    break;

                case 'helyoszto_5_6':
                    $team6 = $game->getTeamA();
                    $team5 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team5 = $game->getTeamA();
                        $team6 = $game->getTeamB();
                    }

                    break;

                case 'donto_3':
                    $team4 = $game->getTeamA();
                    $team3 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team3 = $game->getTeamA();
                        $team4 = $game->getTeamB();
                    }

                    break;

                case 'donto_1':
                    $team2 = $game->getTeamA();
                    $team1 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team1 = $game->getTeamA();
                        $team2 = $game->getTeamB();
                    }

                    break;
            }
        }

        if ($onlyIds) {
            $team1 = $team1 ? $team1->getId() : null;
            $team2 = $team2 ? $team2->getId() : null;
            $team3 = $team3 ? $team3->getId() : null;
            $team4 = $team4 ? $team4->getId() : null;
            $team5 = $team5 ? $team5->getId() : null;
            $team6 = $team6 ? $team6->getId() : null;
            $team7 = $team7 ? $team7->getId() : null;
            $team8 = $team8 ? $team8->getId() : null;
            $team9 = $team9 ? $team9->getId() : null;
            $team10 = $team10 ? $team10->getId() : null;
        }

        $ret = array(
            'team1' => $team1,
            'team2' => $team2,
            'team3' => $team3,
            'team4' => $team4,
            'team5' => $team5,
            'team6' => $team6,
            'team7' => $team7,
            'team8' => $team8,
            'team9' => $team9,
            'team10' => $team10,
        );

        return $ret;
    }

    public function getRealRoundResults(Round $round, $onlyIds = false) {
        $em = $this->em;
        $rrRepo = $em->getRepository('SkyAthlonSkyBall2016Bundle:RoundResult');
        $rrTypeRepo = $em->getRepository('SkyAthlonSkyBall2016Bundle:RoundResultType');

        $team10 = $team9 = $team8 = $team7 = $team6 = $team5 = $team4 = $team3 = $team2 = $team1 = new Team();

        for ($i = 1; $i <= 10; $i++) {
            $roundResultType = $rrTypeRepo->findOneBy(array(
                'computerName' => 'hely_' . $i,
            ));
            $roundResult = $rrRepo->findOneBy(array(
                'round' => $round,
                'roundResultType' => $roundResultType
            ));

            if ($roundResult) {
                $teamStr = 'team' . $i;
                $$teamStr = $roundResult->getTeam();
            }
        }

        if ($onlyIds) {
            $team1 = $team1 ? $team1->getId() : null;
            $team2 = $team2 ? $team2->getId() : null;
            $team3 = $team3 ? $team3->getId() : null;
            $team4 = $team4 ? $team4->getId() : null;
            $team5 = $team5 ? $team5->getId() : null;
            $team6 = $team6 ? $team6->getId() : null;
            $team7 = $team7 ? $team7->getId() : null;
            $team8 = $team8 ? $team8->getId() : null;
            $team9 = $team9 ? $team9->getId() : null;
            $team10 = $team10 ? $team10->getId() : null;
        }

        $ret = array(
            'team1' => $team1,
            'team2' => $team2,
            'team3' => $team3,
            'team4' => $team4,
            'team5' => $team5,
            'team6' => $team6,
            'team7' => $team7,
            'team8' => $team8,
            'team9' => $team9,
            'team10' => $team10,
        );

        return $ret;
    }

    public function createRoundResults(Round $round, $results) {
        $em = $this->em;
        $teamRepo = $em->getRepository('SkyAthlonSkyBall2016Bundle:Team');
        $rrTypeRepo = $em->getRepository('SkyAthlonSkyBall2016Bundle:RoundResultType');
        $rrRepo = $em->getRepository('SkyAthlonSkyBall2016Bundle:RoundResult');

        foreach ($results as $standing => $teamId) {
            $standing = 'hely_' . str_replace('round-team-', '', $standing);
            $roundResultType = $rrTypeRepo->findOneBy(array('computerName' => $standing));
            $team = new Team();
            if ($teamId !== 0) {
                $team = $teamRepo->find($teamId);
            }
            $roundResult = $rrRepo->findOneBy(array(
                'round' => $round,
                'roundResultType' => $roundResultType
            ));

            if (!$roundResult) {
                $roundResult = new RoundResult();
                $roundResult->setRound($round);
                $roundResult->setRoundResultType($roundResultType);
            }

            $roundResult->setTeam($team);

            $em->persist($roundResult);
        }

        $em->flush();
    }
}
