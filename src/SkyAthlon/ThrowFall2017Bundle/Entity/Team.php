<?php

namespace SkyAthlon\ThrowFall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="throwfall2017_team")
 * @ORM\Entity(repositoryClass="SkyAthlon\ThrowFall2017Bundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=255)
     */
    protected $computerName;

    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="team")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    protected $players;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamA")
     */
    protected $gamesA;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamB")
     */
    protected $gamesB;

    /**
     * @var int
     *
     * @ORM\Column(name="point", type="integer", nullable=true)
     */
    protected $point;

    /**
     * @var int
     *
     * @ORM\Column(name="goal", type="integer", nullable=true)
     */
    protected $goal;

    /**
     *
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", nullable=true)
     */
    protected $rank;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->gamesA = new ArrayCollection();
        $this->gamesB = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add player
     *
     * @param Player $player
     *
     * @return Team
     */
    public function addPlayer(Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param Player $player
     */
    public function removePlayer(Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add gamesA
     *
     * @param Game $gamesA
     *
     * @return Team
     */
    public function addGamesA(Game $gamesA)
    {
        $this->gamesA[] = $gamesA;

        return $this;
    }

    /**
     * Remove gamesA
     *
     * @param Game $gamesA
     */
    public function removeGamesA(Game $gamesA)
    {
        $this->gamesA->removeElement($gamesA);
    }

    /**
     * Get gamesA
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesA()
    {
        return $this->gamesA;
    }

    /**
     * Add gamesB
     *
     * @param Game $gamesB
     *
     * @return Team
     */
    public function addGamesB(Game $gamesB)
    {
        $this->gamesB[] = $gamesB;

        return $this;
    }

    /**
     * Remove gamesB
     *
     * @param Game $gamesB
     */
    public function removeGamesB(Game $gamesB)
    {
        $this->gamesB->removeElement($gamesB);
    }

    /**
     * Get gamesB
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesB()
    {
        return $this->gamesB;
    }

    /**
     * Set point
     *
     * @param integer $point
     *
     * @return Team
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set goal
     *
     * @param integer $goal
     *
     * @return Team
     */
    public function setGoal($goal)
    {
        $this->goal = $goal;

        return $this;
    }

    /**
     * Get goal
     *
     * @return integer
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return Team
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return Team
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }
}
