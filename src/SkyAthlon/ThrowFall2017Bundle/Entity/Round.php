<?php

namespace SkyAthlon\ThrowFall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Round
 *
 * @ORM\Table(name="throwfall2017_round")
 * @ORM\Entity(repositoryClass="SkyAthlon\ThrowFall2017Bundle\Repository\RoundRepository")
 */
class Round
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint", unique=true)
     */
    protected $number;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="round")
     */
    protected $games;

    /**
     * @ORM\Column(name="closed", type="boolean", nullable=true)
     */
    protected $closed;

    /**
     * @ORM\Column(name="reversed", type="boolean", nullable=true)
     */
    protected $reversed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    protected $date;

    public function __toString() {
        return $this->name;
    }

    public function getFullName() {
        $str = $this->name;
        if ($this->date && strpos($this->date->format('Y'), '-000') === false) {
            $str .= ' (' . $this->date->format('Y.m.d');

            $str .= ')';
        }

        return $str;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Round
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Round
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Add game
     *
     * @param Game $game
     *
     * @return Round
     */
    public function addGame(Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param Game $game
     */
    public function removeGame(Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set closed
     *
     * @param boolean $closed
     *
     * @return Round
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return boolean
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Set reversed
     *
     * @param boolean $reversed
     *
     * @return Round
     */
    public function setReversed($reversed)
    {
        $this->reversed = $reversed;

        return $this;
    }

    /**
     * Get reversed
     *
     * @return boolean
     */
    public function getReversed()
    {
        return $this->reversed;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Round
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
