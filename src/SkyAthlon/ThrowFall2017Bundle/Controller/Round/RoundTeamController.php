<?php

namespace SkyAthlon\ThrowFall2017Bundle\Controller\Round;

use SkyAthlon\ThrowFall2017Bundle\Entity\Round;
use SkyAthlon\ThrowFall2017Bundle\Entity\RoundResultType;
use SkyAthlon\ThrowFall2017Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/throwfall-2017/fordulo")
 */
class RoundTeamController extends Controller
{
    /**
     * Show current round player results.
     *
     * @Route("/{round}/csapat-statisztika/", name="throwfall2017_round_teams", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexTeamsAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonThrowFall2017Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            /** @var Round $r */
            $r = $repo->findOneBy(array('number' => $round));

            $teamService = $this->get('throwfall2017_cache');
            $teams = $teamService->getRoundTeams($r);

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            return array(
                'rounds' => $pagination,
                'teams' => $teams,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
