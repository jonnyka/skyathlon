<?php

namespace SkyAthlon\ThrowFall2017Bundle\Controller;

use AppBundle\Entity\Settings;
use Doctrine\ORM\Query;
use SkyAthlon\ThrowFall2017Bundle\Entity\Game;
use SkyAthlon\ThrowFall2017Bundle\Entity\GameType;
use SkyAthlon\ThrowFall2017Bundle\Entity\Round;
use SkyAthlon\ThrowFall2017Bundle\Repository\GameRepository;
use SkyAthlon\ThrowFall2017Bundle\Repository\GameTypeRepository;
use SkyAthlon\ThrowFall2017Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/throwfall-2017/fordulo")
 */
class RoundController extends Controller
{
    /**
     * Redirect to the current round page.
     *
     * @Route("/", name="throwfall2017_round_redirect", options={"expose" = true})
     * @Method("GET")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToIndexAction() {
        if ($this->getUser()) {
            $round = 1;
            /** @var Settings $currentRoundObject */
            $currentRoundObject = $this->getDoctrine()->getManager()->getRepository('AppBundle:Settings')->findOneBy(array(
                'name' => 'throwfall2017_currentRound',
            ));
            if ($currentRoundObject) {
                $round = (int)$currentRoundObject->getValue();
            }

            return $this->redirectToRoute('throwfall2017_round', array('round' => $round));
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Show current round matches.
     *
     * @Route("/{round}/", name="throwfall2017_round", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonThrowFall2017Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            return array(
                'rounds' => $pagination,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing Round entity.
     *
     * @Route("/{id}/szerkesztes/", name="throwfall2017_round_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Round $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editAction(Request $request, Round $round)
    {
        if ($this->getUser()) {
            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $em = $this->getDoctrine()->getManager();
                $roundNum = null;
                /** @var Settings $currentRoundObject */
                $currentRoundObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                    'name' => 'throwfall2017_currentRound',
                ));
                if ($currentRoundObject) {
                    $roundNum = (int)$currentRoundObject->getValue();
                }

                $deleteForm = $this->createDeleteForm($round);
                $editForm = $this->createForm('SkyAthlon\ThrowFall2017Bundle\Form\RoundType', $round, array('roundNum' => $roundNum));
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $em->persist($round);

                    $gameTypes = $em->getRepository('SkyAthlonThrowFall2017Bundle:GameType')->findAll();
                    /** @var GameType $gameType */
                    foreach ($gameTypes as $gameType) {
                        $game = $em->getRepository('SkyAthlonThrowFall2017Bundle:Game')->findOneBy(array(
                            'round' => $round,
                            'gameType' => $gameType,
                        ));
                        /** @var Game $game */
                        if ($game) {
                            $weight = $round->getReversed() ? $gameType->getReversedWeight() : $gameType->getWeight();
                            $game->setWeight($weight);

                            $em->persist($game);
                        }
                    }

                    $isCurrent = $editForm->get('current')->getData();
                    if ($isCurrent) {
                        if (!$currentRoundObject) {
                            $currentRoundObject = new Settings();
                            $currentRoundObject->setName('throwfall2017_currentRound');
                        }
                        elseif ($currentRoundObject->getValue() !== $round->getNumber()) {
                            $es1 = $em->getRepository('SkyAthlonThrowFall2017Bundle:GameType')->findOneBy(array('computerName' => 'eloselejtezo_1'));

                            if ($es1) {
                                $es1game = $em->getRepository('SkyAthlonThrowFall2017Bundle:Game')->findOneBy(array(
                                    'round' => $round,
                                    'gameType' => $es1,
                                ));

                                if ($es1game) {
                                    $currentGameObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                                        'name' => 'throwfall2017_currentGame',
                                    ));

                                    if (!$currentGameObject) {
                                        $currentGameObject = new Settings();
                                        $currentGameObject->setName('throwfall2017_currentGame');
                                    }

                                    $currentGameObject->setValue($es1game->getId());
                                    $em->persist($currentGameObject);
                                }
                            }
                        }

                        $currentRoundObject->setValue($round->getNumber());
                        $em->persist($currentRoundObject);
                    }
                    elseif ($currentRoundObject && $currentRoundObject->getValue() == $round->getNumber()) {
                        $currentRoundObject->setValue(1);
                        $em->persist($currentRoundObject);
                    }

                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.throwfall2017.round.edited');

                    return $this->redirectToRoute('throwfall2017_round', array('round' => $round->getNumber()));
                }

                return array(
                    'round' => $round,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                );
            }
            else {
                return $this->redirectToRoute('throwfall2017_round', array('round' => $round->getNumber()));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Deletes a Round entity.
     *
     * @Route("/{id}/", name="throwfall2017_round_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Round $round
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Round $round)
    {
        if ($this->getUser()) {
            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $form = $this->createDeleteForm($round);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();

                    $games = $em->getRepository('SkyAthlonThrowFall2017Bundle:Game')->findBy(array(
                        'round' => $round,
                    ));
                    if ($games) {
                        foreach ($games as $game) {
                            $em->remove($game);
                        }
                    }

                    $em->remove($round);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.throwfall2017.round.deleted', 'warning');
                }

                return $this->redirectToRoute('throwfall2017_round_redirect');
            }
            else {
                return $this->redirectToRoute('throwfall2017_round_redirect');
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a form to delete a Round entity.
     *
     * @param Round $round The Round entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Round $round)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('throwfall2017_round_delete', array('id' => $round->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
