$('#team-table').dataTable({
    'paging':   false,
    'info':     false,
    'bFilter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columns': [
        {'name': 'first', 'orderable': true},
        {'name': 'players', 'orderable': false},
        {'name': 'player2', 'orderable': false},
        {'name': 'player3', 'orderable': false},
        {'name': 'player4', 'orderable': false},
        {'name': 'point', 'orderable': true, 'orderDataType': 'point', type: 'numeric'}
    ],
    'order': [
        [5, 'asc'],
        [0, 'asc']
    ]
});