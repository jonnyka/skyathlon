<?php

namespace SkyAthlon\ThrowFall2017Bundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $em */
        $em = $this->em;
        $current = false;

        if ($options['data']->getId()) {
            $currentGameId = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                'name' => 'throwfall2017_currentGame'
            ));

            if ($currentGameId && $currentGameId->getValue() == $options['data']->getId()) {
                $current = true;
            }
        }

        $builder
            ->add('teamA', null, array(
                'label' => 'throwfall2017.game.teamA',
                'attr'  => array(
                    'startFieldset' => 'throwfall2017.game.basicStuff',
                ),
            ))
            ->add('teamB', null, array(
                'label' => 'throwfall2017.game.teamB',
            ))
            ->add('pointA', null, array(
                'label' => 'throwfall2017.game.pointA',
            ))
            ->add('pointB', null, array(
                'label' => 'throwfall2017.game.pointB',
                'attr'  => array(
                    'endFieldset' => true,
                ),
            ))
            ->add('ref', null, array(
                'label' => 'throwfall2017.game.ref',
                'attr'  => array(
                    'startFieldset' => 'throwfall2017.game.refStuff',
                ),
            ))
            ->add('sref', null, array(
                'label' => 'throwfall2017.game.sref'
            ))
            ->add('jref', null, array(
                'label' => 'throwfall2017.game.jref',
                'attr'  => array(
                    'endFieldset' => true,
                ),
            ))
            ->add('current', CheckboxType::class, array(
                'mapped' => false,
                'label' => 'throwfall2017.game.current',
                'data' => $current,
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SkyAthlon\ThrowFall2017Bundle\Entity\Game',
            'custom' => array(),
        ));
    }
}
