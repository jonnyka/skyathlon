<?php

namespace SkyAthlon\ThrowFall2017Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class RoundType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $current = false;
        $date = new \DateTime();
        if ($options['data']) {
        	if ($options['data']->getDate() && strpos($options['data']->getDate()->format('Y'), '-000') === false) {
        		$date = $options['data']->getDate();
        	}
            if ($options['roundNum'] && $options['data']->getNumber()) {
                if ($options['data']->getNumber() === $options['roundNum']) {
                    $current = true;
                }
            }
        }

        $builder
            ->add('name', null, array(
                'label' => 'throwfall2017.round.name'
            ))
            ->add('date', DateType::class, array(
    			'widget' => 'choice',
    			'format' => 'yyyy.MMMM.dd',
            	'data' => $date,
                'label' => 'throwfall2017.round.date'
            ))
           ->add('current', CheckboxType::class, array(
                'mapped' => false,
                'label' => 'throwfall2017.round.current',
                'data' => $current,
            	'required' => false,
            ))
            ->add('reversed', CheckboxType::class, array(
                'label' => 'throwfall2017.round.reversed',
                'required' => false,
            ))
            ->add('closed', CheckboxType::class, array(
                'label' => 'throwfall2017.round.closed',
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SkyAthlon\ThrowFall2017Bundle\Entity\Round',
            'roundNum' => null,
        ));
    }
}
