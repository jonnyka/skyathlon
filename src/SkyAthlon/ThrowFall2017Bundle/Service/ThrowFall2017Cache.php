<?php

namespace SkyAthlon\ThrowFall2017Bundle\Service;

use AppBundle\Entity\Settings;
use AppBundle\Service\WebSocket;
use SkyAthlon\ThrowFall2017Bundle\Entity\Game;
use SkyAthlon\ThrowFall2017Bundle\Entity\PlayerResult;
use SkyAthlon\ThrowFall2017Bundle\Entity\Round;
use SkyAthlon\ThrowFall2017Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Predis\Client as Redis;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class ThrowFall2017Cache extends Controller
{
    protected $em;

    /**
     * Constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function calculateTeamPoints() {
        $em = $this->em;

        $teamsArray = array();
        $teamObjects = array();
        $teams = $em->getRepository('SkyAthlonThrowFall2017Bundle:Team')->findAll();
        foreach ($teams as $team) {
            $teamsArray[$team->getId()] = array('point' => 0, 'goal' => 0, 'computerName' => $team->getComputerName());
            $teamObjects[$team->getId()] = $team;
        }

        $rounds = $em->getRepository('SkyAthlonThrowFall2017Bundle:Round')->getAllRoundsQuery()->getResult();

        if ($rounds) {
            /** @var Round $round */
            foreach ($rounds as $round) {
                /** @var Game $game */
                foreach ($round->getGames() as $game) {
                    $gameType = $game->getGameType();
                    $gameTypeName = $gameType->getComputerName();
                    $teamA = $game->getTeamA();
                    $teamB = $game->getTeamB();
                    $pointA = $game->getPointA();
                    $pointB = $game->getPointB();
                    /** @var Team $winner */

                    if ($teamA && $teamB && is_int($pointA) && is_int($pointB) && $this->isPointEligable($gameTypeName)) {
                        $teamsArray[$teamA->getId()]['point'] += $pointA - $pointB;
                        $teamsArray[$teamB->getId()]['point'] += $pointB - $pointA;
                        $teamsArray[$teamA->getId()]['goal'] += $pointA;
                        $teamsArray[$teamB->getId()]['goal'] += $pointB;
                    }
                }
            }
        }

        uasort($teamsArray, array($this, 'sortTeams'));

        $lastPoint = $lastGoal = -9999;
        $i = 0;
        $j = 1;

        foreach ($teamsArray as $teamId => $teamPoints) {
            $teamPoint = $teamPoints['point'];
            $teamGoal = $teamPoints['goal'];
            $i++;
            if ($teamPoint !== $lastPoint && $teamGoal !== $lastGoal) {
                $j = $i;
            }

            /** @var Team $team */
            $team = $teamObjects[$teamId];
            $team->setPoint($teamPoint);
            $team->setGoal($teamGoal);
            $team->setRank($j);
            $em->persist($team);

            $lastPoint = $teamPoint;
            $lastGoal = $teamGoal;
        }

        $em->flush();
    }

    public function getRoundTeams(Round $round = null) {
        $em = $this->em;

        $teamsArray = array();
        $teams = $em->getRepository('SkyAthlonThrowFall2017Bundle:Team')->findAll();
        foreach ($teams as $team) {
            $teamsArray[$team->getId()] = array(
                'computerName' => $team->getComputerName(),
                'name'         => $team->getName(),
                'point'        => 0,
                'goal'         => 0,
            );
        }

        if (!$round) {
            $rounds = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findAll();

            foreach ($rounds as $round) {
                $teamsArray = $this->getTeamsForRound($teamsArray, $round);
            }
        }
        else {
            $teamsArray = $this->getTeamsForRound($teamsArray, $round);
        }

        usort($teamsArray, array($this, 'sortTeams'));

        return $teamsArray;
    }

    protected function getTeamsForRound($teamsArray, Round $round) {
        /** @var Game $game */
        foreach ($round->getGames() as $game) {
            $gameType = $game->getGameType();
            $gameTypeName = $gameType->getComputerName();
            $teamA  = $game->getTeamA();
            $teamB  = $game->getTeamB();
            $pointA = $game->getPointA();
            $pointB = $game->getPointB();
            /** @var Team $winner */

            if ($teamA && $teamB && is_int($pointA) && is_int($pointB) && $this->isPointEligable($gameTypeName)) {
                $a = &$teamsArray[$teamA->getId()];
                $b = &$teamsArray[$teamB->getId()];

                $a['point'] += ($pointA - $pointB);
                $a['goal'] += $pointA;
                $b['point'] += ($pointB - $pointA);
                $b['goal'] += $pointB;
            }
        }

        return $teamsArray;
    }

    protected function gameFinished($game, $light = false) {
        if ($light) {
            return ($game && $game['teamA'] && $game['teamB']);
        }
        else {
            return ($game && $game['gameLength'] && $game['teamA'] && $game['teamB'] && is_int($game['pointA']) && is_int($game['pointB']));
        }
    }

    protected function isPointEligable($type) {
        return strpos($type, 'eloselejtezo') === false;
    }

    protected function sortTeams($a, $b) {
        if (array_key_exists('point', $a) && array_key_exists('point', $b)) {
            if ($a['point'] === $b['point']) {
                if ($a['goal'] === $b['goal']) {
                    return ($a['computerName'] > $b['computerName']) ? 1 : -1;
                }
                else {
                    return ($a['goal'] < $b['goal']) ? 1 : -1;
                }
            }
            else {
                return ($a['point'] < $b['point']) ? 1 : -1;
            }
        }

        return 0;
    }
}
