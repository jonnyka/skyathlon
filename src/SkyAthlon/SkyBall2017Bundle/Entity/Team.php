<?php

namespace SkyAthlon\SkyBall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="skyball2017_team")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2017Bundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=255)
     */
    protected $computerName;

    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="team")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    protected $players;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamA")
     */
    protected $gamesA;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamB")
     */
    protected $gamesB;

    /**
     * @ORM\OneToMany(targetEntity="RoundResult", mappedBy="team")
     */
    protected $roundResults;

    /**
     * @var int
     *
     * @ORM\Column(name="point", type="integer", nullable=true)
     */
    protected $point;
    /**
     *
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", nullable=true)
     */
    protected $rank;

    /**
     * @var int
     *
     * @ORM\Column(name="orig", type="smallint", nullable=true)
     */
    protected $orig;

    /**
     * @var string
     *
     * @ORM\Column(name="c1", type="string", length=255)
     */
    protected $c1;

    /**
     * @var string
     *
     * @ORM\Column(name="c2", type="string", length=255)
     */
    protected $c2;

    /**
     * @ORM\OneToMany(targetEntity="Round", mappedBy="host")
     */
    protected $hostedRounds;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->gamesA = new ArrayCollection();
        $this->gamesB = new ArrayCollection();
        $this->roundResults = new ArrayCollection();
        $this->hostedRounds = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add player
     *
     * @param Player $player
     *
     * @return Team
     */
    public function addPlayer(Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param Player $player
     */
    public function removePlayer(Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add gamesA
     *
     * @param Game $gamesA
     *
     * @return Team
     */
    public function addGamesA(Game $gamesA)
    {
        $this->gamesA[] = $gamesA;

        return $this;
    }

    /**
     * Remove gamesA
     *
     * @param Game $gamesA
     */
    public function removeGamesA(Game $gamesA)
    {
        $this->gamesA->removeElement($gamesA);
    }

    /**
     * Get gamesA
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesA()
    {
        return $this->gamesA;
    }

    /**
     * Add gamesB
     *
     * @param Game $gamesB
     *
     * @return Team
     */
    public function addGamesB(Game $gamesB)
    {
        $this->gamesB[] = $gamesB;

        return $this;
    }

    /**
     * Remove gamesB
     *
     * @param Game $gamesB
     */
    public function removeGamesB(Game $gamesB)
    {
        $this->gamesB->removeElement($gamesB);
    }

    /**
     * Get gamesB
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesB()
    {
        return $this->gamesB;
    }

    /**
     * Set point
     *
     * @param integer $point
     *
     * @return Team
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Add roundResult
     *
     * @param RoundResult $roundResult
     *
     * @return Team
     */
    public function addRoundResult(RoundResult $roundResult)
    {
        $this->roundResults[] = $roundResult;

        return $this;
    }

    /**
     * Remove roundResult
     *
     * @param RoundResult $roundResult
     */
    public function removeRoundResult(RoundResult $roundResult)
    {
        $this->roundResults->removeElement($roundResult);
    }

    /**
     * Get roundResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundResults()
    {
        return $this->roundResults;
    }

    /**
     * Set orig
     *
     * @param integer $orig
     *
     * @return Team
     */
    public function setOrig($orig)
    {
        $this->orig = $orig;

        return $this;
    }

    /**
     * Get orig
     *
     * @return integer
     */
    public function getOrig()
    {
        return $this->orig;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return Team
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return Team
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }

    /**
     * Set c1
     *
     * @param string $c1
     *
     * @return Team
     */
    public function setC1($c1)
    {
        $this->c1 = $c1;

        return $this;
    }

    /**
     * Get c1
     *
     * @return string
     */
    public function getC1()
    {
        return $this->c1;
    }

    /**
     * Set c2
     *
     * @param string $c2
     *
     * @return Team
     */
    public function setC2($c2)
    {
        $this->c2 = $c2;

        return $this;
    }

    /**
     * Get c2
     *
     * @return string
     */
    public function getC2()
    {
        return $this->c2;
    }

    /**
     * Add hostedRound
     *
     * @param Round $hostedRound
     *
     * @return Team
     */
    public function addHostedRound(Round $hostedRound)
    {
        $this->hostedRounds[] = $hostedRound;

        return $this;
    }

    /**
     * Remove hostedRound
     *
     * @param Round $hostedRound
     */
    public function removeHostedRound(Round $hostedRound)
    {
        $this->hostedRounds->removeElement($hostedRound);
    }

    /**
     * Get hostedRounds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHostedRounds()
    {
        return $this->hostedRounds;
    }
}
