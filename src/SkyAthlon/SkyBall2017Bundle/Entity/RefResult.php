<?php

namespace SkyAthlon\SkyBall2017Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Player as Playa;

/**
 * RefResult
 *
 * @ORM\Table(name="skyball2017_ref_result")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2017Bundle\Repository\RefResultRepository")
 */
class RefResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="smallint")
     */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="refResults")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;

    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="refResults")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="spiclis")
     * @ORM\JoinColumn(name="spicli_id", referencedColumnName="id")
     */
    protected $spicli;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    protected $type;

    public function __toString() {
        return $this->player . ' - ' . $this->value;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return RefResult
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set player
     *
     * @param Playa $player
     *
     * @return RefResult
     */
    public function setPlayer(Playa $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return Playa
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set game
     *
     * @param Game $game
     *
     * @return RefResult
     */
    public function setGame(Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set spicli
     *
     * @param Playa $spicli
     *
     * @return RefResult
     */
    public function setSpicli(Playa $spicli = null)
    {
        $this->spicli = $spicli;

        return $this;
    }

    /**
     * Get spicli
     *
     * @return Playa
     */
    public function getSpicli()
    {
        return $this->spicli;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return RefResult
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
}
