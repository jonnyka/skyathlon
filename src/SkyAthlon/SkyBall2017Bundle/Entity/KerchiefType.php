<?php

namespace SkyAthlon\SkyBall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Kerchief
 *
 * @ORM\Table(name="skyball2017_kerchief_type")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2017Bundle\Repository\KerchiefTypeRepository")
 */
class KerchiefType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=5, unique=true)
     */
    protected $computerName;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint")
     */
    protected $weight;

    /**
     * @ORM\OneToMany(targetEntity="PlayerResult", mappedBy="kerchiefType")
     */
    protected $playerResults;

    /**
     * @ORM\OneToMany(targetEntity="RoundKerchiefResult", mappedBy="kerchiefType")
     */
    protected $roundKerchiefResults;

    /**
     * @ORM\ManyToOne(targetEntity="KerchiefCategory", inversedBy="kerchiefTypes")
     * @ORM\JoinColumn(name="kerchiefcategory_id", referencedColumnName="id")
     */
    protected $kerchiefCategory;

    public function __toString() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playerResults = new ArrayCollection();
        $this->roundKerchiefResults = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return KerchiefType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return KerchiefType
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return KerchiefType
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add playerResult
     *
     * @param PlayerResult $playerResult
     *
     * @return KerchiefType
     */
    public function addPlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults[] = $playerResult;

        return $this;
    }

    /**
     * Remove playerResult
     *
     * @param PlayerResult $playerResult
     */
    public function removePlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults->removeElement($playerResult);
    }

    /**
     * Get playerResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerResults()
    {
        return $this->playerResults;
    }

    /**
     * Add roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     *
     * @return KerchiefType
     */
    public function addRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults[] = $roundKerchiefResult;

        return $this;
    }

    /**
     * Remove roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     */
    public function removeRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults->removeElement($roundKerchiefResult);
    }

    /**
     * Get roundKerchiefResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundKerchiefResults()
    {
        return $this->roundKerchiefResults;
    }

    /**
     * Set kerchiefCategory
     *
     * @param \SkyAthlon\SkyBall2017Bundle\Entity\KerchiefCategory $kerchiefCategory
     *
     * @return KerchiefType
     */
    public function setKerchiefCategory(\SkyAthlon\SkyBall2017Bundle\Entity\KerchiefCategory $kerchiefCategory = null)
    {
        $this->kerchiefCategory = $kerchiefCategory;

        return $this;
    }

    /**
     * Get kerchiefCategory
     *
     * @return \SkyAthlon\SkyBall2017Bundle\Entity\KerchiefCategory
     */
    public function getKerchiefCategory()
    {
        return $this->kerchiefCategory;
    }
}
