<?php

namespace SkyAthlon\SkyBall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Round
 *
 * @ORM\Table(name="skyball2017_round")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2017Bundle\Repository\RoundRepository")
 */
class Round
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint", unique=true)
     */
    protected $number;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="round")
     */
    protected $games;

    /**
     * @ORM\OneToMany(targetEntity="RoundResult", mappedBy="round")
     */
    protected $roundResults;

    /**
     * @ORM\OneToMany(targetEntity="RoundKerchiefResult", mappedBy="round")
     */
    protected $roundKerchiefResults;

    /**
     * @ORM\OneToMany(targetEntity="PlayerResult", mappedBy="game")
     */
    protected $playerResults;

    /**
     * @ORM\Column(name="closed", type="boolean", nullable=true)
     */
    protected $closed;

    /**
     * @ORM\Column(name="reversed", type="boolean", nullable=true)
     */
    protected $reversed;

    /**
     * @ORM\Column(name="playoffs", type="boolean", nullable=true)
     */
    protected $playoffs;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="hostedRounds")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id")
     */
    protected $host;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="sorder", type="string", length=10, options={"default": "DCBA"})
     */
    protected $sorder;

    public function __toString() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games = new ArrayCollection();
        $this->playerResults = new ArrayCollection();
        $this->roundResults = new ArrayCollection();
        $this->roundKerchiefResults = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Round
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Round
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Add game
     *
     * @param Game $game
     *
     * @return Round
     */
    public function addGame(Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param Game $game
     */
    public function removeGame(Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Add playerResult
     *
     * @param PlayerResult $playerResult
     *
     * @return Round
     */
    public function addPlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults[] = $playerResult;

        return $this;
    }

    /**
     * Remove playerResult
     *
     * @param PlayerResult $playerResult
     */
    public function removePlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults->removeElement($playerResult);
    }

    /**
     * Get playerResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerResults()
    {
        return $this->playerResults;
    }

    /**
     * Set closed
     *
     * @param boolean $closed
     *
     * @return Round
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return boolean
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Set sorder
     *
     * @param string $sorder
     *
     * @return Round
     */
    public function setSorder($sorder)
    {
        $this->sorder = $sorder;

        return $this;
    }

    /**
     * Get sorder
     *
     * @return string
     */
    public function getSorder()
    {
        return $this->sorder;
    }

    /**
     * Add roundResult
     *
     * @param RoundResult $roundResult
     *
     * @return Round
     */
    public function addRoundResult(RoundResult $roundResult)
    {
        $this->roundResults[] = $roundResult;

        return $this;
    }

    /**
     * Remove roundResult
     *
     * @param RoundResult $roundResult
     */
    public function removeRoundResult(RoundResult $roundResult)
    {
        $this->roundResults->removeElement($roundResult);
    }

    /**
     * Get roundResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundResults()
    {
        return $this->roundResults;
    }

    /**
     * Add roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     *
     * @return Round
     */
    public function addRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults[] = $roundKerchiefResult;

        return $this;
    }

    /**
     * Remove roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     */
    public function removeRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults->removeElement($roundKerchiefResult);
    }

    /**
     * Get roundKerchiefResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundKerchiefResults()
    {
        return $this->roundKerchiefResults;
    }

    /**
     * Set reversed
     *
     * @param boolean $reversed
     *
     * @return Round
     */
    public function setReversed($reversed)
    {
        $this->reversed = $reversed;

        return $this;
    }

    /**
     * Get reversed
     *
     * @return boolean
     */
    public function getReversed()
    {
        return $this->reversed;
    }

    /**
     * Set host
     *
     * @param Team $host
     *
     * @return Round
     */
    public function setHost(Team $host = null)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return Team
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Round
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set playoffs
     *
     * @param boolean $playoffs
     *
     * @return Round
     */
    public function setPlayoffs($playoffs)
    {
        $this->playoffs = $playoffs;

        return $this;
    }

    /**
     * Get playoffs
     *
     * @return boolean
     */
    public function getPlayoffs()
    {
        return $this->playoffs;
    }
    
    public function getFullName() {
    	$str = $this->name;
    	if ($this->date && strpos($this->date->format('Y'), '-000') === false) {
    		$str .= ' (' . $this->date->format('Y.m.d');
    		
    		if ($this->host) {
    			$str .= ' - hg: ' . $this->host;
    		}
    		
    		$str .= ')';
    	}
    	else if ($this->host) {
    			$str .= ' (hg: ' . $this->host . ')';
    	}
    	return $str;
    }
}
