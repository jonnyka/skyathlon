<?php

namespace SkyAthlon\SkyBall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * KerchiefCategory
 *
 * @ORM\Table(name="skyball2017_kerchief_category")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2017Bundle\Repository\KerchiefCategoryRepository")
 */
class KerchiefCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=100, unique=true)
     */
    protected $computerName;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=10, unique=true)
     */
    protected $color;

    /**
     * @var string
     *
     * @ORM\Column(name="fontcolor", type="string", length=10)
     */
    protected $fontColor;

    /**
     * @ORM\OneToMany(targetEntity="KerchiefType", mappedBy="kerchiefCategory")
     */
    protected $kerchiefTypes;

    public function __toString() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kerchiefTypes = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return KerchiefCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return KerchiefCategory
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return KerchiefCategory
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add kerchiefType
     *
     * @param KerchiefType $kerchiefType
     *
     * @return KerchiefCategory
     */
    public function addKerchiefType(KerchiefType $kerchiefType)
    {
        $this->kerchiefTypes[] = $kerchiefType;

        return $this;
    }

    /**
     * Remove kerchiefType
     *
     * @param KerchiefType $kerchiefType
     */
    public function removeKerchiefType(KerchiefType $kerchiefType)
    {
        $this->kerchiefTypes->removeElement($kerchiefType);
    }

    /**
     * Get kerchiefTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKerchiefTypes()
    {
        return $this->kerchiefTypes;
    }

    /**
     * Set fontColor
     *
     * @param string $fontColor
     *
     * @return KerchiefCategory
     */
    public function setFontColor($fontColor)
    {
        $this->fontColor = $fontColor;

        return $this;
    }

    /**
     * Get fontColor
     *
     * @return string
     */
    public function getFontColor()
    {
        return $this->fontColor;
    }
}
