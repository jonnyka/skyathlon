<?php

namespace SkyAthlon\SkyBall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table(name="skyball2017_player")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2017Bundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint", unique=true)
     */
    protected $number;
    
    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="players")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * @var string
     *
     * @ORM\Column(name="leader", type="boolean", options={"default" = false})
     */
    protected $leader;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint", options={"default" = 1})
     */
    protected $weight;

    /**
     * @ORM\OneToMany(targetEntity="PlayerResult", mappedBy="player")
     */
    protected $playerResults;

    /**
     * @ORM\OneToMany(targetEntity="RoundKerchiefResult", mappedBy="kerchiefType")
     */
    protected $roundKerchiefResults;

    public function __toString() {
        return $this->number . ' - ' . $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playerResults = new ArrayCollection();
        $this->roundKerchiefResults = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Player
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set team
     *
     * @param Team $team
     *
     * @return Player
     */
    public function setTeam(Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set leader
     *
     * @param boolean $leader
     *
     * @return Player
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Get leader
     *
     * @return boolean
     */
    public function getLeader()
    {
        return $this->leader;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Player
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add playerResult
     *
     * @param PlayerResult $playerResult
     *
     * @return Player
     */
    public function addPlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults[] = $playerResult;

        return $this;
    }

    /**
     * Remove playerResult
     *
     * @param PlayerResult $playerResult
     */
    public function removePlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults->removeElement($playerResult);
    }

    /**
     * Get playerResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerResults()
    {
        return $this->playerResults;
    }

    /**
     * Add roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     *
     * @return Player
     */
    public function addRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults[] = $roundKerchiefResult;

        return $this;
    }

    /**
     * Remove roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     */
    public function removeRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults->removeElement($roundKerchiefResult);
    }

    /**
     * Get roundKerchiefResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundKerchiefResults()
    {
        return $this->roundKerchiefResults;
    }
}
