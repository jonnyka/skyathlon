<?php

namespace SkyAthlon\SkyBall2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Player as Ref;

/**
 * Game
 *
 * @ORM\Table(name="skyball2017_game")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkyBall2017Bundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesA")
     * @ORM\JoinColumn(name="teama_id", referencedColumnName="id")
     */
    protected $teamA;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesB")
     * @ORM\JoinColumn(name="teamb_id", referencedColumnName="id")
     */
    protected $teamB;

    /**
     * @var int
     *
     * @ORM\Column(name="pointa", type="smallint", nullable=true)
     */
    protected $pointA;

    /**
     * @var int
     *
     * @ORM\Column(name="qsa", type="smallint", nullable=true)
     */
    protected $qsA;

    /**
     * @var int
     *
     * @ORM\Column(name="qgka", type="smallint", nullable=true)
     */
    protected $qgkA;

    /**
     * @var int
     *
     * @ORM\Column(name="pointb", type="smallint", nullable=true)
     */
    protected $pointB;

    /**
     * @var int
     *
     * @ORM\Column(name="qsB", type="smallint", nullable=true)
     */
    protected $qsB;

    /**
     * @var int
     *
     * @ORM\Column(name="qgkb", type="smallint", nullable=true)
     */
    protected $qgkB;

    /**
     * @ORM\OneToMany(targetEntity="PlayerResult", mappedBy="game")
     */
    protected $playerResults;

    /**
     * @ORM\OneToMany(targetEntity="RefResult", mappedBy="game")
     */
    protected $refResults;

    /**
     * @ORM\ManyToOne(targetEntity="GameType", inversedBy="games")
     * @ORM\JoinColumn(name="gametype_id", referencedColumnName="id")
     */
    protected $gameType;

    /**
     * @ORM\ManyToOne(targetEntity="GameLength", inversedBy="games")
     * @ORM\JoinColumn(name="gamelength_id", referencedColumnName="id")
     */
    protected $gameLength;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint")
     */
    protected $weight;

    /**
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="games")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    protected $round;

    protected $kerch;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="refs")
     * @ORM\JoinColumn(name="ref_id", referencedColumnName="id")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    protected $ref;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="srefs")
     * @ORM\JoinColumn(name="sref_id", referencedColumnName="id")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    protected $sref;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="srefbs")
     * @ORM\JoinColumn(name="srefb_id", referencedColumnName="id")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    protected $srefb;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="jrefs")
     * @ORM\JoinColumn(name="jref_id", referencedColumnName="id")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    protected $jref;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="gameSpiclis")
     * @ORM\JoinColumn(name="spicli_id", referencedColumnName="id")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    protected $spicli;

    public function __toString() {
        $teamA = $this->teamA ? $this->teamA : '?';
        $teamB = $this->teamB ? $this->teamB : '?';
        return $teamA . ' vs ' . $teamB;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playerResults = new ArrayCollection();
        $this->refResults = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set kerch
     *
     * @param array $kerch
     *
     * @return Game
     */
    public function setKerch($kerch)
    {
        $this->kerch = $kerch;

        return $this;
    }

    /**
     * Get kerch
     *
     * @return array
     */
    public function getKerch()
    {
        return $this->kerch;
    }

    /**
     * Set pointA
     *
     * @param integer $pointA
     *
     * @return Game
     */
    public function setPointA($pointA)
    {
        $this->pointA = $pointA;

        return $this;
    }

    /**
     * Get pointA
     *
     * @return integer
     */
    public function getPointA()
    {
        return $this->pointA;
    }

    /**
     * Set pointB
     *
     * @param integer $pointB
     *
     * @return Game
     */
    public function setPointB($pointB)
    {
        $this->pointB = $pointB;

        return $this;
    }

    /**
     * Get pointB
     *
     * @return integer
     */
    public function getPointB()
    {
        return $this->pointB;
    }

    /**
     * Set teamA
     *
     * @param Team $teamA
     *
     * @return Game
     */
    public function setTeamA(Team $teamA = null)
    {
        $this->teamA = $teamA;

        return $this;
    }

    /**
     * Get teamA
     *
     * @return Team
     */
    public function getTeamA()
    {
        return $this->teamA;
    }

    /**
     * Set teamB
     *
     * @param Team $teamB
     *
     * @return Game
     */
    public function setTeamB(Team $teamB = null)
    {
        $this->teamB = $teamB;

        return $this;
    }

    /**
     * Get teamB
     *
     * @return Team
     */
    public function getTeamB()
    {
        return $this->teamB;
    }

    /**
     * Add playerResult
     *
     * @param PlayerResult $playerResult
     *
     * @return Game
     */
    public function addPlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults[] = $playerResult;

        return $this;
    }

    /**
     * Remove playerResult
     *
     * @param PlayerResult $playerResult
     */
    public function removePlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults->removeElement($playerResult);
    }

    /**
     * Get playerResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerResults()
    {
        return $this->playerResults;
    }

    /**
     * Add refResult
     *
     * @param RefResult $refResult
     *
     * @return Game
     */
    public function addRefResult(RefResult $refResult)
    {
        $this->refResults[] = $refResult;

        return $this;
    }

    /**
     * Remove refResult
     *
     * @param RefResult $refResult
     */
    public function removeRefResult(RefResult $refResult)
    {
        $this->refResults->removeElement($refResult);
    }

    /**
     * Get refResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRefResults()
    {
        return $this->refResults;
    }

    /**
     * Set gameType
     *
     * @param GameType $gameType
     *
     * @return Game
     */
    public function setGameType(GameType $gameType = null)
    {
        $this->gameType = $gameType;

        return $this;
    }

    /**
     * Get gameType
     *
     * @return GameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * Get real gameType
     *
     * @return string
     */
    public function getRealGameType()
    {
        $gameType = $this->getGameType()->getName();

        if (!$this->getGameType()) {
            if (strpos($gameType, 'Selejtező #') !== false) {
                /** @var Round $round */
                $round = $this->getRound();
                $sorder = $round->getSorder();
                $mit = array('D', 'C', 'B', 'A');
                $mireFirst = array('W', 'X', 'Y', 'Z');
                $mire = str_split($sorder);

                $gameType = str_replace($mit, $mireFirst, $gameType);
                $gameType = str_replace($mireFirst, $mire, $gameType);
            }
        }
        else {
            $gameType = $this->getGameType();
        }

        return $gameType;
    }

    /**
     * Set gameLength
     *
     * @param GameLength $gameLength
     *
     * @return Game
     */
    public function setGameLength(GameLength $gameLength = null)
    {
        $this->gameLength = $gameLength;

        return $this;
    }

    /**
     * Get gameLength
     *
     * @return GameLength
     */
    public function getGameLength()
    {
        return $this->gameLength;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return Game
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set ref
     *
     * @param Ref $ref
     *
     * @return Game
     */
    public function setRef(Ref $ref = null)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return Ref
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set sref
     *
     * @param Ref $sref
     *
     * @return Game
     */
    public function setSref(Ref $sref = null)
    {
        $this->sref = $sref;

        return $this;
    }

    /**
     * Get sref
     *
     * @return Ref
     */
    public function getSref()
    {
        return $this->sref;
    }

    /**
     * Set jref
     *
     * @param Ref $jref
     *
     * @return Game
     */
    public function setJref(Ref $jref = null)
    {
        $this->jref = $jref;

        return $this;
    }

    /**
     * Get jref
     *
     * @return Ref
     */
    public function getJref()
    {
        return $this->jref;
    }

    /**
     * Set srefb
     *
     * @param Ref $srefb
     *
     * @return Game
     */
    public function setSrefb(Ref $srefb = null)
    {
        $this->srefb = $srefb;

        return $this;
    }

    /**
     * Get srefb
     *
     * @return Ref
     */
    public function getSrefb()
    {
        return $this->srefb;
    }

    /**
     * Set spicli
     *
     * @param Ref $spicli
     *
     * @return Game
     */
    public function setSpicli(Ref $spicli = null)
    {
        $this->spicli = $spicli;

        return $this;
    }

    /**
     * Get spicli
     *
     * @return Ref
     */
    public function getSpicli()
    {
        return $this->spicli;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight() {
        return $this->getRound()->getReversed() ? $this->getGameType()->getReversedWeight() : $this->getGameType()->getWeight();
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Game
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    public function isPreGame() {
        return strpos($this->getGameType()->getComputerName(), 'eloselejtezo') !== false;
    }

    /**
     * Set qsA
     *
     * @param integer $qsA
     *
     * @return Game
     */
    public function setQsA($qsA)
    {
        $this->qsA = $qsA;

        return $this;
    }

    /**
     * Get qsA
     *
     * @return integer
     */
    public function getQsA()
    {
        return $this->qsA;
    }

    /**
     * Set qgkA
     *
     * @param integer $qgkA
     *
     * @return Game
     */
    public function setQgkA($qgkA)
    {
        $this->qgkA = $qgkA;

        return $this;
    }

    /**
     * Get qgkA
     *
     * @return integer
     */
    public function getQgkA()
    {
        return $this->qgkA;
    }

    /**
     * Set qsB
     *
     * @param integer $qsB
     *
     * @return Game
     */
    public function setQsB($qsB)
    {
        $this->qsB = $qsB;

        return $this;
    }

    /**
     * Get qsB
     *
     * @return integer
     */
    public function getQsB()
    {
        return $this->qsB;
    }

    /**
     * Set qgkB
     *
     * @param integer $qgkB
     *
     * @return Game
     */
    public function setQgkB($qgkB)
    {
        $this->qgkB = $qgkB;

        return $this;
    }

    /**
     * Get qgkB
     *
     * @return integer
     */
    public function getQgkB()
    {
        return $this->qgkB;
    }
}
