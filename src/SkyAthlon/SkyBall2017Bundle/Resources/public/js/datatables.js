$('#edit-player-results').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10,11,12],
            'orderSequence': ["desc", "asc"],
            'orderDataType': 'sortbyInput',
            'sType': 'numeric'
        }
    ]
}).find('input').bind('keyup mouseup', function() {
    var player = $(this).data('player'),
        action = $('*[data-player="' + player + '"][data-type="A"]'),
        actionVal = action.val() || 0,
        center = $('*[data-player="' + player + '"][data-type="C"]'),
        centerVal = center.val() || 0,
        sniper = $('*[data-player="' + player + '"][data-type="S"]'),
        sniperVal = sniper.val() || 0,
        deadeye = $('*[data-player="' + player + '"][data-type="Y"]'),
        deadeyeVal = deadeye.val() || 0,
        qs = $('*[data-player="' + player + '"][data-type="QS"]'),
        qsVal = qs.val() || 0,
        gk = $('*[data-player="' + player + '"][data-type="GK"]'),
        gkVal = gk.val() || 0,
        gki = $('*[data-player="' + player + '"][data-type="GKI"]'),
        gkiVal = gki.val() || 0,

        goal = $('*[data-player="' + player + '"][data-type="G"]'),
        goalSum = ((+deadeyeVal * 4) + (+sniperVal * 3) + (+centerVal * 2) + (+actionVal)),
        canadian = $('*[data-player="' + player + '"][data-type="SP"]'),
        canadianSum = (+actionVal + +centerVal + +sniperVal + +deadeyeVal + +gkVal + +gkiVal)
        ;

    goal.val(goalSum);
    canadian.val(canadianSum);
});

$('#by-players-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10,11,12,13],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [13, 'desc']
    ]
});

$('.round-round-table, .by-players-table:not("#by-players-table")').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10,11,12],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [11, 'desc']
    ]
});

$('.refs-table').each(function() {
    var t = $(this).DataTable({
        'paging':   false,
        'info':     false,
        'filter':   false,
        'language': {
            'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
        },
        'columnDefs': [
            {
                'targets': [0, 1],
                'orderable': false
            },

            {
                'targets': [2,3,4],
                'orderSequence': ["desc", "asc"]
            }
        ],
        //'orderFixed': [ 0, 'asc' ],
        'order': [
            [4, 'desc'],
            [2, 'desc']
        ]
    });

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    });
});

$('.refnums-table').each(function() {
    var t = $(this).DataTable({
        'paging':   false,
        'info':     false,
        'filter':   false,
        'language': {
            'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
        },
        'columnDefs': [
            {
                'targets': [0, 1],
                'orderable': false
            },

            {
                'targets': [2,3,4,5,6],
                'orderSequence': ["desc", "asc"]
            }
        ],
        'order': [
            [2, 'desc'],
            [5, 'desc'],
            [3, 'desc'],
            [4, 'desc']
        ]
    });

    t.on( 'order.dt search.dt', function () {
        t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    });
});

$('#by-players-ranks-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7,8,9,10,11,12],
            'orderSequence': ["asc", "desc"]
        },
        {
            'targets': 13,
            'orderDataType': 'sortbyshow',
            'sType': 'numeric'
        }
    ],
    'order': [
        [13, 'asc']
    ]
});

$('#team-table').dataTable({
    'paging':   false,
    'info':     false,
    'bFilter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columns': [
        {'name': 'first', 'orderable': true},
        {'name': 'flag', 'orderable': false},
        {'name': 'player1', 'orderable': false},
        {'name': 'player2', 'orderable': false},
        {'name': 'point', 'orderable': true, 'orderDataType': 'point', type: 'numeric'}
    ],
    'order': [
        [4, 'asc'],
        [0, 'asc']
    ]
});