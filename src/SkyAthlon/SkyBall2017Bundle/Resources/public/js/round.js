var loc = window.location.href;

// Load player stats via ajax
$('.round-game-showhide2').click(function() {
    var gid = $(this).data('target'),
        otherDiv = $('#round-game-body-' + gid),
        thisDiv = $('#round-game-body2-' + gid),
        i,
        k,
        player,
        kerchief,
        value,
        computerName,
        maxValue,
        tr,
        text,
        table = $('#round-game-table-' + gid),
        spinner = $('#spinner-' + gid);

    otherDiv.hide();

    if (thisDiv.is(':visible')) {
        thisDiv.hide();
    }
    else if (table.hasClass('content-loaded')) {
        spinner.hide();
        table.show();
        thisDiv.show();
    }
    else {
        spinner.show();
        thisDiv.show();
        table.hide();

        $.ajax({
            type: "GET",
            url: Routing.generate('skyball2017_game_kerchiefs', { game: gid}),
            success: function (data) {
                spinner.hide();
                table.show().addClass('content-loaded');

                var div = $('#round-game-tbody-' + gid);

                for (i in data['players']) {
                    player = data['players'][i];

                    tr = div.find('#round-game-player-' + gid + '-' + i);
                    text = '<span class="sortby">' + i + '</span><span class="player-number">' + player[0]['player']['number'] + '</span> <b>' + player[0].player.name + '</b>';
                    tr.addClass(player[0]['player']['team']).find('#round-game-player-td-' + gid + '-p').html(text).addClass(player[0]['player']['team'] + '-text');

                    for (k in player) {
                        kerchief = player[k];
                        computerName = kerchief['kerchiefType']['computerName'];
                        maxValue = data['maxValues'][computerName];
                        value = kerchief.value;
                        if (value === maxValue && value !== 0) {
                            value = '<b>' + value + '</b>';
                        }

                        tr.find('#round-game-player-td-' + gid + '-' + k).html(value);
                    }
                }

                table.dataTable({
                    'paging':   false,
                    'info':     false,
                    'filter':   false,
                    'language': {
                        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
                    },
                    'columnDefs': [
                        {
                            'targets': 0,
                            'orderDataType': 'sortby'
                        },
                        {
                            'targets': [1,2,3,4,5,6,7,8,9,10],
                            'orderSequence': ["desc", "asc"]
                        }
                    ]
                });
            }
        });
    }
});

// Filter teams on round view
$('#filter-team').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $('.round-game').show().filter(function() {
        var text = $(this).find('.round-game-head').find('.round-game-team').text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

// Focus first number on jegyzokony form
if (loc.indexOf('jatekos-statisztika/szerkesztes') > -1) {
    function focusFirstNumber() {
        $(':input[type="number"]').first().focus().select();
    }

    focusFirstNumber();
    setTimeout(focusFirstNumber, 1000);
}

// Hide stuff if it's a playoff round
function playOffs() {
    if ($('#isplayoff').length) {
        $('#orderPlayoffButton').css("visibility", "visible");
    }

    if (typeof isPlayoffs !== 'undefined' && isPlayoffs) {
        $('#content').addClass('rajatszas');
        $('#filter-teams').hide();
    }

    $('.pagination').find('a, span').each(function (aelem) {
        var t = $(this).text();

        if (t === '13' || t === '14' || t === '15' || t === '16' || t === '17') {
            $(this).addClass('rajatszas');
        }
    })
}

playOffs();

function stickyHeader() {
    var stickyHeader = $('.sticky-header'),
        top = 75;

    if (stickyHeader.length) {
        if (window.innerWidth < 1000) {
            top = 54;
        }

        stickyHeader.floatThead({top: top});
    }
}

stickyHeader();