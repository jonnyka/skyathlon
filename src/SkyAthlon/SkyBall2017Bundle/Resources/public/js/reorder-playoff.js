$('#sortable').sortable().disableSelection();

$('#reorder').click(function () {
    var ids = [];

    $(this).prop('disabled', true);

    $('li.ui-sortable-handle').each(function () {
        ids.push($(this).attr('id'));
    });

    $.ajax({
        url: Routing.generate('skyball2017_reorder_playoff_save'),
        type: 'POST',
        data: {data: ids},
        success: function (data) {
            window.location.href = Routing.generate('skyball2017_round_redirect');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $(this).prop('disabled', false);
            console.log('Error: ' + errorThrown);
        }
    });
});