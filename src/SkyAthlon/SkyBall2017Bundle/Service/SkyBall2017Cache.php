<?php

namespace SkyAthlon\SkyBall2017Bundle\Service;

use AppBundle\Entity\Settings;
use AppBundle\Service\WebSocket;
use Doctrine\ORM\Query;
use SkyAthlon\SkyBall2017Bundle\Entity\Game;
use SkyAthlon\SkyBall2017Bundle\Entity\PlayerResult;
use SkyAthlon\SkyBall2017Bundle\Entity\Round;
use SkyAthlon\SkyBall2017Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Predis\Client as Redis;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class SkyBall2017Cache extends Controller
{
    protected $em;
    protected $redis;
    protected $teams;
    protected $games;
    protected $round;
    protected $roundObj;
    protected $roundTeams;
    protected $currentGameId;
    protected $currentRoundId;
    protected $currentType;
    protected $numbers;
    protected $playerService;
    protected $translator;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param Redis $redis
     * @param SkyBall2017Player $playerService
     * @param Translator $translator
     */
    public function __construct(EntityManager $em, Redis $redis, SkyBall2017Player $playerService, Translator $translator)
    {
        $this->em = $em;
        $this->teams = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->getTeamsForApi();
        $this->games = array();
        $this->round = NULL;
        $this->roundObj = NULL;
        $this->roundTeams = NULL;
        $this->redis = $redis;
        $this->playerService = $playerService;
        $this->translator = $translator;
        $this->currentGameId = 0;
        $this->currentRoundId = 1;
        $this->currentType = 'NUL';

        $settingsRepo = $em->getRepository('AppBundle:Settings');

        $roundRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round');
        $currentRound = $settingsRepo->findOneBy(array(
            'name' => 'skyball2017_currentRound',
        ));
        if ($currentRound) {
            $this->currentRoundId = (int)$currentRound->getValue();

            $this->round = $roundRepo->getRoundForApi($this->currentRoundId);
            if ($this->round) {
                $this->roundObj = $roundRepo->findOneBy(array('number' => $this->currentRoundId));
                $this->roundTeams = $this->getRoundTeams($this->roundObj);
                $this->games = $this->round['games'];
            }

            $currentGame = $settingsRepo->findOneBy(array(
                'name' => 'skyball2017_currentGame',
            ));
            if ($currentGame) {
                $this->currentGameId = (int)$currentGame->getValue();

                /** @var Game $game */
                $game = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->find($this->currentGameId);
                $type = $game->getGameType()->getComputerName();
                $typeId = $this->getMainTypeFromName($type);
                $this->currentType = $typeId;
                $this->setDashboard($typeId);
            }
        }

        $raj = 25;
        if ($this->currentRoundId === 13) {
            $raj = 25;
        }
        if ($this->currentRoundId === 14) {
            $raj = 20;
        }
        if ($this->currentRoundId === 15) {
            $raj = 21;
        }
        if ($this->currentRoundId === 16) {
            $raj = 14;
        }
        if ($this->currentRoundId === 17) {
            $raj = 9;
        }

        $rajt = 10;
        if ($this->currentRoundId === 13) {
            $rajt = 10;
        }
        if ($this->currentRoundId === 14) {
            $rajt = 8;
        }
        if ($this->currentRoundId === 15) {
            $rajt = 6;
        }
        if ($this->currentRoundId === 16) {
            $rajt = 4;
        }
        if ($this->currentRoundId === 17) {
            $rajt = 2;
        }

        $this->numbers = array(
            'game' => array(
                'RAJ' => $raj,
                'NUL' => 6,
                'ES'  => 6,
                'S'   => 12,
                'N'   => 4,
                'KD'  => 3,
                'ND'  => 2,
                'D'   => 1,
            ),
            'team' => array(
                'RAJ' => $rajt,
                'NUL' => 12,
                'ES'  => 6,
                'S'   => 12,
                'N'   => 8,
                'KD'  => 6,
                'ND'  => 4,
                'D'   => 2,
            ),
        );
    }

    public function recalcGAndSP() {
        $em = $this->em;
        $games = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->findAll();
        if ($games) {
            $prRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult');
            $goal = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array(
                'computerName' => 'G',
            ));
            $sft = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array(
                'computerName' => 'SP',
            ));
            foreach ($games as $game) {
                if (strpos($game->getGameType()->getComputerName(), 'eloselejtezo') === false) {
                    $playerResults = $game->getPlayerResults();

                    foreach (array('A', 'B') as $letter) {
                        /** @var Team $team */
                        if ($letter === 'A') {
                            $team = $game->getTeamA();
                        }
                        else {
                            $team = $game->getTeamB();
                        }
                        if ($team) {
                            $players = $team->getPlayers();
                            foreach ($players as $player) {
                                $p = 0;
                                $sf = 0;
                                /** @var PlayerResult $playerResult */
                                foreach ($playerResults as $playerResult) {
                                    $n = $playerResult->getKerchiefType()->getComputerName();
                                    if ($playerResult->getPlayer()->getId() == $player->getId()) {
                                        if ($n === 'A') {
                                            $p += ($playerResult->getValue());
                                            $sf += $playerResult->getValue();
                                        }
                                        if ($n === 'C') {
                                            $p += ($playerResult->getValue() * 2);
                                            $sf += $playerResult->getValue();
                                        }
                                        if ($n === 'S') {
                                            $p += ($playerResult->getValue() * 3);
                                            $sf += $playerResult->getValue();
                                        }
                                        if ($n === 'Y') {
                                            $p += ($playerResult->getValue() * 4);
                                            $sf += $playerResult->getValue();
                                        }
                                        if ($n === 'GK') {
                                            $sf += $playerResult->getValue();
                                        }
                                        if ($n === 'GKI') {
                                            $sf += $playerResult->getValue();
                                        }
                                    }
                                }

                                $sfResult = $prRepo->findBy(array(
                                    'player' => $player,
                                    'kerchiefType' => $sft,
                                    'game' => $game,
                                ));
                                if ($sfResult) {
                                    foreach ($sfResult as $sfR) {
                                        $em->remove($sfR);
                                    }
                                }
                                $sfResult = new PlayerResult();
                                $sfResult->setGame($game);
                                $sfResult->setKerchiefType($sft);
                                $sfResult->setPlayer($player);
                                $sfResult->setRound($game->getRound());

                                $sfResult->setValue($sf);
                                $em->persist($sfResult);

                                $goalResult = $prRepo->findOneBy(array(
                                    'player' => $player,
                                    'kerchiefType' => $goal,
                                    'game' => $game,
                                ));
                                if ($goalResult) {
                                    $em->remove($goalResult);
                                }
                                $goalResult = new PlayerResult();
                                $goalResult->setGame($game);
                                $goalResult->setKerchiefType($goal);
                                $goalResult->setPlayer($player);
                                $goalResult->setRound($game->getRound());

                                $goalResult->setValue($p);
                                $em->persist($goalResult);
                            }
                        }
                    }
                }
            }

            $em->flush();
        }
    }

    public function calculateTeamPoints() {
        $em = $this->em;

        $teamsArray = array();
        $teamObjects = array();
        $teams = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->findAll();
        foreach ($teams as $team) {
            $teamsArray[$team->getId()] = array(
                'computerName' => $team->getComputerName(),
                'name'         => $team->getName(),
                'point'        => 0,
                'g'            => 0,
                'q'            => 0,
                'ga'           => 0,
                'kq'           => 0,
            );
            $teamObjects[$team->getId()] = $team;
        }

        $rounds = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->getNonPlayoffRoundsQuery()->getResult();

        if ($rounds) {
            /** @var Round $round */
            foreach ($rounds as $round) {
                /** @var Game $game */
                foreach ($round->getGames() as $game) {
                    if ($game->getGameLength()) {
                        $gameType = $game->getGameType();
                        $gameTypeName = $gameType->getComputerName();
                        $typeId = $this->getMainTypeFromName($gameTypeName);
                        $teamA = $game->getTeamA();
                        $teamB = $game->getTeamB();
                        $pointA = $game->getPointA();
                        $pointB = $game->getPointB();
                        /** @var Team $winner */
                        $winner = $pointA > $pointB ? $teamA : $teamB;
                        $loser  = $pointA < $pointB ? $teamA : $teamB;
                        $winnerTeam = $pointA > $pointB ? 'A' : 'B';

                        if ($teamA && $teamB && is_int($pointA) && is_int($pointB) && $this->isPointEligable($typeId)) {
                            $length = $game->getGameLength()->getComputerName();
                            $point = $this->getScoreFromLength($length);

                            $w = &$teamsArray[$winner->getId()];
                            $l = &$teamsArray[$loser->getId()];

                            $w['point'] += $point;
                            if ($winnerTeam === 'A') {
                                $w['g'] += $pointA;
                                $w['q'] += $game->getQsA();
                                $w['ga'] += ($pointA - $pointB);
                                $w['kq'] += $game->getQgkB();

                                $l['g'] += $pointB;
                                $l['q'] += $game->getQsB();
                                $l['ga'] += ($pointB - $pointA);
                                $l['kq'] += $game->getQgka();
                            }
                            else {
                                $l['g'] += $pointA;
                                $l['q'] += $game->getQsA();
                                $l['ga'] += ($pointA - $pointB);
                                $l['kq'] += $game->getQgkB();

                                $w['g'] += $pointB;
                                $w['q'] += $game->getQsB();
                                $w['ga'] += ($pointB - $pointA);
                                $w['kq'] += $game->getQgka();
                            }
                        }
                    }
                }
            }
        }

        uasort($teamsArray, array($this, 'sortTeams'));

        $i = 0;
        foreach ($teamsArray as $teamId => $t) {
            $i++;

            /** @var Team $team */
            $team = $teamObjects[$teamId];
            $team->setPoint($t['point']);
            $team->setRank($i);
            $em->persist($team);
        }

        $em->flush();
    }

    public function getRoundTeams(Round $round = null, $global = false) {
        $em = $this->em;

        $teamsArray = array();
        $teams = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->findAll();
        foreach ($teams as $team) {
            $teamsArray[$team->getId()] = array(
                'computerName' => $team->getComputerName(),
                'name'         => $team->getName(),
                'point'        => 0,
                'g'            => 0,
                'q'            => 0,
                'ga'           => 0,
                'kq'           => 0,
                'rank'         => $team->getRank(),
            );
        }

        if (!$round) {
            $rounds = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findAll();

            foreach ($rounds as $ro) {
                $teamsArray = $this->getTeamsForRound($teamsArray, $ro);
            }
        }
        else {
            $removeEmpty = false;
            if ($round->getNumber() > 12) {
                $removeEmpty = true;
            }
            $teamsArray = $this->getTeamsForRound($teamsArray, $round, $removeEmpty);
        }

        usort($teamsArray, array($this, 'sortTeams'));
        if (!$round && $global) {
            usort($teamsArray, array($this, 'sortTeamsRank'));
        }

        if (!$global && $round && $round->getPlayoffs() && $this->numbers) {
            $num = 10;
            $rnum = $round->getNumber();

            if ($rnum === 13) {
                $num = 10;
            }
            if ($rnum === 14) {
                $num = 8;
            }
            if ($rnum === 15) {
                $num = 6;
            }
            if ($rnum === 16) {
                $num = 4;
            }
            if ($rnum === 17) {
                $num = 2;
            }

            for ($i = count($teamsArray) - 1; $i >= 0; $i--) {
                if ($i >= $num) {
                    unset($teamsArray[$i]);
                }
            }
        }

        return $teamsArray;
    }

    protected function getTeamsForRound($teamsArray, Round $round, $removeEmpty = false) {
        $teamsInRound = [];

        /** @var Game $game */
        foreach ($round->getGames() as $game) {
            $gameType = $game->getGameType();
            $gameTypeName = $gameType->getComputerName();
            $typeId = $this->getMainTypeFromName($gameTypeName);
            $teamA  = $game->getTeamA();
            $teamB  = $game->getTeamB();
            $pointA = $game->getPointA();
            $pointB = $game->getPointB();
            /** @var Team $winner */
            $winner = $pointA > $pointB ? $teamA : $teamB;
            $loser  = $pointA < $pointB ? $teamA : $teamB;
            $winnerTeam = $pointA > $pointB ? 'A' : 'B';

            if ($teamA) {
                $teamsInRound[] = $teamA->getId();
            }
            if ($teamB) {
                $teamsInRound[] = $teamB->getId();
            }

            if ($teamA && $teamB && is_int($pointA) && is_int($pointB) && $this->isPointEligable($typeId, true)) {
                if ($game->getGameLength()) {
                    $length = $game->getGameLength()->getComputerName();
                    $point = $this->getScoreFromLength($length);

                    $w = &$teamsArray[$winner->getId()];
                    $l = &$teamsArray[$loser->getId()];

                    $w['point'] += $point;
                    if ($winnerTeam === 'A') {
                        $w['g'] += $pointA;
                        $w['q'] += $game->getQsA();
                        $w['ga'] += ($pointA - $pointB);
                        $w['kq'] += $game->getQgkB();

                        $l['g'] += $pointB;
                        $l['q'] += $game->getQsB();
                        $l['ga'] += ($pointB - $pointA);
                        $l['kq'] += $game->getQgka();
                    }
                    else {
                        $l['g'] += $pointA;
                        $l['q'] += $game->getQsA();
                        $l['ga'] += ($pointA - $pointB);
                        $l['kq'] += $game->getQgkB();

                        $w['g'] += $pointB;
                        $w['q'] += $game->getQsB();
                        $w['ga'] += ($pointB - $pointA);
                        $w['kq'] += $game->getQgka();
                    }
                }
            }
        }

        if ($removeEmpty) {
            foreach ($teamsArray as $id => $team) {
                if (!in_array($id, $teamsInRound)) {
                    unset($teamsArray[$id]);
                }
            }
        }

        return $teamsArray;
    }

    public function setPointsFromPlayerResults() {
        $em = $this->em;
        $games = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->findAll();

        foreach ($games as $game) {
            $this->setGamePointsFromPlayerResults($game);
        }

        $em->flush();
    }

    public function setGamePointsFromPlayerResults(Game $game) {
        $em = $this->em;

        if (!$game->isPreGame()) {
            $teamAPoint = $teamBPoint = $teamAQS = $teamBQS = $teamAQGK = $teamBQGK = 0;

            $teamAPlayers = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findBy(array(
                'team' => $game->getTeamA(),
            ));

            $teamAResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
                'player' => $teamAPlayers,
                'game' => $game,
            ));

            /** @var PlayerResult $t */
            foreach ($teamAResults as $t) {
                $n = $t->getKerchiefType()->getComputerName();
                if ($n === 'QS') {
                    $teamAQS += $t->getValue();
                }
                if ($n === 'QF') {
                    $teamAQGK += $t->getValue();
                }
                if ($n === 'G' || $t->getKerchiefType()->getComputerName() === 'QS') {
                    $teamAPoint += $t->getValue();
                }
            }

            $teamBPlayers = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findBy(array(
                'team' => $game->getTeamB(),
            ));

            $teamBResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
                'player' => $teamBPlayers,
                'game' => $game,
            ));
            foreach ($teamBResults as $t) {
                if ($t->getKerchiefType()->getComputerName() === 'QS') {
                    $teamBQS += $t->getValue();
                }
                if ($t->getKerchiefType()->getComputerName() === 'QF') {
                    $teamBQGK += $t->getValue();
                }
                if ($t->getKerchiefType()->getComputerName() === 'G' || $t->getKerchiefType()->getComputerName() === 'QS') {
                    $teamBPoint += $t->getValue();
                }
            }

            $game->setPointA($teamAPoint);
            $game->setPointB($teamBPoint);
            $game->setQsA($teamAQS);
            $game->setQsB($teamBQS);
            $game->setQgkA($teamAQGK);
            $game->setQgkB($teamBQGK);

            $em->persist($game);
        }

        $em->flush();
    }

    protected function gameFinished($game, $light = false) {
        if ($light) {
            return ($game && $game['teamA'] && $game['teamB']);
        }
        else {
            return ($game && $game['gameLength'] && $game['teamA'] && $game['teamB'] && is_int($game['pointA']) && is_int($game['pointB']));
        }
    }

    protected function getScores($game) {
        $ret = array(
            'winner' => array(
                'computerName' => 'questionmark',
                'name'         => '?',
                'point'        => 0,
                'g'            => 0,
                'q'            => 0,
                'ga'           => 0,
                'kq'           => 0,
            ),
            'loser' => array(
                'computerName' => 'questionmark',
                'name'         => '?',
                'point'        => 0,
                'g'            => 0,
                'q'            => 0,
                'ga'           => 0,
                'kq'           => 0,
            ),
        );

        $pointA = $game['pointA'] ? $game['pointA'] : 0;
        $pointB = $game['pointB'] ? $game['pointB'] : 0;
        $teamA  = $game['teamA'];
        $teamB  = $game['teamB'];
        $length = $game['gameLength'];
        $point = $this->getScoreFromLength($length['computerName']);
        if (!$pointA && !$pointB) {
            $point = 0;
        }

        if ($pointA > $pointB || $pointA === $pointB) {
            $ret['winner'] = array(
                'computerName' => $teamA['computerName'],
                'name'         => $teamA['name'],
                'point'        => $point,
                'g'            => $pointA,
                'q'            => $game['qsA'],
                'ga'           => $pointA - $pointB,
                'kq'           => $game['qgkB'],
            );
            $ret['loser'] = array(
                'computerName' => $teamB['computerName'],
                'name'         => $teamB['name'],
                'point'        => 0,
                'g'            => $pointB,
                'q'            => $game['qsB'],
                'ga'           => $pointB - $pointA,
                'kq'           => $game['qgkA'],
            );
        }
        elseif ($pointB > $pointA) {
            $ret['winner'] = array(
                'computerName' => $teamB['computerName'],
                'name'         => $teamB['name'],
                'point'        => $point,
                'g'            => $pointB,
                'q'            => $game['qsB'],
                'ga'           => $pointB - $pointA,
                'kq'           => $game['qgkA'],
            );
            $ret['loser'] = array(
                'computerName' => $teamA['computerName'],
                'name'         => $teamA['name'],
                'point'        => 0,
                'g'            => $pointA,
                'q'            => $game['qsA'],
                'ga'           => $pointA - $pointB,
                'kq'           => $game['qgkB'],
            );
        }

        return $ret;
    }

    protected function getMainTypeFromName($name) {
        $ret = '';

        if (strpos($name, 'rajatszas') !== false) {
            $ret = 'RAJ';
        }
        elseif (strpos($name, 'nulla') !== false) {
            $ret = 'NUL';
        }
        elseif (strpos($name, 'eloselejtezo') !== false) {
            $ret = 'ES';
        }
        elseif (strpos($name, 'selejtezo') !== false) {
            $ret = 'S';
        }
        elseif (strpos($name, 'normal') !== false) {
            $ret = 'N';
        }
        elseif (strpos($name, 'kozepdonto') !== false) {
            $ret = 'KD';
        }
        elseif (strpos($name, 'negyeddonto') !== false) {
            $ret = 'ND';
        }
        elseif (strpos($name, 'donto') !== false) {
            $ret = 'D';
        }

        return $ret;
    }

    protected function isPointEligable($type, $lazy = false) {
        $eligable = array('S', 'N', 'KD', 'ND', 'D');
        if ($lazy) {
            $eligable[] = 'RAJ';
        }

        return in_array($type, $eligable);
    }

    protected function getScoreFromLength($length) {
        $score = 0;

        switch($length) {
            case 'Q1':
                $score = 1;
                break;
            case 'B':
                $score = 1;
                break;
            case 'E':
                $score = 3;
                break;
            case 'Q2':
                $score = 3;
                break;
            case 'S':
                $score = 6;
                break;
            case 'Q3':
                $score = 6;
                break;
            case 'U':
                $score = 10;
                break;
        }

        return $score;
    }

    protected function sortTeams($a, $b) {
        if (array_key_exists('point', $a) && array_key_exists('point', $b)) {
            if ($a['point'] === $b['point']) {
                if ($a['g'] === $b['g']) {
                    if ($a['q'] === $b['q']) {
                        if ($a['ga'] === $b['ga']) {
                            if ($a['kq'] !== $b['kq']) {
                                return ($a['kq'] > $b['kq']) ? -1 : 1;
                            }
                            else {
                                if ($this->teams && array_key_exists($a['computerName'], $this->teams) && array_key_exists($b['computerName'], $this->teams) ) {
                                    return $this->teams[$a['computerName']]['rank'] > $this->teams[$b['computerName']]['rank'] ? 1 : -1;
                                }
                                return ($a['computerName'] > $b['computerName']) ? 1 : -1;
                            }
                        }
                        else {
                            return ($a['ga'] < $b['ga']) ? 1 : -1;
                        }
                    }
                    else {
                        return ($a['q'] < $b['q']) ? 1 : -1;
                    }
                }
                else {
                    return ($a['g'] < $b['g']) ? 1 : -1;
                }
            }
            else {
                return ($a['point'] < $b['point']) ? 1 : -1;
            }
        }

        return 0;
    }

    protected function sortTeamsRank($a, $b) {
        if (array_key_exists('rank', $a) && array_key_exists('rank', $b)) {
            if ($a['rank'] === $b['rank']) {
                return ($a['computerName'] > $b['computerName']) ? 1 : -1;
            }
            else {
                return ($a['rank'] > $b['rank']) ? 1 : -1;
            }
        }

        return 0;
    }

    protected function sortRefsByPoint($a, $b) {
        if ($a['point'] === $b['point']) {
            if ($a['avg'] === $b['avg']) {
                return ($a['number'] > $b['number']) ? 1 : -1;
            }
            else {
                return ($a['point'] < $b['point']) ? 1 : -1;
            }
        }
        else {
            return ($a['point'] < $b['point']) ? 1 : -1;
        }
    }

    protected function sortRefsByAvg($a, $b) {
        if ($a['avg'] === $b['avg']) {
            if ($a['point'] === $b['point']) {
                return ($a['number'] > $b['number']) ? 1 : -1;
            }
            else {
                return ($a['avg'] < $b['avg']) ? 1 : -1;
            }
        }
        else {
            return ($a['avg'] < $b['avg']) ? 1 : -1;
        }
    }

    protected function getGames($types = array(), $round = false) {
        $ret = array();
        $games = $this->games;
        if ($round) {
            $games = $round['games'];
        }

        if ($games) {
            if (!$types) {
                $ret = $games;
            }
            else {
                foreach ($games as $gameType => $game) {
                    $typeId = $this->getMainTypeFromName($gameType);
                    if (in_array($typeId, $types)) {
                        $ret[$gameType] = $game;
                    }
                }
            }
        }

        return $ret;
    }

    protected function getNumberOfGamesPerType($type = 'S') {
        return $this->numbers['game'][$type];
    }

    protected function getNumberOfRanksPerType($type = 'S') {
        return $this->numbers['team'][$type];
    }

    protected function createDummyCurrentGamesForType($type = 'S') {
        $ret = array();
        $numberOfGames = $this->getNumberOfGamesPerType($type);
        $groupTitle = null;

        for ($i = 0; $i < $numberOfGames; $i++) {
            if ($i === 0) {
                $groupTitle = '?';
            }

            $ret['z' . $i] = array(
                'gameLength' => array('name' => '?'),
                'gameType'   => array('computerName' => '?', 'name' => '?', 'groupTitle' => $groupTitle),
                'pointA' => '?',
                'pointB' => '?',
                'qgkA'   => '?',
                'qgkB'   => '?',
                'qsA'    => '?',
                'qsB'    => '?',
                'ref'    => null,
                'sref'   => null,
                'jref'   => null,
                'srefb'  => null,
                'weight' => 0,
                'teamA'  => array('computerName' => 'questionmark', 'name' => '?'),
                'teamB'  => array('computerName' => 'questionmark', 'name' => '?'),
            );

            $groupTitle = null;
        }

        return $ret;
    }

    protected function createDummyGamesForType($type = 'S', $num = null) {
        $ret = array();
        $numberOfGames = $this->getNumberOfGamesPerType($type);
        if ($num) {
            $numberOfGames = $num;
        }

        for ($i = 0; $i < $numberOfGames; $i++) {
            $ret[$i] = array(
                'teamA'  => array('computerName' => 'questionmark', 'name' => '?'),
                'teamB'  => array('computerName' => 'questionmark', 'name' => '?'),
                'ref'    => null,
                'sref'   => null,
                'jref'   => null,
                'srefb'  => null,
            );
        }

        return $ret;
    }

    protected function createDummyRanksForType($type = 'S') {
        $ret = array();
        $numberOfRanks = $this->getNumberOfRanksPerType($type);

        for ($i = 1; $i <= $numberOfRanks; $i++) {
            $ret['z' . $i] = array(
                'computerName' => 'questionmark',
                'name'         => '?',
                'rank'         => $i,
                'point'        => '?',
                'g'            => 0,
                'q'            => 0,
                'ga'           => 0,
                'kq'           => 0,
                'groupRank'    => '?',
            );
        }

        return $ret;
    }

    protected function pairTeamsForGames(&$ret, $ranks, $reversed = false) {
        if (!$reversed) {
            for ($i = 0; $i < count($ret); $i++) {
                if ($ranks[$i]) {
                    $ret[$i]['teamA'] = array(
                        'computerName' => $ranks[$i]['computerName'],
                        'name' => $ranks[$i]['name'],
                    );
                }
            }

            $teamNum = count($ret) * 2 - 1;
            $j = 0;
            for ($i = $teamNum; $i >= count($ret); $i--) {
                if (array_key_exists($i, $ranks)) {
                    $ret[$j]['teamB'] = array(
                        'computerName' => $ranks[$i]['computerName'],
                        'name' => $ranks[$i]['name'],
                    );
                }

                $j++;
            }
        }
        else {
            $j = 0;
            for ($i = count($ret) - 1; $i >= 0; $i--) {
                if ($ranks[$i]) {
                    $ret[$i]['teamA'] = array(
                        'computerName' => $ranks[$j]['computerName'],
                        'name' => $ranks[$j]['name'],
                    );
                    $j++;
                }
            }

            $teamNum = count($ret) * 2 - 1;
            $j = 0;
            for ($i = count($ret); $i <= $teamNum; $i++) {
                if (array_key_exists($i, $ranks)) {
                    $ret[$j]['teamB'] = array(
                        'computerName' => $ranks[$i]['computerName'],
                        'name' => $ranks[$i]['name'],
                    );
                }

                $j++;
            }
        }

        return $ret;
    }

    protected function shuffleStable($a) {
        $ret = array();

        if (count($a) === 12) {
            $sorrend = array(
                '1'  => 1,
                '2'  => 2,
                '3'  => 3,
                '4'  => 4,
                '5'  => 8,
                '6'  => 7,
                '7'  => 6,
                '8'  => 5,
                '9'  => 11,
                '10' => 9,
                '11' => 10,
                '12' => 12
            );
            
            foreach ($sorrend as $rank => $realRank) {
                $ret[$rank] = array_key_exists($realRank, $a) ? $a[$realRank] : $a['z' . $realRank];
            }
        }
        else {
            $ret = $a;
        }

        return $ret;
    }

    protected function getGroup($type) {
        $group = 'a';

        if (strpos($type, '_b') !== false) {
            $group = 'b';
        }
        elseif (strpos($type, '_c') !== false) {
            $group = 'c';
        }
        elseif (strpos($type, '_d') !== false) {
            $group = 'd';
        }

        return $group;
    }

    protected function createInitialTeams() {
        $em = $this->em;
        $teamsArray = array();
        $teams = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->findAll();
        foreach ($teams as $team) {
            $teamsArray[$team->getComputerName()] = array(
                'computerName' => $team->getComputerName(),
                'name'         => $team->getName(),
                'point'        => 0,
                'g'            => 0,
                'q'            => 0,
                'ga'           => 0,
                'kq'           => 0,
                'ishere'       => false,
            );
        }

        return $teamsArray;
    }

    protected function loadPreviousGames(&$teamsArray, $type) {
        if ($type) {
            $games = $this->getGames(array($type));

            if ($games) {
                foreach ($games as $gid => $game) {
                    if ($this->gameFinished($game, true)) {
                        $scores = $this->getScores($game);
                        $winner = $scores['winner'];
                        $loser = $scores['loser'];
                        $wc = $winner['computerName'];
                        $lc = $loser['computerName'];
                        $w = &$teamsArray[$wc];
                        $l = &$teamsArray[$lc];

                        $w['point'] += $winner['point'];
                        $w['g'] += $winner['g'];
                        $w['q'] += $winner['q'];
                        $w['ga'] += $winner['ga'];
                        $w['kq'] += $winner['kq'];

                        $l['point'] += $loser['point'];
                        $l['g'] += $loser['g'];
                        $l['q'] += $loser['q'];
                        $l['ga'] += $loser['ga'];
                        $l['kq'] += $loser['kq'];

                        if (in_array($type, array('N', 'KD', 'ND', 'D'))) {
                            $w['ishere'] = true;
                            $l['ishere'] = true;
                        }
                    }
                }
            }
        }
    }

    protected function setDashboard($type) {
        $em = $this->em;
        /** @var Settings $currentDashboardObject */
        $currentDashboardObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
            'name' => 'currentDashboard',
        ));
        if (!$currentDashboardObject) {
            $currentDashboardObject = new Settings();
            $currentDashboardObject->setName('currentDashboard');
        }

        $dashboard = 'skyball2017_live_' . strtolower($type);
        $currentDashboardObject->setValue($dashboard);

        $em->persist($currentDashboardObject);
        $em->flush();

        $ws = new WebSocket(array(
            'host' => $this->getParameter('skyathlon_url'),
            'port' => 8080,
        ));
        $ws->send($dashboard);
        $ws->close();
    }

    /**
     * @param string $type
     * @param $array
     */
    protected function setRedis($type = 'ES', $array) {
        $redis = $this->redis;

        $settings = array(
            'currentRoundId' => $this->currentRoundId,
            'currentGameId'  => $this->currentGameId,
            'currentType'    => $this->currentType,
        );

        foreach ($array as $key => $value) {
            $redis->set('livestats.' . $type . '.' . $key, json_encode($value));
        }

        $redis->set('settings', json_encode($settings));
    }

    protected function setGlobalRedis() {
        $redis = $this->redis;

        $redis->set('livestats.refsByPoint', json_encode($this->getRefs('ref')));
        $redis->set('livestats.srefsByPoint', json_encode($this->getRefs('sref')));
        $redis->set('livestats.refsByAvg', json_encode($this->getRefs('ref', 'Avg')));
        $redis->set('livestats.srefsByAvg', json_encode($this->getRefs('sref', 'Avg')));

        $redis->set('livestats.kerchiefsGlobal', json_encode($this->getKerchiefs()));
        $redis->set('livestats.kerchiefsRound', json_encode($this->getKerchiefs($this->currentRoundId)));

        $redis->set('livestats.teamsGlobal', json_encode($this->getTeams(false, true)));
        $redis->set('livestats.teamsRound', json_encode($this->getTeams($this->currentRoundId)));

        $redis->set('livestats.currentRound', json_encode($this->getCurrentRound()));

        $this->setRanks();
    }

    protected function setRanks() {
        $teams = array(
            'myrmidons',
            'musketasok',
            'regnum',
            'vikings',
            'knights',
            'myrmidonswarriors',
            'toreadors',
            'dubelz',
            'predators',
            'hunters',
            'agapeus',
            'meteors',
            'wildgiants',
            'aliens',
            'liberty',
            'rangers',
            'phoenix',
            'explorers',
        );

        $em = $this->em;
        $teamobjects = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->getTeamsForApi(Query::HYDRATE_OBJECT);

        $i = 1;
        foreach ($teams as $team) {
            $teamobjects[$team]->setRank($i);
            $em->persist($teamobjects[$team]);
            $i++;
        }

        $em->flush();
    }

    protected function getCurrentRound() {
        $round = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->getRound($this->currentRoundId);
        if ($round) {
            foreach ($round['games'] as &$game) {
                $gt = &$game['gameType'];
                if ($gt['groupTitle']) {
                    $gt['groupTitleTrans'] = $this->translator->trans('skyball2017.' . $gt['groupTitle']);
                }
            }
        }

        return $round;
    }

    protected function getTeams($round = false, $global = false) {
        if (!$round) {
            $teams = $this->getRoundTeams(null, $global);
        }
        else {
            $teams = $this->getRoundTeams($this->roundObj);
        }

        return $teams;
    }

    protected function getKerchiefs($round = false) {
        $em = $this->em;
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->getKerchiefTypes();

        $playerService = $this->playerService;

        if ($round) {
            $kerchiefs = $playerService->getRoundKerchiefsJson($this->roundObj);
        }
        else {
            $kerchiefs = $playerService->getAllGlobalKerchiefsJson();
        }

        $players = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->getPlayers();

        $ret = array(
            'kerchiefs' => $kerchiefs,
            'kcTypes'   => $kcTypes,
            'players'   => $players,
        );

        return $ret;
    }

    protected function getRefs($type = 'ref', $order = 'Point') {
        $refs = $this->playerService->getRefs();
        $refArr = array();
        foreach ($refs as $rid => $ref) {
            $refArr[$rid] = array(
                'name'    => $ref['name'],
                'number'  => $ref['number'],
                'sex'     => $ref['sex'],
                'point'   => array_key_exists($type, $ref) ? $ref[$type]['point'] : 0,
                'rnumber' => array_key_exists($type, $ref) ? $ref[$type]['number'] : 0,
                'avg'     => array_key_exists($type, $ref) ? $ref[$type]['avg'] : 0,
            );
        }

        usort($refArr, array($this, 'sortRefsBy' . $order));

        return $refArr;
    }

    public function setAllRedisData() {
        $types = array('RAJ', 'NUL', 'ES', 'S', 'N', 'KD', 'ND', 'D');

        foreach ($types as $type) {
            $this->currentType = $type;

            switch ($type) {
                case 'RAJ':
                    $this->getDataForRAJ();
                    break;
                case 'NUL':
                    $this->getDataForNUL();
                    break;
                case 'ES':
                    $this->getDataForES();
                    break;
                case 'S':
                    $this->getDataForS();
                    break;
                case 'N':
                    $this->getDataForN();
                    break;
                case 'KD':
                    $this->getDataForKD();
                    break;
                case 'ND':
                    $this->getDataForND();
                    break;
                case 'D':
                    $this->getDataForD();
                    break;
            }
        }

        $this->setGlobalRedis();
    }

    /**
     * @param Game $game
     *
     * @return array
     */
    public function getCurrentData($game = null) {
        $ret = array();

        if (!$game) {
            $game = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->find($this->currentGameId);
        }

        /** @var Game $game */
        if ($game) {
            $type = $game->getGameType()->getComputerName();
            $typeId = $this->getMainTypeFromName($type);
            $this->currentType = $typeId;
            $this->setDashboard($typeId);

            switch($typeId) {
                case 'RAJ':
                    $ret = $this->getDataForRAJ();
                    break;
                case 'NUL':
                    $ret = $this->getDataForNUL();
                    break;
                case 'ES':
                    $ret = $this->getDataForES();
                    break;
                case 'S':
                    $ret = $this->getDataForS();
                    break;
                case 'N':
                    $ret = $this->getDataForN();
                    break;
                case 'KD':
                    $ret = $this->getDataForKD();
                    break;
                case 'ND':
                    $ret = $this->getDataForND();
                    break;
                case 'D':
                    $ret = $this->getDataForD();
                    break;
            }
        }

        $this->calculateTeamPoints();
        $this->setGlobalRedis();

        return $ret;
    }

    protected function getDataForRAJ() {
        $raj = 4;
        if ($this->currentRoundId === 13) {
            $raj = 4;
        }
        if ($this->currentRoundId === 14) {
            $raj = 3;
        }
        if ($this->currentRoundId === 15) {
            $raj = 2;
        }
        if ($this->currentRoundId === 16) {
            $raj = 1;
        }
        if ($this->currentRoundId === 17) {
            $raj = 1;
        }

        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('RAJ'),
            'RAJs'  => $this->createDummyRanksForType('RAJ'),
            'nRAJm' => $this->createDummyGamesForType('RAJ', $raj),
            'ended' => false,
        );
        $vegleges = false;

        $teamsArray = $this->createInitialTeams();

        $games = $this->getGames();
        if ($games) {
            $i = 0;
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }

                    $scores = $this->getScores($game);
                    $winner = $scores['winner'];
                    $loser = $scores['loser'];
                    $wc = $winner['computerName'];
                    $lc = $loser['computerName'];
                    $w = &$teamsArray[$wc];
                    $l = &$teamsArray[$lc];

                    $w['point'] += $winner['point'];
                    $w['g'] += $winner['g'];
                    $w['q'] += $winner['q'];
                    $w['ga'] += $winner['ga'];
                    $w['kq'] += $winner['kq'];
                    $w['ishere'] = true;

                    $l['point'] += $loser['point'];
                    $l['g'] += $loser['g'];
                    $l['q'] += $loser['q'];
                    $l['ga'] += $loser['ga'];
                    $l['kq'] += $loser['kq'];
                    $l['ishere'] = true;
                }
            }

            if ($i >= $this->numbers['game']['RAJ'] && $allFinished) {
                $vegleges = true;
            }

            $i = 1;
            foreach ($teamsArray as $tname => $t) {
                if ($t['ishere']) {
                    unset($ret['RAJs']['z' . $i]);
                    $ret['RAJs'][$tname] = $t;
                    $i++;
                }
            }

            usort($ret['RAJs'], array($this, 'sortTeams'));
        }

        $this->pairTeamsForGames($ret['nRAJm'], $ret['RAJs'], true);

        $ret['ended'] = $vegleges;

        $this->setRedis('RAJ', $ret);

        return $ret;
    }

    protected function getDataForNUL() {
        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('NUL'),
            'ESs'   => $this->createDummyRanksForType('ES'),
            'ESm'   => $this->createDummyGamesForType('ES'),
            'St'    => $this->createDummyRanksForType('S'),
            'ended' => false,
        );
        $vegleges = false;

        for ($i = 1; $i <= 6; $i++) {
            $ret['St'][$i] = $ret['St']['z' . $i];
            unset($ret['St']['z' . $i]);
        }
        $ret['St'][1]['computerName'] = 'myrmidons';
        $ret['St'][1]['name'] = 'Myrmidons';
        $ret['St'][2]['computerName'] = 'wildgiants';
        $ret['St'][2]['name'] = 'Wild Giants';
        $ret['St'][3]['computerName'] = 'vikings';
        $ret['St'][3]['name'] = 'Vikings';
        $ret['St'][4]['computerName'] = 'dubelz';
        $ret['St'][4]['name'] = 'Dübelz';
        $ret['St'][5]['computerName'] = 'regnum';
        $ret['St'][5]['name'] = 'Regnum';
        $ret['St'][6]['computerName'] = 'meteors';
        $ret['St'][6]['name'] = 'Meteors';

        $games = $this->getGames(array('NUL'));
        if ($games) {
            $i = 0;
            $winners = array();
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;
                    unset($ret['ESs']['z' . $i]);

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }

                    $scores = $this->getScores($game);
                    $ret['ESs'][$i] = $scores['loser'];
                    $winners[$i] = $scores['winner'];
                }
            }

            if ($i >= $this->getNumberOfGamesPerType('NUL') && $allFinished) {
                $vegleges = true;
            }

            usort($ret['ESs'], array($this, 'sortTeams'));
            usort($winners, array($this, 'sortTeams'));
            $this->pairTeamsForGames($ret['ESm'], $ret['ESs']);

            $j = 7;
            foreach ($winners as $winner) {
                unset($ret['St']['z' . $j]);

                $ret['St'][$j] = $winner;
                $j++;
            }

            $ret['St'] = $this->shuffleStable($ret['St']);
        }

        $ret['ended'] = $vegleges;

        $this->setRedis('NUL', $ret);

        return $ret;
    }

    protected function getDataForES() {
        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('ES'),
            'ESs'   => $this->createDummyRanksForType('ES'),
            'ESw'   => array(),
            'ESl'   => array(),
            'ended' => false,
        );
        $vegleges = false;

        $teamsArray = $this->createInitialTeams();

        for ($i = 1; $i <= $this->numbers['team']['ES']; $i++) {
            if ($i <= 4) {
                $ret['ESw'][$i] = $ret['ESs']['z' . $i];
            }
            else {
                $ret['ESl'][$i] = $ret['ESs']['z' . $i];
            }
        }

        $games = $this->getGames(array('ES'));
        if ($games) {
            $i = 0;
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }

                    $scores = $this->getScores($game);
                    $winner = $scores['winner'];
                    $loser = $scores['loser'];
                    $wc = $winner['computerName'];
                    $lc = $loser['computerName'];
                    $w = &$teamsArray[$wc];
                    $l = &$teamsArray[$lc];

                    $w['point'] += $winner['point'];
                    $w['g'] += $winner['g'];
                    $w['q'] += $winner['q'];
                    $w['ga'] += $winner['ga'];
                    $w['kq'] += $winner['kq'];
                    $w['ishere'] = true;

                    $l['point'] += $loser['point'];
                    $l['g'] += $loser['g'];
                    $l['q'] += $loser['q'];
                    $l['ga'] += $loser['ga'];
                    $l['kq'] += $loser['kq'];
                    $l['ishere'] = true;
                }
            }

            if ($i >= $this->numbers['game']['ES'] && $allFinished) {
                $vegleges = true;
            }

            $i = 1;
            foreach ($teamsArray as $tname => $t) {
                if ($t['ishere']) {
                    unset($ret['ESs']['z' . $i]);
                    $ret['ESs'][$tname] = $t;
                    $i++;
                }
            }

            usort($ret['ESs'], array($this, 'sortTeams'));

            for ($i = 0; $i < count($ret['ESs']); $i++) {
                $j = $i + 1;
                if ($i < 4) {
                    $ret['ESw'][$j] = $ret['ESs'][$i];
                }
                elseif ($i >= 4 && $i < 6) {
                    $ret['ESl'][$j] = $ret['ESs'][$i];
                }
            }
        }

        $ret['ended'] = $vegleges;

        $this->setRedis('ES', $ret);

        return $ret;
    }

    protected function getDataForS() {
        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('S'),
            'Ss'    => $this->createDummyRanksForType('S'),
            'St'    => $this->createDummyRanksForType('S'),
            'ESs'   => $this->createDummyRanksForType('ND'),
            'Nm'    => $this->createDummyGamesForType('N'),
            'ended' => false,
        );
        $vegleges = false;
        $teamsArray = $this->createInitialTeams();

        if ($this->currentRoundId === 1) {
            $ranks = array(
                '1'  => 'myrmidons',
                '2'  => 'vikings',
                '3'  => 'knights',
                '4'  => 'regnum',
                '5'  => 'dubelz',
                '6'  => 'musketasok',
                '7'  => 'toreadors',
                '8'  => 'hunters',
                '9'  => 'predators',
                '10' => 'wildgiants',
                '11' => 'meteors',
                '12' => 'myrmidonswarriors',
            );
            $r = &$ret['St'];

            for ($i = 1; $i <= 12; $i++) {
                unset($r['z' . $i]);
                $r[$i] = $teamsArray[$ranks[$i]];
            }
        }
        elseif ($this->currentRoundId > 1) {
            $lastRoundId = $this->currentRoundId - 1;
            $repo = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Round');
            $ro = $repo->findOneBy(array('number' => $lastRoundId));
            $teams = $this->getRoundTeams($ro);
            usort($teams, array($this, 'sortTeams'));
            $r = &$ret['St'];

            for ($i = 0; $i < 8; $i++) {
                $j = $i + 1;
                unset($r['z' . $j]);
                $r[$j] = $teams[$i];
            }
            
            $esw = $this->createDummyRanksForType('ND');
            
            $roGames = $repo->getRoundForApi($lastRoundId);
            $esGames = $this->getGames(array('ES'), $roGames);
            $esTeamsArray = $this->createInitialTeams();

            if ($esGames) {
                foreach ($esGames as $gid => $game) {
                    if ($this->gameFinished($game, true)) {
                        $scores = $this->getScores($game);
                        $winner = $scores['winner'];
                        $loser = $scores['loser'];
                        $wc = $winner['computerName'];
                        $lc = $loser['computerName'];
                        $w = &$esTeamsArray[$wc];
                        $l = &$esTeamsArray[$lc];

                        $w['point'] += $winner['point'];
                        $w['g'] += $winner['g'];
                        $w['q'] += $winner['q'];
                        $w['ga'] += $winner['ga'];
                        $w['kq'] += $winner['kq'];
                        $w['ishere'] = true;

                        $l['point'] += $loser['point'];
                        $l['g'] += $loser['g'];
                        $l['q'] += $loser['q'];
                        $l['ga'] += $loser['ga'];
                        $l['kq'] += $loser['kq'];
                        $l['ishere'] = true;
                    }
                }

                usort($esTeamsArray, array($this, 'sortTeams'));
                for ($i = 0; $i < 4; $i++) {
                    $j = $i + 1;
                    unset($esw['z' . $j]);
                    $esw[$i] = $esTeamsArray[$i];
                }

                usort($esw, array($this, 'sortTeams'));
            }

            $i = 8;
            foreach ($esw as $eswinner) {
                $i++;
                unset($r['z' . $i]);
                $r[$i] = $eswinner;
            }
        }

        $ret['St'] = $this->shuffleStable($ret['St']);

        $a = $b = $c = $d = array();
        $af = $bf = $cf = $df = 0;
        $games = $this->getGames(array('S'));
        if ($games) {
            $i = 0;
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;

                    $group = $this->getGroup($gid);

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }
                    else {
                        ${$group . 'f'}++;
                    }

                    $scores = $this->getScores($game);
                    $winner = $scores['winner'];
                    $loser = $scores['loser'];
                    $wc = $winner['computerName'];
                    $lc = $loser['computerName'];
                    $w = &$teamsArray[$wc];
                    $l = &$teamsArray[$lc];

                    if (!array_key_exists($wc, $$group)) {
                        ${$group}[$wc] = $winner;
                    }
                    $gw = &${$group}[$wc];

                    $w['point'] += $winner['point'];
                    $w['g'] += $winner['g'];
                    $w['q'] += $winner['q'];
                    $w['ga'] += $winner['ga'];
                    $w['kq'] += $winner['kq'];
                    $w['ishere'] = true;

                    $gw['point'] = $w['point'];
                    $gw['g'] = $w['g'];
                    $gw['q'] = $w['q'];
                    $gw['ga'] = $w['ga'];
                    $gw['kq'] = $w['kq'];

                    if (!array_key_exists($lc, $$group)) {
                        ${$group}[$lc] = $loser;
                    }
                    $gl = &${$group}[$lc];

                    $l['point'] += $loser['point'];
                    $l['g'] += $loser['g'];
                    $l['q'] += $loser['q'];
                    $l['ga'] += $loser['ga'];
                    $l['kq'] += $loser['kq'];
                    $l['ishere'] = true;

                    $gl['point'] = $l['point'];
                    $gl['g'] = $l['g'];
                    $gl['q'] = $l['q'];
                    $gl['ga'] = $l['ga'];
                    $gl['kq'] = $l['kq'];
                }
            }

            if ($i >= $this->numbers['game']['S'] && $allFinished) {
                $vegleges = true;
            }

            usort($a, array($this, 'sortTeams'));
            usort($b, array($this, 'sortTeams'));
            usort($c, array($this, 'sortTeams'));
            usort($d, array($this, 'sortTeams'));

            $i = 1;
            foreach ($teamsArray as $tname => $t) {
                if ($t['ishere']) {
                    unset($ret['Ss']['z' . $i]);
                    $ret['Ss'][$tname] = $t;
                    $i++;
                }
            }

            if (count($a) === 3 && count($b) === 3 && count($c) === 3 && count($d) === 3) {
                $ret['ESs']['z1'] = $a[2];
                $ret['ESs']['z2'] = $b[2];
                $ret['ESs']['z3'] = $c[2];
                $ret['ESs']['z4'] = $d[2];

                $j = 1;
                for ($i = 'a'; $i <= 'd'; $i++) {
                    $wi = ${$i}[0]['computerName'];
                    if ($wi !== '?') {
                        unset($ret['Ss']['z' . $j]);
                        $j++;
                        if (${$i . 'f'} === 3) {
                            $ret['Ss'][$wi]['groupRank'] = 'CS.E.';
                        }
                    }
                    $li = ${$i}[2]['computerName'];
                    if ($li !== '?') {
                        unset($ret['Ss']['z' . $j]);
                        $j++;
                        if (${$i . 'f'} === 3) {
                            $ret['Ss'][$li]['groupRank'] = 'CS.U.';
                        }
                    }
                }

                $points = array($a[1], $a[2], $b[1], $b[2], $c[1], $c[2], $d[1], $d[2]);
                usort($points, array($this, 'sortTeams'));

                $i = 1;
                foreach ($points as $point) {
                    $team = $point['computerName'];
                    $rank = &$ret['Ss'][$team]['groupRank'];

                    if ($i < 5 && ($af + $bf + $cf + $df >= 4)) {
                        if ($rank === '?' || !$rank) {
                            $rank = 'P' . $i;
                        } else {
                            $rank .= ' (P' . $i . ')';
                        }
                    }

                    $i++;
                }

                usort($ret['Ss'], array($this, 'sortTeams'));

                $normal = array($a[0], $b[0], $c[0], $d[0], $points[0], $points[1], $points[2], $points[3]);
                usort($normal, array($this, 'sortTeams'));
                $this->pairTeamsForGames($ret['Nm'], $normal, true);
            }
        }

        $ret['ended'] = $vegleges;

        $this->setRedis('S', $ret);

        return $ret;
    }

    protected function getDataForN() {
        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('N'),
            't'     => $this->createDummyRanksForType('N'),
            'KDs'   => $this->createDummyRanksForType('KD'),
            'KDm'   => $this->createDummyGamesForType('KD'),
            'ended' => false,
        );
        $vegleges = false;

        $teamsArray = $this->createInitialTeams();

        $this->loadPreviousGames($teamsArray, 'S');

        $games = $this->getGames(array('N'));
        if ($games) {
            $i = 0;
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }

                    $scores = $this->getScores($game);
                    $winner = $scores['winner'];
                    $loser = $scores['loser'];
                    $wc = $winner['computerName'];
                    $lc = $loser['computerName'];
                    $w = &$teamsArray[$wc];
                    $l = &$teamsArray[$lc];

                    $w['point'] += $winner['point'];
                    $w['g'] += $winner['g'];
                    $w['q'] += $winner['q'];
                    $w['ga'] += $winner['ga'];
                    $w['kq'] += $winner['kq'];
                    $w['ishere'] = true;

                    $l['point'] += $loser['point'];
                    $l['g'] += $loser['g'];
                    $l['q'] += $loser['q'];
                    $l['ga'] += $loser['ga'];
                    $l['kq'] += $loser['kq'];
                    $l['ishere'] = true;
                }
            }

            if ($i >= $this->numbers['game']['N'] && $allFinished) {
                $vegleges = true;
            }
        }

        $i = 1;
        foreach ($teamsArray as $tname => $t) {
            if ($t['ishere']) {
                unset($ret['t']['z' . $i]);
                $ret['t'][$tname] = $t;
                $i++;
            }
        }

        usort($ret['t'], array($this, 'sortTeams'));

        for ($i = 0; $i < $this->numbers['team']['KD']; $i++) {
            $j = $i + 1;
            unset($ret['KDs']['z' . $j]);
            $ret['KDs'][$i] = $ret['t'][$i];
        }

        $this->pairTeamsForGames($ret['KDm'], $ret['t']);

        $ret['ended'] = $vegleges;

        $this->setRedis('N', $ret);

        return $ret;
    }

    protected function getDataForKD() {
        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('KD'),
            't'     => $this->createDummyRanksForType('KD'),
            'NDs'   => $this->createDummyRanksForType('ND'),
            'NDm'   => $this->createDummyGamesForType('ND'),
            'ended' => false,
        );
        $vegleges = false;

        $teamsArray = $this->createInitialTeams();

        $this->loadPreviousGames($teamsArray, 'S');
        $this->loadPreviousGames($teamsArray, 'N');

        $games = $this->getGames(array('KD'));
        if ($games) {
            $i = 0;
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }

                    $scores = $this->getScores($game);
                    $winner = $scores['winner'];
                    $loser = $scores['loser'];
                    $wc = $winner['computerName'];
                    $lc = $loser['computerName'];
                    $w = &$teamsArray[$wc];
                    $l = &$teamsArray[$lc];

                    $w['point'] += $winner['point'];
                    $w['g'] += $winner['g'];
                    $w['q'] += $winner['q'];
                    $w['ga'] += $winner['ga'];
                    $w['kq'] += $winner['kq'];
                    $w['ishere'] = true;

                    $l['point'] += $loser['point'];
                    $l['g'] += $loser['g'];
                    $l['q'] += $loser['q'];
                    $l['ga'] += $loser['ga'];
                    $l['kq'] += $loser['kq'];
                    $l['ishere'] = true;
                }
            }

            if ($i >= $this->numbers['game']['KD'] && $allFinished) {
                $vegleges = true;
            }
        }

        $i = 1;
        foreach ($teamsArray as $team) {
            if ($team['ishere']) {
                unset($ret['t']['z' . $i]);
                $ret['t'][$team['computerName']] = $team;
                $i++;
            }
        }

        usort($ret['t'], array($this, 'sortTeams'));

        for ($i = 0; $i < $this->numbers['team']['ND']; $i++) {
            $j = $i + 1;
            unset($ret['NDs']['z' . $j]);
            $ret['NDs'][$i] = $ret['t'][$i];
        }

        $this->pairTeamsForGames($ret['NDm'], $ret['t'], true);

        $ret['ended'] = $vegleges;

        $this->setRedis('KD', $ret);

        return $ret;
    }

    protected function getDataForND() {
        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('ND'),
            't'     => $this->createDummyRanksForType('ND'),
            'Dm'    => $this->createDummyGamesForType('D'),
            'ended' => false,
        );
        $vegleges = false;

        $teamsArray = $this->createInitialTeams();

        $this->loadPreviousGames($teamsArray, 'S');
        $this->loadPreviousGames($teamsArray, 'N');
        $this->loadPreviousGames($teamsArray, 'KD');

        $games = $this->getGames(array('ND'));
        if ($games) {
            $i = 0;
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }

                    $scores = $this->getScores($game);
                    $winner = $scores['winner'];
                    $loser = $scores['loser'];
                    $wc = $winner['computerName'];
                    $lc = $loser['computerName'];
                    $w = &$teamsArray[$wc];
                    $l = &$teamsArray[$lc];

                    $w['point'] += $winner['point'];
                    $w['g'] += $winner['g'];
                    $w['q'] += $winner['q'];
                    $w['ga'] += $winner['ga'];
                    $w['kq'] += $winner['kq'];
                    $w['ishere'] = true;

                    $l['point'] += $loser['point'];
                    $l['g'] += $loser['g'];
                    $l['q'] += $loser['q'];
                    $l['ga'] += $loser['ga'];
                    $l['kq'] += $loser['kq'];
                    $l['ishere'] = true;
                }
            }

            if ($i >= $this->numbers['game']['ND'] && $allFinished) {
                $vegleges = true;
            }
        }

        $i = 1;
        foreach ($teamsArray as $team) {
            if ($team['ishere']) {
                unset($ret['t']['z' . $i]);
                $ret['t'][$team['computerName']] = $team;
                $i++;
            }
        }

        usort($ret['t'], array($this, 'sortTeams'));

        $this->pairTeamsForGames($ret['Dm'], $ret['t']);

        $ret['ended'] = $vegleges;

        $this->setRedis('ND', $ret);

        return $ret;
    }

    protected function getDataForD() {
        $ret = array(
            'games' => $this->createDummyCurrentGamesForType('D'),
            't'     => $this->createDummyRanksForType('D'),
            'ended' => false,
        );
        $vegleges = false;

        $teamsArray = $this->createInitialTeams();

        $this->loadPreviousGames($teamsArray, 'S');
        $this->loadPreviousGames($teamsArray, 'N');
        $this->loadPreviousGames($teamsArray, 'KD');
        $this->loadPreviousGames($teamsArray, 'ND');

        $games = $this->getGames(array('D'));
        if ($games) {
            $i = 0;
            $allFinished = true;

            foreach ($games as $gid => $game) {
                if ($this->gameFinished($game, true)) {
                    unset($ret['games']['z' . $i]);
                    $i++;

                    $ret['games'][$gid] = $game;
                    if (!$this->gameFinished($game)) {
                        $allFinished = false;
                    }

                    $scores = $this->getScores($game);
                    $winner = $scores['winner'];
                    $loser = $scores['loser'];
                    $wc = $winner['computerName'];
                    $lc = $loser['computerName'];
                    $w = &$teamsArray[$wc];
                    $l = &$teamsArray[$lc];

                    $w['point'] += $winner['point'];
                    $w['g'] += $winner['g'];
                    $w['q'] += $winner['q'];
                    $w['ga'] += $winner['ga'];
                    $w['kq'] += $winner['kq'];
                    $w['ishere'] = true;

                    $l['point'] += $loser['point'];
                    $l['g'] += $loser['g'];
                    $l['q'] += $loser['q'];
                    $l['ga'] += $loser['ga'];
                    $l['kq'] += $loser['kq'];
                    $l['ishere'] = true;
                }
            }

            if ($i >= $this->numbers['game']['D'] && $allFinished) {
                $vegleges = true;
            }
        }

        $i = 1;
        foreach ($teamsArray as $team) {
            if ($team['ishere']) {
                unset($ret['t']['z' . $i]);
                $ret['t'][$team['computerName']] = $team;
                $i++;
            }
        }

        usort($ret['t'], array($this, 'sortTeams'));

        $ret['ended'] = $vegleges;

        $this->setRedis('D', $ret);

        return $ret;
    }

    public function getSelejtezo($roundNumber) {
        $this->currentRoundId = (int)$roundNumber;
        $roundRepo = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Round');

        $this->round = $roundRepo->getRoundForApi($this->currentRoundId);
        if ($this->round) {
            $this->roundObj = $roundRepo->findOneBy(array('number' => $this->currentRoundId));
            $this->roundTeams = $this->getRoundTeams($this->roundObj);
            $this->games = $this->round['games'];
        }

        $ret = array(
            'St'    => $this->createDummyRanksForType('S'),
        );
        $teamsArray = $this->createInitialTeams();

        if ($this->currentRoundId === 1) {
            $ranks = array(
                '1'  => 'myrmidons',
                '2'  => 'vikings',
                '3'  => 'knights',
                '4'  => 'regnum',
                '5'  => 'dubelz',
                '6'  => 'musketasok',
                '7'  => 'toreadors',
                '8'  => 'hunters',
                '9'  => 'predators',
                '10' => 'wildgiants',
                '11' => 'meteors',
                '12' => 'myrmidonswarriors',
            );
            $r = &$ret['St'];

            for ($i = 1; $i <= 12; $i++) {
                unset($r['z' . $i]);
                $r[$i] = $teamsArray[$ranks[$i]];
            }
        }
        elseif ($this->currentRoundId > 1) {
            $lastRoundId = $this->currentRoundId - 1;
            $repo = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Round');
            $ro = $repo->findOneBy(array('number' => $lastRoundId));
            $teams = $this->getRoundTeams($ro);
            usort($teams, array($this, 'sortTeams'));
            $r = &$ret['St'];

            for ($i = 0; $i < 8; $i++) {
                $j = $i + 1;
                unset($r['z' . $j]);
                $r[$j] = $teams[$i];
            }

            $esw = $this->createDummyRanksForType('ND');

            $roGames = $repo->getRoundForApi($lastRoundId);
            $esGames = $this->getGames(array('ES'), $roGames);
            $esTeamsArray = $this->createInitialTeams();

            if ($esGames) {
                foreach ($esGames as $gid => $game) {
                    if ($this->gameFinished($game, true)) {
                        $scores = $this->getScores($game);
                        $winner = $scores['winner'];
                        $loser = $scores['loser'];
                        $wc = $winner['computerName'];
                        $lc = $loser['computerName'];
                        $w = &$esTeamsArray[$wc];
                        $l = &$esTeamsArray[$lc];

                        $w['point'] += $winner['point'];
                        $w['g'] += $winner['g'];
                        $w['q'] += $winner['q'];
                        $w['ga'] += $winner['ga'];
                        $w['kq'] += $winner['kq'];
                        $w['ishere'] = true;

                        $l['point'] += $loser['point'];
                        $l['g'] += $loser['g'];
                        $l['q'] += $loser['q'];
                        $l['ga'] += $loser['ga'];
                        $l['kq'] += $loser['kq'];
                        $l['ishere'] = true;
                    }
                }

                usort($esTeamsArray, array($this, 'sortTeams'));
                for ($i = 0; $i < 4; $i++) {
                    $j = $i + 1;
                    unset($esw['z' . $j]);
                    $esw[$i] = $esTeamsArray[$i];
                }

                usort($esw, array($this, 'sortTeams'));
            }

            $i = 8;
            foreach ($esw as $eswinner) {
                $i++;
                unset($r['z' . $i]);
                $r[$i] = $eswinner;
            }
        }

        $ret['St'] = $this->shuffleStable($ret['St']);

        return $ret['St'];
    }
}
