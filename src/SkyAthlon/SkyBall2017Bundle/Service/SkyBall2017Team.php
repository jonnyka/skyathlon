<?php

namespace SkyAthlon\SkyBall2017Bundle\Service;

use SkyAthlon\SkyBall2017Bundle\Entity\Game;
use SkyAthlon\SkyBall2017Bundle\Entity\Round;
use SkyAthlon\SkyBall2017Bundle\Entity\RoundResult;
use SkyAthlon\SkyBall2017Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SkyBall2017Team extends Controller
{
    protected $em;
    protected $cacheService;

    /**
     * Constructor
     * @param EntityManager $em
     * @param SkyBall2017Cache $cacheService
     */
    public function __construct(EntityManager $em, SkyBall2017Cache $cacheService)
    {
        $this->em = $em;
        $this->cacheService = $cacheService;
    }

    public function getRealRoundResults(Round $round) {
        return $this->cacheService->getRoundTeams($round);
    }

    public function createRoundResults(Round $round, $results) {
        $em = $this->em;
        $teamRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team');
        $rrTypeRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:RoundResultType');
        $rrRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:RoundResult');

        foreach ($results as $standing => $teamId) {
            $standing = 'hely_' . str_replace('round-team-', '', $standing);
            $roundResultType = $rrTypeRepo->findOneBy(array('computerName' => $standing));
            $team = new Team();
            if ($teamId !== 0) {
                $team = $teamRepo->find($teamId);
            }
            $roundResult = $rrRepo->findOneBy(array(
                'round' => $round,
                'roundResultType' => $roundResultType
            ));

            if (!$roundResult) {
                $roundResult = new RoundResult();
                $roundResult->setRound($round);
                $roundResult->setRoundResultType($roundResultType);
            }

            $roundResult->setTeam($team);

            $em->persist($roundResult);
        }

        $em->flush();
    }
}
