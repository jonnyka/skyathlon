<?php

namespace SkyAthlon\SkyBall2017Bundle\Service;

use AppBundle\Entity\Player as Ref;
use SkyAthlon\SkyBall2017Bundle\Entity\Game;
use SkyAthlon\SkyBall2017Bundle\Entity\Player;
use SkyAthlon\SkyBall2017Bundle\Entity\PlayerResult;
use SkyAthlon\SkyBall2017Bundle\Entity\Round;
use SkyAthlon\SkyBall2017Bundle\Entity\RoundKerchiefResult;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SkyBall2017Player extends Controller
{
    protected $em;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getKerchiefs(Game $game) {
        $em = $this->em;
        $ret = array('maxValues' => array(), 'players' => array());
        $i = 0;

        $kerchiefTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
        $teamA = $game->getTeamA();
        $teamB = $game->getTeamB();
        $playersA = $teamA->getPlayers();
        $playersB = $teamB->getPlayers();

        $kcValues = array();
        foreach ($kerchiefTypes as $kerchiefType) {
            $kcValues[$kerchiefType->getComputerName()] = -1;
        }

        foreach ($playersA as $player) {
            $ret['players'][$i] = array();

            foreach ($kerchiefTypes as $kerchiefType) {
                $playerResult = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findOneBy(array(
                    'game' => $game,
                    'player' => $player,
                    'kerchiefType' => $kerchiefType
                ));
                if (!$playerResult) {
                    $playerResult = new PlayerResult();
                    $playerResult->setValue(0);
                    $playerResult->setPlayer($player);
                    $playerResult->setKerchiefType($kerchiefType);
                    $playerResult->setGame($game);

                    $em->persist($playerResult);
                    $em->flush();
                }

                $value = $playerResult ? $playerResult->getValue() : 0;
                if ($value > $kcValues[$kerchiefType->getComputerName()]) {
                    $kcValues[$kerchiefType->getComputerName()] = $value;
                }

                $ret['players'][$i][] = array(
                    'player' => array(
                        'id' => $player->getId(),
                        'name' => $player->getName(),
                        'number' => $player->getNumber(),
                        'team' => $player->getTeam()->getComputerName(),
                    ),
                    'kerchiefType' => array(
                        'name' => $kerchiefType->getName(),
                        'computerName' => $kerchiefType->getComputerName(),
                    ),
                    'value' => $value,
                    'id' => $playerResult->getId(),
                );
            }

            $i++;
        }
        foreach ($playersB as $player) {
            $ret['players'][$i] = array();

            foreach ($kerchiefTypes as $kerchiefType) {
                $playerResult = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findOneBy(array(
                    'game' => $game,
                    'player' => $player,
                    'kerchiefType' => $kerchiefType
                ));
                if (!$playerResult) {
                    $playerResult = new PlayerResult();
                    $playerResult->setValue(0);
                    $playerResult->setPlayer($player);
                    $playerResult->setKerchiefType($kerchiefType);
                    $playerResult->setGame($game);

                    $em->persist($playerResult);
                    $em->flush();
                }

                $value = $playerResult ? $playerResult->getValue() : 0;
                if ($value > $kcValues[$kerchiefType->getComputerName()]) {
                    $kcValues[$kerchiefType->getComputerName()] = $value;
                }

                $ret['players'][$i][] = array(
                    'player' => array(
                        'id' => $player->getId(),
                        'name' => $player->getName(),
                        'number' => $player->getNumber(),
                        'team' => $player->getTeam()->getComputerName(),
                    ),
                    'kerchiefType' => array(
                        'name' => $kerchiefType->getName(),
                        'computerName' => $kerchiefType->getComputerName(),
                    ),
                    'value' => $value,
                    'id' => $playerResult->getId(),
                );
            }

            $i++;
        }

        $ret['maxValues'] = $kcValues;

        return $ret;
    }

    public function getKerchiefsForRound(Round $round) {
        $em = $this->em;

        $players = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();

        if ($round->getPlayoffs()) {
            $players = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->getPlayoffPlayers();
        }

        $kerchiefTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
        $ret = array();
        $kcValues = array();

        /** @var Player $player */
        foreach ($players as $player) {
            $ret[$player->getId()] = array();
            foreach ($kerchiefTypes as $kerchiefType) {
                $ret[$player->getId()][$kerchiefType->getComputerName()] = 0;
                $kcValues[$kerchiefType->getComputerName()] = -1;
            }
        }

        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
            'round' => $round,
        ));


        foreach ($playerResults as $playerResult) {
            if (array_key_exists($playerResult->getPlayer()->getId(), $ret)) {
                $ret[$playerResult->getPlayer()->getId()][$playerResult->getKerchiefType()->getComputerName()] += $playerResult->getValue();
            }
        }

        uasort($ret, function ($a, $b) {
            return $b['G'] - $a['G'];
        });

        foreach ($ret as $pid => $kerchiefs) {
            foreach ($kerchiefs as $kcType => $kcVal) {
                if ($kcVal > $kcValues[$kcType]) {
                    $kcValues[$kcType] = $kcVal;
                }
            }
        }

        $return = array(
            'ret' => $ret,
            'kcValues' => $kcValues,
        );

        return $return;
    }

    public function getKerchiefResultsForRound(Round $round, $roundKerchiefs = null) {
        $kerchiefs = $this->getKerchiefsForRound($round);
        $kcs = $kerchiefs['ret'];
        $kcValues = $kerchiefs['kcValues'];
        $ret = array('ret' => array(), 'kcValues' => array(), 'kerchiefs' => $kerchiefs['ret']);
       	$i = 0;

        $kcEgal = array();
        $kerchiefTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
        foreach ($kerchiefTypes as $kerchiefType) {
            $kcEgal[$kerchiefType->getComputerName()] = false;
        }
       	
        foreach ($kcValues as $kcId => $kcValue) {
            $ret['ret'][$kcId]['id_' . $i] = array('id' => -1, 'val' => -1);
            $ret['kcValues'][$kcId] = -1;
            $i++;
        }

        foreach ($kcs as $pid => $pkcs) {
        	$i = 0;
            foreach ($pkcs as $pkcsId => $pkcsVal) {
                $ret['ret'][$pkcsId]['id_' . $i] = array('id' => $pid, 'val' => $pkcsVal);

                if ($roundKerchiefs) {
                    if ($pid === $roundKerchiefs[$pkcsId]) {
                        $ret['kcValues'][$pkcsId] = $pid;
                    }
                }
                else if ($pkcsVal === $kcValues[$pkcsId]) {
                    $ret['kcValues'][$pkcsId] = $pid;
                }

                usort($ret['ret'][$pkcsId], function ($a, $b) {
                    return $b['val'] - $a['val'];
                });
                
                $i++;
            }
        }

        foreach ($ret['ret'] as $kcType => $kcVals) {
            $j = 0;
            $temp = -1;
            foreach ($kcVals as $kcV) {
                if ($j == 0) {
                    $temp = $kcV['val'];
                }
                if ($j == 1) {
                    if ($kcV['val'] == $temp) {
                        $kcEgal[$kcType] = true;
                    }
                }
                $j++;
            }
        }

        $ret['kcEgal'] = $kcEgal;

        return $ret;
    }

    public function getRealKerchiefResultsForRound(Round $round) {
        $kerchiefs = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:RoundKerchiefResult')->getForRound($round);
        if (count($kerchiefs) === 12) {
            return $this->getKerchiefResultsForRound($round, $kerchiefs);
        }

        return $this->getKerchiefResultsForRound($round);
    }

    public function getAllGlobalKerchiefs($onlyAlapszakasz = false) {
        $ret = array();

        $kerchiefs = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findAll();
        $players = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();
        $kerchiefTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

        foreach ($kerchiefTypes as $kerchiefType) {
            foreach ($players as $player) {
                $ret[$kerchiefType->getComputerName()][$player->getId()] = 0;
            }
        }

        foreach ($kerchiefs as $kerchief) {
            if (!$onlyAlapszakasz || $kerchief->getGame()->getRound()->getNumber() <= 12) {
                $ret[$kerchief->getKerchiefType()->getComputerName()][$kerchief->getPlayer()->getId()] += $kerchief->getValue();
            }
        }

        foreach ($ret as &$r) {
            arsort($r);
        }

        return $ret;
    }

    public function getAllGlobalKerchiefsJson() {
        $ret = array();

        $kerchiefs = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findAll();
        $players = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();
        $kerchiefTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

        foreach ($kerchiefTypes as $kerchiefType) {
            foreach ($players as $player) {
                $ret[$kerchiefType->getComputerName()]['p' . $player->getId()] = 0;
            }
        }

        foreach ($kerchiefs as $kerchief) {
            $ret[$kerchief->getKerchiefType()->getComputerName()]['p' . $kerchief->getPlayer()->getId()] += $kerchief->getValue();
        }

        foreach ($ret as &$r) {
            arsort($r);
        }

        return $ret;
    }

    public function getRoundKerchiefsJson(Round $round) {
        $ret = array();

        $kerchiefs = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
            'round' => $round,
        ));
        $players = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();
        $kerchiefTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

        foreach ($kerchiefTypes as $kerchiefType) {
            foreach ($players as $player) {
                $ret[$kerchiefType->getComputerName()]['p' . $player->getId()] = 0;
            }
        }

        foreach ($kerchiefs as $kerchief) {
            $ret[$kerchief->getKerchiefType()->getComputerName()]['p' . $kerchief->getPlayer()->getId()] += $kerchief->getValue();
        }

        foreach ($ret as &$r) {
            arsort($r);
        }

        return $ret;
    }

    public function getAllGlobalRanks() {
        $kerchiefs = $this->getAllGlobalKerchiefs();
        $ret = array('ranks' => array(), 'values' => array(), 'sumValues' => array());
        $i = 1;
        $j = 0;
        $tempValue = -1;

        foreach ($kerchiefs as $kcType => $results) {
            foreach ($results as $pid => $value) {
                $ret['values'][$pid][$kcType] = 36;
                $ret['sumValues'][$pid] = 0;
                $ret['realValues'][$pid][$kcType] = 0;
                $ret['ranks'][$pid] = 36;
            }
        }

        foreach ($kerchiefs as $kcType => $results) {
            foreach ($results as $pid => $value) {
                if ($value !== $tempValue) {
                    $tempValue = $value;
                    $j = $i;
                }

                $ret['values'][$pid][$kcType] = $j;
                $ret['realValues'][$pid][$kcType] += $value;
                $ret['sumValues'][$pid] += $j;

                $i++;
            }

            $i = 1;
            $j = 0;
            $tempValue = -1;
        }

        asort($ret['sumValues']);

        foreach ($ret['sumValues'] as $pid => $sumValue) {
            if ($sumValue !== $tempValue) {
                $tempValue = $sumValue;
                $j = $i;
            }

            $ret['ranks'][$pid] = $j;

            $i++;
        }

        return $ret;
    }

    public function getAllKerchiefResults() {
        $ret = array();

        $rounds = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findAll();
        foreach ($rounds as $round) {
            $kerchiefs = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:RoundKerchiefResult')->getForRound($round);
            if (count($kerchiefs) === 12) {
                $ret[$round->getNumber()] = $kerchiefs;
            }

            $ret[$round->getNumber()] = $this->getKerchiefResultsForRound($round);
        }

        return $ret;
    }

    public function getAllKerchiefResultsByPlayers() {
        $ret = array();

        //$players = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();
        $kcTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
        $krRepo = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:RoundKerchiefResult');

        /*
        foreach ($players as $player) {
            $ret[$player->getId()] = array();

            foreach ($kcTypes as $kcType) {
                $ret[$player->getId()][$kcType->getComputerName()] = 0;
            }
        }
        */

        $playerKerchiefs = $krRepo->findAll();
        if ($playerKerchiefs) {
            foreach ($playerKerchiefs as $playerKerchief) {
                $p = $playerKerchief->getPlayer();
                if ($p) {
                	if (!array_key_exists($p->getId(), $ret)) {
                		$ret[$p->getId()] = array();
                		foreach ($kcTypes as $kcType) {
                			$ret[$p->getId()][$kcType->getComputerName()] = 0;
                		}
                	}
                    $ret[$p->getId()][$playerKerchief->getKerchiefType()->getComputerName()] += 1;
                }
            }
        }

        foreach ($ret as $pid => $kcArray) {
            $sum = 0;
            foreach ($kcArray as $kc) {
                $sum += $kc;
            }
            $ret[$pid]['zSum'] = $sum;
        }

        return $ret;
    }

    public function getSumKerchiefResultsByPlayers() {
        $ret = array();
        $kcs = $this->getAllKerchiefResultsByPlayers();

        foreach ($kcs as $pid => $kcArray) {
            $ret[$pid] = 0;
            foreach ($kcArray as $kcType => $kc) {
                if ($kcType === 'zSum') {
                    $ret[$pid] += $kc;
                }
            }
        }

        arsort($ret);

        return $ret;
    }

    public function getTop3KerchiefResultsByPlayers() {
    	$ret = array();
    
    	//$players = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();
    	$kcTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
    	$krRepo = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:RoundKerchiefResult');
    
    	$playerKerchiefs = $krRepo->findAll();
    	if ($playerKerchiefs) {
    		foreach ($playerKerchiefs as $playerKerchief) {
    			$p = $playerKerchief->getPlayer();
    			if ($p) {
    				if (!array_key_exists($p->getId(), $ret)) {
    					$ret[$p->getId()] = array();
    					foreach ($kcTypes as $kcType) {
    						$ret[$p->getId()][$kcType->getComputerName()] = 0;
    					}
    				}

    				$ret[$p->getId()][$playerKerchief->getKerchiefType()->getComputerName()] += 1;
    			}
    		}
    	}
    
    	return $ret;
    }

    public function getAllKerchiefsByPlayers() {
        $ret = array();

        $players = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();
        $kcTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
        $prRepo = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult');

        foreach ($players as $player) {
            $ret[$player->getId()] = array();

            foreach ($kcTypes as $kcType) {
                $ret[$player->getId()][$kcType->getWeight() . '_' . $kcType->getComputerName()] = 0;
            }
        }

        $playerKerchiefs = $prRepo->findAll();
        if ($playerKerchiefs) {
            foreach ($playerKerchiefs as $playerKerchief) {
                $p = $playerKerchief->getPlayer();
                if ($p) {
                    $index = $playerKerchief->getKerchiefType()->getWeight() . '_' . $playerKerchief->getKerchiefType()->getComputerName();
                    $ret[$p->getId()][$index] += $playerKerchief->getValue();
                }
            }
        }

        return $ret;
    }

    public function getAllKerchiefsByTeams() {
        $ret = array();

        $teams = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->findAll();
        $kcTypes = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
        $prRepo = $this->em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult');

        foreach ($teams as $team) {
            $ret[$team->getId()] = array();

            foreach ($kcTypes as $kcType) {
                $ret[$team->getId()][$kcType->getWeight() . '_' . $kcType->getComputerName()] = 0;
            }
        }

        $playerKerchiefs = $prRepo->findAll();
        if ($playerKerchiefs) {
            foreach ($playerKerchiefs as $playerKerchief) {
                $p = $playerKerchief->getPlayer();
                if ($p) {
                    $index = $playerKerchief->getKerchiefType()->getWeight() . '_' . $playerKerchief->getKerchiefType()->getComputerName();
                    $ret[$p->getTeam()->getId()][$index] += $playerKerchief->getValue();
                }
            }
        }

        return $ret;
    }

    public function createRoundResults(Round $round, $results) {
        $em = $this->em;
        $playerRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player');
        $kcTypeRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType');
        $rkrRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:RoundKerchiefResult');

        foreach ($results as $kcType => $playerId) {
            $kcType = str_replace('round-player-', '', $kcType);
            $kcType = $kcTypeRepo->findOneBy(array('computerName' => $kcType));
            $player = new Player();
            if ($playerId !== -1) {
                $player = $playerRepo->find($playerId);
            }
            $roundKerchiefResult = $rkrRepo->findOneBy(array(
                'round' => $round,
                'kerchiefType' => $kcType
            ));

            if (!$roundKerchiefResult) {
                $roundKerchiefResult = new RoundKerchiefResult();
                $roundKerchiefResult->setRound($round);
                $roundKerchiefResult->setKerchiefType($kcType);
            }

            $roundKerchiefResult->setPlayer($player);

            $em->persist($roundKerchiefResult);
        }

        $em->flush();
    }

    public function getRefs() {
        $em = $this->em;
        $playerRepo = $em->getRepository('AppBundle:Player');
        $players = $playerRepo->getRefs();

        return $players;
    }
}
