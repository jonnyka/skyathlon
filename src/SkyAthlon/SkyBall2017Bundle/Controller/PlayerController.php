<?php

namespace SkyAthlon\SkyBall2017Bundle\Controller;

use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SkyAthlon\SkyBall2017Bundle\Entity\Player;

/**
 * Player controller.
 *
 * @Route("/skyball-2017/")
 */
class PlayerController extends Controller
{
    /**
     * Lists all Player entities.
     *
     * @Route("jatekosok/", name="skyball2017_player_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $players = $this->getDoctrine()
            ->getRepository('SkyAthlonSkyBall2017Bundle:Player')
            ->createQueryBuilder('p')
            ->select('p, t')
            ->join('p.team', 't')
            ->where('t.id = p.team')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        return new JsonResponse($players);
    }

    /**
     * Creates a new Player entity.
     *
     * @Route("jatekosok/uj/", name="skyball2017_player_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function newAction(Request $request)
    {
        $player = new Player();
        $form = $this->createForm('SkyAthlon\SkyBall2017Bundle\Form\PlayerType', $player);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();

            return new JsonResponse('ok', 200);
        }

        return new JsonResponse('error', 401);
    }

    /**
     * Finds and displays a Player entity.
     *
     * @Route("jatekosok/{id}/", name="skyball2017_player_show")
     * @Method("GET")
     *
     * @param $id
     * @return JsonResponse
     */
    public function showAction($id)
    {
        $player = $this->getDoctrine()
            ->getRepository('SkyAthlonSkyBall2017Bundle:Player')
            ->createQueryBuilder('p')
            ->select('p, t')
            ->join('p.team', 't')
            ->where('t.id = p.team')
            ->andWhere('p.id = ' . $id)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        return new JsonResponse($player);
    }

    /**
     * Displays a form to edit an existing Player entity.
     *
     * @Route("jatekosok/{player}/szerkesztes/", name="skyball2017_player_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $player = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->find($id);
        $editForm = $this->createForm('SkyAthlon\SkyBall2017Bundle\Form\PlayerType', $player);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();

            return new JsonResponse('ok', 200);
        }

        return new JsonResponse('error', 403);
    }

    /**
     * Deletes a Player entity.
     *
     * @Route("jatekosok/{id}/", name="skyball2017_player_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $player = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->find($id);
        $form = $this->createDeleteForm($player);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($player);
            $em->flush();

            return new JsonResponse('ok', 200);
        }

        return new JsonResponse('error', 403);
    }

    /**
     * Creates a form to delete a Player entity.
     *
     * @param Player $player The Player entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Player $player)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('skyball2017_player_delete', array('id' => $player->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    /**
     * Lists all Kerchief entities.
     *
     * @Route("kendok/", name="skyball2017_kerchief")
     * @Method("GET")
     * @Template()
     */
    public function kerchiefAction()
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            $kerchiefTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));
            $players = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->getPlayers();
            $teams = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->getTeams();

            $playerService = $this->get('skyball2017_player');
            $kcByPlayers = $playerService->getAllKerchiefResultsByPlayers();
            $kcSumByPlayers = $playerService->getSumKerchiefResultsByPlayers();
            //$kcAllByPlayers = $playerService->getAllKerchiefsByPlayers();
            $kcAllByTeams = $playerService->getAllKerchiefsByTeams();
            $kcAllGlobal = $playerService->getAllGlobalKerchiefs();
            $kcAlapGlobal = $playerService->getAllGlobalKerchiefs(true);
            $kcAllRank = $playerService->getAllGlobalRanks();

            return array(
                'kcByPlayers' => $kcByPlayers,
                'kcSumByPlayers' => $kcSumByPlayers,
                //'kcAllByPlayers' => $kcAllByPlayers,
                'kcAllByTeams' => $kcAllByTeams,
                'kcAllGlobal' => $kcAllGlobal,
                'kcAlapGlobal' => $kcAlapGlobal,
                'kcAllRank' => $kcAllRank,
                'kcTypes' => $kerchiefTypes,
                'players' => $players,
                'teams' => $teams,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Gets all referees.
     *
     * @Route("birok/", name="skyball2017_referee")
     * @Method("GET")
     * @Template()
     */
    public function refereeAction()
    {
        if ($this->getUser()) {
            $playerService = $this->get('skyball2017_player');
            $refsByPlayers = $playerService->getRefs();

            return array(
                'refsByPlayers' => $refsByPlayers,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Ref admin page.
     *
     * @Route("birok/admin/", name="skyball2017_referee_admin")
     * @Method("GET")
     * @Template()
     */
    public function refereeAdminAction()
    {
        if ($this->getUser()) {
            if ($this->getUser()->isAdmin()) {
                $em = $this->getDoctrine()->getManager();
                $refs = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->getRefs();

                return array(
                    'refs' => $refs,
                );
            }

            return $this->redirectToRoute('skyball2017_referee');
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
