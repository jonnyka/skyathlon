<?php

namespace SkyAthlon\SkyBall2017Bundle\Controller;

use SkyAthlon\SkyBall2017Bundle\Repository\GameRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Reports controller.
 *
 * @Route("/skyball-2017/")
 */
class ReportController extends Controller
{
    /**
     * Gets report data.
     *
     * @Route("jegyzokonyvek/", name="skyball2017_reports")
     * @Template()
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function reportAction() {
    	if ($this->getUser()) {
    		$em = $this->getDoctrine()->getManager();
    		/** @var GameRepository $repo */
    		$repo = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game');
    		$games = $repo->getAllGames();
            $kerchiefTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

    		return array(
    			'kcTypes' => $kerchiefTypes,
    			'games' => $games,
    		);
    	}
    	else {
    		return $this->redirectToRoute('index');
    	}
    }
}
