<?php

namespace SkyAthlon\SkyBall2017Bundle\Controller\Round;

use AppBundle\Entity\Settings;
use Doctrine\ORM\EntityManager;
use SkyAthlon\SkyBall2017Bundle\Entity\Game;
use SkyAthlon\SkyBall2017Bundle\Entity\PlayerResult;
use SkyAthlon\SkyBall2017Bundle\Entity\RefResult;
use SkyAthlon\SkyBall2017Bundle\Entity\Round;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/skyball-2017/fordulo")
 */
class RoundGameController extends Controller
{

    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{roundid}/merkozes/uj/", name="skyball2017_game_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function newGameAction(Request $request, $roundid)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed() && $round->getPlayoffs()) || $this->getUser()->isSu()) {
                $game = new Game();

                $games = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->findBy(array(
                    'round' => $round,
                ));
                $arr = array(
                    'attr' => $games,
                );

                $form = $this->createForm('SkyAthlon\SkyBall2017Bundle\Form\GameType', $game, $arr);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $gameType = $form['gameType']->getData();

                    $game->setWeight($gameType->getWeight());
                    $game->setRound($round);

                    $em->persist($game);

                    $em->flush();

                    $cacheService = $this->get('skyball2017_cache');
                    $cacheService->getCurrentData();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2017.game.created');

                    if ($game->getTeamA() && $game->getTeamB()) {
                        $playerService = $this->get('skyball2017_player');
                        $playerService->getKerchiefs($game);
                    }

                    if (!$game->getPointA() && !$game->getPointB() && !$game->isPreGame() && $game->getTeamA() && $game->getTeamB()) {
                        return $this->redirectToRoute('skyball2017_game_edit_player_stats', array('roundid' => $roundid, 'game' => $game->getId()));
                    }

                    $anchor = '#' . $game->getGameType()->getComputerName();
                    return $this->redirect($this->generateUrl('skyball2017_round', array('round' => $roundid)) . $anchor);
                }

                return array(
                    'game' => $game,
                    'roundid' => $roundid,
                    'form' => $form->createView(),
                );
            }
            else {
                return $this->redirectToRoute('skyball2017_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{roundid}/merkozes/{game}/szerkesztes/", name="skyball2017_game_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editGameAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $oldTeamA = $game->getTeamA();
                $oldTeamB = $game->getTeamB();

                $games = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->findBy(array(
                    'round' => $round,
                ));
                $refResultRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:RefResult');

                $point = null;
                $mainRef = $refResultRepo->findOneBy(array(
                    'game' => $game,
                    'type' => 1
                ));
                if ($mainRef) {
                    $point = $mainRef->getValue();
                }

                $arr = array(
                    'custom' => array(
                        'games' => $games,
                        'point' => $point,
                    ),
                );

                $editForm = $this->createForm('SkyAthlon\SkyBall2017Bundle\Form\GameType', $game, $arr);
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $weight = $round->getReversed() ? $game->getGameType()->getReversedWeight() : $game->getGameType()->getWeight();
                    $game->setWeight($weight);
                    $em->persist($game);

                    if (!($game->getTeamA() === $oldTeamA && $game->getTeamB() === $oldTeamB)) {
                        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
                            'game' => $game,
                        ));

                        foreach ($playerResults as $playerResult) {
                            $em->remove($playerResult);
                        }
                    }

                    /** @var Settings $currentGameObject */
                    $currentGameObject = $this->getDoctrine()->getManager()->getRepository('AppBundle:Settings')->findOneBy(array(
                        'name' => 'skyball2017_currentGame',
                    ));
                    $isCurrent = $editForm->get('current')->getData();
                    if ($isCurrent) {
                        if (!$currentGameObject) {
                            $currentGameObject = new Settings();
                            $currentGameObject->setName('skyball2017_currentGame');
                        }

                        $currentGameObject->setValue($game->getId());
                        $em->persist($currentGameObject);
                    }
                    elseif ($currentGameObject && $currentGameObject->getValue() == $game->getId()) {
                        $currentGameObject->setValue(0);
                        $em->persist($currentGameObject);
                    }

                    $em->flush();

                    $points = $editForm->get('points')->getData();

                    if (!$points) {
                        $rrs = $refResultRepo->findBy(array('game' => $game));

                        if ($rrs) {
                            foreach ($rrs as $rr) {
                                $em->remove($rr);
                            }

                            $em->flush();
                        }
                    }
                    else if ($points && $game->getRef() && $game->getSref() && $game->getSpicli()) {
                        if (!$mainRef) {
                            $mainRef = new RefResult();
                            $mainRef->setGame($game);
                            $mainRef->setType(1);
                        }

                        $mainRef->setPlayer($game->getRef());
                        $mainRef->setSpicli($game->getSpicli());
                        $mainRef->setValue($points);

                        $em->persist($mainRef);

                        $sRef = $refResultRepo->findOneBy(array(
                            'game' => $game,
                            'type' => 2
                        ));
                        if (!$sRef) {
                            $sRef = new RefResult();
                            $sRef->setGame($game);
                            $sRef->setType(2);
                        }

                        $sRef->setPlayer($game->getSref());
                        $sRef->setSpicli($game->getSpicli());
                        $sRef->setValue($points);

                        $em->persist($sRef);

                        $em->flush();
                    }

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2017.game.edited');

                    if ($game->getTeamA() && $game->getTeamB()) {
                        $playerService = $this->get('skyball2017_player');
                        $playerService->getKerchiefs($game);
                    }

                    $cacheService = $this->get('skyball2017_cache');
                    $cacheService->getCurrentData($game);

                    if (!$game->getPointA() && !$game->getPointB() && !$game->isPreGame() && $game->getTeamA() && $game->getTeamB()) {
                        return $this->redirectToRoute('skyball2017_game_edit_player_stats', array('roundid' => $roundid, 'game' => $game->getId()));
                    }

                    $anchor = '#' . $game->getGameType()->getComputerName();
                    return $this->redirect($this->generateUrl('skyball2017_round', array('round' => $roundid)) . $anchor);
                }

                $form = $this->createDeleteGameForm($roundid, $game);

                return array(
                    'game' => $game,
                    'roundid' => $roundid,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $form->createView(),
                );
            }
            else {
                return $this->redirectToRoute('skyball2017_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit the players stats for a game.
     *
     * @Route("/{roundid}/merkozes/{game}/jatekos-statisztika/szerkesztes/", name="skyball2017_game_edit_player_stats")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editPlayerStatsAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if ((($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) && !$game->isPreGame() && $game->getTeamA() && $game->getTeamB()) {
                $values = array();

                $current = false;
                /** @var Settings $currentGameObject */
                $currentGameObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                    'name' => 'skyball2017_currentGame'
                ));

                if ($currentGameObject && $currentGameObject->getValue() == $game->getId()) {
                    $current = true;
                }

                if ($request->getMethod() === 'POST') {
                    $values = $request->request->all();
                    $recalc = false;
                    $hasCurrent = false;

                    foreach ($values as $id => $value) {
                        if ($id === 'recalc') {
                            $recalc = true;
                        }
                        else if ($id === 'description') {
                            if (!$value) {
                                $value = '';
                            }
                            $game->setDescription($value);
                        }
                        else if ($id === 'current') {
                            $hasCurrent = true;
                            $isCurrent = $value;
                            if ($isCurrent) {
                                if (!$currentGameObject) {
                                    $currentGameObject = new Settings();
                                    $currentGameObject->setName('skyball2017_currentGame');
                                }

                                $currentGameObject->setValue($game->getId());
                                $em->persist($currentGameObject);
                            }
                        }
                        else if ($id === 'gameLength') {
                            $length = null;
                            if ($value) {
                                $length = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameLength')->find($value);
                            }

                            $game->setGameLength($length);
                        }
                        else {
                            /** @var PlayerResult $playerResult */
                            $playerResult = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->find($id);
                            $playerResult->setValue($value);
                            $playerResult->setRound($round);
                            $em->persist($playerResult);
                        }
                    }

                    $em->persist($game);

                    if (!$hasCurrent) {
                        if ($currentGameObject && $currentGameObject->getValue() == $game->getId()) {
                            $currentGameObject->setValue(0);
                            $em->persist($currentGameObject);
                        }
                    }

                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2017.playerstats.edited');

                    $cacheService = $this->get('skyball2017_cache');

                    if ($recalc) {
                        $cacheService->setGamePointsFromPlayerResults($game);
                    }

                    $cacheService->getCurrentData($game);

                    $anchor = '#' . $game->getGameType()->getComputerName();
                    return $this->redirect($this->generateUrl('skyball2017_round', array('round' => $roundid)) . $anchor);
                }

                $playerService = $this->get('skyball2017_player');
                $playerResults = $playerService->getKerchiefs($game);
                $kerchiefTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

                $gameLengths = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameLength')->findAll();

                return array(
                    'game'        => $game,
                    'results'     => $playerResults,
                    'roundid'     => $roundid,
                    'kcTypes'     => $kerchiefTypes,
                    'gameLengths' => $gameLengths,
                    'values'      => $values,
                    'current'     => $current,
                );
            }
            else {
                return $this->redirectToRoute('skyball2017_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Deletes a Game entity.
     *
     * @Route("/{roundid}/merkozes/{id}/", name="skyball2017_game_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteGameAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array('number' => $roundid));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $form = $this->createDeleteGameForm($roundid, $game);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                    $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
                        'game' => $game,
                    ));
                    if ($playerResults) {
                        foreach ($playerResults as $playerResult) {
                            $em->remove($playerResult);
                        }
                    }

                    $em->remove($game);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skyball2017.game.deleted', 'warning');
                }

                return $this->redirectToRoute('skyball2017_round', array('round' => $roundid));
            }
            else {
                return $this->redirectToRoute('skyball2017_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a form to delete a Game entity.
     *
     * @param $roundid
     * @param Game $game The Game entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteGameForm($roundid, Game $game)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('skyball2017_game_delete', array('id' => $game->getId(), 'roundid' => $roundid)))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    /**
     * Get kerchiefs for a game.
     *
     * @Route("/merkozes/{game}/kendo/", name="skyball2017_game_kerchiefs", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function getKerchiefs(Game $game) {
        $playerService = $this->get('skyball2017_player');
        $ret = $playerService->getKerchiefs($game);

        return new JsonResponse($ret);
    }
}
