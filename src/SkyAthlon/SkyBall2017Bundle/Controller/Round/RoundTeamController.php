<?php

namespace SkyAthlon\SkyBall2017Bundle\Controller\Round;

use SkyAthlon\SkyBall2017Bundle\Entity\Round;
use SkyAthlon\SkyBall2017Bundle\Entity\RoundResultType;
use SkyAthlon\SkyBall2017Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/skyball-2017/fordulo")
 */
class RoundTeamController extends Controller
{
    /**
     * Show current round player results.
     *
     * @Route("/{round}/csapat-statisztika/", name="skyball2017_round_teams", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexTeamsAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            /** @var Round $r */
            $r = $repo->findOneBy(array('number' => $round));

            $teamService = $this->get('skyball2017_team');
            $teams = $teamService->getRealRoundResults($r);

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            $rrTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:RoundResultType')->findAll();
            $rrTypesArr = [];
            /** @var RoundResultType $rrType */
            foreach ($rrTypes as $rrType) {
                $id = str_replace('hely_', '', $rrType->getComputerName());
                $rrTypesArr[$id] = $rrType->getPoint();
            }

            return array(
                'rounds' => $pagination,
                'teams' => $teams,
                'rrTypes' => $rrTypesArr,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
