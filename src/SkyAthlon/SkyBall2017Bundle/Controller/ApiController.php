<?php

namespace SkyAthlon\SkyBall2017Bundle\Controller;

use AppBundle\Repository\SettingsRepository;
use Doctrine\ORM\EntityManager;
use SkyAthlon\SkyBall2017Bundle\Entity\Team;
use SkyAthlon\SkyBall2017Bundle\Repository\GameRepository;
use SkyAthlon\SkyBall2017Bundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Translator;

/**
 * Api controller.
 *
 * @Route("/skyball-2017/api/")
 */
class ApiController extends Controller
{
    /**
     * Gets all Teams.
     *
     * @Route("teams", name="skyball2017_api_teams")
     * @Method("GET")
     */
    public function teamsAction()
    {
        /** @var TeamRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('SkyAthlonSkyBall2017Bundle:Team');
        $teams = $repo->getTeams();

        usort($teams, function($a, $b) {
            return $b['point'] - $a['point'];
        });

        return new JsonResponse($teams);
    }

    protected function echoUtf8($str) {
        echo iconv("UTF-8", "ISO-8859-2//TRANSLIT", $str);
    }

    /**
     * Gets all Games.
     *
     * @Route("games", name="skyball2017_api_games")
     * @Method("GET")
     */
    public function gamesAction()
    {
        /** @var GameRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('SkyAthlonSkyBall2017Bundle:Game');
        $games = $repo->getAllGamesForApi();
        $nl = "\r\n<br />";

        $this->echoUtf8('ID;Forduló;Mérkőzés;Csapat1;Gól1;Csapat2;Gól2;Hossz;Pontszám;');
        $this->echoUtf8($nl);
        foreach ($games as $game) {
            $l = $game['gameLength']['computerName'];
            if (strpos($game['gameType']['computerName'], 'eloselejtezo') === false && $l) {
                $p = 1;
                switch ($l) {
                    case 'B':
                        $p = 1;
                        break;
                    case 'E':
                        $p = 3;
                        break;
                    case 'S':
                        $p = 6;
                        break;
                    case 'U':
                        $p = 10;
                        break;
                }

                $this->echoUtf8($game['round']['number'] . '_' . $game['gameType']['computerName'] . ';');
                $this->echoUtf8($game['round']['name'] . ';');
                $this->echoUtf8($game['gameType']['name'] . ';');
                $this->echoUtf8($game['teamA']['name'] . ';');
                $this->echoUtf8($game['pointA'] . ';');
                $this->echoUtf8($game['teamB']['name'] . ';');
                $this->echoUtf8($game['pointB'] . ';');
                $this->echoUtf8($game['gameLength']['computerName'] . ';');
                $this->echoUtf8($p . ';');
                $this->echoUtf8($nl);
            }
        }

        $this->echoUtf8("$nl=============================================================$nl$nl");

        $this->echoUtf8('ID;Játékos;QF;QS;A;C;S;Y;I;D;GK;GKI;G;SP;');
        foreach ($games as $game) {
            $l = $game['gameLength']['computerName'];
            if (strpos($game['gameType']['computerName'], 'eloselejtezo') === FALSE && $l) {
                $currentNumber = -1;
                foreach ($game['playerResults'] as $pr) {
                    $num = $pr['player']['number'];

                    if ($num !== $currentNumber) {
                        $currentNumber = $num;
                        $this->echoUtf8($nl);
                        $this->echoUtf8($game['round']['number'] . '_' . $game['gameType']['computerName'] . ';');
                        $this->echoUtf8($num . ';');
                    }

                    $this->echoUtf8($pr['value'] . ';');
                }
            }
        }

        $this->echoUtf8($nl);
        $this->echoUtf8($nl);

        //return new JsonResponse($games);
        return new Response('');
    }

    /**
     * Gets current status.
     *
     * @Route("current-status", name="skyball2017_api_currentstatus", options={"expose" = true})
     * @Method("GET")
     */
    public function currentStatusAction()
    {
        $cacheService = $this->get('skyball2017_cache');
        $status = $cacheService->setAllRedisData();

        return new JsonResponse($status);
    }

    /**
     * Gets current status.
     *
     * @Route("refs", name="skyball2017_api_refs")
     * @Method("GET")
     */
    public function refsAction()
    {
        $playerService = $this->get('skyball2017_player');
        $refsByPlayers = $playerService->getRefs();

        return new JsonResponse($refsByPlayers);
    }

    /**
     * Gets all Kerchief results.
     *
     * @Route("kerchiefs", name="skyball2017_api_kerchiefs")
     * @Method("GET")
     */
    public function kerchiefsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->getKerchiefTypes();

        $playerService = $this->get('skyball2017_player');
        $kerchiefs = $playerService->getAllGlobalKerchiefsJson();

        $players = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->getPlayers();

        $ret = array(
            'kerchiefs' => $kerchiefs,
            'kcTypes'   => $kcTypes,
            'players'   => $players,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets all Kerchief results.
     *
     * @Route("kerchief-types", name="skyball2017_api_kerchief_types")
     * @Method("GET")
     */
    public function kerchiefTypesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->getKerchiefTypes();

        return new JsonResponse($kcTypes);
    }

    /**
     * Gets all current data.
     *
     * @Route("current-data", name="skyball2017_api_current_data")
     * @Method("GET")
     */
    public function currentDataAction()
    {
        $em = $this->getDoctrine()->getManager();
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->getKerchiefTypes();

        $playerService = $this->get('skyball2017_player');
        $kerchiefs = $playerService->getAllGlobalKerchiefsJson();

        $teams = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->getTeams();

        usort($teams, function($a, $b) {
            return $b['point'] - $a['point'];
        });

        foreach ($teams as &$team) {
            $team['logo'] = 'http://' . $this->getParameter('skyathlon_url') . '/bundles/skyathlonskyball2017/images/logos/' . $team['computerName'] . '.png';
        }

        $ret = array(
            'kerchiefs' => $kerchiefs,
            'kcTypes'   => $kcTypes,
            'teams'     => $teams,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets current round data.
     *
     * @Route("current-round", name="skyball2017_api_current_round")
     * @Method("GET")
     */
    public function currentRoundAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');
        $currentGameId = 0;
        $currentGameObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2017_currentGame',
        ));
        if ($currentGameObject) {
            $currentGameId = $currentGameObject->getValue();
        }

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2017_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }

        $round = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->getRound($currentRoundNumber);
        $groupTitles = $games = 0;

        if ($round) {
            $translator = $this->get('translator');

            foreach ($round['games'] as &$game) {
                $games++;
                $gt = &$game['gameType'];
                if ($gt['groupTitle']) {
                    $groupTitles++;
                    $gt['groupTitleTrans'] = $translator->trans('skyball2017.' . $gt['groupTitle']);
                }
            }
        }

        $ret = array(
            'round'       => $round,
            'currentGame' => $currentGameId,
            'groupTitles' => $groupTitles,
            'games'       => $games,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets round Teams.
     *
     * @Route("round-teams", name="skyball2017_api_round_teams")
     * @Method("GET")
     */
    public function roundTeamsAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2017_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }
        $currentRound = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array(
            'number' => $currentRoundNumber,
        ));

        $teams = array();
        if ($currentRound) {
            $teamService = $this->get('skyball2017_team');
            $teamObjects = $teamService->getRealRoundResults($currentRound);
            $i = 0;
            $points = array(
                array(
                    'point' => '12',
                    'rank'  => '1',
                ),
                array(
                    'point' => '9',
                    'rank'  => '2',
                ),
                array(
                    'point' => '7',
                    'rank'  => '3',
                ),
                array(
                    'point' => '5',
                    'rank'  => '4',
                ),
                array(
                    'point' => '4',
                    'rank'  => '5',
                ),
                array(
                    'point' => '3',
                    'rank'  => '6',
                ),
                array(
                    'point' => '2',
                    'rank'  => '7',
                ),
                array(
                    'point' => '1',
                    'rank'  => '8',
                ),
                array(
                    'point' => '0',
                    'rank'  => '9',
                ),
                array(
                    'point' => '0',
                    'rank'  => '10',
                ),
            );

            /** @var Team $teamObject */
            foreach ($teamObjects as $teamObject) {
                $teams[$i] = array(
                    'computerName' => $teamObject ? $teamObject['computerName'] : 'noteam',
                    'name'         => $teamObject ? $teamObject['name'] : '?',
                    'rank'         => $points[$i]['rank'],
                    'point'        => $points[$i]['point'],
                );

                $i++;
            }
        }

        return new JsonResponse($teams);
    }

    /**
     * Gets round Kerchief results.
     *
     * @Route("round-kerchiefs", name="skyball2017_api_round_kerchiefs")
     * @Method("GET")
     */
    public function roundKerchiefsAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2017_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }
        $currentRound = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array(
            'number' => $currentRoundNumber,
        ));
        $kcTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->getKerchiefTypes();

        $playerService = $this->get('skyball2017_player');
        $kerchiefs = $playerService->getRoundKerchiefsJson($currentRound);

        $players = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->getPlayers();

        $ret = array(
            'kerchiefs' => $kerchiefs,
            'kcTypes'   => $kcTypes,
            'players'   => $players,
        );

        return new JsonResponse($ret);
    }

    /**
     * Gets current round.
     *
     * @Route("round", name="skyball2017_api_round")
     * @Method("GET")
     */
    public function roundAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');

        $currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skyball2017_currentRound',
        ));
        if ($currentRoundObject) {
            $currentRoundNumber = $currentRoundObject->getValue();
        }
        $currentRound = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->getPureRound($currentRoundNumber);

        if ($currentRound) {
            $currentRound = $currentRound[0];
        }

        $ret = array(
            'round' => $currentRound,
        );

        return new JsonResponse($ret);
    }
}
