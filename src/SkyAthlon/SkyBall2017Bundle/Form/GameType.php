<?php

namespace SkyAthlon\SkyBall2017Bundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    protected $em;

    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $em */
        $em = $this->em;
        $current = false;

        /*
        if (!$options['data']->getId() || ($options['data']->getGameType() && $options['data']->getGameType()->getPlayoffs())) {
            $gameTypes = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameType')->getPlayoffGameTypes($options['custom']['games'], $options['data']->getId());

            $builder
                ->add('gameType', ChoiceType::class, array(
                    'choices' => $gameTypes,
                    'label' => 'skyball2017.game.type',
                ));
        }
        */

        $point = $options['custom']['point'];

        if ($options['data']->getId()) {
            $currentGameId = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                'name' => 'skyball2017_currentGame'
            ));

            if ($currentGameId && $currentGameId->getValue() == $options['data']->getId()) {
                $current = true;
            }
        }

        $possiblePoints = array(
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
        );

        $builder
            ->add('teamA', null, array(
                'label' => 'skyball2017.game.teamA',
                'attr'  => array(
                    'startFieldset' => 'skyball2017.game.basicStuff',
                ),
            ))
            ->add('teamB', null, array(
                'label' => 'skyball2017.game.teamB',
            ))
            ->add('pointA', null, array(
                'label' => 'skyball2017.game.pointA',
            ))
            ->add('pointB', null, array(
                'label' => 'skyball2017.game.pointB',
            ))
            ->add('gameLength', null, array(
                'label' => 'skyball2017.game.length',
                'attr'  => array(
                    'endFieldset' => true,
                ),
            ))
            ->add('qsA', null, array(
                'label' => 'skyball2017.game.qsA',
                'attr'  => array(
                    'startFieldset' => 'skyball2017.game.pointStuff',
                ),
            ))
            ->add('qsB', null, array(
                'label' => 'skyball2017.game.qsB',
            ))
            ->add('qgkA', null, array(
                'label' => 'skyball2017.game.qgkA',
            ))
            ->add('qgkB', null, array(
                'label' => 'skyball2017.game.qgkB',
                'attr'  => array(
                    'endFieldset' => true,
                ),
            ))
            ->add('ref', null, array(
                'label' => 'skyball2017.game.ref',
                'attr'  => array(
                    'startFieldset' => 'skyball2017.game.refStuff',
                ),
            ))
            ->add('sref', null, array(
                'label' => 'skyball2017.game.sref'
            ))
            ->add('jref', null, array(
                'label' => 'skyball2017.game.jref'
            ))
            ->add('srefb', null, array(
                'label' => 'skyball2017.game.jrefb'
            ))
            ->add('spicli', null, array(
                'label' => 'skyball2017.game.spicli'
            ))
            ->add('points', ChoiceType::class, array(
                'mapped' => false,
                'label' => 'skyball2017.ref.points',
                'choices' => $possiblePoints,
                'placeholder' => 'skyball2017.ref.nopoints',
                'data' => $point,
                'required' => false,
                'attr'  => array(
                    'endFieldset' => true,
                ),
            ))
            ->add('current', CheckboxType::class, array(
                'mapped' => false,
                'label' => 'skyball2017.game.current',
                'data' => $current,
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SkyAthlon\SkyBall2017Bundle\Entity\Game',
            'custom' => array(),
        ));
    }
}
