<?php

namespace SkyAthlon\BaseFall2016Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoundKerchiefResult
 *
 * @ORM\Table(name="basefall2016_round_kerchief_result")
 * @ORM\Entity(repositoryClass="SkyAthlon\BaseFall2016Bundle\Repository\RoundKerchiefResultRepository")
 */
class RoundKerchiefResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="KerchiefType", inversedBy="roundKerchiefResults")
     * @ORM\JoinColumn(name="kerchieftype_id", referencedColumnName="id")
     */
    protected $kerchiefType;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="roundKerchiefResults")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;

    /**
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="roundKerchiefResults")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    protected $round;

    public function __toString() {
        return $this->round . ' - ' . $this->kerchiefType . ' - ' . $this->player;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kerchiefType
     *
     * @param KerchiefType $kerchiefType
     *
     * @return RoundKerchiefResult
     */
    public function setKerchiefType(KerchiefType $kerchiefType = null)
    {
        $this->kerchiefType = $kerchiefType;

        return $this;
    }

    /**
     * Get kerchiefType
     *
     * @return KerchiefType
     */
    public function getKerchiefType()
    {
        return $this->kerchiefType;
    }

    /**
     * Set player
     *
     * @param Player $player
     *
     * @return RoundKerchiefResult
     */
    public function setPlayer(Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return RoundKerchiefResult
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }
}
