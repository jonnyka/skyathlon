<?php

namespace SkyAthlon\BaseFall2016Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlayerResult
 *
 * @ORM\Table(name="basefall2016_player_result")
 * @ORM\Entity(repositoryClass="SkyAthlon\BaseFall2016Bundle\Repository\PlayerResultRepository")
 */
class PlayerResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="smallint")
     */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="playerResults")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;

    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="playerResults")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;

    /**
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="playerResults")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    protected $round;

    /**
     * @ORM\ManyToOne(targetEntity="KerchiefType", inversedBy="playerResults")
     * @ORM\JoinColumn(name="kerchieftype_id", referencedColumnName="id")
     */
    protected $kerchiefType;

    public function __toString() {
        return $this->game . ' - ' . $this->player . ' - ' . $this->kerchiefType . ' - ' . $this->value;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return PlayerResult
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set player
     *
     * @param Player $player
     *
     * @return PlayerResult
     */
    public function setPlayer(Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set game
     *
     * @param Game $game
     *
     * @return PlayerResult
     */
    public function setGame(Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set kerchiefType
     *
     * @param KerchiefType $kerchiefType
     *
     * @return PlayerResult
     */
    public function setKerchiefType(KerchiefType $kerchiefType = null)
    {
        $this->kerchiefType = $kerchiefType;

        return $this;
    }

    /**
     * Get kerchiefType
     *
     * @return KerchiefType
     */
    public function getKerchiefType()
    {
        return $this->kerchiefType;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return PlayerResult
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }
}
