<?php

namespace SkyAthlon\BaseFall2016Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoundResult
 *
 * @ORM\Table(name="basefall2016_round_result")
 * @ORM\Entity(repositoryClass="SkyAthlon\BaseFall2016Bundle\Repository\RoundResultRepository")
 */
class RoundResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RoundResultType", inversedBy="roundResults")
     * @ORM\JoinColumn(name="roundresulttype_id", referencedColumnName="id")
     */
    protected $roundResultType;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="roundResults")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="roundResults")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    protected $round;

    public function __toString() {
        return $this->team . ' - ' . $this->roundResultType;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roundResultType
     *
     * @param RoundResultType $roundResultType
     *
     * @return RoundResult
     */
    public function setRoundResultType(RoundResultType $roundResultType = null)
    {
        $this->roundResultType = $roundResultType;

        return $this;
    }

    /**
     * Get roundResultType
     *
     * @return RoundResultType
     */
    public function getRoundResultType()
    {
        return $this->roundResultType;
    }

    /**
     * Set team
     *
     * @param Team $team
     *
     * @return RoundResult
     */
    public function setTeam(Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return RoundResult
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }
}
