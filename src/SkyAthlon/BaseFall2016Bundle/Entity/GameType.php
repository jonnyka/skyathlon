<?php

namespace SkyAthlon\BaseFall2016Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * GameType
 *
 * @ORM\Table(name="basefall2016_game_type")
 * @ORM\Entity(repositoryClass="SkyAthlon\BaseFall2016Bundle\Repository\GameTypeRepository")
 */
class GameType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=255, unique=true)
     */
    protected $computerName;

    /**
     * @var string
     *
     * @ORM\Column(name="groupTitle", type="string", length=255, nullable=true)
     */
    protected $groupTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint")
     */
    protected $weight;

    /**
     * @var int
     *
     * @ORM\Column(name="reversedWeight", type="smallint")
     */
    protected $reversedWeight;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="gameType")
     */
    protected $games;

    /**
     * @ORM\Column(name="notie", type="boolean")
     */
    protected $notie;

    /**
     * @ORM\Column(name="playoffs", type="boolean", nullable=true)
     */
    protected $playoffs;

    public function __toString() {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return GameType
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GameType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return GameType
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add game
     *
     * @param Game $game
     *
     * @return GameType
     */
    public function addGame(Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param Game $game
     */
    public function removeGame(Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set groupTitle
     *
     * @param string $groupTitle
     *
     * @return GameType
     */
    public function setGroupTitle($groupTitle)
    {
        $this->groupTitle = $groupTitle;

        return $this;
    }

    /**
     * Get groupTitle
     *
     * @return string
     */
    public function getGroupTitle()
    {
        return $this->groupTitle;
    }

    /**
     * Set notie
     *
     * @param boolean $notie
     *
     * @return GameType
     */
    public function setNotie($notie)
    {
        $this->notie = $notie;

        return $this;
    }

    /**
     * Get notie
     *
     * @return boolean
     */
    public function getNotie()
    {
        return $this->notie;
    }

    /**
     * Set reversedWeight
     *
     * @param integer $reversedWeight
     *
     * @return GameType
     */
    public function setReversedWeight($reversedWeight)
    {
        $this->reversedWeight = $reversedWeight;

        return $this;
    }

    /**
     * Get reversedWeight
     *
     * @return integer
     */
    public function getReversedWeight()
    {
        return $this->reversedWeight;
    }

    /**
     * Set playoffs
     *
     * @param boolean $playoffs
     *
     * @return GameType
     */
    public function setPlayoffs($playoffs)
    {
        $this->playoffs = $playoffs;

        return $this;
    }

    /**
     * Get playoffs
     *
     * @return boolean
     */
    public function getPlayoffs()
    {
        return $this->playoffs;
    }
}
