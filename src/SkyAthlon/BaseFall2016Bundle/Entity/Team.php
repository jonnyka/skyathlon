<?php

namespace SkyAthlon\BaseFall2016Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="basefall2016_team")
 * @ORM\Entity(repositoryClass="SkyAthlon\BaseFall2016Bundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=255)
     */
    protected $computerName;

    /**
     * @var string
     *
     * @ORM\Column(name="shortName", type="string", length=255)
     */
    protected $shortName;

    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="team")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    protected $players;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamA")
     */
    protected $gamesA;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamB")
     */
    protected $gamesB;

    /**
     * @ORM\OneToMany(targetEntity="Penality", mappedBy="team")
     */
    protected $penalities;

    /**
     * @ORM\OneToMany(targetEntity="RoundResult", mappedBy="team")
     */
    protected $roundResults;

    /**
     * @var int
     *
     * @ORM\Column(name="point", type="integer", nullable=true)
     */
    protected $point;
    /**
     *
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", nullable=true)
     */
    protected $rank;

    public function __toString()
    {
        return ($this->name && $this->shortName) ? $this->name . ' (' . $this->shortName . ')' : '?';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->gamesA = new ArrayCollection();
        $this->gamesB = new ArrayCollection();
        $this->roundResults = new ArrayCollection();
        $this->penalities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add player
     *
     * @param Player $player
     *
     * @return Team
     */
    public function addPlayer(Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param Player $player
     */
    public function removePlayer(Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add gamesA
     *
     * @param Game $gamesA
     *
     * @return Team
     */
    public function addGamesA(Game $gamesA)
    {
        $this->gamesA[] = $gamesA;

        return $this;
    }

    /**
     * Remove gamesA
     *
     * @param Game $gamesA
     */
    public function removeGamesA(Game $gamesA)
    {
        $this->gamesA->removeElement($gamesA);
    }

    /**
     * Get gamesA
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesA()
    {
        return $this->gamesA;
    }

    /**
     * Add gamesB
     *
     * @param Game $gamesB
     *
     * @return Team
     */
    public function addGamesB(Game $gamesB)
    {
        $this->gamesB[] = $gamesB;

        return $this;
    }

    /**
     * Remove gamesB
     *
     * @param Game $gamesB
     */
    public function removeGamesB(Game $gamesB)
    {
        $this->gamesB->removeElement($gamesB);
    }

    /**
     * Get gamesB
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesB()
    {
        return $this->gamesB;
    }

    /**
     * Set point
     *
     * @param integer $point
     *
     * @return Team
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Add roundResult
     *
     * @param RoundResult $roundResult
     *
     * @return Team
     */
    public function addRoundResult(RoundResult $roundResult)
    {
        $this->roundResults[] = $roundResult;

        return $this;
    }

    /**
     * Remove roundResult
     *
     * @param RoundResult $roundResult
     */
    public function removeRoundResult(RoundResult $roundResult)
    {
        $this->roundResults->removeElement($roundResult);
    }

    /**
     * Get roundResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundResults()
    {
        return $this->roundResults;
    }

    public function getRealName()
    {
        return ($this->name && $this->shortName) ? $this->name . ' (' . $this->shortName . ')' : '?';
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return Team
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return Team
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return Team
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Add penality
     *
     * @param Penality $penality
     *
     * @return Team
     */
    public function addPenality(Penality $penality)
    {
        $this->penalities[] = $penality;

        return $this;
    }

    /**
     * Remove penality
     *
     * @param Penality $penality
     */
    public function removePenality(Penality $penality)
    {
        $this->penalities->removeElement($penality);
    }

    /**
     * Get penalities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPenalities()
    {
        return $this->penalities;
    }
}
