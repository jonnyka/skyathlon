<?php

namespace SkyAthlon\BaseFall2016Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table(name="basefall2016_player")
 * @ORM\Entity(repositoryClass="SkyAthlon\BaseFall2016Bundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint", unique=true)
     */
    protected $number;
    
    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="players")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * @var string
     *
     * @ORM\Column(name="leader", type="boolean", options={"default" = false})
     */
    protected $leader;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint", options={"default" = 1})
     */
    protected $weight;

    /**
     * @ORM\OneToMany(targetEntity="PlayerResult", mappedBy="player")
     */
    protected $playerResults;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="ref")
     */
    protected $refs;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="sref")
     */
    protected $srefs;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="srefb")
     */
    protected $srefbs;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="srefc")
     */
    protected $srefcs;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="srefd")
     */
    protected $srefds;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="jref")
     */
    protected $jrefs;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="jrefb")
     */
    protected $jrefbs;

    /**
     * @ORM\OneToMany(targetEntity="RoundKerchiefResult", mappedBy="kerchiefType")
     */
    protected $roundKerchiefResults;

    /**
     * @ORM\OneToMany(targetEntity="Penality", mappedBy="player")
     */
    protected $penalities;

    public function __toString() {
        return $this->number . ' - ' . $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playerResults = new ArrayCollection();
        $this->roundKerchiefResults = new ArrayCollection();
        $this->penalities = new ArrayCollection();
        $this->refs = new ArrayCollection();
        $this->srefs = new ArrayCollection();
        $this->srefbs = new ArrayCollection();
        $this->srefcs = new ArrayCollection();
        $this->srefds = new ArrayCollection();
        $this->jrefs = new ArrayCollection();
        $this->jrefbs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Player
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set team
     *
     * @param Team $team
     *
     * @return Player
     */
    public function setTeam(Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set leader
     *
     * @param boolean $leader
     *
     * @return Player
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Get leader
     *
     * @return boolean
     */
    public function getLeader()
    {
        return $this->leader;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Player
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add playerResult
     *
     * @param PlayerResult $playerResult
     *
     * @return Player
     */
    public function addPlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults[] = $playerResult;

        return $this;
    }

    /**
     * Remove playerResult
     *
     * @param PlayerResult $playerResult
     */
    public function removePlayerResult(PlayerResult $playerResult)
    {
        $this->playerResults->removeElement($playerResult);
    }

    /**
     * Get playerResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerResults()
    {
        return $this->playerResults;
    }

    /**
     * Add roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     *
     * @return Player
     */
    public function addRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults[] = $roundKerchiefResult;

        return $this;
    }

    /**
     * Remove roundKerchiefResult
     *
     * @param RoundKerchiefResult $roundKerchiefResult
     */
    public function removeRoundKerchiefResult(RoundKerchiefResult $roundKerchiefResult)
    {
        $this->roundKerchiefResults->removeElement($roundKerchiefResult);
    }

    /**
     * Get roundKerchiefResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundKerchiefResults()
    {
        return $this->roundKerchiefResults;
    }

    /**
     * Add ref
     *
     * @param Game $ref
     *
     * @return Player
     */
    public function addRef(Game $ref)
    {
        $this->refs[] = $ref;

        return $this;
    }

    /**
     * Remove ref
     *
     * @param Game $ref
     */
    public function removeRef(Game $ref)
    {
        $this->refs->removeElement($ref);
    }

    /**
     * Get refs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRefs()
    {
        return $this->refs;
    }

    /**
     * Add sref
     *
     * @param Game $sref
     *
     * @return Player
     */
    public function addSref(Game $sref)
    {
        $this->srefs[] = $sref;

        return $this;
    }

    /**
     * Remove sref
     *
     * @param Game $sref
     */
    public function removeSref(Game $sref)
    {
        $this->srefs->removeElement($sref);
    }

    /**
     * Get srefs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSrefs()
    {
        return $this->srefs;
    }

    /**
     * Add jref
     *
     * @param Game $jref
     *
     * @return Player
     */
    public function addJref(Game $jref)
    {
        $this->jrefs[] = $jref;

        return $this;
    }

    /**
     * Remove jref
     *
     * @param Game $jref
     */
    public function removeJref(Game $jref)
    {
        $this->jrefs->removeElement($jref);
    }

    /**
     * Get jrefs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJrefs()
    {
        return $this->jrefs;
    }

    /**
     * Add srefb
     *
     * @param Game $srefb
     *
     * @return Player
     */
    public function addSrefb(Game $srefb)
    {
        $this->srefbs[] = $srefb;

        return $this;
    }

    /**
     * Remove srefb
     *
     * @param Game $srefb
     */
    public function removeSrefb(Game $srefb)
    {
        $this->srefbs->removeElement($srefb);
    }

    /**
     * Get srefbs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSrefbs()
    {
        return $this->srefbs;
    }

    /**
     * Add srefc
     *
     * @param Game $srefc
     *
     * @return Player
     */
    public function addSrefc(Game $srefc)
    {
        $this->srefcs[] = $srefc;

        return $this;
    }

    /**
     * Remove srefc
     *
     * @param Game $srefc
     */
    public function removeSrefc(Game $srefc)
    {
        $this->srefcs->removeElement($srefc);
    }

    /**
     * Get srefcs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSrefcs()
    {
        return $this->srefcs;
    }

    /**
     * Add srefd
     *
     * @param Game $srefd
     *
     * @return Player
     */
    public function addSrefd(Game $srefd)
    {
        $this->srefds[] = $srefd;

        return $this;
    }

    /**
     * Remove srefd
     *
     * @param Game $srefd
     */
    public function removeSrefd(Game $srefd)
    {
        $this->srefds->removeElement($srefd);
    }

    /**
     * Get srefds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSrefds()
    {
        return $this->srefds;
    }

    /**
     * Add jrefb
     *
     * @param Game $jrefb
     *
     * @return Player
     */
    public function addJrefb(Game $jrefb)
    {
        $this->jrefbs[] = $jrefb;

        return $this;
    }

    /**
     * Remove jrefb
     *
     * @param Game $jrefb
     */
    public function removeJrefb(Game $jrefb)
    {
        $this->jrefbs->removeElement($jrefb);
    }

    /**
     * Get jrefbs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJrefbs()
    {
        return $this->jrefbs;
    }

    /**
     * Add penality
     *
     * @param Penality $penality
     *
     * @return Player
     */
    public function addPenality(Penality $penality)
    {
        $this->penalities[] = $penality;

        return $this;
    }

    /**
     * Remove penality
     *
     * @param Penality $penality
     */
    public function removePenality(Penality $penality)
    {
        $this->penalities->removeElement($penality);
    }

    /**
     * Get penalities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPenalities()
    {
        return $this->penalities;
    }
}
