$('#sortable').sortable().disableSelection();

$('#reorder').click(function () {
    var ids = [];

    $(this).prop('disabled', true);

    $('li.ui-sortable-handle').each(function () {
        ids.push($(this).attr('id'));
    });

    $.ajax({
        url: Routing.generate('reorder_playoff_save'),
        type: 'POST',
        data: {data: ids},
        success: function (data) {
            window.location.href = Routing.generate('round_redirect');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $(this).prop('disabled', false);
            console.log('Error: ' + errorThrown);
        }
    });
});