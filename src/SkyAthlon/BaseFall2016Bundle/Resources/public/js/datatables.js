$('#edit-player-results').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6],
            'orderSequence': ["desc", "asc"],
            'orderDataType': 'sortbyInput',
            'sType': 'numeric'
        }
    ]
});

$('#by-players-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6,7],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [11, 'desc']
    ]
});

$('.round-round-table, .by-players-table:not("#by-players-table")').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [2, 'desc']
    ]
});

$('.refs-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': [0],
            'orderable': false
        },

        {
            'targets': [1,2,3],
            'orderSequence': ["desc", "asc"]
        }
    ],
    'order': [
        [1, 'desc']
    ]
});

$('#by-players-ranks-table').dataTable({
    'paging':   false,
    'info':     false,
    'filter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columnDefs': [
        {
            'targets': 0,
            'orderDataType': 'sortby',
            'sType': 'numeric'
        },
        {
            'targets': [1,2,3,4,5,6],
            'orderSequence': ["asc", "desc"]
        },
        {
            'targets': 7,
            'orderDataType': 'sortbyshow',
            'sType': 'numeric'
        }
    ],
    'order': [
        [7, 'asc']
    ]
});

$('#team-table').dataTable({
    'paging':   false,
    'info':     false,
    'bFilter':   false,
    'language': {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json'
    },
    'columns': [
        {'name': 'first', 'orderable': true},
        {'name': 'flag', 'orderable': false},
        {'name': 'player1', 'orderable': false},
        {'name': 'player2', 'orderable': false},
        {'name': 'player3', 'orderable': false},
        {'name': 'point', 'orderable': true, 'orderDataType': 'point', type: 'numeric'}
    ],
    'order': [
        [5, 'asc'],
        [0, 'asc']
    ]
});