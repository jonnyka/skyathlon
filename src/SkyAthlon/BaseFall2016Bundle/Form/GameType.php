<?php

namespace SkyAthlon\BaseFall2016Bundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    protected $em;

    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['data']->getId() || ($options['data']->getGameType() && $options['data']->getGameType()->getPlayoffs())) {
            /** @var EntityManager $em */
            $em = $this->em;
            $gameTypes = $em->getRepository('SkyAthlonBaseFall2016Bundle:GameType')->getPlayoffGameTypes($options['attr'], $options['data']->getId());

            $builder
                ->add('gameType', ChoiceType::class, array(
                    'choices' => $gameTypes,
                    'label' => 'basefall2016.game.type',
                ));
        }

        $builder
            ->add('teamA', null, array(
                'label' => 'basefall2016.game.teamA',
                'attr' => array(
                    'class' => 'teamA'
                )
            ))
            ->add('teamB', null, array(
                'label' => 'basefall2016.game.teamB'
            ))
            ->add('pointA', null, array(
                'label' => 'basefall2016.game.pointA'
            ))
            ->add('pointB', null, array(
                'label' => 'basefall2016.game.pointB'
            ))
            ->add('ref', null, array(
                'label' => 'basefall2016.game.ref'
            ))
            ->add('sref', null, array(
                'label' => 'basefall2016.game.sref'
            ))
            ->add('srefb', null, array(
                'label' => 'basefall2016.game.srefb'
            ))
            ->add('srefc', null, array(
                'label' => 'basefall2016.game.srefc'
            ))
            ->add('srefd', null, array(
                'label' => 'basefall2016.game.srefd'
            ))
            ->add('jref', null, array(
                'label' => 'basefall2016.game.jref'
            ))
            ->add('jrefb', null, array(
                'label' => 'basefall2016.game.jrefb'
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SkyAthlon\BaseFall2016Bundle\Entity\Game'
        ));
    }
}
