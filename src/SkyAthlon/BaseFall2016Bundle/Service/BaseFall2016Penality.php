<?php

namespace SkyAthlon\BaseFall2016Bundle\Service;

use SkyAthlon\BaseFall2016Bundle\Entity\Game;
use SkyAthlon\BaseFall2016Bundle\Entity\Round;
use SkyAthlon\BaseFall2016Bundle\Entity\RoundResult;
use SkyAthlon\BaseFall2016Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseFall2016Penality extends Controller
{
    protected $em;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getPenalities() {
        $em = $this->em;
        $ret = array();

        $teams = $em->getRepository('SkyAthlonBaseFall2016Bundle:Team')->getTeams();
        foreach ($teams as $tid => $team) {
            $ret[$tid] = array('team' => $team);
            $ret[$tid]['team']['points'] = 0;

            foreach ($team['players'] as $pid => $player) {
                $ret[$tid]['players'][$player['id']] = $player;
                $ret[$tid]['players'][$player['id']]['points'] = 0;
            }
        }

        $penalities = $em->getRepository('SkyAthlonBaseFall2016Bundle:Penality')->findAll();

        foreach ($penalities as $penality) {
            $ret[$penality->getTeam()->getId()]['team']['points'] += $penality->getPoint();
            $ret[$penality->getTeam()->getId()]['players'][$penality->getPlayer()->getId()]['points'] += $penality->getPoint();
        }

        return $ret;
    }
}
