<?php

namespace SkyAthlon\BaseFall2016Bundle\Service;

use SkyAthlon\BaseFall2016Bundle\Entity\Game;
use SkyAthlon\BaseFall2016Bundle\Entity\Round;
use SkyAthlon\BaseFall2016Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseFall2016Cache extends Controller
{
    protected $em;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function calculateTeamPoints() {
        $em = $this->em;

        $teamsArray = array();
        $teamObjects = array();
        $teams = $em->getRepository('SkyAthlonBaseFall2016Bundle:Team')->findAll();
        foreach ($teams as $team) {
            $teamsArray[$team->getId()] = 0;
            $teamObjects[$team->getId()] = $team;
        }

        $rounds = $em->getRepository('SkyAthlonBaseFall2016Bundle:Round')->getAllRoundsQuery()->getResult();

        if ($rounds) {
            /** @var Round $round */
            foreach ($rounds as $round) {
                /** @var Game $game */
                foreach ($round->getGames() as $game) {
                    $gameType = $game->getGameType();
                    $gameTypeName = $gameType->getComputerName();
                    $teamA = $game->getTeamA();
                    $teamB = $game->getTeamB();
                    $pointA = $game->getPointA();
                    $pointB = $game->getPointB();
                    /** @var Team $winner */
                    $winner = $pointA > $pointB ? $teamA : $teamB;
                    $loser = $pointA < $pointB ? $teamA : $teamB;

                    if ($teamA && $teamB && is_int($pointA) && is_int($pointB)) {
                        switch ($gameTypeName) {
                            case 'helyoszto_7_8':
                                $teamsArray[$winner->getId()] += 2;
                                $teamsArray[$loser->getId()] += 1;
                                break;
                            case 'helyoszto_5_6':
                                $teamsArray[$winner->getId()] += 4;
                                $teamsArray[$loser->getId()] += 3;
                                break;
                            case 'donto_3':
                                $teamsArray[$winner->getId()] += 7;
                                $teamsArray[$loser->getId()] += 5;
                                break;
                            case 'donto_1':
                                $teamsArray[$winner->getId()] += 12;
                                $teamsArray[$loser->getId()] += 9;
                                break;
                        }
                    }
                }
            }
        }

        arsort($teamsArray);

        $lastPoint = -9999;
        $i = 0;
        $j = 1;

        foreach ($teamsArray as $teamId => $teamPoint) {
            $i++;
            if ($teamPoint !== $lastPoint) {
                $j = $i;
            }

            /** @var Team $team */
            $team = $teamObjects[$teamId];
            $team->setPoint($teamPoint);
            $team->setRank($j);
            $em->persist($team);

            $lastPoint = $teamPoint;
        }
        $em->flush();
    }
}
