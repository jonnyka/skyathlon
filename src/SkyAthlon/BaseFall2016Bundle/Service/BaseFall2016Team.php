<?php

namespace SkyAthlon\BaseFall2016Bundle\Service;

use SkyAthlon\BaseFall2016Bundle\Entity\Game;
use SkyAthlon\BaseFall2016Bundle\Entity\Round;
use SkyAthlon\BaseFall2016Bundle\Entity\RoundResult;
use SkyAthlon\BaseFall2016Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseFall2016Team extends Controller
{
    protected $em;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRoundResults(Round $round, $onlyIds = false) {
        $em = $this->em;

        $games = $em->getRepository('SkyAthlonBaseFall2016Bundle:Game')->findBy(array(
            'round' => $round,
        ));

        $teams = $em->getRepository('SkyAthlonBaseFall2016Bundle:Team')->findAll();

        $team8 = $team7 = $team6 = $team5 = $team4 = $team3 = $team2 = $team1 = new Team();

        foreach ($games as $game) {
            switch ($game->getGameType()->getComputerName()) {
                case 'helyoszto_7_8':
                    $team8 = $game->getTeamA();
                    $team7 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team7 = $game->getTeamA();
                        $team8 = $game->getTeamB();
                    }

                    break;

                case 'helyoszto_5_6':
                    $team6 = $game->getTeamA();
                    $team5 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team5 = $game->getTeamA();
                        $team6 = $game->getTeamB();
                    }

                    break;

                case 'donto_3':
                    $team4 = $game->getTeamA();
                    $team3 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team3 = $game->getTeamA();
                        $team4 = $game->getTeamB();
                    }

                    break;

                case 'donto_1':
                    $team2 = $game->getTeamA();
                    $team1 = $game->getTeamB();
                    if ($game->getPointA() > $game->getPointB()) {
                        $team1 = $game->getTeamA();
                        $team2 = $game->getTeamB();
                    }

                    break;
            }
        }

        if ($onlyIds) {
            $team1 = $team1 ? $team1->getId() : null;
            $team2 = $team2 ? $team2->getId() : null;
            $team3 = $team3 ? $team3->getId() : null;
            $team4 = $team4 ? $team4->getId() : null;
            $team5 = $team5 ? $team5->getId() : null;
            $team6 = $team6 ? $team6->getId() : null;
            $team7 = $team7 ? $team7->getId() : null;
            $team8 = $team8 ? $team8->getId() : null;
        }

        $ret = array(
            'team1' => $team1,
            'team2' => $team2,
            'team3' => $team3,
            'team4' => $team4,
            'team5' => $team5,
            'team6' => $team6,
            'team7' => $team7,
            'team8' => $team8,
        );

        foreach ($teams as $team) {
            if (!in_array($team->getId(), $ret)) {
                $ret['team9'] = $team->getId();
            }
        }

        return $ret;
    }

    public function getRealRoundResults(Round $round, $onlyIds = false) {
        $em = $this->em;
        $rrRepo = $em->getRepository('SkyAthlonBaseFall2016Bundle:RoundResult');
        $rrTypeRepo = $em->getRepository('SkyAthlonBaseFall2016Bundle:RoundResultType');

        $team9 = $team8 = $team7 = $team6 = $team5 = $team4 = $team3 = $team2 = $team1 = new Team();

        for ($i = 1; $i <= 9; $i++) {
            $roundResultType = $rrTypeRepo->findOneBy(array(
                'computerName' => 'hely_' . $i,
            ));
            $roundResult = $rrRepo->findOneBy(array(
                'round' => $round,
                'roundResultType' => $roundResultType
            ));

            if ($roundResult) {
                $teamStr = 'team' . $i;
                $$teamStr = $roundResult->getTeam();
            }
        }

        if ($onlyIds) {
            $team1 = $team1 ? $team1->getId() : null;
            $team2 = $team2 ? $team2->getId() : null;
            $team3 = $team3 ? $team3->getId() : null;
            $team4 = $team4 ? $team4->getId() : null;
            $team5 = $team5 ? $team5->getId() : null;
            $team6 = $team6 ? $team6->getId() : null;
            $team7 = $team7 ? $team7->getId() : null;
            $team8 = $team8 ? $team8->getId() : null;
            $team9 = $team9 ? $team9->getId() : null;
        }

        $ret = array(
            'team1' => $team1,
            'team2' => $team2,
            'team3' => $team3,
            'team4' => $team4,
            'team5' => $team5,
            'team6' => $team6,
            'team7' => $team7,
            'team8' => $team8,
            'team9' => $team9,
        );

        return $ret;
    }

    public function createRoundResults(Round $round, $results) {
        $em = $this->em;
        $teamRepo = $em->getRepository('SkyAthlonBaseFall2016Bundle:Team');
        $rrTypeRepo = $em->getRepository('SkyAthlonBaseFall2016Bundle:RoundResultType');
        $rrRepo = $em->getRepository('SkyAthlonBaseFall2016Bundle:RoundResult');

        foreach ($results as $standing => $teamId) {
            $standing = 'hely_' . str_replace('round-team-', '', $standing);
            $roundResultType = $rrTypeRepo->findOneBy(array('computerName' => $standing));
            $team = new Team();
            if ($teamId !== 0) {
                $team = $teamRepo->find($teamId);
            }
            $roundResult = $rrRepo->findOneBy(array(
                'round' => $round,
                'roundResultType' => $roundResultType
            ));

            if (!$roundResult) {
                $roundResult = new RoundResult();
                $roundResult->setRound($round);
                $roundResult->setRoundResultType($roundResultType);
            }

            $roundResult->setTeam($team);

            $em->persist($roundResult);
        }

        $em->flush();
    }
}
