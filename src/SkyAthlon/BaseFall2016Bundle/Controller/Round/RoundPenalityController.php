<?php

namespace SkyAthlon\BaseFall2016Bundle\Controller\Round;

use SkyAthlon\BaseFall2016Bundle\Entity\Penality;
use SkyAthlon\BaseFall2016Bundle\Entity\Round;
use SkyAthlon\BaseFall2016Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/basefall-2016/fordulo")
 */
class RoundPenalityController extends Controller
{
    /**
     * Show current round player results.
     *
     * @Route("/{round}/buntetopont/", name="basefall2016_round_penalities", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexPenalitiesAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonBaseFall2016Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();

            /** @var Round $r */
            $r = $repo->findOneBy(array('number' => $round));
            $penalities = $em->getRepository('SkyAthlonBaseFall2016Bundle:Penality')->findBy(array(
                'round' => $r,
            ));

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            return array(
                'rounds'     => $pagination,
                'penalities' => $penalities,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{roundid}/buntetopont/uj/", name="basefall2016_penality_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function newPenalityAction(Request $request, $roundid)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonBaseFall2016Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $penality = new Penality();
                $form = $this->createForm('SkyAthlon\BaseFall2016Bundle\Form\PenalityType', $penality);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $penality->setRound($round);
                    $penality->setTeam($penality->getPlayer()->getTeam());

                    $em->persist($penality);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.basefall2016.penality.created');

                    return $this->redirect($this->generateUrl('basefall2016_round_penalities', array('round' => $roundid)));
                }

                return array(
                    'penality' => $penality,
                    'roundid' => $roundid,
                    'form' => $form->createView(),
                );
            }
            else {
                return $this->redirectToRoute('basefall2016_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing Round entity.
     *
     * @Route("/{roundid}/buntetopont/{id}/szerkesztes/", name="basefall2016_penality_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param roundid
     * @param Penality $penality
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editPenalityAction(Request $request, $roundid, Penality $penality)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonBaseFall2016Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $deleteForm = $this->createDeletePenalityForm($roundid, $penality);
                $editForm = $this->createForm('SkyAthlon\BaseFall2016Bundle\Form\PenalityType', $penality);
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $penality->setTeam($penality->getPlayer()->getTeam());
                    $em->persist($penality);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.basefall2016.penality.edited');

                    return $this->redirectToRoute('basefall2016_round_penalities', array('round' => $round->getNumber()));
                }

                return array(
                    'round' => $round,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                );
            }
            else {
                return $this->redirectToRoute('basefall2016_round', array('round' => $round->getNumber()));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Deletes a Penality entity.
     *
     * @Route("/{roundid}/buntetopont/{id}/", name="basefall2016_penality_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param $roundid
     * @param Penality $penality
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deletePenalityAction(Request $request, $roundid, Penality $penality)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonBaseFall2016Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $form = $this->createDeletePenalityForm($roundid, $penality);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em->remove($penality);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.basefall2016.penality.deleted', 'warning');
                }

                return $this->redirectToRoute('basefall2016_round_penalities', array('round' => $round->getNumber()));
            }
            else {
                return $this->redirectToRoute('basefall2016_round_redirect');
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a form to delete a Penality entity.
     *
     * @param $roundid
     * @param Penality $penality
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeletePenalityForm($roundid, Penality $penality)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('basefall2016_penality_delete', array('roundid' => $roundid, 'id' => $penality->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
