<?php

namespace SkyAthlon\BaseFall2016Bundle\Controller\Round;

use SkyAthlon\BaseFall2016Bundle\Entity\Player;
use SkyAthlon\BaseFall2016Bundle\Entity\Round;
use SkyAthlon\BaseFall2016Bundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round player controller.
 *
 * @Route("/basefall-2016/fordulo")
 */
class RoundPlayerController extends Controller
{
    /**
     * Show current round player results.
     *
     * @Route("/{round}/jatekos-statisztika/", name="basefall2016_round_players", requirements={"round" = "\d+"}, defaults={"round" = 1}, options={"expose" = true})
     * @Method("GET")
     * @Template()
     * @param int $round
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexPlayersAction($round)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoundRepository $repo */
            $repo = $em->getRepository('SkyAthlonBaseFall2016Bundle:Round');
            $rounds = $repo->getAllRoundsQuery();
            $kerchiefTypes = $em->getRepository('SkyAthlonBaseFall2016Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

            /** @var Round $r */
            $r = $repo->findOneBy(array('number' => $round));
            $playerService = $this->get('basefall2016_player');
            $kerchiefs = $playerService->getKerchiefsForRound($r);
            $rankings = $playerService->getRealKerchiefResultsForRound($r);

            $players = $em->getRepository('SkyAthlonBaseFall2016Bundle:Player')->findAll();
            $playersArray = array();
            /** @var Player $player */
            foreach ($players as $player) {
                $playersArray[$player->getId()] = $player;
            }

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $round,
                1,
                array(
                    'pageParameterName' => 'round',
                    'wrap-queries' => true,
                )
            );

            return array(
                'rounds'    => $pagination,
                'kcTypes'   => $kerchiefTypes,
                'kerchiefs' => $kerchiefs['ret'],
                'kcValues'  => $kerchiefs['kcValues'],
                'players'   => $playersArray,
                'rankings'  => $rankings,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit the player stats for a round.
     *
     * @Route("/{roundid}/jatekos-statisztika/szerkesztes/", name="basefall2016_round_edit_player_stats")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editRoundPlayerStatsAction(Request $request, $roundid)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonBaseFall2016Bundle:Round')->findOneBy(array('number' => $roundid));

            if ((($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) && !$round->getPlayoffs()) {
                $playerService = $this->get('basefall2016_player');

                if ($request->getMethod() === 'POST') {
                    $values = $request->request->all();
                    $playerService->createRoundResults($round, $values);

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.basefall2016.roundplayerstats.edited');

                    return $this->redirectToRoute('basefall2016_round_players', array('round' => $roundid));
                }

                $players = $em->getRepository('SkyAthlonBaseFall2016Bundle:Player')->getPlayers();
                $rankings = $playerService->getRealKerchiefResultsForRound($round);

                return array(
                    'roundid'  => $roundid,
                    'players'  => $players,
                    'rankings' => $rankings,
                );
            }
            else {
                return $this->redirectToRoute('basefall2016_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
