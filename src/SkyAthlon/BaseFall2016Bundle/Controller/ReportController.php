<?php

namespace SkyAthlon\BaseFall2016Bundle\Controller;

use SkyAthlon\BaseFall2016Bundle\Repository\GameRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Report controller.
 *
 * @Route("/basefall-2016/")
 */
class ReportController extends Controller
{
    /**
     * Gets report data.
     *
     * @Route("jegyzokonyvek/", name="basefall2016_reports")
     * @Template()
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function reportAction() {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var GameRepository $repo */
            $repo = $em->getRepository('SkyAthlonBaseFall2016Bundle:Game');
            $games = $repo->getAllGames();
            $kerchiefTypes = $em->getRepository('SkyAthlonBaseFall2016Bundle:KerchiefType')->findBy(array(), array('weight' => 'ASC'));

            return array(
                'kcTypes' => $kerchiefTypes,
                'games' => $games,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Gets penality data.
     *
     * @Route("buntetopontok/", name="basefall2016_penalities")
     * @Template()
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function penalityAction() {
        if ($this->getUser()) {
            $penalityService = $this->get('basefall2016_penality');
            $penalities = $penalityService->getPenalities();

            return array(
                'penalities' => $penalities,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }
}
