<?php

namespace SkyAthlon\SkySports2017Bundle\Service;

use AppBundle\Repository\SettingsRepository;
use SkyAthlon\SkySports2017Bundle\Entity\Game;
use SkyAthlon\SkySports2017Bundle\Entity\GameType;
use SkyAthlon\SkySports2017Bundle\Entity\Round;
use SkyAthlon\SkySports2017Bundle\Entity\Team;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Predis\Client as Redis;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class SkySports2017Cache extends Controller
{
    protected $em;
    protected $redis;
    protected $currentRoundNumber;
    protected $translator;
    protected $sports;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param Redis $redis
     * @param Translator $translator
     */
    public function __construct(EntityManager $em, Redis $redis, Translator $translator)
    {
        $this->em = $em;
        $this->redis = $redis;
        $this->translator = $translator;

        $this->sports = array(
            'waterskyball',
            'roplabda',
            'foci',
            'pingpong',
        );

        /** @var SettingsRepository $settingsRepo */
        $settingsRepo = $em->getRepository('AppBundle:Settings');
        $this->currentRoundNumber = 1;
        $currentRoundObject = $settingsRepo->findOneBy(array(
            'name' => 'skysports2017_currentRound',
        ));
        if ($currentRoundObject) {
            $this->currentRoundNumber = $currentRoundObject->getValue();
        }
    }

    public function calculateTeamPoints() {
        $em = $this->em;

        $teamsArray = array();
        $teamObjects = array();
        $teams = $em->getRepository('SkyAthlonSkySports2017Bundle:Team')->findAll();
        /** @var Team $team */
        foreach ($teams as $team) {
            $teamsArray[$team->getId()] = array(
                'point' => 0,
                'goal' => 0,
                'computerName' => $team->getComputerName()
            );
            $teamObjects[$team->getId()] = $team;
        }

        $rounds = $em->getRepository('SkyAthlonSkySports2017Bundle:Round')->getAllRoundsQuery()->getResult();

        if ($rounds) {
            /** @var Round $round */
            foreach ($rounds as $round) {
                /** @var Game $game */
                foreach ($round->getGames() as $game) {
                    $teamA = $game->getTeamA();
                    $teamB = $game->getTeamB();
                    $pointA = $game->getPointA();
                    $pointB = $game->getPointB();
                    $pa = $pb = 1;

                    if ($teamA && $teamB && is_int($pointA) && is_int($pointB)) {
                        if ($pointA > $pointB) {
                            $pa = 3;
                            $pb = 0;
                        }
                        elseif ($pointA < $pointB) {
                            $pa = 0;
                            $pb = 3;
                        }

                        $teamsArray[$teamA->getId()]['point'] += $pa;
                        $teamsArray[$teamB->getId()]['point'] += $pb;
                        $teamsArray[$teamA->getId()]['goal'] += $pointA - $pointB;
                        $teamsArray[$teamB->getId()]['goal'] += $pointB - $pointA;
                    }
                }
            }
        }

        uasort($teamsArray, array($this, 'sortTeams'));

        $lastPoint = $lastGoal = -9999;
        $i = 0;
        $j = 1;

        foreach ($teamsArray as $teamId => $teamPoints) {
            $teamPoint = $teamPoints['point'];
            $teamGoal = $teamPoints['goal'];
            $i++;
            if ($teamPoint !== $lastPoint && $teamGoal !== $lastGoal) {
                $j = $i;
            }

            /** @var Team $team */
            $team = $teamObjects[$teamId];
            $team->setPoint($teamPoint);
            $team->setGoal($teamGoal);
            $team->setRank($j);
            $em->persist($team);

            $lastPoint = $teamPoint;
            $lastGoal = $teamGoal;
        }

        $em->flush();
    }

    public function getRoundGames(Round $round) {
        $gamesArray = array();

        foreach ($round->getGames() as $game) {
            /** @var Team $teamA */
            $teamA = $game->getTeamA();
            /** @var Team $teamB */
            $teamB = $game->getTeamB();
            $pointA = $game->getPointA();
            $pointB = $game->getPointB();
            /** @var GameType $gameType */
            $gameType = $game->getGameType();
            $gt = $gameType->getGroupTitle() ? $this->translator->trans($gameType->getGroupTitle()) : '';

            if ($teamA && $teamB && is_int($pointA) && is_int($pointB)) {
                $gamesArray[] = array(
                    'title' => $gt,
                    'teamA' => array(
                        'computerName' => $teamA->getComputerName(),
                        'name'         => $teamA->getName(),
                        'point'        => $pointA,
                    ),
                    'teamB' => array(
                        'computerName' => $teamB->getComputerName(),
                        'name'         => $teamB->getName(),
                        'point'        => $pointB
                    ),
                );
            }
        }

        $this->setRedis('SA', array('roundGames' => $gamesArray));

        return $gamesArray;
    }

    public function getRoundTeams(Round $round) {
        $em = $this->em;

        $teamsArray = array();
        $teams = $em->getRepository('SkyAthlonSkySports2017Bundle:Team')->findAll();
        foreach ($teams as $team) {
            $teamsArray[$team->getId()] = array(
                'computerName' => $team->getComputerName(),
                'name'         => $team->getName(),
                'point'        => 0,
                'goal'         => 0,
                'waterskyball' => array(
                    'point'        => 0,
                    'goal'         => 0,
                ),
                'roplabda' => array(
                    'point'        => 0,
                    'goal'         => 0,
                ),
                'foci' => array(
                    'point'        => 0,
                    'goal'         => 0,
                ),
                'pingpong' => array(
                    'point'        => 0,
                    'goal'         => 0,
                ),
            );
        }

        $teamsArray = $this->getTeamsForRound($teamsArray, $round);

        usort($teamsArray, array($this, 'sortTeams'));

        $this->setRedis('SA', array('roundTeams' => $teamsArray));

        return $teamsArray;
    }

    public function getRoundRanks(Round $round) {
        $teamsArray = $this->getRoundTeams($round);
        $sports = $this->sports;
        $sportsArray = array();
        foreach ($sports as $sport) {
            $sportsArray[$sport] = array();
        }

        foreach ($sports as $sport) {
            foreach ($teamsArray as $team) {
                $sportsArray[$sport][] = array(
                    'computerName' => $team['computerName'],
                    'name'         => $team['name'],
                    'point'        => $team[$sport]['point'],
                    'goal'         => $team[$sport]['goal'],
                );
            }
        }

        foreach ($sportsArray as &$s) {
            usort($s, array($this, 'sortTeams'));
        }

        $this->setRedis('SA', array('roundRanks' => $sportsArray));

        return $sportsArray;
    }

    protected function getTeamsForRound($teamsArray, Round $round) {
        /** @var Game $game */
        foreach ($round->getGames() as $game) {
            $gameType = $game->getGameType();
            $gameTypeName = $gameType->getComputerName();
            $sport = $this->getSportFromGameType($gameTypeName);
            $teamA  = $game->getTeamA();
            $teamB  = $game->getTeamB();
            $pointA = $game->getPointA();
            $pointB = $game->getPointB();
            $pa = $pb = 1;

            if ($teamA && $teamB && is_int($pointA) && is_int($pointB)) {
                $a = &$teamsArray[$teamA->getId()];
                $b = &$teamsArray[$teamB->getId()];
                if ($pointA > $pointB) {
                    $pa = 3;
                    $pb = 0;
                }
                elseif ($pointA < $pointB) {
                    $pa = 0;
                    $pb = 3;
                }

                $a['point'] += $pa;
                $a['goal'] += ($pointA - $pointB);
                $b['point'] += $pb;
                $b['goal'] += ($pointB - $pointA);
                $a[$sport]['point'] += $pa;
                $a[$sport]['goal'] += ($pointA - $pointB);
                $b[$sport]['point'] += $pb;
                $b[$sport]['goal'] += ($pointB - $pointA);
            }
        }

        return $teamsArray;
    }

    protected function getSportFromGameType($gameType) {
        $ret = 'waterskyball';
        $sports = $this->sports;

        foreach ($sports as $sport) {
            if (strpos($gameType, $sport) !== false) {
                $ret = $sport;
            }
        }

        return $ret;
    }

    protected function gameFinished($game, $light = false) {
        if ($light) {
            return ($game && $game['teamA'] && $game['teamB']);
        }
        else {
            return ($game && $game['gameLength'] && $game['teamA'] && $game['teamB'] && is_int($game['pointA']) && is_int($game['pointB']));
        }
    }

    protected function sortTeams($a, $b) {
        if (array_key_exists('point', $a) && array_key_exists('point', $b)) {
            if ($a['point'] === $b['point']) {
                if ($a['goal'] === $b['goal']) {
                    return ($a['computerName'] > $b['computerName']) ? 1 : -1;
                }
                else {
                    return ($a['goal'] < $b['goal']) ? 1 : -1;
                }
            }
            else {
                return ($a['point'] < $b['point']) ? 1 : -1;
            }
        }

        return 0;
    }

    public function setAllRedis() {
        $round = $this->em->getRepository('SkyAthlonSkySports2017Bundle:Round')->find($this->currentRoundNumber);

        return array(
            'games' => $this->getRoundGames($round),
            'teams' => $this->getRoundTeams($round),
            'ranks' => $this->getRoundRanks($round),
        );
    }

    /**
     * @param string $type
     * @param $array
     */
    protected function setRedis($type = 'SA', $array) {
        $redis = $this->redis;

        $settings = array(
            'currentRoundIdSA' => $this->currentRoundNumber,
        );

        foreach ($array as $key => $value) {
            $redis->set('livestats.' . $type . '.' . $key, json_encode($value));
        }

        $redis->set('settings', json_encode($settings));
    }
}
