<?php

namespace SkyAthlon\SkySports2017Bundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teamA', null, array(
                'label' => 'skysports2017.game.teamA',
                'attr'  => array(
                    'startFieldset' => 'skysports2017.game.basicStuff',
                ),
            ))
            ->add('teamB', null, array(
                'label' => 'skysports2017.game.teamB',
            ))
            ->add('pointA', null, array(
                'label' => 'skysports2017.game.pointA',
            ))
            ->add('pointB', null, array(
                'label' => 'skysports2017.game.pointB',
                'attr'  => array(
                    'endFieldset' => true,
                ),
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SkyAthlon\SkySports2017Bundle\Entity\Game',
            'custom' => array(),
        ));
    }
}
