<?php

namespace SkyAthlon\SkySports2017Bundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Player as Ref;

/**
 * Game
 *
 * @ORM\Table(name="skysports2017_game")
 * @ORM\Entity(repositoryClass="SkyAthlon\SkySports2017Bundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesA")
     * @ORM\JoinColumn(name="teama_id", referencedColumnName="id")
     */
    protected $teamA;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesB")
     * @ORM\JoinColumn(name="teamb_id", referencedColumnName="id")
     */
    protected $teamB;

    /**
     * @var int
     *
     * @ORM\Column(name="pointa", type="smallint", nullable=true)
     */
    protected $pointA;

    /**
     * @var int
     *
     * @ORM\Column(name="pointb", type="smallint", nullable=true)
     */
    protected $pointB;

    /**
     * @ORM\ManyToOne(targetEntity="GameType", inversedBy="games")
     * @ORM\JoinColumn(name="gametype_id", referencedColumnName="id")
     */
    protected $gameType;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint")
     */
    protected $weight;

    /**
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="games")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    protected $round;

    public function __toString() {
        $teamA = $this->teamA ? $this->teamA : '?';
        $teamB = $this->teamB ? $this->teamB : '?';
        return $teamA . ' vs ' . $teamB;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set pointA
     *
     * @param integer $pointA
     *
     * @return Game
     */
    public function setPointA($pointA)
    {
        $this->pointA = $pointA;

        return $this;
    }

    /**
     * Get pointA
     *
     * @return integer
     */
    public function getPointA()
    {
        return $this->pointA;
    }

    /**
     * Set pointB
     *
     * @param integer $pointB
     *
     * @return Game
     */
    public function setPointB($pointB)
    {
        $this->pointB = $pointB;

        return $this;
    }

    /**
     * Get pointB
     *
     * @return integer
     */
    public function getPointB()
    {
        return $this->pointB;
    }

    /**
     * Set teamA
     *
     * @param Team $teamA
     *
     * @return Game
     */
    public function setTeamA(Team $teamA = null)
    {
        $this->teamA = $teamA;

        return $this;
    }

    /**
     * Get teamA
     *
     * @return Team
     */
    public function getTeamA()
    {
        return $this->teamA;
    }

    /**
     * Set teamB
     *
     * @param Team $teamB
     *
     * @return Game
     */
    public function setTeamB(Team $teamB = null)
    {
        $this->teamB = $teamB;

        return $this;
    }

    /**
     * Get teamB
     *
     * @return Team
     */
    public function getTeamB()
    {
        return $this->teamB;
    }

    /**
     * Set gameType
     *
     * @param GameType $gameType
     *
     * @return Game
     */
    public function setGameType(GameType $gameType = null)
    {
        $this->gameType = $gameType;

        return $this;
    }

    /**
     * Get gameType
     *
     * @return GameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return Game
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight() {
        return $this->getRound()->getReversed() ? $this->getGameType()->getReversedWeight() : $this->getGameType()->getWeight();
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Game
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    public function isPreGame() {
        return strpos($this->getGameType()->getComputerName(), 'eloselejtezo') !== false;
    }
}
