<?php

namespace SkyAthlon\SkySports2017Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Team controller.
 *
 * @Route("/skysports-2017/api/")
 */
class ApiController extends Controller
{
    /**
     * Gets current status.
     *
     * @Route("current-status", name="skysports2017_api_currentstatus", options={"expose" = true})
     * @Method("GET")
     */
    public function currentStatusAction()
    {
        $cacheService = $this->get('skysports2017_cache');
        $ret = $cacheService->setAllRedis();

        return new JsonResponse($ret);
    }
}
