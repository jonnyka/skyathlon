<?php

namespace SkyAthlon\SkySports2017Bundle\Controller\Round;

use AppBundle\Entity\Settings;
use Doctrine\ORM\EntityManager;
use SkyAthlon\SkySports2017Bundle\Entity\Game;
use SkyAthlon\SkySports2017Bundle\Entity\PlayerResult;
use SkyAthlon\SkySports2017Bundle\Entity\RefResult;
use SkyAthlon\SkySports2017Bundle\Entity\Round;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Round controller.
 *
 * @Route("/skysports-2017/fordulo")
 */
class RoundGameController extends Controller
{
    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{roundid}/merkozes/{game}/szerkesztes/", name="skysports2017_game_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function editGameAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();

            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkySports2017Bundle:Round')->findOneBy(array(
                'number' => $roundid,
            ));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $games = $em->getRepository('SkyAthlonSkySports2017Bundle:Game')->findBy(array(
                    'round' => $round,
                ));

                $arr = array(
                    'custom' => array(
                        'games' => $games,
                    ),
                );

                $editForm = $this->createForm('SkyAthlon\SkySports2017Bundle\Form\GameType', $game, $arr);
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $cacheService = $this->get('skysports2017_cache');
                    $cacheService->setAllRedis();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skysports2017.game.edited');

                    $anchor = '#' . $game->getGameType()->getComputerName();
                    return $this->redirect($this->generateUrl('skysports2017_round', array('round' => $roundid)) . $anchor);
                }

                $form = $this->createDeleteGameForm($roundid, $game);

                return array(
                    'game' => $game,
                    'roundid' => $roundid,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $form->createView(),
                );
            }
            else {
                return $this->redirectToRoute('skysports2017_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Deletes a Game entity.
     *
     * @Route("/{roundid}/merkozes/{id}/", name="skysports2017_game_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param $roundid
     * @param Game $game
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteGameAction(Request $request, $roundid, Game $game)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Round $round */
            $round = $em->getRepository('SkyAthlonSkySports2017Bundle:Round')->findOneBy(array('number' => $roundid));

            if (($this->getUser()->isAdmin() && !$round->getClosed()) || $this->getUser()->isSu()) {
                $form = $this->createDeleteGameForm($roundid, $game);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                    $playerResults = $em->getRepository('SkyAthlonSkySports2017Bundle:PlayerResult')->findBy(array(
                        'game' => $game,
                    ));
                    if ($playerResults) {
                        foreach ($playerResults as $playerResult) {
                            $em->remove($playerResult);
                        }
                    }

                    $em->remove($game);
                    $em->flush();

                    $messageService = $this->get('skyathlon_message');
                    $messageService->setMessage('flashbag.skysports2017.game.deleted', 'warning');
                }

                return $this->redirectToRoute('skysports2017_round', array('round' => $roundid));
            }
            else {
                return $this->redirectToRoute('skysports2017_round', array('round' => $roundid));
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a form to delete a Game entity.
     *
     * @param $roundid
     * @param Game $game The Game entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteGameForm($roundid, Game $game)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('skysports2017_game_delete', array('id' => $game->getId(), 'roundid' => $roundid)))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
