var roundTeamSelect = function(data) {
    return '<div class="round-team-interact-from">' +
        '<span class="teamlogo"><img src="' + data.src + '" alt="' + data.text + '" /></span>' +
        '<span class="round-game-team">' + data.text + '</span>' +
        '</div>'
};

$('.round-team-select').selectize({
    render: {
        option: function(data, escape) {
            return roundTeamSelect(data);
        },
        item: function(data, escape) {
            return roundTeamSelect(data);
        }
    }
});

var roundPlayerSelect = function(data) {
    if (!data.isnull) {
        return '<div class="round-team-interact-from ' + data.team + '">' +
            '<span class="edit-player-number">' + data.number + '</span>' +
            '<span class="edit-player-name">' + data.name + '</span>' +
            '(<span class="edit-player-score">' + data.score + '</span>)' +
            '</div>';
    }

    return '<div class="round-team-interact-from">' +
        '<span class="edit-player-number">?</span>' +
        '<span class="edit-player-name">?</span>' +
        '<span class="edit-player-score"></span>' +
        '</div>';
};

$('.round-player-select').selectize({
    render: {
        option: function(data, escape) {
            return roundPlayerSelect(data);
        }
    }
});
