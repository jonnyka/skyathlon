// Filter teams on round view
$('#filter-team').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

    $('.round-game').show().filter(function() {
        var text = $(this).find('.round-game-head').find('.round-game-team').text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

function stickyHeader() {
    var stickyHeader = $('.sticky-header'),
        top = 75;

    if (stickyHeader.length) {
        if (window.innerWidth < 1000) {
            top = 54;
        }

        stickyHeader.floatThead({top: top});
    }
}

stickyHeader();