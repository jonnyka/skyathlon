var bClicked = false;
$('#header-signin-button').click(function() {
    bClicked = true;
});

/**
 * Google button hack.
 */
var checkExist = setInterval(function() {
    if ($('.abcRioButtonContentWrapper').length) {
        $("span:contains('Sign in with Google')").text('Akadémiás bejelentkezés');

        clearInterval(checkExist);
    }
}, 100);

/**
 * Set a localStorage setting.
 * @param name
 * @param value
 */
function setSetting(name, value) {
    var testObject = { 'value': value };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * Get a localStorage setting.
 * @param name
 * @param defaultVal
 */
function getSetting(name, defaultVal) {
    var retrievedObject = localStorage.getItem(name);
    if (!retrievedObject) {
        return defaultVal || false;
    }

    var jsonObject = JSON.parse(retrievedObject);
    return jsonObject.value;
}

function sendMessage(message, type) {
    type = type || 'notice';
    $.ajax({
        type: "POST",
        async: false,
        url: Routing.generate('message'),
        data: {
            message: message,
            type: type
        }
    });
}

/**
 * Google login success handler.
 * @param googleUser
 */
function loginSuccess(googleUser) {
    if (bClicked) {
        var gauth = gapi.auth2.getAuthInstance(),
            username = googleUser.getBasicProfile().getName(),
            mail = googleUser.getBasicProfile().getEmail(),
            token = googleUser.getAuthResponse().id_token;

        $.ajax({
            type: "POST",
            url: Routing.generate('auth'),
            data: {
                token: token,
                mail: mail
            },
            success: function (data) {
                if (data) {
                    setSetting('skyball-username', username);
                    setSetting('skyball-mail', mail);
                    setSetting('skyball-token', token);

                    sendMessage('flashbag.login');
                    console.log('Login success', mail);

                    var loc = getSetting('lastSport', 'skyball2017_round_redirect');

                    window.location.replace(Routing.generate(loc));
                }
                else {
                    console.log('Login error, wrong email address', mail);
                }
            }
        });
    }
}

/**
 * Google login error handler.
 * @param error
 */
function loginFail(error) {
    console.log('Login error', error);
}

function renderButton() {
    gapi.client.load('plus', 'v1').then(function() {
        gapi.signin2.render('header-signin-button', {
            'scope': 'https://www.googleapis.com/auth/plus.login',
            'width': 260,
            'height': 52,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': loginSuccess,
            'onfailure': loginFail
        });
    });
}