/**
 * Set a localStorage setting.
 * @param name
 * @param value
 */
function setSetting(name, value) {
    var testObject = { 'value': value };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * Get a localStorage setting.
 * @param name
 */
function getSetting(name) {
    var retrievedObject = localStorage.getItem(name);
    if (!retrievedObject) {
        return false;
    }

    var jsonObject = JSON.parse(retrievedObject);
    return jsonObject.value;
}

/**
 * Remove a localStorage setting.
 * @param name
 */
function delSetting(name) {
    localStorage.removeItem(name);
}

/**
 * Set a cookie value.
 * @param name
 * @param value
 * @param days
 */
function setCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

/**
 * Delete a cookie value.
 * @param name
 */
function delCookie(name) {
    setCookie(name, "", -1);
}

/**
 * Logout from google.
 */
function logout() {
    window.location.replace(Routing.generate('logout'));
}

/**
 * Print function.
 */
function print() {
    var html = "";

    $('link').each(function() {
        if ($(this).attr('rel').indexOf('stylesheet') !== -1) {
            html += '<link rel="stylesheet" href="' + $(this).attr("href") + '" />';
        }
    });

    html += '<body onload="window.focus();">' + $('.PrintArea').html() + '</body>';
    var w = window.open("", "Nyomtatás", "");
    if (w) {
        w.document.write(html);
        w.document.close();
        setTimeout(function() {
            w.print();
        }, 100);
    }
}

$('.print').click(function() {
    var html = "",
        toPrint = $(this).data('print'),
        title = $(this).data('title'),
        w;

    $('link').each(function() {
        if ($(this).attr('rel').indexOf('stylesheet') !== -1) {
            html += '<link rel="stylesheet" href="' + $(this).attr("href") + '" />';
        }
    });

    html += '<h2 class="print-title">' + title + '</h2>';
    html += '<body onload="window.focus();">' + $('.' + toPrint).html() + '</body>';

    w = window.open("", "Nyomtatás", "");
    if (w) {
        w.document.write(html);
        w.document.close();
        setTimeout(function() {
            w.print();
        }, 100);
    }
});

$('.noformcontrol').removeClass('form-control');

/**
 * Activate the current main menu item.
 */
function activeMenu() {
    var currentHref = window.location.href,
        activeMenu = 'logo',
        menus = ['csapat', 'csapatok', 'versenyszamok', 'nemzet', 'nemzetek', 'versenyzok', 'versenynap', 'fordulo', 'jatek', 'kendok', 'jegyzokonyvek', 'biraskodasok', 'birok', 'buntetopontok', 'grafikonok'],
        sports = ['skyball-2016', 'basefall-2016', 'skypong-2016', 'skyolimpia-2017', 'skyball-2017'],
        i,
        menu,
        sport;

    for (i in menus) {
        menu = menus[i];
        if (currentHref.indexOf('/' + menu + '/') > -1) {
            activeMenu = 'menu-' + menu;
        }
    }

    for (i in sports) {
        sport = sports[i];
        if (currentHref.indexOf('/' + sport + '/') > -1) {
            setSetting('currentSport', sport.replace('-', '') + '_round_redirect');
        }
    }

    $('.nav').find('.active').removeClass('active');
    $('.' + activeMenu).addClass('active');
}

activeMenu();

function alertBox() {
    $('.alert-box').find('.fa').click(function() {
        $(this).parent().slideUp();
    });
}

alertBox();