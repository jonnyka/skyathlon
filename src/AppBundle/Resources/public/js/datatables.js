$.fn.dataTable.ext.order['sortby'] = function(settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        return $(td).find('span.sortby').text() * 1;
    });
};

$.fn.dataTable.ext.order['sortbytext'] = function(settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        return $(td).find('span.sortby').text();
    });
};

$.fn.dataTable.ext.order['sortbyresult'] = function(settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        var n = $(td).find('span.result-number').text();
        if (n === '-') {
            n = 0;
        }

        return parseInt(n);
    });
};

$.fn.dataTable.ext.order['sortbyshow'] = function(settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        return $(td).find('span.sortbyshow').text() * 1;
    });
};

$.fn.dataTable.ext.order['sortbyInput'] = function(settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        return $(td).find('input').val() * 1;
    });
};

$.fn.dataTable.ext.order['point'] = function(settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map(function (td, i) {
        return $(td).find('span.teamRank').text() * 1;
    });
};

$.fn.dataTable.ext.order['numeric'] = function(settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map(function (td, i) {
        return $(td).text() * 1;
    });
};