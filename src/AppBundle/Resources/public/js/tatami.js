$('.tatami').mousedown(function(e) {
    if (e.which == 3) {
        $(this).toggleClass('red');
        return false;
    }

    return true;
});

var update = function(js) {
    //console.log(js);
};

$('document').ready(function () {
    var tatamiObjects = document.getElementsByClassName('tatami'),
        tatamis = $('#tatamis'),
        radios = document.getElementsByName('tool'),
        input = document.getElementById('jscolorbutton'),
        picker = new jscolor(input),
        canvas,
        image,
        context,
        isPaint = false,
        lastPointerPosition,
        mode = 'brush',
        width = tatamis.width(),
        height = tatamis.height(),
        stage = new Konva.Stage({
            container: 'tatami-canvas',
            width: width,
            height: height
        }),
        layer = new Konva.Layer();

    stage.add(layer);

    tatamis[0].oncontextmenu = function() {
        return false;
    };

    function sendMessage(message, type) {
        type = type || 'notice';
        $.ajax({
            type: "POST",
            async: false,
            url: Routing.generate('message'),
            data: {
                message: message,
                type: type
            }
        });
    }

    function passThrough(e) {
        if (e.evt.button === 2 || e.evt.button === 3) {
            var mouseX = e.evt.pageX - tatamis.offset().left,
                mouseY = e.evt.pageY - tatamis.offset().top;

            for (var i = 0; i < tatamiObjects.length; i++) {
                var obj = tatamiObjects[i],
                    width = obj.clientWidth,
                    height = obj.clientHeight;

                if (mouseX > obj.offsetLeft && mouseX < obj.offsetLeft + width
                    && mouseY > obj.offsetTop && mouseY < obj.offsetTop + height) {
                    $(tatamiObjects[i]).trigger({
                        type: 'mousedown',
                        which: 3
                    });
                }
            }
        }
    }

    canvas = document.createElement('canvas');
    canvas.width = stage.width();
    canvas.height = stage.height();

    image = new Konva.Image({
        image: canvas,
        x : 0,
        y : 0,
        stroke: 'green',
        shadowBlur: 5
    });
    layer.add(image);
    stage.draw();

    context = canvas.getContext('2d');
    context.strokeStyle = "#41dfb1";
    context.lineJoin = "round";
    context.lineWidth = 5;
    context.globalCompositeOperation = 'source-over';

    stage.on('contentMousedown.proto', function(e) {
        passThrough(e);
        isPaint = true;
        lastPointerPosition = stage.getPointerPosition();
    });
    stage.on('contentMouseup.proto', function() {
        isPaint = false;
    });

    stage.on('contentMousemove.proto', function(e) {
        if (e.evt.which === 2 || e.evt.which === 3) {
            return false;
        }

        if (!isPaint) {
            return;
        }
        if (mode === 'brush') {
            context.lineWidth = 5;
            context.globalCompositeOperation = 'source-over';
        }
        if (mode === 'eraser') {
            context.lineWidth = 100;
            context.globalCompositeOperation = 'destination-out';
        }
        context.beginPath();
        var localPos = {
            x: lastPointerPosition.x - image.x(),
            y: lastPointerPosition.y - image.y()
        };
        context.moveTo(localPos.x, localPos.y);
        var pos = stage.getPointerPosition();
        localPos = {
            x: pos.x - image.x(),
            y: pos.y - image.y()
        };
        context.lineTo(localPos.x, localPos.y);
        context.closePath();
        context.stroke();
        lastPointerPosition = pos;
        layer.draw();
    });

    for (var i = 0, max = radios.length; i < max; i++) {
        radios[i].onclick = function() {
            mode = this.value;
        }
    }

    update = function(jscolor) {
        context.strokeStyle = jscolor.toHEXString();
    };

    picker.closable = true;
    picker.closeText = 'Mehet';
    picker.fromRGB(65, 223, 177);
    picker.onFineChange = 'update(this)';

    $.ajax({
        type: 'GET',
        async: true,
        url: Routing.generate('tatami_get'),
        success: function (data) {
            var tatamis = data.tatamis,
                img = new Image();

            img.onload = function(){
                context.drawImage(img,0,0);
                layer.draw();
            };
            img.src = data.drawing;

            picker.fromString(data.color);

            $('.tatami').each(function (index, tatami) {
                if (tatamis[index] && tatamis[index] === 'true') {
                    $(this).addClass('red');
                }
            });
        }
    });

    $('#save').click(function () {
        var drawing = canvas.toDataURL("image/png"),
            tatamis = [],
            red,
            text = CKEDITOR.instances.editor1.getData(),
            color = picker.toHEXString();

        $('.tatami').each(function (index, tatami) {
            red = $(tatami).hasClass('red');

            tatamis.push(red);
        });

        $.ajax({
            type: 'POST',
            async: true,
            url: Routing.generate('tatami_set'),
            data: {
                drawing: drawing,
                tatamis: tatamis,
                text: text,
                color: color
            },
            success: function (data) {
                sendMessage('draw.saved');
                location.reload();
            }
        });
    });
});
