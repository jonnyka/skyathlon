<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\DataFixtures\ThrowFall2017\LoadThrowFall2017Data;

class ThrowFall2017Fixt implements FixtureInterface
{
    public function load(ObjectManager $em)
    {
        LoadThrowFall2017Data::load($em);
    }
}