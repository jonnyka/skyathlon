<?php
namespace AppBundle\DataFixtures\ThrowFall2017;

use AppBundle\Entity\Settings;
use Doctrine\Common\Persistence\ObjectManager;
use SkyAthlon\ThrowFall2017Bundle\Entity\Game;
use SkyAthlon\ThrowFall2017Bundle\Entity\GameType;
use SkyAthlon\ThrowFall2017Bundle\Entity\Player;
use SkyAthlon\ThrowFall2017Bundle\Entity\Round;
use SkyAthlon\ThrowFall2017Bundle\Entity\Team;

class LoadThrowFall2017Data
{
    /**
     * @param ObjectManager $em
     */
    public static function load(ObjectManager $em)
    {
        $data = ThrowFall2017Data::getData();

        $players = $data['players'];
        $teams = $data['teams'];
        $gameTypes = $data['gameTypes'];

        $playerNum = $teamNum = $gameTypeNum = $roundNum = $gameNum =
        $playerNumU = $teamNumU = $gameTypeNumU = $roundNumU = 0;

        foreach ($players as $player) {
            $p = $em->getRepository('SkyAthlonThrowFall2017Bundle:Player')->findOneBy(array('number' => $player['number']));
            if ($p) {
                $playerNumU++;
            }
            else {
                $p = new Player();
                $playerNum++;
            }

            $p->setName($player['name']);
            $p->setNumber($player['number']);
            $p->setLeader($player['leader']);
            $p->setWeight($player['weight']);
            $em->persist($p);
        }

        $em->flush();

        echo "\t---------------------------------\n";
        echo "\t> " . $playerNum . " players added.\n";
        echo "\t> " . $playerNumU . " players updated.\n";
        echo "\t---------------------------------\n";

        $playerObjects = $em->getRepository('SkyAthlonThrowFall2017Bundle:Player')->findAll();
        $players = array();
        /** @var Player $playerObject */
        foreach ($playerObjects as $playerObject) {
            $players['p' . $playerObject->getNumber()] = $playerObject;
        }

        foreach ($teams as $team) {
            $t = $em->getRepository('SkyAthlonThrowFall2017Bundle:Team')->findOneBy(array('name' => $team['name']));
            if ($t) {
                $teamNumU++;
            }
            else {
                $t = new Team();
                $teamNum++;
            }

            /** @var Player $p1 */
            $p1 = $players['p' . $team['p1']];
            /** @var Player $p2 */
            $p2 = $players['p' . $team['p2']];
            /** @var Player $p3 */
            $p3 = $players['p' . $team['p3']];
            /** @var Player $p4 */
            $p4 = $players['p' . $team['p4']];

            $t->setName($team['name']);
            $t->setComputerName($team['strip']);
            $em->persist($t);

            $p1->setTeam($t);
            $p2->setTeam($t);
            $p3->setTeam($t);
            $p4->setTeam($t);
            $em->persist($p1);
            $em->persist($p2);
            $em->persist($p3);
            $em->persist($p4);
        }

        $em->flush();

        echo "\t> " . $teamNum . " teams added.\n";
        echo "\t> " . $teamNumU . " teams updated.\n";
        echo "\t---------------------------------\n";

        foreach ($gameTypes as $gameType) {
            $m = $em->getRepository('SkyAthlonThrowFall2017Bundle:GameType')->findOneBy(array('computerName' => $gameType['computerName']));
            if ($m) {
                $gameTypeNumU++;
            }
            else {
                $m = new GameType();
                $gameTypeNum++;
            }

            $m->setName($gameType['name']);
            $m->setComputerName($gameType['computerName']);
            $m->setWeight($gameType['weight']);
            $m->setReversedWeight($gameType['reversedWeight']);
            if (array_key_exists('groupTitle', $gameType)) {
                $m->setGroupTitle($gameType['groupTitle']);
            }

            $em->persist($m);
        }

        $em->flush();

        echo "\t> " . $gameTypeNum . " game types added.\n";
        echo "\t> " . $gameTypeNumU . " game types updated.\n";
        echo "\t---------------------------------\n";

        for ($i = 1; $i <= 10; $i++) {
            $r = $em->getRepository('SkyAthlonThrowFall2017Bundle:Round')->findOneBy(array('number' => $i));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setNumber($i);
            $r->setName($i . '. forduló');

            $em->persist($r);

            $gameTypes = $em->getRepository('SkyAthlonThrowFall2017Bundle:GameType')->findAll();
            /** @var GameType $gameType */
            foreach ($gameTypes as $gameType) {
                $game = $em->getRepository('SkyAthlonThrowFall2017Bundle:Game')->findOneBy(array(
                    'round' => $r,
                    'gameType' => $gameType,
                ));
                if (!$game) {
                    $game = new Game();
                    $game->setRound($r);
                    $game->setGameType($gameType);
                    $game->setWeight($gameType->getWeight());

                    $em->persist($game);
                    $gameNum++;
                }
            }
        }

        $em->flush();

        echo "\t> " . $roundNum . " rounds added.\n";
        echo "\t> " . $roundNumU . " rounds updated.\n";
        echo "\t> " . $gameNum . " games added.\n";
        echo "\t---------------------------------\n";

        $s = $em->getRepository('AppBundle:Settings')->findOneBy(array('name' => 'throwfall2017_currentRound'));
        if ($s) {
            echo "\t> 1 setting updated.\n";
        }
        else {
            $s = new Settings();
            echo "\t> 1 setting added.\n";
        }

        $s->setName('throwfall2017_currentRound');
        $s->setValue(2);

        $em->persist($s);
        $em->flush();

        echo "\t---------------------------------\n";
    }
}