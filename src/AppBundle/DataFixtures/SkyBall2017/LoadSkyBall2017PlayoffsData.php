<?php
namespace AppBundle\DataFixtures\SkyBall2017;

use SkyAthlon\SkyBall2017Bundle\Entity\Game;
use SkyAthlon\SkyBall2017Bundle\Entity\GameType;
use SkyAthlon\SkyBall2017Bundle\Entity\Round;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSkyBall2017PlayoffsData
{
    public static function load(ObjectManager $em)
    {
        $data = SkyBall2017PlayoffsData::getData();
        $rounds = $data['rounds'];
        $gameTypes = $data['gameTypes'];
        $gameTypeNum = $gameTypeNumU = $gameNum = $gameNumU = $roundNum = $roundNumU = 0;

        foreach ($rounds as $round) {
            $r = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array('number' => $round['number']));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setName($round['name']);
            $r->setNumber($round['number']);
            $r->setDate(new \DateTime($round['date']));
            $r->setPlayoffs(true);
            $r->setSorder('DCBA');

            $em->persist($r);
        }

        $em->flush();

        echo "\t---------------------------------\n";
        echo "\t> " . $roundNum . " rounds added.\n";
        echo "\t> " . $roundNumU . " rounds updated.\n";
        echo "\t---------------------------------\n";

        $roundObjects = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findAll();
        $roundZ = array();
        foreach ($roundObjects as $roundObject) {
            $roundZ[$roundObject->getNumber()] = $roundObject;
        }

        $playoffGames = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->getPlayoffGames();
        if ($playoffGames) {
            foreach ($playoffGames as $playoffGame) {
                $em->remove($playoffGame);
            }
        }

        $em->flush();

        foreach ($gameTypes as $rid => $rgameTypes) {
            foreach ($rgameTypes as $gameType) {
                $gt = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameType')->findOneBy(array('computerName' => $gameType['computerName']));
                if ($gt) {
                    $gameTypeNumU++;
                } else {
                    $gt = new GameType();
                    $gameTypeNum++;
                }

                $gt->setName($gameType['name']);
                $gt->setComputerName($gameType['computerName']);
                $gt->setWeight($gameType['weight']);
                $gt->setReversedWeight($gameType['reversedWeight']);
                $gt->setNotie($gameType['notie']);
                $gt->setPlayoffs(true);
                if (array_key_exists('groupTitle', $gameType)) {
                    $gt->setGroupTitle($gameType['groupTitle']);
                }

                $em->persist($gt);

                $g = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->findOneBy(array('gameType' => $gt));
                if ($g) {
                    $gameNumU++;
                } else {
                    $g = new Game();
                    $g->setGameType($gt);
                    $gameNum++;
                }

                $g->setRound($roundZ[$rid]);
                $g->setWeight($gt->getWeight());
                $em->persist($g);
            }
        }

        $em->flush();

        echo "\t> " . $gameTypeNum . " game types added.\n";
        echo "\t> " . $gameTypeNumU . " game types updated.\n";
        echo "\t---------------------------------\n";

        echo "\t> " . $gameNum . " games added.\n";
        echo "\t> " . $gameNumU . " games updated.\n";
        echo "\t---------------------------------\n";
    }
}