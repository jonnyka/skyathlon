<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\SkyBall2017\LoadSkyBall2017PlayoffsData;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\DataFixtures\SkyBall2017\LoadSkyBall2017Data;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SkyBall2017Fixt implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $em
     */
    public function load(ObjectManager $em)
    {
        LoadSkyBall2017Data::load($em);
        LoadSkyBall2017PlayoffsData::load($em);

        //$this->container->get('skyball2017_cache')->recalcGAndSP();
        $this->container->get('skyball2017_cache')->setAllRedisData();
    }
}