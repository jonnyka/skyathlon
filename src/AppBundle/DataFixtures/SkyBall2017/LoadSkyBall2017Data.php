<?php
namespace AppBundle\DataFixtures\SkyBall2017;

use SkyAthlon\Skyball2017Bundle\Entity\Game;
use SkyAthlon\SkyBall2017Bundle\Entity\GameLength;
use SkyAthlon\Skyball2017Bundle\Entity\GameType;
use SkyAthlon\SkyBall2017Bundle\Entity\KerchiefCategory;
use SkyAthlon\Skyball2017Bundle\Entity\KerchiefType;
use SkyAthlon\SkyBall2017Bundle\Entity\PlayerResult;
use SkyAthlon\Skyball2017Bundle\Entity\Round;
use SkyAthlon\Skyball2017Bundle\Entity\RoundResultType;
use AppBundle\Entity\Settings;
use Doctrine\Common\Persistence\ObjectManager;
use SkyAthlon\Skyball2017Bundle\Entity\Team;
use SkyAthlon\Skyball2017Bundle\Entity\Player;

class LoadSkyBall2017Data
{
    public static function load(ObjectManager $em)
    {
        $data = SkyBall2017Data::getData();
        $players = $data['players'];
        $teams = $data['teams'];
        $gameTypes = $data['gameTypes'];
        $gameLengths = $data['gameLengths'];
        $kerchiefTypes = $data['kerchiefTypes'];
        $kerchiefCats = $data['kerchiefCategories'];
        $roundResultTypes = $data['roundResultTypes'];
        $playerNum = $teamNum = $gameTypeNum = $gameLengthNum = $kerchiefTypeNum = $kerchiefCatNum = $roundResultTypeNum = $roundNum = $gameNum =
        $playerNumU = $teamNumU = $gameTypeNumU = $gameLengthNumU = $kerchiefTypeNumU = $kerchiefCatNumU = $roundResultTypeNumU = $roundNumU = 0;

        foreach ($players as $player) {
            $p = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findOneBy(array('number' => $player['number']));
            if ($p) {
                $playerNumU++;
            }
            else {
                $p = new Player();
                $playerNum++;
            }

            $p->setName($player['name']);
            $p->setNumber($player['number']);
            $p->setLeader($player['leader']);
            $p->setWeight($player['weight']);
            $em->persist($p);
        }

        $em->flush();

        echo "\t---------------------------------\n";
        echo "\t> " . $playerNum . " players added.\n";
        echo "\t> " . $playerNumU . " players updated.\n";
        echo "\t---------------------------------\n";

        $playerObjects = $em->getRepository('SkyAthlonSkyBall2017Bundle:Player')->findAll();
        $players = array();
        /** @var Player $playerObject */
        foreach ($playerObjects as $playerObject) {
            $players['p' . $playerObject->getNumber()] = $playerObject;
        }

        foreach ($teams as $team) {
            $t = $em->getRepository('SkyAthlonSkyBall2017Bundle:Team')->findOneBy(array('computerName' => $team['strip']));
            if ($t) {
                $teamNumU++;
            }
            else {
                $t = new Team();
                $teamNum++;
            }

            /** @var Player $p1 */
            $p1 = $players['p' . $team['p1']];
            /** @var Player $p2 */
            $p2 = $players['p' . $team['p2']];

            $t->setName($team['name']);
            $t->setComputerName($team['strip']);
            $t->setC1($team['c1']);
            $t->setC2($team['c2']);
            $em->persist($t);

            $p1->setTeam($t);
            $p2->setTeam($t);
            $em->persist($p1);
            $em->persist($p2);
        }

        $em->flush();

        echo "\t> " . $teamNum . " teams added.\n";
        echo "\t> " . $teamNumU . " teams updated.\n";
        echo "\t---------------------------------\n";

        foreach ($gameTypes as $gameType) {
            $m = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameType')->findOneBy(array('computerName' => $gameType['computerName']));
            if ($m) {
                $gameTypeNumU++;
            }
            else {
                $m = new GameType();
                $gameTypeNum++;
            }

            $playoffs = $nulla = false;
            if (array_key_exists('playoffs', $gameType)) {
                $playoffs = $gameType['playoffs'];
            }
            if (array_key_exists('nulla', $gameType)) {
                $nulla = $gameType['nulla'];
            }
            $m->setPlayoffs($playoffs);
            $m->setNulla($nulla);

            $m->setName($gameType['name']);
            $m->setComputerName($gameType['computerName']);
            $m->setWeight($gameType['weight']);
            $m->setReversedWeight($gameType['reversedWeight']);
            $m->setNotie($gameType['notie']);
            if (array_key_exists('groupTitle', $gameType)) {
                $m->setGroupTitle($gameType['groupTitle']);
            }

            $em->persist($m);
        }

        $em->flush();

        echo "\t> " . $gameTypeNum . " game types added.\n";
        echo "\t> " . $gameTypeNumU . " game types updated.\n";
        echo "\t---------------------------------\n";

        $glRepo = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameLength');
        $gRepo  = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game');

        $q1 = $glRepo->findOneBy(array('computerName' => 'Q1'));
        if ($q1) {
            $basic = $glRepo->findOneBy(array('computerName' => 'B'));
            $q1Games = $gRepo->findBy(array('gameLength' => $q1));
            if ($q1Games) {
                foreach ($q1Games as $q1Game) {
                    $q1Game->setGameLength($basic);
                    $em->persist($q1Game);
                }
            }
        }
        $q2 = $glRepo->findOneBy(array('computerName' => 'Q2'));
        if ($q2) {
            $extra = $glRepo->findOneBy(array('computerName' => 'E'));
            $q2Games = $gRepo->findBy(array('gameLength' => $q2));
            if ($q2Games) {
                foreach ($q2Games as $q2Game) {
                    $q2Game->setGameLength($extra);
                    $em->persist($q2Game);
                }
            }
        }
        $q3 = $glRepo->findOneBy(array('computerName' => 'Q3'));
        if ($q3) {
            $super = $glRepo->findOneBy(array('computerName' => 'S'));
            $q3Games = $gRepo->findBy(array('gameLength' => $q3));
            if ($q3Games) {
                foreach ($q3Games as $q3Game) {
                    $q3Game->setGameLength($super);
                    $em->persist($q3Game);
                }
            }
        }

        $em->flush();
        if ($q1) {
            $em->remove($q1);
        }
        if ($q2) {
            $em->remove($q2);
        }
        if ($q3) {
            $em->remove($q3);
        }
        $em->flush();

        foreach ($gameLengths as $gameLength) {
            $m = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameLength')->findOneBy(array('computerName' => $gameLength['computerName']));
            if ($m) {
                $gameLengthNumU++;
            }
            else {
                $m = new GameLength();
                $gameLengthNum++;
            }

            $m->setName($gameLength['name']);
            $m->setComputerName($gameLength['computerName']);
            $m->setWeight($gameLength['weight']);

            $em->persist($m);
        }

        $em->flush();

        echo "\t> " . $gameLengthNum . " game lengths added.\n";
        echo "\t> " . $gameLengthNumU . " game lengths updated.\n";
        echo "\t---------------------------------\n";

        foreach ($kerchiefCats as $kerchiefCat) {
            $k = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefCategory')->findOneBy(array('computerName' => $kerchiefCat['computerName']));
            if ($k) {
                $kerchiefCatNumU++;
            }
            else {
                $k = new KerchiefCategory();
                $kerchiefCatNum++;
            }

            $k->setName($kerchiefCat['name']);
            $k->setComputerName($kerchiefCat['computerName']);
            $k->setColor($kerchiefCat['color']);
            $k->setFontColor($kerchiefCat['fontColor']);

            $em->persist($k);
        }

        $em->flush();

        echo "\t> " . $kerchiefCatNum . " kerchief categories added.\n";
        echo "\t> " . $kerchiefCatNumU . " kerchief categories updated.\n";
        echo "\t---------------------------------\n";

        $kerchiefCatObjects = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefCategory')->findAll();
        $kerchiefCategories = array();
        foreach ($kerchiefCatObjects as $kerchiefCatObject) {
            $kerchiefCategories[$kerchiefCatObject->getComputerName()] = $kerchiefCatObject;
        }

        $kerchiefTypeP = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'P'));
        if ($kerchiefTypeP) {
            $kerchiefTypeP->setComputerName('G');
            $em->persist($kerchiefTypeP);
        }
        $kerchiefTypeSF = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'SF'));
        if ($kerchiefTypeSF) {
            $kerchiefTypeSF->setComputerName('SP');
            $em->persist($kerchiefTypeSF);
        }
        $em->flush();

        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
            'round' => null,
        ));

        if ($playerResults) {
            foreach ($playerResults as $playerResult) {
                $em->remove($playerResult);
            }
        }

        $em->flush();
        /*
        $kerchiefTypeSQ = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'P'));
        $kerchiefTypeGKQ = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'SF'));
        $kerchiefTypeQS = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'G'));
        $kerchiefTypeQF = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'SP'));

        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
            'kerchiefType' => $kerchiefTypeSQ
        ));
        if ($playerResults) {
            /** @var PlayerResult $playerResult /
            foreach ($playerResults as $playerResult) {
                $playerResult->setKerchiefType($kerchiefTypeQS);
                $em->persist($playerResult);
            }
        }
        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:RoundKerchiefResult')->findBy(array(
            'kerchiefType' => $kerchiefTypeSQ
        ));
        if ($playerResults) {
            /** @var PlayerResult $playerResult /
            foreach ($playerResults as $playerResult) {
                $playerResult->setKerchiefType($kerchiefTypeQS);
                $em->persist($playerResult);
            }
        }
        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
            'kerchiefType' => $kerchiefTypeGKQ
        ));
        if ($playerResults) {
            /** @var PlayerResult $playerResult /
            foreach ($playerResults as $playerResult) {
                $playerResult->setKerchiefType($kerchiefTypeQF);
                $em->persist($playerResult);
            }
        }
        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:RoundKerchiefResult')->findBy(array(
            'kerchiefType' => $kerchiefTypeGKQ
        ));
        if ($playerResults) {
            /** @var PlayerResult $playerResult /
            foreach ($playerResults as $playerResult) {
                $playerResult->setKerchiefType($kerchiefTypeQF);
                $em->persist($playerResult);
            }
        }

        $em->flush();
        if ( $kerchiefTypeSQ ) $em->remove($kerchiefTypeSQ);
        if ( $kerchiefTypeGKQ ) $em->remove($kerchiefTypeGKQ);
        $em->flush();
        */

        /*
        $kerchiefTypeSQ = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'SQ'));
        $kerchiefTypeSQ->setComputerName('QS');
        $em->persist($kerchiefTypeSQ);
        $kerchiefTypeGKQ = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'GKQ'));
        $kerchiefTypeGKQ->setComputerName('QF');
        $em->persist($kerchiefTypeGKQ);
        $em->flush();
        $kerchiefTypeQS = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'QS'));
        $kerchiefTypeQF = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'QF'));

        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
            'kerchiefType' => $kerchiefTypeSQ
        ));
        if ($playerResults) {
            /** @var PlayerResult $playerResult /
            foreach ($playerResults as $playerResult) {
                $playerResult->setKerchiefType($kerchiefTypeQS);
                $em->persist($playerResult);
            }
        }
        $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
            'kerchiefType' => $kerchiefTypeGKQ
        ));
        if ($playerResults) {
            /** @var PlayerResult $playerResult /
            foreach ($playerResults as $playerResult) {
                $playerResult->setKerchiefType($kerchiefTypeQF);
                $em->persist($playerResult);
            }
        }

        $em->flush();
        $em->remove($kerchiefTypeSQ);
        $em->remove($kerchiefTypeGKQ);
        $em->flush();
        */

        foreach ($kerchiefTypes as $kerchiefType) {
            $k = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => $kerchiefType['computerName']));
            if ($k) {
                $kerchiefTypeNumU++;
            }
            else {
                $k = new KerchiefType();
                $kerchiefTypeNum++;
            }

            $k->setName($kerchiefType['name']);
            $k->setComputerName($kerchiefType['computerName']);
            $k->setWeight($kerchiefType['weight']);
            $k->setKerchiefCategory($kerchiefCategories[$kerchiefType['category']]);

            $em->persist($k);
        }

        $k = $em->getRepository('SkyAthlonSkyBall2017Bundle:KerchiefType')->findOneBy(array('computerName' => 'B'));
        if ($k) {
            $prs = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array('kerchiefType' => $k));

            if ($prs) {
                foreach ($prs as $pr) {
                    $em->remove($pr);
                }
            }
            $em->remove($k);
        }

        $em->flush();


        echo "\t> " . $kerchiefTypeNum . " kerchief types added.\n";
        echo "\t> " . $kerchiefTypeNumU . " kerchief types updated.\n";
        echo "\t---------------------------------\n";

        foreach ($roundResultTypes as $roundResultType) {
            $rrt = $em->getRepository('SkyAthlonSkyBall2017Bundle:RoundResultType')->findOneBy(array('name' => $roundResultType['name']));
            if ($rrt) {
                $roundResultTypeNumU++;
            }
            else {
                $rrt = new RoundResultType();
                $roundResultTypeNum++;
            }

            $rrt->setName($roundResultType['name']);
            $rrt->setComputerName($roundResultType['computerName']);
            $rrt->setPoint($roundResultType['point']);

            $em->persist($rrt);
        }

        $em->flush();

        echo "\t> " . $roundResultTypeNum . " round result types added.\n";
        echo "\t> " . $roundResultTypeNumU . " round result types updated.\n";
        echo "\t---------------------------------\n";

        $gRep = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game');
        $gtRep = $em->getRepository('SkyAthlonSkyBall2017Bundle:GameType');

        for ($i = 1; $i <= 12; $i++) {
            $r = $em->getRepository('SkyAthlonSkyBall2017Bundle:Round')->findOneBy(array('number' => $i));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setNumber($i);
            $r->setName($i . '. forduló');
            if (!$r->getSorder()) {
                $r->setSorder('DCBA');
            }

            $em->persist($r);

            if ($i === 1) {
                $gameTypes = $gtRep->findBy(array(
                    'nulla' => true,
                ));
                /** @var GameType $gameType */
                foreach ($gameTypes as $gameType) {
                    $game = $gRep->findOneBy(array(
                        'round' => $r,
                        'gameType' => $gameType,
                    ));
                    if (!$game) {
                        $game = new Game();
                        $game->setRound($r);
                        $game->setGameType($gameType);
                        $game->setWeight($gameType->getWeight());

                        $em->persist($game);
                        $gameNum++;
                    }
                }
            }

            /*
            $playOffsGameTypes = $gtRep->findBy(array(
                'playoffs' => true,
            ));
            /** @var GameType $playOffsGameType /
            foreach ($playOffsGameTypes as $playOffsGameType) {
                $nonPlayOffGames = $gRep->findBy(array(
                    'gameType' => $playOffsGameType,
                ));

                foreach ($nonPlayOffGames as $nonPlayOffGame) {
                    $playerResults = $em->getRepository('SkyAthlonSkyBall2017Bundle:PlayerResult')->findBy(array(
                        'game' => $nonPlayOffGame,
                    ));

                    foreach ($playerResults as $playerResult) {
                        $em->remove($playerResult);
                    }

                    $em->remove($nonPlayOffGame);
                }
            }
            */

            $gameTypes = $gtRep->findBy(array(
                'playoffs' => false,
                'nulla' => false,
            ));
            /** @var GameType $gameType */
            foreach ($gameTypes as $gameType) {
                $game = $gRep->findOneBy(array(
                    'round' => $r,
                    'gameType' => $gameType,
                ));
                if (!$game) {
                    $game = new Game();
                    $game->setRound($r);
                    $game->setGameType($gameType);
                    $game->setWeight($gameType->getWeight());

                    $em->persist($game);
                    $gameNum++;
                }
            }
        }

        $em->flush();

        echo "\t> " . $roundNum . " rounds added.\n";
        echo "\t> " . $roundNumU . " rounds updated.\n";
        echo "\t> " . $gameNum . " games added.\n";
        echo "\t---------------------------------\n";

        $sRepo = $em->getRepository('AppBundle:Settings');

        $cr = $sRepo->findOneBy(array('name' => 'skyball2017_currentRound'));
        if (!$cr) {
            $cr = new Settings();
            $cr->setName('skyball2017_currentRound');
            $cr->setValue(1);

            $em->persist($cr);
            $em->flush();

            echo "\t> current round setting added.\n";
        }

        echo "\t---------------------------------\n";

        /*
        $l = 0;
        $games = $em->getRepository('SkyAthlonSkyBall2017Bundle:Game')->findAll();
        foreach ($games as $game) {
            if ($game->getGameLength()) {
                $le = $game->getGameLength()->getComputerName();
                if ($le === 'B') {
                    $l += 5;
                }
                elseif ($le === 'E') {
                    $l += 10;
                }
                elseif ($le === 'S') {
                    $l += 20;
                }
                elseif ($le === 'U') {
                    $l += 30;
                }
            }
        }

        echo "\t---------------------------------\n";
        echo "\t Sum length of games: $l minutes\n";
        echo "\t---------------------------------\n";
        */
    }
}