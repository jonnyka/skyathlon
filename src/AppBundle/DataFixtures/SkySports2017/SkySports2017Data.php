<?php
namespace AppBundle\DataFixtures\SkySports2017;

class SkySports2017Data
{
    public static function getData() {
        $teams = array(
            array(
                'name'  => 'Zöld',
                'strip' => 'zold',
            ),
            array(
                'name'  => 'Sárga',
                'strip' => 'sarga',
            ),
            array(
                'name'  => 'Piros',
                'strip' => 'piros',
            ),
            array(
                'name'  => 'Kék',
                'strip' => 'kek',
            ),

        );

        $gameTypes = array(
            array(
                'name' => 'Water Skyball #1',
                'computerName' => 'wsb_1',
                'weight' => 11,
                'groupTitle' => 'skysports2017.gameType.waterskyball.etap1',
            ),
            array(
                'name' => 'Water Skyball #2',
                'computerName' => 'wsb_2',
                'weight' => 12,
            ),
            array(
                'name' => 'Water Skyball #3',
                'computerName' => 'wsb_3',
                'weight' => 13,
            ),
            array(
                'name' => 'Water Skyball #4',
                'computerName' => 'wsb_4',
                'weight' => 14,
            ),
            array(
                'name' => 'Water Skyball #5',
                'computerName' => 'wsb_5',
                'weight' => 15,
            ),
            array(
                'name' => 'Water Skyball #6',
                'computerName' => 'wsb_6',
                'weight' => 16,
            ),
            array(
                'name' => 'Water Skyball #7',
                'computerName' => 'wsb_7',
                'weight' => 20,
                'groupTitle' => 'skysports2017.gameType.waterskyball.etap2',
            ),
            array(
                'name' => 'Water Skyball #8',
                'computerName' => 'wsb_8',
                'weight' => 21,
            ),
            array(
                'name' => 'Water Skyball #9',
                'computerName' => 'wsb_9',
                'weight' => 22,
            ),
            array(
                'name' => 'Water Skyball #10',
                'computerName' => 'wsb_10',
                'weight' => 23,
            ),
            array(
                'name' => 'Water Skyball #11',
                'computerName' => 'wsb_11',
                'weight' => 24,
            ),
            array(
                'name' => 'Water Skyball #12',
                'computerName' => 'wsb_12',
                'weight' => 25,
            ),
            array(
                'name' => 'Water Skyball Döntő',
                'computerName' => 'wsb_donto',
                'weight' => 30,
                'groupTitle' => 'skysports2017.gameType.waterskyball.donto',
            ),

            array(
                'name' => 'Röplabda #1',
                'computerName' => 'roplabda_1',
                'weight' => 111,
                'groupTitle' => 'skysports2017.gameType.roplabda.etap1',
            ),
            array(
                'name' => 'Röplabda #2',
                'computerName' => 'roplabda_2',
                'weight' => 112,
            ),
            array(
                'name' => 'Röplabda #3',
                'computerName' => 'roplabda_3',
                'weight' => 113,
            ),
            array(
                'name' => 'Röplabda #4',
                'computerName' => 'roplabda_4',
                'weight' => 114,
            ),
            array(
                'name' => 'Röplabda #5',
                'computerName' => 'roplabda_5',
                'weight' => 115,
            ),
            array(
                'name' => 'Röplabda #6',
                'computerName' => 'roplabda_6',
                'weight' => 116,
            ),
            array(
                'name' => 'Röplabda #7',
                'computerName' => 'roplabda_7',
                'weight' => 120,
                'groupTitle' => 'skysports2017.gameType.roplabda.etap2',
            ),
            array(
                'name' => 'Röplabda #8',
                'computerName' => 'roplabda_8',
                'weight' => 121,
            ),
            array(
                'name' => 'Röplabda #9',
                'computerName' => 'roplabda_9',
                'weight' => 122,
            ),
            array(
                'name' => 'Röplabda #10',
                'computerName' => 'roplabda_10',
                'weight' => 123,
            ),
            array(
                'name' => 'Röplabda #11',
                'computerName' => 'roplabda_11',
                'weight' => 124,
            ),
            array(
                'name' => 'Röplabda #12',
                'computerName' => 'roplabda_12',
                'weight' => 125,
            ),
            array(
                'name' => 'Röplabda Döntő',
                'computerName' => 'roplabda_donto',
                'weight' => 130,
                'groupTitle' => 'skysports2017.gameType.roplabda.donto',
            ),

            array(
                'name' => 'Foci #1',
                'computerName' => 'foci_1',
                'weight' => 211,
                'groupTitle' => 'skysports2017.gameType.foci.etap1',
            ),
            array(
                'name' => 'Foci #2',
                'computerName' => 'foci_2',
                'weight' => 212,
            ),
            array(
                'name' => 'Foci #3',
                'computerName' => 'foci_3',
                'weight' => 213,
            ),
            array(
                'name' => 'Foci #4',
                'computerName' => 'foci_4',
                'weight' => 214,
            ),
            array(
                'name' => 'Foci #5',
                'computerName' => 'foci_5',
                'weight' => 215,
            ),
            array(
                'name' => 'Foci #6',
                'computerName' => 'foci_6',
                'weight' => 216,
            ),
            array(
                'name' => 'Foci #7',
                'computerName' => 'foci_7',
                'weight' => 220,
                'groupTitle' => 'skysports2017.gameType.foci.etap2',
            ),
            array(
                'name' => 'Foci #8',
                'computerName' => 'foci_8',
                'weight' => 221,
            ),
            array(
                'name' => 'Foci #9',
                'computerName' => 'foci_9',
                'weight' => 222,
            ),
            array(
                'name' => 'Foci #10',
                'computerName' => 'foci_10',
                'weight' => 223,
            ),
            array(
                'name' => 'Foci #11',
                'computerName' => 'foci_11',
                'weight' => 224,
            ),
            array(
                'name' => 'Foci #12',
                'computerName' => 'foci_12',
                'weight' => 225,
            ),
            array(
                'name' => 'Foci Döntő',
                'computerName' => 'foci_donto',
                'weight' => 230,
                'groupTitle' => 'skysports2017.gameType.foci.donto',
            ),

            /*
            array(
                'name' => 'Lábtengó #1',
                'computerName' => 'labtengo_1',
                'weight' => 311,
                'groupTitle' => 'skysports2017.gameType.labtengo.etap1',
            ),
            array(
                'name' => 'Lábtengó #2',
                'computerName' => 'labtengo_2',
                'weight' => 312,
            ),
            array(
                'name' => 'Lábtengó #3',
                'computerName' => 'labtengo_3',
                'weight' => 313,
            ),
            array(
                'name' => 'Lábtengó #4',
                'computerName' => 'labtengo_4',
                'weight' => 314,
            ),
            array(
                'name' => 'Lábtengó #5',
                'computerName' => 'labtengo_5',
                'weight' => 315,
            ),
            array(
                'name' => 'Lábtengó #6',
                'computerName' => 'labtengo_6',
                'weight' => 316,
            ),
            array(
                'name' => 'Lábtengó #7',
                'computerName' => 'labtengo_7',
                'weight' => 320,
                'groupTitle' => 'skysports2017.gameType.labtengo.etap2',
            ),
            array(
                'name' => 'Lábtengó #8',
                'computerName' => 'labtengo_8',
                'weight' => 321,
            ),
            array(
                'name' => 'Lábtengó #9',
                'computerName' => 'labtengo_9',
                'weight' => 322,
            ),
            array(
                'name' => 'Lábtengó #10',
                'computerName' => 'labtengo_10',
                'weight' => 323,
            ),
            array(
                'name' => 'Lábtengó #11',
                'computerName' => 'labtengo_11',
                'weight' => 324,
            ),
            array(
                'name' => 'Lábtengó #12',
                'computerName' => 'labtengo_12',
                'weight' => 325,
            ),
            array(
                'name' => 'Lábtengó Döntő',
                'computerName' => 'labtengo_donto',
                'weight' => 330,
                'groupTitle' => 'skysports2017.gameType.labtengo.donto',
            ),
            */

            array(
                'name' => 'Pingpong #1',
                'computerName' => 'pingpong_1',
                'weight' => 411,
                'groupTitle' => 'skysports2017.gameType.pingpong.etap1',
            ),
            array(
                'name' => 'Pingpong #2',
                'computerName' => 'pingpong_2',
                'weight' => 412,
            ),
            array(
                'name' => 'Pingpong #3',
                'computerName' => 'pingpong_3',
                'weight' => 413,
            ),
            array(
                'name' => 'Pingpong #4',
                'computerName' => 'pingpong_4',
                'weight' => 414,
            ),
            array(
                'name' => 'Pingpong #5',
                'computerName' => 'pingpong_5',
                'weight' => 415,
            ),
            array(
                'name' => 'Pingpong #6',
                'computerName' => 'pingpong_6',
                'weight' => 416,
            ),
            array(
                'name' => 'Pingpong #7',
                'computerName' => 'pingpong_7',
                'weight' => 420,
                'groupTitle' => 'skysports2017.gameType.pingpong.etap2',
            ),
            array(
                'name' => 'Pingpong #8',
                'computerName' => 'pingpong_8',
                'weight' => 421,
            ),
            array(
                'name' => 'Pingpong #9',
                'computerName' => 'pingpong_9',
                'weight' => 422,
            ),
            array(
                'name' => 'Pingpong #10',
                'computerName' => 'pingpong_10',
                'weight' => 423,
            ),
            array(
                'name' => 'Pingpong #11',
                'computerName' => 'pingpong_11',
                'weight' => 424,
            ),
            array(
                'name' => 'Pingpong #12',
                'computerName' => 'pingpong_12',
                'weight' => 425,
            ),
            array(
                'name' => 'Pingpong Döntő',
                'computerName' => 'pingpong_donto',
                'weight' => 430,
                'groupTitle' => 'skysports2017.gameType.pingpong.donto',
            ),
        );

        return array(
            'teams'      => $teams,
            'gameTypes'  => $gameTypes,
        );
    }
}