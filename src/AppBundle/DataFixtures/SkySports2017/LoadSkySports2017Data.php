<?php
namespace AppBundle\DataFixtures\SkySports2017;

use AppBundle\Entity\Settings;
use Doctrine\Common\Persistence\ObjectManager;
use SkyAthlon\SkySports2017Bundle\Entity\Game;
use SkyAthlon\SkySports2017Bundle\Entity\GameType;
use SkyAthlon\SkySports2017Bundle\Entity\Round;
use SkyAthlon\SkySports2017Bundle\Entity\Team;

class LoadSkySports2017Data
{
    /**
     * @param ObjectManager $em
     */
    public static function load(ObjectManager $em)
    {
        $data = SkySports2017Data::getData();

        $teams = $data['teams'];
        $gameTypes = $data['gameTypes'];

        $teamNum = $gameTypeNum = $roundNum = $gameNum =
        $teamNumU = $gameTypeNumU = $roundNumU = 0;

        foreach ($teams as $team) {
            $t = $em->getRepository('SkyAthlonSkySports2017Bundle:Team')->findOneBy(array('name' => $team['name']));
            if ($t) {
                $teamNumU++;
            }
            else {
                $t = new Team();
                $teamNum++;
            }

            $t->setName($team['name']);
            $t->setComputerName($team['strip']);
            $em->persist($t);
        }

        $em->flush();

        echo "\t> " . $teamNum . " teams added.\n";
        echo "\t> " . $teamNumU . " teams updated.\n";
        echo "\t---------------------------------\n";

        foreach ($gameTypes as $gameType) {
            $m = $em->getRepository('SkyAthlonSkySports2017Bundle:GameType')->findOneBy(array('computerName' => $gameType['computerName']));
            if ($m) {
                $gameTypeNumU++;
            }
            else {
                $m = new GameType();
                $gameTypeNum++;
            }

            $m->setName($gameType['name']);
            $m->setComputerName($gameType['computerName']);
            $m->setWeight($gameType['weight']);
            if (array_key_exists('groupTitle', $gameType)) {
                $m->setGroupTitle($gameType['groupTitle']);
            }

            $em->persist($m);
        }

        $em->flush();

        echo "\t> " . $gameTypeNum . " game types added.\n";
        echo "\t> " . $gameTypeNumU . " game types updated.\n";
        echo "\t---------------------------------\n";

        for ($i = 1; $i <= 10; $i++) {
            $r = $em->getRepository('SkyAthlonSkySports2017Bundle:Round')->findOneBy(array('number' => $i));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setNumber($i);
            $r->setName($i . '. forduló');

            $em->persist($r);

            $gameTypes = $em->getRepository('SkyAthlonSkySports2017Bundle:GameType')->findAll();
            /** @var GameType $gameType */
            foreach ($gameTypes as $gameType) {
                $game = $em->getRepository('SkyAthlonSkySports2017Bundle:Game')->findOneBy(array(
                    'round' => $r,
                    'gameType' => $gameType,
                ));
                if (!$game) {
                    $game = new Game();
                    $game->setRound($r);
                    $game->setGameType($gameType);
                    $game->setWeight($gameType->getWeight());

                    $em->persist($game);
                    $gameNum++;
                }
            }
        }

        $em->flush();

        echo "\t> " . $roundNum . " rounds added.\n";
        echo "\t> " . $roundNumU . " rounds updated.\n";
        echo "\t> " . $gameNum . " games added.\n";
        echo "\t---------------------------------\n";

        $s = $em->getRepository('AppBundle:Settings')->findOneBy(array('name' => 'skysports2017_currentRound'));
        if ($s) {
            echo "\t> 1 setting updated.\n";
        }
        else {
            $s = new Settings();
            echo "\t> 1 setting added.\n";
        }

        $s->setName('skysports2017_currentRound');
        $s->setValue(1);

        $em->persist($s);
        $em->flush();

        echo "\t---------------------------------\n";
    }
}