<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\DataFixtures\SkySports2017\LoadSkySports2017Data;

class SkySports2017Fixt implements FixtureInterface
{
    public function load(ObjectManager $em)
    {
        LoadSkySports2017Data::load($em);
    }
}