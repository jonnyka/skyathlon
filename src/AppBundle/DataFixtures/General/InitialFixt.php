<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\SkyBall2017\LoadSkyBall2017Data;
use AppBundle\DataFixtures\SkyOlimpia2017\LoadSkyOlimpia2017Data;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\DataFixtures\SkyBall2016\LoadSkyBall2016Data;
use AppBundle\DataFixtures\BaseFall2016\LoadThrowFall2017Data;

class InitialFixt implements FixtureInterface
{
    public function load(ObjectManager $em)
    {
        @mkdir(getcwd() . "/web/files/", 0777, true);

        LoadUserData::load($em);
        LoadPlayerData::load($em);
        //LoadSkyBall2016Data::load($em);
        //LoadBaseFall2016Data::load($em);
        //LoadSkyOlimpia2017Data::load($em);
        //LoadSkyBall2017Data::load($em);
    }
}