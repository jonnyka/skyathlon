<?php
namespace AppBundle\DataFixtures\General;

class PlayerData
{
    public static function getData() {
        $players = array(
            array(
                'name'   => 'Domi',
                'number' => 0,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Kuszkusz',
                'number' => 1,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Farkas',
                'number' => 2,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Bika',
                'number' => 3,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Pávi',
                'number' => 4,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Jak',
                'number' => 5,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Sas',
                'number' => 6,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Csiga',
                'number' => 7,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Gyík',
                'number' => 8,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Robi',
                'number' => 9,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Seri',
                'number' => 10,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Wombat',
                'number' => 11,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Orka',
                'number' => 12,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Hero',
                'number' => 13,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Tücsök',
                'number' => 14,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Bálint',
                'number' => 15,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Vé',
                'number' => 16,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Mongúz',
                'number' => 17,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Darázs',
                'number' => 18,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Niki',
                'number' => 19,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Viki',
                'number' => 20,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Sikló',
                'number' => 21,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Szunyi',
                'number' => 22,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'MPeti',
                'number' => 23,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Pele',
                'number' => 24,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Gópi',
                'number' => 25,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Méhecske',
                'number' => 26,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Kengu',
                'number' => 27,
                'sex'    => 'm',
            ),
            /*
            array(
                'name'   => 'Klaudia',
                'number' => 28,
                'sex'    => 'f',
            ),
            */
            array(
                'name'   => 'Szarvas',
                'number' => 29,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Katacsőr',
                'number' => 30,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Rebeka',
                'number' => 31,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Vércse',
                'number' => 32,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Attila',
                'number' => 33,
                'sex'    => 'm',
            ),
            /*
            array(
                'name'   => 'Gábor',
                'number' => 34,
                'sex'    => 'm',
            ),
            */
            array(
                'name'   => 'Laksh',
                'number' => 36,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Róka',
                'number' => 37,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Delfin',
                'number' => 39,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Antilop',
                'number' => 40,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Lola',
                'number' => 41,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Bolha',
                'number' => 42,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Lepke',
                'number' => 43,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Zebi',
                'number' => 44,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Zsuzsi',
                'number' => 45,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Szamár',
                'number' => 46,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Emő',
                'number' => 47,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Medúza',
                'number' => 49,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'TM',
                'number' => 50,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Imi',
                'number' => 51,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Parvati',
                'number' => 55,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Légy',
                'number' => 56,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Dikdik',
                'number' => 60,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Eszti',
                'number' => 61,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Fruzsi',
                'number' => 62,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Csöpi',
                'number' => 63,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Viktor',
                'number' => 64,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Gibbi',
                'number' => 66,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'KÉvi',
                'number' => 68,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Gergő',
                'number' => 69,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Paci',
                'number' => 70,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Fóka',
                'number' => 71,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Szita',
                'number' => 72,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Tamás',
                'number' => 74,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Nyérc',
                'number' => 75,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Crow',
                'number' => 76,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Mormesz',
                'number' => 77,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Virág',
                'number' => 78,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Tenge',
                'number' => 79,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Panda',
                'number' => 80,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Nyuszi',
                'number' => 81,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Balu',
                'number' => 82,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Kiang',
                'number' => 83,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Kecsi',
                'number' => 84,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Yayi',
                'number' => 86,
                'sex'    => 'f',
            ),
            /*
            array(
                'name'   => 'Szonja',
                'number' => 87,
                'sex'    => 'f',
            ),
            */
            array(
                'name'   => 'Szöcsi',
                'number' => 88,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Pocok',
                'number' => 89,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Mókus',
                'number' => 90,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Feka',
                'number' => 91,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Otti',
                'number' => 92,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Kata',
                'number' => 93,
                'sex'    => 'f',
            ),
            /*
            array(
                'name'   => 'Emu',
                'number' => 94,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'BBea',
                'number' => 95,
                'sex'    => 'f',
            ),
            */
            array(
                'name'   => 'Nandu',
                'number' => 96,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Kanári',
                'number' => 97,
                'sex'    => 'f',
            ),
            array(
                'name'   => 'Coyote',
                'number' => 98,
                'sex'    => 'm',
            ),
            array(
                'name'   => 'Kutya',
                'number' => 99,
                'sex'    => 'f',
            ),
        );

        return array(
            'players' => $players,
        );
    }
}