<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\General\UserData;
use AppBundle\Entity\GoogleUser;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData
{
    /**
     * @param ObjectManager $em
     */
    public static function load(ObjectManager $em)
    {
        $users = UserData::getData()['users'];
        $userNum = $userNumU = 0;

        foreach ($users as $user) {
            /** @var GoogleUser $u */
            $u = $em->getRepository('AppBundle:GoogleUser')->findOneBy(array('email' => $user['mail']));
            if ($u) {
                $userNumU++;
            }
            else {
                $u = new GoogleUser();
                $userNum++;
            }

            $u->setUsername($user['name']);
            $u->setFullName($user['fullName']);
            $u->setEmail($user['mail']);
            $u->setRule($user['rule']);
            $u->setPassword('1f5bf9b04e093cf622d20c3454075700');
            $em->persist($u);
        }

        $em->flush();

        echo "\t> " . $userNum . " users added.\n";
        echo "\t> " . $userNumU . " users updated.\n";
        echo "\t---------------------------------\n";
    }
}