<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\General\PlayerData;
use AppBundle\Entity\Player;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPlayerData
{
    /**
     * @param ObjectManager $em
     */
    public static function load(ObjectManager $em)
    {
        $players = PlayerData::getData()['players'];
        $playerNum = $playerNumU = 0;

        /*
        $playerkek = $em->getRepository('AppBundle:Player')->findAll();
        foreach ($playerkek as $playerke) {
            $em->remove($playerke);
        }
        $em->flush();
        */

        foreach ($players as $player) {
            /** @var Player $p */
            $p = $em->getRepository('AppBundle:Player')->findOneBy(array('number' => $player['number']));
            if ($p) {
                $playerNumU++;
            }
            else {
                $p = new Player();
                $playerNum++;
            }

            $p->setNumber($player['number']);
            $p->setName($player['name']);
            $p->setSex($player['sex']);

            $em->persist($p);
        }

        $em->flush();

        echo "\t> " . $playerNum . " players added.\n";
        echo "\t> " . $playerNumU . " players updated.\n";
        echo "\t---------------------------------\n";
    }
}