<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\DataFixtures\BaseFall2016\LoadThrowFall2017Data;

class BaseFall2016Fixt implements FixtureInterface
{
    public function load(ObjectManager $em)
    {
        LoadThrowFall2017Data::load($em);
    }
}