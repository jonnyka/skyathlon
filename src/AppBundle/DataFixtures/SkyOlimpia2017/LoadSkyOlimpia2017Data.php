<?php
namespace AppBundle\DataFixtures\SkyOlimpia2017;

use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameResult;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameSubType;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Team;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Player;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Game;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\GameType;
use SkyAthlon\SkyOlimpia2017Bundle\Entity\Round;
use AppBundle\Entity\Settings;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSkyOlimpia2017Data
{
    public static function load(ObjectManager $em)
    {
        $data = SkyOlimpia2017Data::getData();
        $players      = $data['players'];
        $teams        = $data['teams'];
        $gameTypes    = $data['gameTypes'];
        $gameSubTypes = $data['gameSubTypes'];
        $rounds       = $data['rounds'];
        $games        = $data['games'];

        $playerNum = $teamNum = $gameTypeNum = $gameSubTypeNum = $roundNum = $gameNum = $gameResultNum =
        $playerNumU = $teamNumU = $gameTypeNumU = $gameSubTypeNumU = $roundNumU = $gameNumU = $gameResultNumU = 0;

        foreach ($players as $player) {
            $p = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Player')->findOneBy(array('number' => $player['number']));
            if ($p) {
                $playerNumU++;
            }
            else {
                $p = new Player();
                $playerNum++;
            }

            $p->setName($player['name']);
            $p->setNumber($player['number']);
            $p->setSex($player['sex']);
            $p->setLeader($player['leader']);
            $p->setWeight($player['weight']);
            $em->persist($p);
        }

        $em->flush();

        echo "\t---------------------------------\n";
        echo "\t> " . $playerNum . " players added.\n";
        echo "\t> " . $playerNumU . " players updated.\n";
        echo "\t---------------------------------\n";

        $playerObjects = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Player')->findAll();
        $players = array();
        /** @var Player $playerObject */
        foreach ($playerObjects as $playerObject) {
            $players['p' . $playerObject->getNumber()] = $playerObject;
        }

        foreach ($teams as $team) {
            $t = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Team')->findOneBy(array('name' => $team['name']));
            if ($t) {
                $teamNumU++;
            }
            else {
                $t = new Team();
                $teamNum++;
            }

            foreach ($team['players'] as $pid) {
                /** @var Player $player */
                $player = $players['p' . $pid];
                $player->setTeam($t);
                $em->persist($player);
            }

            $t->setName($team['name']);
            $t->setComputerName($team['strip']);
            $t->setShortName($team['short']);
            if (!$t->getPoint()) {
                $t->setPoint(0);
            }
            $em->persist($t);
        }

        $em->flush();

        echo "\t> " . $teamNum . " teams added.\n";
        echo "\t> " . $teamNumU . " teams updated.\n";
        echo "\t---------------------------------\n";

        foreach ($rounds as $round) {
            $r = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round')->findOneBy(array('number' => $round['number']));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setNumber($round['number']);
            $r->setDate($round['date']);
            if (!$r->getClosed()) {
                $r->setClosed(false);
            }
            $em->persist($r);
        }

        $em->flush();

        echo "\t> " . $roundNum . " rounds added.\n";
        echo "\t> " . $roundNumU . " rounds updated.\n";
        echo "\t---------------------------------\n";

        $roundObjects = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Round')->findAll();
        $rounds = array();
        /** @var Round $roundObject */
        foreach ($roundObjects as $roundObject) {
            $rounds['r' . $roundObject->getNumber()] = $roundObject;
        }

        foreach ($gameTypes as $gameType) {
            $gt = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameType')->findOneBy(array('computerName' => $gameType['computerName']));
            if ($gt) {
                $gameTypeNumU++;
            }
            else {
                $gt = new GameType();
                $gameTypeNum++;
            }

            $gt->setName($gameType['name']);
            $gt->setComputerName($gameType['computerName']);
            $gt->setSexType($gameType['sexType']);
            $gt->setWeight($gameType['weight']);
            $gt->setRound($rounds['r' . $gameType['round']]);

            $em->persist($gt);
        }

        $em->flush();

        echo "\t> " . $gameTypeNum . " game types added.\n";
        echo "\t> " . $gameTypeNumU . " game types updated.\n";
        echo "\t---------------------------------\n";

        $gameTypeObjects = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameType')->findAll();
        $gameTypes = array();
        /** @var GameType $gameTypeObject */
        foreach ($gameTypeObjects as $gameTypeObject) {
            $gameTypes[$gameTypeObject->getComputerName()] = $gameTypeObject;
        }

        foreach ($gameSubTypes as $gameSubType) {
            $gst = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameSubType')->findOneBy(array('computerName' => $gameSubType['computerName']));
            if ($gst) {
                $gameSubTypeNumU++;
            }
            else {
                $gst = new GameSubType();
                $gameSubTypeNum++;
            }

            $gst->setName($gameSubType['name']);
            $gst->setComputerName($gameSubType['computerName']);
            $gst->setWeight($gameSubType['weight']);

            $em->persist($gst);
        }

        $em->flush();

        echo "\t> " . $gameSubTypeNum . " game sub types added.\n";
        echo "\t> " . $gameSubTypeNumU . " game sub types updated.\n";
        echo "\t---------------------------------\n";

        $gameSubTypeObjects = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameSubType')->findAll();
        $gameSubTypes = array();
        /** @var GameSubType $gameSubTypeObject */
        foreach ($gameSubTypeObjects as $gameSubTypeObject) {
            $gameSubTypes[$gameSubTypeObject->getComputerName()] = $gameSubTypeObject;
        }

        $gr = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game');

        $g = $gr->find(221);
        if ($g) $em->remove($g);

        $aDontos = array(209, 211, 213, 215, 217, 219);
        $bDontos = array(210, 212, 214, 216, 218, 220);

        foreach ($aDontos as $ad) {
            $g = $gr->find($ad);
            if ($g) {
                $g->setName('"A" Döntő');
                $g->setGroupTitle('"A" Döntő');
                $em->persist($g);
            }
        }
        foreach ($bDontos as $bd) {
            $g = $gr->find($bd);
            if ($g) {
                $g->setName('"B" Döntő');
                $g->setGroupTitle('"B" Döntő');
                $em->persist($g);
            }
        }

        $em->flush();

        foreach ($games as $game) {
            $g = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game')->findOneBy(array(
                'gameType'    => $gameTypes[$game['gameType']],
                'gameSubType' => $gameSubTypes[$game['gameSubType']],
                'name'        => $game['name'],
            ));
            if ($g) {
                $gameNumU++;
            }
            else {
                $g = new Game();
                $gameNum++;
            }

            $g->setGameType($gameTypes[$game['gameType']]);
            $g->setGameSubType($gameSubTypes[$game['gameSubType']]);
            $g->setName($game['name']);
            $g->setWeight($game['weight']);
            $g->setNumberOfPlayers($game['numberOfPlayers']);
            $g->setNumberOfWinners($game['numberOfWinners']);
            if (array_key_exists('groupTitle', $game)) {
                $g->setGroupTitle($game['groupTitle']);
            }
            $numberOfTeamPlayers = (array_key_exists('numberOfTeamPlayers', $game)) ? $game['numberOfTeamPlayers'] : 1;
            $g->setNumberOfTeamPlayers($numberOfTeamPlayers);

            $em->persist($g);

            if (array_key_exists('players', $game)) {
                if (!$g->getClosed()) {
                    $gameResults = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameResult')->findBy(array(
                        'game' => $g,
                    ));
                    foreach ($gameResults as $gameResult) {
                        $em->remove($gameResult);
                    }
                }

                foreach ($game['players'] as $pid) {
                    if ($pid) {
                        $player = $players['p' . $pid];

                        $gr = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameResult')->findOneBy(array(
                            'game'   => $g,
                            'player' => $player,
                        ));

                        if ($gr) {
                            $gameResultNumU++;
                        } else {
                            $gr = new GameResult();
                            $gameResultNum++;
                        }

                        $gr->setGame($g);
                        $gr->setPlayer($player);
                        $gr->setTeam($player->getTeam());
                        $medal = false;
                        if ($g->getGameSubType()->getComputerName() === 'adonto') {
                            $medal = true;
                        }
                        $gr->setMedal($medal);

                        $em->persist($gr);
                    }
                }
            }
        }

        $em->flush();

        echo "\t> " . $gameNum . " games added.\n";
        echo "\t> " . $gameNumU . " games updated.\n";
        echo "\t---------------------------------\n";

        echo "\t> " . $gameResultNum . " game results added.\n";
        echo "\t> " . $gameResultNumU . " game results updated.\n";
        echo "\t---------------------------------\n";

        $s = $em->getRepository('AppBundle:Settings')->findOneBy(array('name' => 'skyolimpia2017_currentRound'));
        if ($s) {
            echo "\t> 1 setting updated.\n";
        }
        else {
            $s = new Settings();
            echo "\t> 1 setting added.\n";
        }

        $s->setName('skyolimpia2017_currentRound');
        $s->setValue(20);

        $em->persist($s);
        $em->flush();

        /*
        $gamesToDel = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game')->findAll();
        foreach ($gamesToDel as $game) {
            if (!$game->getGameResults() || count($game->getGameResults()) === 0) {
                $em->remove($game);
            }
        }

        $em->flush();
        */

        /*
        $gamesToDel = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game')->getSzar800Results('elodonto');
        /** @var Game $game /
        foreach ($gamesToDel as $game) {
            $em->remove($game);
        }

        $em->flush();

        $elodonto = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameSubType')->findOneBy(array(
            'computerName' => 'elodonto',
        ));
        $gamesToMove = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game')->getSzar800Results('elofutam');
        /** @var Game $game /
        foreach ($gamesToMove as $game) {
            $game->setGameSubType($elodonto);

            $title = $game->getName();
            $parts = explode(' ', $title);
            $newTitle = 'Elődöntő ' . $parts[1];
            $game->setName($newTitle);

            if ($game->getGroupTitle()) {
                $game->setGroupTitle('Elődöntők');
            }

            $em->persist($game);
        }

        $em->flush();
        */

        /*
        $grsToDel = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameResult')->getSzarResults();
        foreach ($grsToDel as $gr) {
            $em->remove($gr);
        }

        $em->flush();
        */

        /*
        $grsToMove = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameResult')->getSzarHelyenResults();
        $goodGameType = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameType')->findOneBy(array(
            'computerName' => 'f_vegyes_prmny_80',
        ));

        /** @var GameResult $gr */
        /*
        foreach ($grsToMove as $gr) {
            $game = $gr->getGame();
            $game->setGameType($goodGameType);
            $em->persist($game);
        }

        $em->flush();
        */

        /*
        $gts = array(
            'n_huzodzkodas',
            'f_huzodzkodas',
            'n_tolodzkodas',
            'f_tolodzkodas',
            'n_negyutemu',
            'f_negyutemu',
            'n_labemeles',
            'f_labemeles',
        );

        foreach ($gts as $gt) {
            $gto = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:GameType')->findOneBy(array(
                'computerName' => $gt,
            ));

            if ($gto) {
                $em->remove($gto);
            }
        }

        $em->flush();
        */

        /*
        $gamesToDel = $em->getRepository('SkyAthlonSkyOlimpia2017Bundle:Game')->getSzarResults();
        foreach ($gamesToDel as $gr) {
            $em->remove($gr);
        }

        $em->flush();
        */

        echo "\t---------------------------------\n";
    }
}