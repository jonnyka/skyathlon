<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\SkyOlimpia2017\LoadSkyOlimpia2017Data;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SkyOlimpia2017Fixt implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $em
     */
    public function load(ObjectManager $em)
    {
        LoadSkyOlimpia2017Data::load($em);

        //$this->container->get('skyolimpia2017_game')->createCache();
    }
}