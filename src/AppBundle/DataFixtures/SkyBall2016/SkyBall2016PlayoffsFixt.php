<?php
namespace AppBundle\DataFixtures\SkyBall2016;

use SkyAthlon\SkyBall2016Bundle\Entity\GameType;
use SkyAthlon\SkyBall2016Bundle\Entity\Round;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SkyBall2016PlayoffsFixt implements FixtureInterface
{
    public function load(ObjectManager $em)
    {
        $data = $this->getData();
        $rounds = $data['rounds'];
        $gameTypes = $data['gameTypes'];
        $gameTypeNum = $gameTypeNumU = $roundNum = $roundNumU = 0;

        foreach ($rounds as $round) {
            $r = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array('number' => $round['number']));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setName($round['name']);
            $r->setNumber($round['number']);
            $r->setDate(new \DateTime($round['date']));
            $r->setPlayoffs(true);

            $em->persist($r);
        }

        $em->flush();

        echo "\t---------------------------------\n";
        echo "\t> " . $roundNum . " rounds added.\n";
        echo "\t> " . $roundNumU . " rounds updated.\n";
        echo "\t---------------------------------\n";

        foreach ($gameTypes as $gameType) {
            $gt = $em->getRepository('SkyAthlonSkyBall2016Bundle:GameType')->findOneBy(array('computerName' => $gameType['computerName']));
            if ($gt) {
                $gameTypeNumU++;
            }
            else {
                $gt = new GameType();
                $gameTypeNum++;
            }

            $gt->setName($gameType['name']);
            $gt->setComputerName($gameType['computerName']);
            $gt->setWeight($gameType['weight']);
            $gt->setReversedWeight($gameType['reversedWeight']);
            $gt->setNotie($gameType['notie']);
            $gt->setPlayoffs(true);
            if (array_key_exists('groupTitle', $gameType)) {
                $gt->setGroupTitle($gameType['groupTitle']);
            }

            $em->persist($gt);
        }

        $em->flush();

        echo "\t> " . $gameTypeNum . " game types added.\n";
        echo "\t> " . $gameTypeNumU . " game types updated.\n";
        echo "\t---------------------------------\n";
    }

    protected function getData() {
        $rounds = array(
            array(
                'name' => 'Rájátszás 1. forduló',
                'number' => 11,
                'playoffs' => true,
                'date' => '2016-08-07',
            ),
            array(
                'name' => 'Rájátszás 2. forduló',
                'number' => 12,
                'playoffs' => true,
                'date' => '2016-08-14',
            ),
            array(
                'name' => 'Rájátszás 3. forduló',
                'number' => 13,
                'playoffs' => true,
                'date' => '2016-08-21',
            ),
        );
        $gameTypes = array(
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #1',
                'computerName' => 'rajatszas_36_helyert_1_1',
                'weight' => 21,
                'reversedWeight' => 21,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.rajatszas36hely1',
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #2',
                'computerName' => 'rajatszas_36_helyert_1_2',
                'weight' => 22,
                'reversedWeight' => 22,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #3',
                'computerName' => 'rajatszas_36_helyert_1_3',
                'weight' => 23,
                'reversedWeight' => 23,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #4',
                'computerName' => 'rajatszas_36_helyert_1_4',
                'weight' => 24,
                'reversedWeight' => 24,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #5',
                'computerName' => 'rajatszas_36_helyert_1_5',
                'weight' => 25,
                'reversedWeight' => 25,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #6',
                'computerName' => 'rajatszas_36_helyert_1_6',
                'weight' => 26,
                'reversedWeight' => 26,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #7',
                'computerName' => 'rajatszas_36_helyert_1_7',
                'weight' => 27,
                'reversedWeight' => 27,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #8',
                'computerName' => 'rajatszas_36_helyert_1_8',
                'weight' => 28,
                'reversedWeight' => 28,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 1. - #9',
                'computerName' => 'rajatszas_36_helyert_1_9',
                'weight' => 29,
                'reversedWeight' => 29,
                'notie' => true,
            ),

            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #1',
                'computerName' => 'rajatszas_36_helyert_2_1',
                'weight' => 31,
                'reversedWeight' => 31,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.rajatszas36hely2',
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #2',
                'computerName' => 'rajatszas_36_helyert_2_2',
                'weight' => 32,
                'reversedWeight' => 32,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #3',
                'computerName' => 'rajatszas_36_helyert_2_3',
                'weight' => 33,
                'reversedWeight' => 33,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #4',
                'computerName' => 'rajatszas_36_helyert_2_4',
                'weight' => 34,
                'reversedWeight' => 34,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #5',
                'computerName' => 'rajatszas_36_helyert_2_5',
                'weight' => 35,
                'reversedWeight' => 35,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #6',
                'computerName' => 'rajatszas_36_helyert_2_6',
                'weight' => 36,
                'reversedWeight' => 36,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #7',
                'computerName' => 'rajatszas_36_helyert_2_7',
                'weight' => 37,
                'reversedWeight' => 37,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #8',
                'computerName' => 'rajatszas_36_helyert_2_8',
                'weight' => 38,
                'reversedWeight' => 38,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás a 3-6. helyért 2. - #9',
                'computerName' => 'rajatszas_36_helyert_2_9',
                'weight' => 39,
                'reversedWeight' => 39,
                'notie' => true,
            ),

            array(
                'name' => 'Rájátszás az 1-2. helyért #1',
                'computerName' => 'rajatszas_12_hely_1',
                'weight' => 41,
                'reversedWeight' => 41,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.rajatszas12hely',
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #2',
                'computerName' => 'rajatszas_12_hely_2',
                'weight' => 42,
                'reversedWeight' => 42,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #3',
                'computerName' => 'rajatszas_12_hely_3',
                'weight' => 43,
                'reversedWeight' => 43,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #4',
                'computerName' => 'rajatszas_12_hely_4',
                'weight' => 44,
                'reversedWeight' => 44,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #5',
                'computerName' => 'rajatszas_12_hely_5',
                'weight' => 45,
                'reversedWeight' => 45,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #6',
                'computerName' => 'rajatszas_12_hely_6',
                'weight' => 46,
                'reversedWeight' => 46,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #7',
                'computerName' => 'rajatszas_12_hely_7',
                'weight' => 47,
                'reversedWeight' => 47,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #8',
                'computerName' => 'rajatszas_12_hely_8',
                'weight' => 48,
                'reversedWeight' => 48,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás az 1-2. helyért #9',
                'computerName' => 'rajatszas_12_hely_9',
                'weight' => 49,
                'reversedWeight' => 49,
                'notie' => true,
            ),

            array(
                'name' => 'Rájátszás elődöntő 1. - #1',
                'computerName' => 'rajatszas_elodonto_1_1',
                'weight' => 51,
                'reversedWeight' => 51,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.rajatszasElodonto1',
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #2',
                'computerName' => 'rajatszas_elodonto_1_2',
                'weight' => 52,
                'reversedWeight' => 52,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #3',
                'computerName' => 'rajatszas_elodonto_1_3',
                'weight' => 53,
                'reversedWeight' => 53,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #4',
                'computerName' => 'rajatszas_elodonto_1_4',
                'weight' => 54,
                'reversedWeight' => 54,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #5',
                'computerName' => 'rajatszas_elodonto_1_5',
                'weight' => 55,
                'reversedWeight' => 55,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #6',
                'computerName' => 'rajatszas_elodonto_1_6',
                'weight' => 56,
                'reversedWeight' => 56,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #7',
                'computerName' => 'rajatszas_elodonto_1_7',
                'weight' => 57,
                'reversedWeight' => 57,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #8',
                'computerName' => 'rajatszas_elodonto_1_8',
                'weight' => 58,
                'reversedWeight' => 58,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 1. - #9',
                'computerName' => 'rajatszas_elodonto_1_9',
                'weight' => 59,
                'reversedWeight' => 59,
                'notie' => true,
            ),

            array(
                'name' => 'Rájátszás elődöntő 2. - #1',
                'computerName' => 'rajatszas_elodonto_2_1',
                'weight' => 61,
                'reversedWeight' => 61,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.rajatszasElodonto2',
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #2',
                'computerName' => 'rajatszas_elodonto_2_2',
                'weight' => 62,
                'reversedWeight' => 62,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #3',
                'computerName' => 'rajatszas_elodonto_2_3',
                'weight' => 63,
                'reversedWeight' => 63,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #4',
                'computerName' => 'rajatszas_elodonto_2_4',
                'weight' => 64,
                'reversedWeight' => 64,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #5',
                'computerName' => 'rajatszas_elodonto_2_5',
                'weight' => 65,
                'reversedWeight' => 65,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #6',
                'computerName' => 'rajatszas_elodonto_2_6',
                'weight' => 66,
                'reversedWeight' => 66,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #7',
                'computerName' => 'rajatszas_elodonto_2_7',
                'weight' => 67,
                'reversedWeight' => 67,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #8',
                'computerName' => 'rajatszas_elodonto_2_8',
                'weight' => 68,
                'reversedWeight' => 68,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás elődöntő 2. - #9',
                'computerName' => 'rajatszas_elodonto_2_9',
                'weight' => 69,
                'reversedWeight' => 69,
                'notie' => true,
            ),

            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #1',
                'computerName' => 'rajatszas_helyoszto_56_hely_1',
                'weight' => 71,
                'reversedWeight' => 71,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.rajatszasHelyoszto56hely',
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #2',
                'computerName' => 'rajatszas_helyoszto_56_hely_2',
                'weight' => 72,
                'reversedWeight' => 72,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #3',
                'computerName' => 'rajatszas_helyoszto_56_hely_3',
                'weight' => 73,
                'reversedWeight' => 73,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #4',
                'computerName' => 'rajatszas_helyoszto_56_hely_4',
                'weight' => 74,
                'reversedWeight' => 74,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #5',
                'computerName' => 'rajatszas_helyoszto_56_hely_5',
                'weight' => 75,
                'reversedWeight' => 75,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #6',
                'computerName' => 'rajatszas_helyoszto_56_hely_6',
                'weight' => 76,
                'reversedWeight' => 76,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #7',
                'computerName' => 'rajatszas_helyoszto_56_hely_7',
                'weight' => 77,
                'reversedWeight' => 77,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #8',
                'computerName' => 'rajatszas_helyoszto_56_hely_8',
                'weight' => 78,
                'reversedWeight' => 78,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó az 5-6. helyért #9',
                'computerName' => 'rajatszas_helyoszto_56_hely_9',
                'weight' => 79,
                'reversedWeight' => 79,
                'notie' => true,
            ),

            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #1',
                'computerName' => 'rajatszas_helyoszto_34_hely_1',
                'weight' => 81,
                'reversedWeight' => 81,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.rajatszasHelyoszto34hely',
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #2',
                'computerName' => 'rajatszas_helyoszto_34_hely_2',
                'weight' => 82,
                'reversedWeight' => 82,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #3',
                'computerName' => 'rajatszas_helyoszto_34_hely_3',
                'weight' => 83,
                'reversedWeight' => 83,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #4',
                'computerName' => 'rajatszas_helyoszto_34_hely_4',
                'weight' => 84,
                'reversedWeight' => 84,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #5',
                'computerName' => 'rajatszas_helyoszto_34_hely_5',
                'weight' => 85,
                'reversedWeight' => 85,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #6',
                'computerName' => 'rajatszas_helyoszto_34_hely_6',
                'weight' => 86,
                'reversedWeight' => 86,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #7',
                'computerName' => 'rajatszas_helyoszto_34_hely_7',
                'weight' => 87,
                'reversedWeight' => 87,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #8',
                'computerName' => 'rajatszas_helyoszto_34_hely_8',
                'weight' => 88,
                'reversedWeight' => 88,
                'notie' => true,
            ),
            array(
                'name' => 'Rájátszás helyosztó a 3-4. helyért #9',
                'computerName' => 'rajatszas_helyoszto_34_hely_9',
                'weight' => 89,
                'reversedWeight' => 89,
                'notie' => true,
            ),

            array(
                'name' => '2016. évi Nagy Döntő #1',
                'computerName' => 'vegso_donto_1',
                'weight' => 91,
                'reversedWeight' => 91,
                'notie' => true,
                'groupTitle' => 'skyball2016.gameType.vegsoDontok',
            ),
            array(
                'name' => '2016. évi Nagy Döntő #2',
                'computerName' => 'vegso_donto_2',
                'weight' => 92,
                'reversedWeight' => 92,
                'notie' => true,
            ),
            array(
                'name' => '2016. évi Nagy Döntő #3',
                'computerName' => 'vegso_donto_3',
                'weight' => 93,
                'reversedWeight' => 93,
                'notie' => true,
            ),
            array(
                'name' => '2016. évi Nagy Döntő #4',
                'computerName' => 'vegso_donto_4',
                'weight' => 94,
                'reversedWeight' => 94,
                'notie' => true,
            ),
            array(
                'name' => '2016. évi Nagy Döntő #5',
                'computerName' => 'vegso_donto_5',
                'weight' => 95,
                'reversedWeight' => 95,
                'notie' => true,
            ),
            array(
                'name' => '2016. évi Nagy Döntő #6',
                'computerName' => 'vegso_donto_6',
                'weight' => 96,
                'reversedWeight' => 96,
                'notie' => true,
            ),
            array(
                'name' => '2016. évi Nagy Döntő #7',
                'computerName' => 'vegso_donto_7',
                'weight' => 97,
                'reversedWeight' => 97,
                'notie' => true,
            ),
            array(
                'name' => '2016. évi Nagy Döntő #8',
                'computerName' => 'vegso_donto_8',
                'weight' => 98,
                'reversedWeight' => 98,
                'notie' => true,
            ),
            array(
                'name' => '2016. évi Nagy Döntő #9',
                'computerName' => 'vegso_donto_9',
                'weight' => 99,
                'reversedWeight' => 99,
                'notie' => true,
            ),
        );

        return array(
            'rounds' => $rounds,
            'gameTypes' => $gameTypes,
        );
    }
}