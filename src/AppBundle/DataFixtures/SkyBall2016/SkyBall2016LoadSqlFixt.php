<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class SkyBall2016LoadSqlFixt implements FixtureInterface, ContainerAwareInterface
{
    protected $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $em
     */
    public function load(ObjectManager $em)
    {
        /** @var Container $container */
        $container = $this->container;

        $finder = new Finder();
        $finder->in('backup');
        $finder->name('skyball2016.sql');

        foreach ($finder as $file) {
            $content = $file->getContents();
            $stmt = $container->get('doctrine.orm.entity_manager')->getConnection()->getWrappedConnection()->prepare($content);
            $stmt->execute();
            $stmt->closeCursor();

            echo "\t---------------------------------\n";
            echo "\t> skyball2016.sql loaded.\n";
            echo "\t---------------------------------\n";
        }
    }
}