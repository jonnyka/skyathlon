<?php
namespace AppBundle\DataFixtures\SkyBall2016;

use SkyAthlon\Skyball2016Bundle\Entity\Game;
use SkyAthlon\Skyball2016Bundle\Entity\GameType;
use SkyAthlon\Skyball2016Bundle\Entity\KerchiefType;
use SkyAthlon\Skyball2016Bundle\Entity\Round;
use SkyAthlon\Skyball2016Bundle\Entity\RoundResultType;
use AppBundle\Entity\Settings;
use Doctrine\Common\Persistence\ObjectManager;
use SkyAthlon\Skyball2016Bundle\Entity\Team;
use SkyAthlon\Skyball2016Bundle\Entity\Player;

class LoadSkyBall2016Data
{
    public static function load(ObjectManager $em)
    {
        $data = SkyBall2016Data::getData();
        $players = $data['players'];
        $teams = $data['teams'];
        $gameTypes = $data['gameTypes'];
        $kerchiefTypes = $data['kerchiefTypes'];
        $roundResultTypes = $data['roundResultTypes'];
        $playerNum = $teamNum = $gameTypeNum = $kerchiefTypeNum = $roundResultTypeNum = $roundNum = $gameNum =
        $playerNumU = $teamNumU = $gameTypeNumU = $kerchiefTypeNumU = $roundResultTypeNumU = $roundNumU = 0;

        foreach ($players as $player) {
            $p = $em->getRepository('SkyAthlonSkyBall2016Bundle:Player')->findOneBy(array('number' => $player['number']));
            if ($p) {
                $playerNumU++;
            }
            else {
                $p = new Player();
                $playerNum++;
            }

            $p->setName($player['name']);
            $p->setNumber($player['number']);
            $p->setLeader($player['leader']);
            $p->setWeight($player['weight']);
            $em->persist($p);
        }

        $em->flush();

        echo "\t---------------------------------\n";
        echo "\t> " . $playerNum . " players added.\n";
        echo "\t> " . $playerNumU . " players updated.\n";
        echo "\t---------------------------------\n";

        $playerObjects = $em->getRepository('SkyAthlonSkyBall2016Bundle:Player')->findAll();
        $players = array();
        /** @var Player $playerObject */
        foreach ($playerObjects as $playerObject) {
            $players['p' . $playerObject->getNumber()] = $playerObject;
        }

        foreach ($teams as $team) {
            $t = $em->getRepository('SkyAthlonSkyBall2016Bundle:Team')->findOneBy(array('name' => $team['name']));
            if ($t) {
                $teamNumU++;
            }
            else {
                $t = new Team();
                $teamNum++;
            }

            /** @var Player $p1 */
            $p1 = $players['p' . $team['p1']];
            /** @var Player $p2 */
            $p2 = $players['p' . $team['p2']];
            /** @var Player $p3 */
            $p3 = $players['p' . $team['p3']];

            $t->setName($team['name']);
            $t->setComputerName($team['strip']);
            $t->setC1($team['c1']);
            $t->setC2($team['c2']);
            $em->persist($t);

            $p1->setTeam($t);
            $p2->setTeam($t);
            $p3->setTeam($t);
            $em->persist($p1);
            $em->persist($p2);
            $em->persist($p3);
        }

        $em->flush();

        echo "\t> " . $teamNum . " teams added.\n";
        echo "\t> " . $teamNumU . " teams updated.\n";
        echo "\t---------------------------------\n";

        foreach ($gameTypes as $gameType) {
            $m = $em->getRepository('SkyAthlonSkyBall2016Bundle:GameType')->findOneBy(array('computerName' => $gameType['computerName']));
            if ($m) {
                $gameTypeNumU++;
            }
            else {
                $m = new GameType();
                $gameTypeNum++;
            }

            $playoffs = false;
            if (array_key_exists('playoffs', $gameType)) {
                $playoffs = $gameType['playoffs'];
            }
            $m->setPlayoffs($playoffs);

            $m->setName($gameType['name']);
            $m->setComputerName($gameType['computerName']);
            $m->setWeight($gameType['weight']);
            $m->setReversedWeight($gameType['reversedWeight']);
            $m->setNotie($gameType['notie']);
            if (array_key_exists('groupTitle', $gameType)) {
                $m->setGroupTitle($gameType['groupTitle']);
            }

            $em->persist($m);
        }

        $em->flush();

        echo "\t> " . $gameTypeNum . " game types added.\n";
        echo "\t> " . $gameTypeNumU . " game types updated.\n";
        echo "\t---------------------------------\n";

        foreach ($kerchiefTypes as $kerchiefType) {
            $k = $em->getRepository('SkyAthlonSkyBall2016Bundle:KerchiefType')->findOneBy(array('computerName' => $kerchiefType['computerName']));
            if ($k) {
                $kerchiefTypeNumU++;
            }
            else {
                $k = new KerchiefType();
                $kerchiefTypeNum++;
            }

            $k->setName($kerchiefType['name']);
            $k->setComputerName($kerchiefType['computerName']);
            $k->setWeight($kerchiefType['weight']);

            $em->persist($k);
        }

        $em->flush();

        echo "\t> " . $kerchiefTypeNum . " kerchief types added.\n";
        echo "\t> " . $kerchiefTypeNumU . " kerchief types updated.\n";
        echo "\t---------------------------------\n";

        foreach ($roundResultTypes as $roundResultType) {
            $rrt = $em->getRepository('SkyAthlonSkyBall2016Bundle:RoundResultType')->findOneBy(array('name' => $roundResultType['name']));
            if ($rrt) {
                $roundResultTypeNumU++;
            }
            else {
                $rrt = new RoundResultType();
                $roundResultTypeNum++;
            }

            $rrt->setName($roundResultType['name']);
            $rrt->setComputerName($roundResultType['computerName']);
            $rrt->setPoint($roundResultType['point']);

            $em->persist($rrt);
        }

        $em->flush();

        echo "\t> " . $roundResultTypeNum . " round result types added.\n";
        echo "\t> " . $roundResultTypeNumU . " round result types updated.\n";
        echo "\t---------------------------------\n";

        for ($i = 1; $i <= 10; $i++) {
            $gtRep = $em->getRepository('SkyAthlonSkyBall2016Bundle:GameType');
            $gRep = $em->getRepository('SkyAthlonSkyBall2016Bundle:Game');
            $r = $em->getRepository('SkyAthlonSkyBall2016Bundle:Round')->findOneBy(array('number' => $i));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setNumber($i);
            $r->setName($i . '. forduló');

            $em->persist($r);

            $playOffsGameTypes = $gtRep->findBy(array(
                'playoffs' => true,
            ));
            /** @var GameType $playOffsGameType */
            foreach ($playOffsGameTypes as $playOffsGameType) {
                $nonPlayOffGames = $gRep->findBy(array(
                    'gameType' => $playOffsGameType,
                ));

                foreach ($nonPlayOffGames as $nonPlayOffGame) {
                    $playerResults = $em->getRepository('SkyAthlonSkyBall2016Bundle:PlayerResult')->findBy(array(
                        'game' => $nonPlayOffGame,
                    ));

                    foreach ($playerResults as $playerResult) {
                        $em->remove($playerResult);
                    }

                    $em->remove($nonPlayOffGame);
                }
            }

            $gameTypes = $gtRep->findBy(array(
                'playoffs' => false,
            ));
            /** @var GameType $gameType */
            foreach ($gameTypes as $gameType) {
                $game = $gRep->findOneBy(array(
                    'round' => $r,
                    'gameType' => $gameType,
                ));
                if (!$game) {
                    $game = new Game();
                    $game->setRound($r);
                    $game->setGameType($gameType);
                    $game->setWeight($gameType->getWeight());

                    $em->persist($game);
                    $gameNum++;
                }
            }
        }

        $em->flush();

        echo "\t> " . $roundNum . " rounds added.\n";
        echo "\t> " . $roundNumU . " rounds updated.\n";
        echo "\t> " . $gameNum . " games added.\n";
        echo "\t---------------------------------\n";

        $s = $em->getRepository('AppBundle:Settings')->findOneBy(array('name' => 'skyball2016_currentRound'));
        if ($s) {
            echo "\t> 1 setting updated.\n";
        }
        else {
            $s = new Settings();
            echo "\t> 1 setting added.\n";
        }

        $s->setName('skyball2016_currentRound');
        $s->setValue(13);

        $em->persist($s);
        $em->flush();

        echo "\t---------------------------------\n";
    }
}