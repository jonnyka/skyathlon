<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\DataFixtures\SkyBall2016\LoadSkyBall2016Data;

class SkyBall2016Fixt implements FixtureInterface
{
    /**
     * @param ObjectManager $em
     */
    public function load(ObjectManager $em)
    {
        LoadSkyBall2016Data::load($em);
    }
}