<?php
namespace AppBundle\DataFixtures\SkyPong2016;

class SkyPong2016Data
{
    public static function getData() {
        $players = array(
            array(
                'name'   => 'Farkas',
                'number' => 2,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Mongúz',
                'number' => 17,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Darázs',
                'number' => 18,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'ElNikoletta',
                'number' => 19,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Viki',
                'number' => 20,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Szunyi',
                'number' => 22,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Pele',
                'number' => 24,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Gópi',
                'number' => 25,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Méhecske',
                'number' => 26,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Katacsőr',
                'number' => 30,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Rebeka',
                'number' => 31,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Laksh',
                'number' => 36,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Róka',
                'number' => 37,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Delfin',
                'number' => 39,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Antilop',
                'number' => 40,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Lola',
                'number' => 41,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Lepke',
                'number' => 43,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Zsuzsi',
                'number' => 45,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Emő',
                'number' => 47,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Medúza',
                'number' => 49,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'TM',
                'number' => 50,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Parv',
                'number' => 55,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Dikdik',
                'number' => 60,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Fruzsi',
                'number' => 62,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Gibbi',
                'number' => 66,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'KÉvi',
                'number' => 68,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Paci',
                'number' => 70,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Fóka',
                'number' => 71,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Szita',
                'number' => 72,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Nyérc',
                'number' => 75,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Mormesz',
                'number' => 77,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Virág',
                'number' => 78,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Tenge',
                'number' => 79,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Panda',
                'number' => 80,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Nyuszi',
                'number' => 81,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Kecsi',
                'number' => 84,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Yayi',
                'number' => 86,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Szöcsi',
                'number' => 88,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Pocok',
                'number' => 89,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Mókus',
                'number' => 90,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Otti',
                'number' => 92,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Kata',
                'number' => 93,
                'leader' => false,
                'weight' => 4,
            ),
            array(
                'name'   => 'Emu',
                'number' => 94,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Nandu',
                'number' => 96,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Kanári',
                'number' => 97,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Kutya',
                'number' => 99,
                'leader' => true,
                'weight' => 1,
            ),
        );

        $teams = array(
            array(
                'name'  => 'Amazonok',
                'strip' => 'amazonok',
                'p1'    => 62,
                'p2'    => 40,
                'p3'    => 77,
                'p4'    => 41,
            ),
            array(
                'name'  => 'Deae Matres',
                'strip' => 'deaematres',
                'p1'    => 19,
                'p2'    => 89,
                'p3'    => 17,
                'p4'    => 49,
            ),
            array(
                'name'  => 'Démonok',
                'strip' => 'demonok',
                'p1'    => 37,
                'p2'    => 72,
                'p3'    => 92,
                'p4'    => 25,
            ),
            array(
                'name'  => 'Druidess',
                'strip' => 'druidess',
                'p1'    => 26,
                'p2'    => 22,
                'p3'    => 60,
                'p4'    => 88,
            ),
            array(
                'name'  => 'Flamenco',
                'strip' => 'flamenco',
                'p1'    => 2,
                'p2'    => 78,
                'p3'    => 31,
                'p4'    => 36,
            ),
            array(
                'name'  => 'Főnix',
                'strip' => 'fonix',
                'p1'    => 20,
                'p2'    => 30,
                'p3'    => 81,
                'p4'    => null,
            ),
            array(
                'name'  => 'Heszperida',
                'strip' => 'heszperida',
                'p1'    => 75,
                'p2'    => 47,
                'p3'    => 90,
                'p4'    => 84,
            ),
            array(
                'name'  => 'Múzsák',
                'strip' => 'muzsak',
                'p1'    => 86,
                'p2'    => 96,
                'p3'    => 94,
                'p4'    => 68,
            ),
            array(
                'name'  => 'Najádok',
                'strip' => 'najadok',
                'p1'    => 99,
                'p2'    => 55,
                'p3'    => 66,
                'p4'    => 18,
            ),
            array(
                'name'  => 'Titaniszok',
                'strip' => 'titaniszok',
                'p1'    => 80,
                'p2'    => 24,
                'p3'    => 93,
                'p4'    => null,
            ),
            array(
                'name'  => 'Vesta',
                'strip' => 'vesta',
                'p1'    => 97,
                'p2'    => 50,
                'p3'    => 79,
                'p4'    => 43,
            ),
            array(
                'name'  => 'WaterFalls',
                'strip' => 'waterfalls',
                'p1'    => 39,
                'p2'    => 71,
                'p3'    => 70,
                'p4'    => 45,
            ),
        );

        return array(
            'players' => $players,
            'teams'   => $teams,
        );
    }
}