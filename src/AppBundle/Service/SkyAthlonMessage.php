<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SkyAthlonMessage extends Controller
{
    public function setMessage($message, $type = 'notice') {
        $session = new Session();

        $session->getFlashBag()->add($type, $message);
    }
}
