<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use SkyAthlon\SkyBall2017Bundle\Entity\RefResult as RefResult;
use SkyAthlon\SkyBall2017Bundle\Entity\Game as Game;

/**
 * Player
 *
 * @ORM\Table(name="player")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="smallint", unique=true)
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=2)
     */
    protected $sex;

    /**
     * @ORM\OneToMany(targetEntity="SkyAthlon\SkyBall2017Bundle\Entity\RefResult", mappedBy="player")
     */
    protected $refResults;

    /**
     * @ORM\OneToMany(targetEntity="SkyAthlon\SkyBall2017Bundle\Entity\RefResult", mappedBy="spicli")
     */
    protected $spiclis;

    /**
     * @ORM\OneToMany(targetEntity="SkyAthlon\SkyBall2017Bundle\Entity\Game", mappedBy="ref")
     */
    protected $refs;

    /**
     * @ORM\OneToMany(targetEntity="SkyAthlon\SkyBall2017Bundle\Entity\Game", mappedBy="sref")
     */
    protected $srefs;

    /**
     * @ORM\OneToMany(targetEntity="SkyAthlon\SkyBall2017Bundle\Entity\Game", mappedBy="srefb")
     */
    protected $srefbs;

    /**
     * @ORM\OneToMany(targetEntity="SkyAthlon\SkyBall2017Bundle\Entity\Game", mappedBy="jref")
     */
    protected $jrefs;

    /**
     * @ORM\OneToMany(targetEntity="SkyAthlon\SkyBall2017Bundle\Entity\Game", mappedBy="spicli")
     */
    protected $gameSpiclis;

    public function __toString() {
        return $this->number . ' - ' . $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Player
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set sex
     *
     * @param string $sex
     *
     * @return Player
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refResults = new ArrayCollection();
        $this->spiclis = new ArrayCollection();
        $this->refs = new ArrayCollection();
        $this->srefs = new ArrayCollection();
        $this->srefbs = new ArrayCollection();
        $this->jrefs = new ArrayCollection();
        $this->gameSpiclis = new ArrayCollection();
    }

    /**
     * Add refResult
     *
     * @param RefResult $refResult
     *
     * @return Player
     */
    public function addRefResult(RefResult $refResult)
    {
        $this->refResults[] = $refResult;

        return $this;
    }

    /**
     * Remove refResult
     *
     * @param RefResult $refResult
     */
    public function removeRefResult(RefResult $refResult)
    {
        $this->refResults->removeElement($refResult);
    }

    /**
     * Get refResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRefResults()
    {
        return $this->refResults;
    }

    /**
     * Add spicli
     *
     * @param RefResult $spicli
     *
     * @return Player
     */
    public function addSpicli(RefResult $spicli)
    {
        $this->spiclis[] = $spicli;

        return $this;
    }

    /**
     * Remove spicli
     *
     * @param RefResult $spicli
     */
    public function removeSpicli(RefResult $spicli)
    {
        $this->spiclis->removeElement($spicli);
    }

    /**
     * Get spiclis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpiclis()
    {
        return $this->spiclis;
    }

    /**
     * Add ref
     *
     * @param Game $ref
     *
     * @return Player
     */
    public function addRef(Game $ref)
    {
        $this->refs[] = $ref;

        return $this;
    }

    /**
     * Remove ref
     *
     * @param Game $ref
     */
    public function removeRef(Game $ref)
    {
        $this->refs->removeElement($ref);
    }

    /**
     * Get refs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRefs()
    {
        return $this->refs;
    }

    /**
     * Add sref
     *
     * @param Game $sref
     *
     * @return Player
     */
    public function addSref(Game $sref)
    {
        $this->srefs[] = $sref;

        return $this;
    }

    /**
     * Remove sref
     *
     * @param Game $sref
     */
    public function removeSref(Game $sref)
    {
        $this->srefs->removeElement($sref);
    }

    /**
     * Get srefs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSrefs()
    {
        return $this->srefs;
    }

    /**
     * Add jref
     *
     * @param Game $jref
     *
     * @return Player
     */
    public function addJref(Game $jref)
    {
        $this->jrefs[] = $jref;

        return $this;
    }

    /**
     * Remove jref
     *
     * @param Game $jref
     */
    public function removeJref(Game $jref)
    {
        $this->jrefs->removeElement($jref);
    }

    /**
     * Get jrefs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJrefs()
    {
        return $this->jrefs;
    }

    /**
     * Add srefb
     *
     * @param Game $srefb
     *
     * @return Player
     */
    public function addSrefb(Game $srefb)
    {
        $this->srefbs[] = $srefb;

        return $this;
    }

    /**
     * Remove srefb
     *
     * @param Game $srefb
     */
    public function removeSrefb(Game $srefb)
    {
        $this->srefbs->removeElement($srefb);
    }

    /**
     * Get srefbs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSrefbs()
    {
        return $this->srefbs;
    }
}
