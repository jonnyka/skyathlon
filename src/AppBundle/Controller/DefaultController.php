<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index", options={"expose" = true})
     * @Route("/kezdolap/", name="index_redirect", options={"expose" = true})
     * @return array
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/auth/", name="auth", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function authAction(Request $request) {
        $token = $request->request->get('token');
        $oauthService = $this->get('skyathlon_oauth');

        return $oauthService->auth($token);
    }

    /**
     * @Route("/check-auth/", name="check_auth", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @return JsonResponse
     */
    public function checkAuthAction() {
        return $this->getUser() ? new JsonResponse(true) : new JsonResponse(false);
    }

    /**
     * Log the user out.
     * @Route("/logout/", name="logout", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logoutAction(Request $request) {
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        $messageService = $this->get('skyathlon_message');
        $messageService->setMessage('flashbag.logout', 'warning');

        return $this->redirectToRoute('index');
    }
    
    /**
     * Sends a flashbag message.
     * @Route("/uzenet/", name="message", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function messageAction(Request $request) {
        $messageService = $this->get('skyathlon_message');
        $message = $request->request->get('message');
        $type = $request->request->get('type') ? $request->request->get('type') : 'notice';

        $messageService->setMessage($message, $type);

        return new JsonResponse('ok');
    }

    /**
     * @Route("/tatami/", name="tatami", options={"expose" = true})
     * @return array
     * @Template()
     */
    public function tatamiAction()
    {
        $text = '';
        $em = $this->getDoctrine()->getManager();
        /** @var Settings $tatamiObject */
        $tatamiObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
            'name' => 'tatami',
        ));
        if ($tatamiObject) {
            $tatami = json_decode($tatamiObject->getBigValue(), true);
            $text = $tatami['text'];
        }

        return array(
            'text' => $text,
        );
    }

    /**
     * Sets the current tatami info.
     *
     * @Route("/tatami-set/", name="tatami_set", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function tatamiSetAction(Request $request) {
        $drawing = $request->request->get('drawing');
        $tatamis = $request->request->get('tatamis');
        $text = $request->request->get('text');
        $color = $request->request->get('color');

        $array = json_encode(array('drawing' => $drawing, 'tatamis' => $tatamis, 'text' => $text, 'color' => $color));

        $em = $this->getDoctrine()->getManager();
        $sRepo = $em->getRepository('AppBundle:Settings');

        $cr = $sRepo->findOneBy(array('name' => 'tatami'));
        if (!$cr) {
            $cr = new Settings();
            $cr->setName('tatami');
        }
        $cr->setValue('tatami');
        $cr->setBigValue($array);

        $em->persist($cr);
        $em->flush();

        return new JsonResponse($array);
    }

    /**
     * Gets the current tatami info.
     *
     * @Route("/tatami-get/", name="tatami_get", options={"expose" = true})
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function tatamiGetAction(Request $request) {
        $array = json_encode(array('drawing' => '', 'tatami' => [], 'text' => '', 'color' => '#41DFB1'));

        $em = $this->getDoctrine()->getManager();
        $sRepo = $em->getRepository('AppBundle:Settings');

        $cr = $sRepo->findOneBy(array('name' => 'tatami'));
        if ($cr) {
            $array = json_decode($cr->getBigValue(), true);
        }

        return new JsonResponse($array);
    }
}
