<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Api controller.
 *
 * @Route("/api/")
 */
class ApiController extends Controller
{

    /**
     * @Route("time", name="time", options={"expose" = true})
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function timeAction(Request $request)
    {
        $date = new \DateTime();
        $time = $date->format('H:i');
        return new JsonResponse($time);
    }
}
