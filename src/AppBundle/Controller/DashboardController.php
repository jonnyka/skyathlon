<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Settings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Predis\Client as Redis;

class DashboardController extends Controller
{
    /**
     * Dashboard.
     *
     * @Route("/dashboard/", name="dashboard")
     * @Method("GET")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function dashboardAction(Request $request)
    {
        $src = 'http://' . $request->getHost() . ':3001/skyball2017_live_null';
        $em = $this->getDoctrine()->getManager();
        $roundNum = null;
        /** @var Settings $currentDashboardObject */
        $currentDashboardObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
            'name' => 'currentDashboard',
        ));
        if ($currentDashboardObject) {
            $src = 'http://' . $request->getHost() . ':3001/' . $currentDashboardObject->getValue();
        }

        return array(
            'src' => $src,
        );
    }

    /**
     * Dashboard admin.
     *
     * @Route("/dashboard-admin/", name="dashboard_admin")
     * @Method("GET")
     * @Template()
     */
    public function dashboardAdminAction()
    {
        if ($this->getUser() && $this->getUser()->isAdmin()) {
            $dashboards = array(
                'skyball2017_current',
                'skyball2017_live_nul',
                'skyball2017_live_n',
                'skyball2017_live_d',
                'skyball2017_news',
                'skyball2017_round',
                'skyball2017_live_es',
                'skyball2017_live_kd',
                'skyball2017_live',
                'skyball2017_refs',
                'skyball2017_live_raj',
                'skyball2017_live_s',
                'skyball2017_live_nd',
            );

            $dashboardCount = count($dashboards);

            $currentDashboard = '';
            $em = $this->getDoctrine()->getManager();
            $roundNum = null;
            /** @var Settings $currentDashboardObject */
            $currentDashboardObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
                'name' => 'currentDashboard',
            ));
            if ($currentDashboardObject) {
                $currentDashboard = $currentDashboardObject->getValue();
            }

            $redis = new Redis();
            $news = $redis->get('skyball2017_news');

            return array(
                'dashboards'       => $dashboards,
                'dashboardCount'   => $dashboardCount,
                'currentDashboard' => $currentDashboard,
                'news'             => $news,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Sets the current dashboard page.
     * @Route("/dashboard-set/", name="dashboard_set", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dashboardSetAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $roundNum = null;
        /** @var Settings $currentDashboardObject */
        $currentDashboardObject = $em->getRepository('AppBundle:Settings')->findOneBy(array(
            'name' => 'currentDashboard',
        ));
        if (!$currentDashboardObject) {
            $currentDashboardObject = new Settings();
            $currentDashboardObject->setName('currentDashboard');
        }
        $dashboard = $request->request->get('dashboard');
        $currentDashboardObject->setValue($dashboard);

        $em->persist($currentDashboardObject);
        $em->flush();

        return new JsonResponse('ok');
    }

    /**
     * Sets the current news.
     *
     * @Route("/news-set/", name="news_set", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newsSetAction(Request $request) {
        $redis = new Redis();
        $redis->set('skyball2017_news', $request->request->get('news'));

        return new JsonResponse('ok');
    }
}
