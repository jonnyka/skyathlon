#!/bin/bash
# 30 0 * * * /home/jonny/skyathlon/script/backup.sh

# Database credentials
user=""
password=""
host=""
db_name=""

IFS=": "
while read -r name value
do
    if [ "$name" == "database_host" ]; then
        host=${value//\"/}
    fi
    if [ "$name" == "database_name" ]; then
        db_name=${value//\"/}
    fi
    if [ "$name" == "database_user" ]; then
        user=${value//\"/}
    fi
    if [ "$name" == "database_password" ]; then
        password=${value//\"/}
    fi
done < ../app/config/parameters.yml

# Other options
backup_path="../backup"
mkdir -p $backup_path
date=$(date +"%Y-%m-%d")
# Set default file permissions
umask 177
# Dump database into SQL file
mysqldump --user=$user --password=$password --host=$host $db_name > $backup_path/$db_name-$date.sql
cd ..
tar zcvf backup/files-$date.tar.gz web/files -P
cd script
# Delete files older than 30 days
find $backup_path/* -mtime +30 -exec rm {} \;